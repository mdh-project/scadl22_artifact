extern "C" __global__
void subgraph2_static_1(const float c_1, float const * const __restrict__ mul_i_1_buf_raw, float const * const __restrict__ mul_i_2_buf_raw, const float c_2, float * const __restrict__ res_g_o_raw, float * const __restrict__ int_res_o_raw) {
    const size_t i_wg_l_1 = blockIdx.y;
    const size_t i_wi_l_1 = threadIdx.y;
    const size_t i_wg_l_2 = 0;
    const size_t i_wi_l_2 = 0;
    const size_t i_wg_l_3 = (blockIdx.z / (1));
    const size_t i_wi_l_3 = 0;
    const size_t i_wg_l_4 = blockIdx.x;
    const size_t i_wi_l_4 = threadIdx.x;
  size_t l_cb_offset_l_1;
  size_t l_cb_offset_l_2;
  size_t l_cb_offset_l_3;
  size_t l_cb_offset_l_4;
  size_t p_cb_offset_l_1;
  size_t p_cb_offset_l_2;
  size_t p_cb_offset_l_3;
  size_t p_cb_offset_l_4;
    float const (*mul_i_1_buf)[((384 - 1)) - (0) + 1][(0) - (0) + 1] = reinterpret_cast<float const (*)[((384 - 1)) - (0) + 1][(0) - (0) + 1]>(mul_i_1_buf_raw);
    float const (*mul_i_2_buf)[(0) - (0) + 1][((384 - 1)) - (0) + 1] = reinterpret_cast<float const (*)[(0) - (0) + 1][((384 - 1)) - (0) + 1]>(mul_i_2_buf_raw);
  float cb_p_mul_i_1[(((0) - (0) + 1))][((((1 - 1)) - (0) + 1) * (1))][((((1 - 1)) - (0) + 1) * (2))];
  float (*res_g_o)[384][16][384] = reinterpret_cast<float (*)[384][16][384]>(res_g_o_raw);
  float (*int_res_o)[(((1 - 1)) - (0) + 1)][(((384 - 1)) - (0) + 1)][(((384 - 1)) - (0) + 1)] = reinterpret_cast<float (*)[(((1 - 1)) - (0) + 1)][(((384 - 1)) - (0) + 1)][(((384 - 1)) - (0) + 1)]>(int_res_o_raw);
  float res_p_o[1][2][1][1];
  l_cb_offset_l_1 = i_wg_l_1 * 2;
  __syncthreads();
  for (size_t l_step_l_1 = 0; l_step_l_1 < ((16 / (8 * 2)) / (2 / 2)); ++l_step_l_1) {
    __syncthreads();
    l_cb_offset_l_2 = i_wg_l_2 * 1;
    for (size_t l_step_l_2 = 0; l_step_l_2 < ((1 / (1 * 1)) / (1 / 1)); ++l_step_l_2) {
      l_cb_offset_l_3 = i_wg_l_3 * 1;
      for (size_t l_step_l_3 = 0; l_step_l_3 < ((384 / (64 * 1)) / (2 / 1)); ++l_step_l_3) {
        __syncthreads();
        {
          size_t p_step_l_1 = 0;
          size_t p_step_l_3 = 0;
          {
            for (size_t step = 0; step < ((1) / 1) * ((((1 - 1)) - (0) + 1)) * ((2) / 1) * ((((1 - 1)) - (0) + 1)) * (((0) - (0) + 1)) / (1); ++step) {
              const size_t flat_index = (0) + step * (1);
              const size_t p_dim_0_index_l_1 = flat_index / (((((1 - 1)) - (0) + 1)) * ((2) / 1) * ((((1 - 1)) - (0) + 1)) * (((0) - (0) + 1)));
              const size_t i = (flat_index / (((2) / 1) * ((((1 - 1)) - (0) + 1)) * (((0) - (0) + 1)))) % ((((1 - 1)) - (0) + 1));
              const size_t p_dim_1_index_l_3 = (flat_index / (((((1 - 1)) - (0) + 1)) * (((0) - (0) + 1)))) % ((2) / 1);
              const size_t j = (flat_index / ((((0) - (0) + 1)))) % ((((1 - 1)) - (0) + 1));
              const size_t k = flat_index % (((0) - (0) + 1));
              size_t p_mem_offset_dim_0 = (((p_dim_0_index_l_1))) * (((1 - 1)) - (0) + 1);
              size_t p_mem_offset_dim_1 = (((p_dim_1_index_l_3))) * (((1 - 1)) - (0) + 1);
              size_t p_mem_offset_dim_2 = 0;
              size_t g_mem_offset_dim_0 = ((l_step_l_1 * (2 / 2) + (((p_step_l_1 * (1) + (p_dim_0_index_l_1 * 1) / 1) * 2 + i_wi_l_1 * 1 + ((p_dim_0_index_l_1 * 1) % 1))) / 2) * (8 * 2) + i_wg_l_1 * 2 + ((((p_step_l_1 * (1) + (p_dim_0_index_l_1 * 1) / 1) * 2 + i_wi_l_1 * 1 + ((p_dim_0_index_l_1 * 1) % 1))) % 2));
              size_t g_mem_offset_dim_1 = ((l_step_l_3 * (2 / 1) + (((p_step_l_3 * (2) + (p_dim_1_index_l_3 * 1) / 1) * 1 + i_wi_l_3 * 1 + ((p_dim_1_index_l_3 * 1) % 1))) / 1) * (64 * 1) + i_wg_l_3 * 1 + ((((p_step_l_3 * (2) + (p_dim_1_index_l_3 * 1) / 1) * 1 + i_wi_l_3 * 1 + ((p_dim_1_index_l_3 * 1) % 1))) % 1));
              size_t g_mem_offset_dim_2 = 0;
              cb_p_mul_i_1[((p_mem_offset_dim_2 + k))][((p_mem_offset_dim_0 + i))][((p_mem_offset_dim_1 + j))] =
                  mul_i_1_buf[g_mem_offset_dim_0 + i][g_mem_offset_dim_1 + j][g_mem_offset_dim_2 + k];
            }
              }
        }
        l_cb_offset_l_4 = i_wg_l_4 * 16;
        for (size_t l_step_l_4 = 0; l_step_l_4 < ((384 / (8 * 16)) / (16 / 16)); ++l_step_l_4) {
          p_cb_offset_l_1 = i_wi_l_1 * 1;
          for (size_t p_step_l_1 = 0; p_step_l_1 < ((2 / 2) / (1)); ++p_step_l_1) {
            p_cb_offset_l_2 = i_wi_l_2 * 1;
            for (size_t p_step_l_2 = 0; p_step_l_2 < ((1 / 1) / (1)); ++p_step_l_2) {
              p_cb_offset_l_3 = i_wi_l_3 * 1;
              for (size_t p_step_l_3 = 0; p_step_l_3 < ((2 / 1) / (2)); ++p_step_l_3) {
                p_cb_offset_l_4 = i_wi_l_4 * 1;
                for (size_t p_step_l_4 = 0; p_step_l_4 < ((16 / 16) / (1)); ++p_step_l_4) {
#pragma unroll
                  for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (1); ++p_iteration_l_1) {
                    for (size_t p_iteration_l_2 = 0; p_iteration_l_2 < (1); ++p_iteration_l_2) {
                      for (size_t p_iteration_l_3 = 0; p_iteration_l_3 < (2); ++p_iteration_l_3) {
                        for (size_t p_iteration_l_4 = 0; p_iteration_l_4 < (1); ++p_iteration_l_4) {
                          res_p_o[(p_iteration_l_2)][(p_iteration_l_3)][(p_iteration_l_1)][(p_iteration_l_4)] = (c_1 - (cb_p_mul_i_1[((((0) * ((0) - (0) + 1)) + ((0) - (0))))][((((((((p_iteration_l_1) / 1)))) * (((1 - 1)) - (0) + 1)) + ((((p_iteration_l_1) % 1)) - (0))))][((((((((p_iteration_l_3) / 1)))) * (((1 - 1)) - (0) + 1)) + ((((p_iteration_l_3) % 1)) - (0))))] * mul_i_2_buf[(l_cb_offset_l_1 + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 2 + 0)) / 2) * (8 * 2) + i_wi_l_1)][0][(l_cb_offset_l_4 + (((p_cb_offset_l_4 + (((p_iteration_l_4)) / 1) * 16 + 0)) / 16) * (8 * 16) + i_wi_l_4)])) * c_2;
                        }
                      }
                    }
                  }
                  for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (1); ++p_iteration_l_1) {
                    for (size_t p_iteration_l_2 = 0; p_iteration_l_2 < (1); ++p_iteration_l_2) {
                      for (size_t p_iteration_l_3 = 0; p_iteration_l_3 < (2); ++p_iteration_l_3) {
                        for (size_t p_iteration_l_4 = 0; p_iteration_l_4 < (1); ++p_iteration_l_4) {
                          int_res_o[(l_cb_offset_l_1 + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 2 + 0)) / 2) * (8 * 2) + i_wi_l_1)][(l_cb_offset_l_2 + (((p_cb_offset_l_2 + (((p_iteration_l_2)) / 1) * 1 + 0)) / 1) * (1 * 1) + i_wi_l_2)][(l_cb_offset_l_3 + (((p_cb_offset_l_3 + (((p_iteration_l_3)) / 1) * 1 + 0)) / 1) * (64 * 1) + i_wi_l_3)][(l_cb_offset_l_4 + (((p_cb_offset_l_4 + (((p_iteration_l_4)) / 1) * 16 + 0)) / 16) * (8 * 16) + i_wi_l_4)] = res_p_o[(p_iteration_l_2)][(p_iteration_l_3)][(p_iteration_l_1)][(p_iteration_l_4)];
                        }
                      }
                    }
                  }
                  p_cb_offset_l_4 += 16 * (1);
                }
                p_cb_offset_l_3 += 1 * (2);
              }
              p_cb_offset_l_2 += 1 * (1);
            }
            p_cb_offset_l_1 += 2 * (1);
          }
          l_cb_offset_l_4 += (8 * 16) * (16 / 16);
        }
        l_cb_offset_l_3 += (64 * 1) * (2 / 1);
      }
      l_cb_offset_l_2 += (1 * 1) * (1 / 1);
    }
    l_cb_offset_l_1 += (8 * 2) * (2 / 2);
  }
}
