extern "C" __global__
void batch_matmul_static_1(float const * const __restrict__ a_buf_raw, float const * const __restrict__ b_buf_raw, float * const __restrict__ res_g_c_raw, float * const __restrict__ int_res_c_raw) {
    const int flat_blockIdx = blockIdx.z * 4 * 1 + blockIdx.y * 1 + blockIdx.x;
    const int flat_threadIdx = threadIdx.z * 16 * 16 + threadIdx.y * 16 + threadIdx.x;
    __shared__ float cb_l_a[1*1*96*32];
    __shared__ float cb_l_b[1*1*32*64];
    float res_p_c[1*1*3*2*4];
    int l_step_r_1 = 0;
    cb_l_a[flat_threadIdx] = a_buf_raw[flat_blockIdx * 36864 + (flat_threadIdx >> 5) * 384 + l_step_r_1 * 32 + (flat_threadIdx & 31)];
    cb_l_a[flat_threadIdx + 256] = a_buf_raw[flat_blockIdx * 36864 + (flat_threadIdx >> 5) * 384 + l_step_r_1 * 32 + (flat_threadIdx & 31) + 3072];
    cb_l_a[flat_threadIdx + 512] = a_buf_raw[flat_blockIdx * 36864 + (flat_threadIdx >> 5) * 384 + l_step_r_1 * 32 + (flat_threadIdx & 31) + 6144];
    cb_l_a[flat_threadIdx + 768] = a_buf_raw[flat_blockIdx * 36864 + (flat_threadIdx >> 5) * 384 + l_step_r_1 * 32 + (flat_threadIdx & 31) + 9216];
    cb_l_a[flat_threadIdx + 1024] = a_buf_raw[flat_blockIdx * 36864 + (flat_threadIdx >> 5) * 384 + l_step_r_1 * 32 + (flat_threadIdx & 31) + 12288];
    cb_l_a[flat_threadIdx + 1280] = a_buf_raw[flat_blockIdx * 36864 + (flat_threadIdx >> 5) * 384 + l_step_r_1 * 32 + (flat_threadIdx & 31) + 15360];
    cb_l_a[flat_threadIdx + 1536] = a_buf_raw[flat_blockIdx * 36864 + (flat_threadIdx >> 5) * 384 + l_step_r_1 * 32 + (flat_threadIdx & 31) + 18432];
    cb_l_a[flat_threadIdx + 1792] = a_buf_raw[flat_blockIdx * 36864 + (flat_threadIdx >> 5) * 384 + l_step_r_1 * 32 + (flat_threadIdx & 31) + 21504];
    cb_l_a[flat_threadIdx + 2048] = a_buf_raw[flat_blockIdx * 36864 + (flat_threadIdx >> 5) * 384 + l_step_r_1 * 32 + (flat_threadIdx & 31) + 24576];
    cb_l_a[flat_threadIdx + 2304] = a_buf_raw[flat_blockIdx * 36864 + (flat_threadIdx >> 5) * 384 + l_step_r_1 * 32 + (flat_threadIdx & 31) + 27648];
    cb_l_a[flat_threadIdx + 2560] = a_buf_raw[flat_blockIdx * 36864 + (flat_threadIdx >> 5) * 384 + l_step_r_1 * 32 + (flat_threadIdx & 31) + 30720];
    cb_l_a[flat_threadIdx + 2816] = a_buf_raw[flat_blockIdx * 36864 + (flat_threadIdx >> 5) * 384 + l_step_r_1 * 32 + (flat_threadIdx & 31) + 33792];
    ((float2*)(cb_l_b + flat_threadIdx * 2))[0] = ((float2*)(b_buf_raw + (flat_blockIdx >> 2) * 24576 + l_step_r_1 * 2048 + flat_threadIdx * 2))[0];
    ((float2*)(cb_l_b + (flat_threadIdx * 2) + 512))[0] = ((float2*)(b_buf_raw + (flat_blockIdx >> 2) * 24576 + l_step_r_1 * 2048 + flat_threadIdx * 2 + 512))[0];
    ((float2*)(cb_l_b + (flat_threadIdx * 2) + 1024))[0] = ((float2*)(b_buf_raw + (flat_blockIdx >> 2) * 24576 + l_step_r_1 * 2048 + flat_threadIdx * 2 + 1024))[0];
    ((float2*)(cb_l_b + (flat_threadIdx * 2) + 1536))[0] = ((float2*)(b_buf_raw + (flat_blockIdx >> 2) * 24576 + l_step_r_1 * 2048 + flat_threadIdx * 2 + 1536))[0];
    __syncthreads();
#pragma unroll
    for (int p_step_l_3 = 0; p_step_l_3 < ((96 / 16) / (2)); ++p_step_l_3) {
#pragma unroll
        for (int p_step_l_4 = 0; p_step_l_4 < ((64 / 16) / (1)); ++p_step_l_4) {
            int p_step_r_1 = 0;
#pragma unroll
            for (int p_iteration_l_3 = 0; p_iteration_l_3 < (2); ++p_iteration_l_3) {
                int p_iteration_r_1 = 0;
                res_p_c[p_step_l_3 * 2 * 4 + p_iteration_l_3 * 4 + p_step_l_4] = cb_l_a[(flat_threadIdx >> 4) * 64 + p_step_r_1 * 4 + p_iteration_l_3 * 32 + p_iteration_r_1 * 1 + p_step_l_3 * 1024] * cb_l_b[p_step_r_1 * 256 + (flat_threadIdx & 15) + p_iteration_r_1 * 64 + p_step_l_4 * 16];
#pragma unroll
                for (p_iteration_r_1 = 1; p_iteration_r_1 < (4); ++p_iteration_r_1) {
                    res_p_c[p_step_l_3 * 2 * 4 + p_iteration_l_3 * 4 + p_step_l_4] += cb_l_a[(flat_threadIdx >> 4) * 64 + p_step_r_1 * 4 + p_iteration_l_3 * 32 + p_iteration_r_1 * 1 + p_step_l_3 * 1024] * cb_l_b[p_step_r_1 * 256 + (flat_threadIdx & 15) + p_iteration_r_1 * 64 + p_step_l_4 * 16];
                }
            }
            for (p_step_r_1 = 1; p_step_r_1 < ((32 / 1) / (4)); ++p_step_r_1) {
#pragma unroll
                for (int p_iteration_l_3 = 0; p_iteration_l_3 < (2); ++p_iteration_l_3) {
#pragma unroll
                    for (int p_iteration_r_1 = 0; p_iteration_r_1 < (4); ++p_iteration_r_1) {
                        res_p_c[p_step_l_3 * 2 * 4 + p_iteration_l_3 * 4 + p_step_l_4] += cb_l_a[(flat_threadIdx >> 4) * 64 + p_step_r_1 * 4 + p_iteration_l_3 * 32 + p_iteration_r_1 * 1 + p_step_l_3 * 1024] * cb_l_b[p_step_r_1 * 256 + (flat_threadIdx & 15) + p_iteration_r_1 * 64 + p_step_l_4 * 16];
                    }
                }
            }
        }
    }
    __syncthreads();
    for (l_step_r_1 = 1; l_step_r_1 < ((384 / (1 * 1)) / (32 / 1)); ++l_step_r_1) {
        cb_l_a[flat_threadIdx] = a_buf_raw[flat_blockIdx * 36864 + (flat_threadIdx >> 5) * 384 + l_step_r_1 * 32 + (flat_threadIdx & 31)];
        cb_l_a[flat_threadIdx + 256] = a_buf_raw[flat_blockIdx * 36864 + (flat_threadIdx >> 5) * 384 + l_step_r_1 * 32 + (flat_threadIdx & 31) + 3072];
        cb_l_a[flat_threadIdx + 512] = a_buf_raw[flat_blockIdx * 36864 + (flat_threadIdx >> 5) * 384 + l_step_r_1 * 32 + (flat_threadIdx & 31) + 6144];
        cb_l_a[flat_threadIdx + 768] = a_buf_raw[flat_blockIdx * 36864 + (flat_threadIdx >> 5) * 384 + l_step_r_1 * 32 + (flat_threadIdx & 31) + 9216];
        cb_l_a[flat_threadIdx + 1024] = a_buf_raw[flat_blockIdx * 36864 + (flat_threadIdx >> 5) * 384 + l_step_r_1 * 32 + (flat_threadIdx & 31) + 12288];
        cb_l_a[flat_threadIdx + 1280] = a_buf_raw[flat_blockIdx * 36864 + (flat_threadIdx >> 5) * 384 + l_step_r_1 * 32 + (flat_threadIdx & 31) + 15360];
        cb_l_a[flat_threadIdx + 1536] = a_buf_raw[flat_blockIdx * 36864 + (flat_threadIdx >> 5) * 384 + l_step_r_1 * 32 + (flat_threadIdx & 31) + 18432];
        cb_l_a[flat_threadIdx + 1792] = a_buf_raw[flat_blockIdx * 36864 + (flat_threadIdx >> 5) * 384 + l_step_r_1 * 32 + (flat_threadIdx & 31) + 21504];
        cb_l_a[flat_threadIdx + 2048] = a_buf_raw[flat_blockIdx * 36864 + (flat_threadIdx >> 5) * 384 + l_step_r_1 * 32 + (flat_threadIdx & 31) + 24576];
        cb_l_a[flat_threadIdx + 2304] = a_buf_raw[flat_blockIdx * 36864 + (flat_threadIdx >> 5) * 384 + l_step_r_1 * 32 + (flat_threadIdx & 31) + 27648];
        cb_l_a[flat_threadIdx + 2560] = a_buf_raw[flat_blockIdx * 36864 + (flat_threadIdx >> 5) * 384 + l_step_r_1 * 32 + (flat_threadIdx & 31) + 30720];
        cb_l_a[flat_threadIdx + 2816] = a_buf_raw[flat_blockIdx * 36864 + (flat_threadIdx >> 5) * 384 + l_step_r_1 * 32 + (flat_threadIdx & 31) + 33792];
        ((float2*)(cb_l_b + flat_threadIdx * 2))[0] = ((float2*)(b_buf_raw + (flat_blockIdx >> 2) * 24576 + l_step_r_1 * 2048 + flat_threadIdx * 2))[0];
        ((float2*)(cb_l_b + (flat_threadIdx * 2) + 512))[0] = ((float2*)(b_buf_raw + (flat_blockIdx >> 2) * 24576 + l_step_r_1 * 2048 + flat_threadIdx * 2 + 512))[0];
        ((float2*)(cb_l_b + (flat_threadIdx * 2) + 1024))[0] = ((float2*)(b_buf_raw + (flat_blockIdx >> 2) * 24576 + l_step_r_1 * 2048 + flat_threadIdx * 2 + 1024))[0];
        ((float2*)(cb_l_b + (flat_threadIdx * 2) + 1536))[0] = ((float2*)(b_buf_raw + (flat_blockIdx >> 2) * 24576 + l_step_r_1 * 2048 + flat_threadIdx * 2 + 1536))[0];
        __syncthreads();
#pragma unroll
        for (int p_step_l_3 = 0; p_step_l_3 < ((96 / 16) / (2)); ++p_step_l_3) {
#pragma unroll
            for (int p_step_l_4 = 0; p_step_l_4 < ((64 / 16) / (1)); ++p_step_l_4) {
                int p_step_r_1 = 0;
#pragma unroll
                for (int p_iteration_l_3 = 0; p_iteration_l_3 < (2); ++p_iteration_l_3) {
#pragma unroll
                    for (int p_iteration_r_1 = 0; p_iteration_r_1 < (4); ++p_iteration_r_1) {
                        res_p_c[p_step_l_3 * 2 * 4 + p_iteration_l_3 * 4 + p_step_l_4] += cb_l_a[(flat_threadIdx >> 4) * 64 + p_step_r_1 * 4 + p_iteration_l_3 * 32 + p_iteration_r_1 * 1 + p_step_l_3 * 1024] * cb_l_b[p_step_r_1 * 256 + (flat_threadIdx & 15) + p_iteration_r_1 * 64 + p_step_l_4 * 16];
                    }
                }
                for (p_step_r_1 = 1; p_step_r_1 < ((32 / 1) / (4)); ++p_step_r_1) {
#pragma unroll
                    for (int p_iteration_l_3 = 0; p_iteration_l_3 < (2); ++p_iteration_l_3) {
#pragma unroll
                        for (int p_iteration_r_1 = 0; p_iteration_r_1 < (4); ++p_iteration_r_1) {
                            res_p_c[p_step_l_3 * 2 * 4 + p_iteration_l_3 * 4 + p_step_l_4] += cb_l_a[(flat_threadIdx >> 4) * 64 + p_step_r_1 * 4 + p_iteration_l_3 * 32 + p_iteration_r_1 * 1 + p_step_l_3 * 1024] * cb_l_b[p_step_r_1 * 256 + (flat_threadIdx & 15) + p_iteration_r_1 * 64 + p_step_l_4 * 16];
                        }
                    }
                }
            }
        }
        __syncthreads();
    }
    {
        for (int p_iteration_l_3 = 0; p_iteration_l_3 < (2); ++p_iteration_l_3) {
#pragma unroll
            for (int p_step_l_3 = 0; p_step_l_3 < ((96 / 16) / (2)); ++p_step_l_3) {
#pragma unroll
                for (int p_step_l_4 = 0; p_step_l_4 < ((64 / 16) / (1)); ++p_step_l_4) {
                    int_res_c_raw[flat_blockIdx * 6144 + (flat_threadIdx >> 4) * 128 + p_iteration_l_3 * 64 + (flat_threadIdx & 15) + p_step_l_3 * 2048 + p_step_l_4 * 16] = res_p_c[p_step_l_3 * 2 * 4 + p_iteration_l_3 * 4 + p_step_l_4];
                }
            }
        }
    }
}
