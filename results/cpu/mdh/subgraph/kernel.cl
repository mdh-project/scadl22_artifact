__kernel void subgraph2_static_1(const float c_1, __global float const * const restrict mul_i_1_buf_raw, __global float const * const restrict mul_i_2_buf_raw, const float c_2, __global float * const restrict res_g_o_raw, __global float * const restrict int_res_o_raw) {
    const size_t i_wg_l_1 = (get_group_id(2) % (16));
    const size_t i_wi_l_1 = 0;
    const size_t i_wg_l_2 = 0;
    const size_t i_wi_l_2 = 0;
    const size_t i_wg_l_3 = get_group_id(0);
    const size_t i_wi_l_3 = get_local_id(0);
    const size_t i_wg_l_4 = 0;
    const size_t i_wi_l_4 = 0;
  size_t l_cb_offset_l_1;
  size_t l_cb_offset_l_2;
  size_t l_cb_offset_l_3;
  size_t l_cb_offset_l_4;
  size_t p_cb_offset_l_1;
  size_t p_cb_offset_l_2;
  size_t p_cb_offset_l_3;
  size_t p_cb_offset_l_4;
    __global float const * const restrict mul_i_1_buf = mul_i_1_buf_raw;
    __global float const * const restrict mul_i_2_buf = mul_i_2_buf_raw;
  __private float cb_p_mul_i_2[((((1 - 1)) - (0) + 1) * (1))][((((1 - 1)) - (0) + 1) * (1))][(((0) - (0) + 1))];
  __global float * const restrict res_g_o = res_g_o_raw;
  __global float * const restrict int_res_o = int_res_o_raw;
  __private float res_p_o[1][1][1][1];
  l_cb_offset_l_1 = i_wg_l_1 * 1;
  barrier(CLK_LOCAL_MEM_FENCE);
  for (size_t l_step_l_1 = 0; l_step_l_1 < ((16 / (16 * 1)) / (1 / 1)); ++l_step_l_1) {
    barrier(CLK_LOCAL_MEM_FENCE);
    l_cb_offset_l_2 = i_wg_l_2 * 1;
    for (size_t l_step_l_2 = 0; l_step_l_2 < ((1 / (1 * 1)) / (1 / 1)); ++l_step_l_2) {
      l_cb_offset_l_3 = i_wg_l_3 * 24;
      for (size_t l_step_l_3 = 0; l_step_l_3 < ((384 / (16 * 24)) / (24 / 24)); ++l_step_l_3) {
        barrier(CLK_LOCAL_MEM_FENCE);
        l_cb_offset_l_4 = i_wg_l_4 * 1;
        for (size_t l_step_l_4 = 0; l_step_l_4 < ((384 / (1 * 1)) / (2 / 1)); ++l_step_l_4) {
          p_cb_offset_l_1 = i_wi_l_1 * 1;
          for (size_t p_step_l_1 = 0; p_step_l_1 < ((1 / 1) / (1)); ++p_step_l_1) {
            p_cb_offset_l_2 = i_wi_l_2 * 1;
            for (size_t p_step_l_2 = 0; p_step_l_2 < ((1 / 1) / (1)); ++p_step_l_2) {
              p_cb_offset_l_3 = i_wi_l_3 * 1;
              for (size_t p_step_l_3 = 0; p_step_l_3 < ((24 / 24) / (1)); ++p_step_l_3) {
                p_cb_offset_l_4 = i_wi_l_4 * 1;
                for (size_t p_step_l_4 = 0; p_step_l_4 < ((2 / 1) / (1)); ++p_step_l_4) {
                  {
                    {
                      for (size_t step = 0; step < ((1) / 1) * ((((1 - 1)) - (0) + 1)) * (((0) - (0) + 1)) * ((1) / 1) * ((((1 - 1)) - (0) + 1)) / (1); ++step) {
                        const size_t flat_index = (0) + step * (1);
                        const size_t p_dim_0_index_l_1 = flat_index / (((((1 - 1)) - (0) + 1)) * (((0) - (0) + 1)) * ((1) / 1) * ((((1 - 1)) - (0) + 1)));
                        const size_t i = (flat_index / ((((0) - (0) + 1)) * ((1) / 1) * ((((1 - 1)) - (0) + 1)))) % ((((1 - 1)) - (0) + 1));
                        const size_t j = (flat_index / (((1) / 1) * ((((1 - 1)) - (0) + 1)))) % (((0) - (0) + 1));
                        const size_t p_dim_2_index_l_4 = (flat_index / (((((1 - 1)) - (0) + 1)))) % ((1) / 1);
                        const size_t k = flat_index % ((((1 - 1)) - (0) + 1));
                        size_t p_mem_offset_dim_0 = (((p_dim_0_index_l_1))) * (((1 - 1)) - (0) + 1);
                        size_t p_mem_offset_dim_1 = 0;
                        size_t p_mem_offset_dim_2 = (((p_dim_2_index_l_4))) * (((1 - 1)) - (0) + 1);
                        size_t g_mem_offset_dim_0 = ((l_step_l_1 * (1 / 1) + (((p_step_l_1 * (1) + (p_dim_0_index_l_1 * 1) / 1) * 1 + i_wi_l_1 * 1 + ((p_dim_0_index_l_1 * 1) % 1))) / 1) * (16 * 1) + i_wg_l_1 * 1 + ((((p_step_l_1 * (1) + (p_dim_0_index_l_1 * 1) / 1) * 1 + i_wi_l_1 * 1 + ((p_dim_0_index_l_1 * 1) % 1))) % 1));
                        size_t g_mem_offset_dim_1 = 0;
                        size_t g_mem_offset_dim_2 = ((l_step_l_4 * (2 / 1) + (((p_step_l_4 * (1) + (p_dim_2_index_l_4 * 1) / 1) * 1 + i_wi_l_4 * 1 + ((p_dim_2_index_l_4 * 1) % 1))) / 1) * (1 * 1) + i_wg_l_4 * 1 + ((((p_step_l_4 * (1) + (p_dim_2_index_l_4 * 1) / 1) * 1 + i_wi_l_4 * 1 + ((p_dim_2_index_l_4 * 1) % 1))) % 1));
                        cb_p_mul_i_2[((p_mem_offset_dim_2 + k))][((p_mem_offset_dim_0 + i))][((p_mem_offset_dim_1 + j))] =
                            mul_i_2_buf[(g_mem_offset_dim_0 + i) * ((0) - (0) + 1) * (((384 - 1)) - (0) + 1) + (g_mem_offset_dim_1 + j) * (((384 - 1)) - (0) + 1) + (g_mem_offset_dim_2 + k)];
                      }
                        }
                  }
#pragma unroll
                  for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (1); ++p_iteration_l_1) {
                    for (size_t p_iteration_l_2 = 0; p_iteration_l_2 < (1); ++p_iteration_l_2) {
                      for (size_t p_iteration_l_3 = 0; p_iteration_l_3 < (1); ++p_iteration_l_3) {
                        for (size_t p_iteration_l_4 = 0; p_iteration_l_4 < (1); ++p_iteration_l_4) {
                          res_p_o[(p_iteration_l_1)][(p_iteration_l_2)][(p_iteration_l_4)][(p_iteration_l_3)] = (c_1 - (mul_i_1_buf[((l_cb_offset_l_1 + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 1 + 0)) / 1) * (16 * 1) + i_wi_l_1)) * (((384 - 1)) - (0) + 1) * ((0) - (0) + 1) + ((l_cb_offset_l_3 + (((p_cb_offset_l_3 + (((p_iteration_l_3)) / 1) * 24 + 0)) / 24) * (16 * 24) + i_wi_l_3)) * ((0) - (0) + 1) + (0)] * cb_p_mul_i_2[((((((((p_iteration_l_4) / 1)))) * (((1 - 1)) - (0) + 1)) + ((((p_iteration_l_4) % 1)) - (0))))][((((((((p_iteration_l_1) / 1)))) * (((1 - 1)) - (0) + 1)) + ((((p_iteration_l_1) % 1)) - (0))))][((((0) * ((0) - (0) + 1)) + ((0) - (0))))])) * c_2;
                        }
                      }
                    }
                  }
                  for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (1); ++p_iteration_l_1) {
                    for (size_t p_iteration_l_2 = 0; p_iteration_l_2 < (1); ++p_iteration_l_2) {
                      for (size_t p_iteration_l_3 = 0; p_iteration_l_3 < (1); ++p_iteration_l_3) {
                        for (size_t p_iteration_l_4 = 0; p_iteration_l_4 < (1); ++p_iteration_l_4) {
                          int_res_o[((l_cb_offset_l_1 + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 1 + 0)) / 1) * (16 * 1) + i_wi_l_1)) * (((1 - 1)) - (0) + 1) * (((384 - 1)) - (0) + 1) * (((384 - 1)) - (0) + 1) + ((l_cb_offset_l_2 + (((p_cb_offset_l_2 + (((p_iteration_l_2)) / 1) * 1 + 0)) / 1) * (1 * 1) + i_wi_l_2)) * (((384 - 1)) - (0) + 1) * (((384 - 1)) - (0) + 1) + ((l_cb_offset_l_3 + (((p_cb_offset_l_3 + (((p_iteration_l_3)) / 1) * 24 + 0)) / 24) * (16 * 24) + i_wi_l_3)) * (((384 - 1)) - (0) + 1) + ((l_cb_offset_l_4 + (((p_cb_offset_l_4 + (((p_iteration_l_4)) / 1) * 1 + 0)) / 1) * (1 * 1) + i_wi_l_4))] = res_p_o[(p_iteration_l_1)][(p_iteration_l_2)][(p_iteration_l_4)][(p_iteration_l_3)];
                        }
                      }
                    }
                  }
                  p_cb_offset_l_4 += 1 * (1);
                }
                p_cb_offset_l_3 += 24 * (1);
              }
              p_cb_offset_l_2 += 1 * (1);
            }
            p_cb_offset_l_1 += 1 * (1);
          }
          l_cb_offset_l_4 += (1 * 1) * (2 / 1);
        }
        l_cb_offset_l_3 += (16 * 24) * (24 / 24);
      }
      l_cb_offset_l_2 += (1 * 1) * (1 / 1);
    }
    l_cb_offset_l_1 += (16 * 1) * (1 / 1);
  }
}
