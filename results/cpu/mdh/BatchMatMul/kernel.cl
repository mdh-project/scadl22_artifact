__kernel void batch_matmul_static_1(__global float *  restrict a_buf_raw, __global float  *  restrict b_buf_raw, __global float *  restrict res_g_c_raw, __global float *  restrict int_res_c_raw) {
  __local float cb_l_a[(1)*(1)*(1)*(1)*(1)*(1)*(1)*(48)*(4)*(24)*(1)*(1)];
  __local float cb_l_b[(1)*(1)*(1)*(1)*(1)*(1)*(24)*(1)*(1)*(1)*(8)*(8)];
  __private float res_p_c[(4)*(8)];
  for (int p_iteration_l_3 = 0; p_iteration_l_3 < (4); ++p_iteration_l_3) {
    for (int p_iteration_l_4 = 0; p_iteration_l_4 < (8); ++p_iteration_l_4) {
      res_p_c[(p_iteration_l_3) * (8) + (p_iteration_l_4)] = 0.0f;
    }
  }
  for (int l_step_r_1 = 0; l_step_r_1 < ((384 / (1 * 1)) / (24 / 1)); ++l_step_r_1) {
    barrier(CLK_LOCAL_MEM_FENCE);
    {
      {
        for (int step = 0; step < 3; ++step) {
          vstore4(vload4(0, a_buf_raw + (((((((int)get_group_id(0)) * 73728) + (step * 24576)) + ((((int)get_local_id(0)) / 6) * 384)) + (l_step_r_1 * 24)) + ((((int)get_local_id(0)) % 6) * 4))), 0, cb_l_a + ((step * 1536) + (((int)get_local_id(0)) * 4)));
        }
      }
    }
    {
      {
        for (int step = 0; step < 2; ++step) {
          vstore2(vload2(0, b_buf_raw + ((((((int)get_group_id(0)/2) * 24576) + (l_step_r_1 * 1536)) + (step * 768)) + (((int)get_local_id(0)) * 2))), 0, cb_l_b + ((step * 768) + (((int)get_local_id(0)) * 2)));
        }
      }
    }
    barrier(CLK_LOCAL_MEM_FENCE);
    for (int p_step_r_1 = 0; p_step_r_1 < ((24 / 1) / (1)); ++p_step_r_1) {
      for (int p_iteration_l_3 = 0; p_iteration_l_3 < (4); ++p_iteration_l_3) {
        for (int p_iteration_l_4 = 0; p_iteration_l_4 < (8); ++p_iteration_l_4) {
            res_p_c[(p_iteration_l_3) * (8) + (p_iteration_l_4)] += cb_l_a[((int)get_local_id(0)>>3) * (4)*(24) + (p_iteration_l_3) * (24) + (p_step_r_1)] * cb_l_b[(p_step_r_1) * (8)*(8) + ((int)get_local_id(0)&7) * (8) + p_iteration_l_4];
            // res_p_c[(((p_iteration_l_3 * 8) + p_iteration_l_4))] = (res_p_c[(((p_iteration_l_3 * 8) + p_iteration_l_4))] + (cb_l_a[(((((((int)get_local_id(0)) >> 3) * 96) + (p_iteration_l_3 * 24)) + p_step_r_1))] * cb_l_b[((((p_step_r_1 * 64) + ((((int)get_local_id(0)) & 7) * 8)) + p_iteration_l_4))]));
        }
      }
    }
  }
  for (int p_iteration_l_3 = 0; p_iteration_l_3 < (4); ++p_iteration_l_3) {
    for (int p_iteration_l_4 = 0; p_iteration_l_4 < (8); ++p_iteration_l_4) {
      int_res_c_raw[((int)get_group_id(0)) * (1)*(48)*(4)*(1)*(8)*(8) + ((int)get_local_id(0)>>3) * (4)*(1)*(8)*(8) + (p_iteration_l_3) * (1)*(8)*(8) + ((int)get_local_id(0)&7) * (8) + (p_iteration_l_4)] = res_p_c[(p_iteration_l_3) * (8) + (p_iteration_l_4)];
      // int_res_c_raw[((((((((int)get_group_id(0)) * 12288) + ((((int)get_local_id(0)) >> 3) * 256)) + (p_iteration_l_3 * 64)) + ((((int)get_local_id(0)) & 7) * 8)) + p_iteration_l_4))] = res_p_c[(((p_iteration_l_3 * 8) + p_iteration_l_4))];
    }
  }
}
