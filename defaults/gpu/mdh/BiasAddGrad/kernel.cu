extern "C" __global__
void bias_add_grad_static_1(float const * const __restrict__ in_buf_raw, float * const __restrict__ res_g_out_raw, float * const __restrict__ int_res_out_raw) {
    const size_t i_wg_l_1 = blockIdx.x;
    const size_t i_wi_l_1 = threadIdx.x;
    const size_t i_wg_r_1 = 0;
    const size_t i_wi_r_1 = threadIdx.y;
  size_t l_cb_offset_l_1;
  size_t l_cb_offset_r_1;
  size_t p_cb_offset_l_1;
  size_t p_cb_offset_r_1;
    float const (*in_buf)[768] = reinterpret_cast<float const (*)[768]>(in_buf_raw);
  float (*res_g_out)[768] = reinterpret_cast<float (*)[768]>(res_g_out_raw);
  float (*int_res_out) = reinterpret_cast<float (*)>(int_res_out_raw);
  float res_p_out[1][((8 / 8) / (1)) + (((8 * (8 / 8)) % (8 * (1)) / 8) > 0) + (((8 * (8 / 8)) % (8 * (1)) % 8) > 0)][1];
  __shared__ float out_l_reduction_mem[19][8];
  l_cb_offset_l_1 = i_wg_l_1 * 8;
  __syncthreads();
  for (size_t l_step_l_1 = 0; l_step_l_1 < ((768 / (96 * 8)) / (8 / 8)); ++l_step_l_1) {
    __syncthreads();
    l_cb_offset_r_1 = i_wg_r_1 * 19;
    size_t l_step_r_1 = 0;
    p_cb_offset_l_1 = i_wi_l_1 * 1;
    for (size_t p_step_l_1 = 0; p_step_l_1 < ((8 / 8) / (1)); ++p_step_l_1) {
      p_cb_offset_r_1 = i_wi_r_1 * 1;
      size_t p_step_r_1 = 0;
#pragma unroll
      for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (1); ++p_iteration_l_1) {
        size_t p_iteration_r_1 = 0;
        res_p_out[(0)][p_step_l_1][(p_iteration_l_1)] = in_buf[(l_cb_offset_r_1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 19 + 0)) / 19) * (1 * 19) + i_wi_r_1)][(l_cb_offset_l_1 + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 8 + 0)) / 8) * (96 * 8) + i_wi_l_1)];
      }
      p_cb_offset_r_1 += 19 * (1);
      p_cb_offset_l_1 += 8 * (1);
    }
    l_cb_offset_r_1 += (1 * 19) * (19 / 19);
    for (l_step_r_1 = 1; l_step_r_1 < ((6144 / (1 * 19)) / (19 / 19)); ++l_step_r_1) {
      p_cb_offset_l_1 = i_wi_l_1 * 1;
      for (size_t p_step_l_1 = 0; p_step_l_1 < ((8 / 8) / (1)); ++p_step_l_1) {
        p_cb_offset_r_1 = i_wi_r_1 * 1;
        size_t p_step_r_1 = 0;
#pragma unroll
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (1); ++p_iteration_l_1) {
          for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < (1); ++p_iteration_r_1) {
            res_p_out[(0)][p_step_l_1][(p_iteration_l_1)] += in_buf[(l_cb_offset_r_1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 19 + 0)) / 19) * (1 * 19) + i_wi_r_1)][(l_cb_offset_l_1 + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 8 + 0)) / 8) * (96 * 8) + i_wi_l_1)];
          }
        }
        p_cb_offset_r_1 += 19 * (1);
        p_cb_offset_l_1 += 8 * (1);
      }
      l_cb_offset_r_1 += (1 * 19) * (19 / 19);
    }
    if (i_wg_r_1 < ((6144 % ((1 * 19) * (19 / 19)) % (1 * 19)) + 19 - 1) / 19)
    {
      const size_t l_step_r_1 = ((6144 / (1 * 19)) / (19 / 19)) + ((6144 % ((1 * 19) * (19 / 19)) / (1 * 19)) > 0);
      p_cb_offset_l_1 = i_wi_l_1 * 1;
      for (size_t p_step_l_1 = 0; p_step_l_1 < ((8 / 8) / (1)); ++p_step_l_1) {
        p_cb_offset_r_1 = i_wi_r_1 * 1;
        if (i_wi_r_1 < (6144 % ((1 * 19) * (19 / 19)) % (1 * 19)) - i_wg_r_1 * 19)
        {
          size_t p_step_r_1 = 0;
#pragma unroll
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (1); ++p_iteration_l_1) {
            {
              size_t p_iteration_r_1 = 0;
              res_p_out[(0)][p_step_l_1][(p_iteration_l_1)] += in_buf[(l_cb_offset_r_1 + ((p_cb_offset_r_1 + ((p_iteration_r_1)))))][(l_cb_offset_l_1 + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 8 + 0)) / 8) * (96 * 8) + i_wi_l_1)];
            }
          }
        }
        p_cb_offset_l_1 += 8 * (1);
      }
    }
    {
      __syncthreads();
      p_cb_offset_l_1 = i_wi_l_1 * 1;
      for (size_t p_step_l_1 = 0; p_step_l_1 < ((8 / 8) / (1)); ++p_step_l_1) {
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (1); ++p_iteration_l_1) {
          {
              {
                  out_l_reduction_mem[i_wi_r_1][i_wi_l_1] = res_p_out[(0)][p_step_l_1][(p_iteration_l_1)];
                }
              res_p_out[(0)][p_step_l_1][(p_iteration_l_1)] = 0;
            }
          __syncthreads();
          {
            size_t stride = 1 << (int) floor(log2((float) (19)));
              if (i_wi_r_1 < stride && i_wi_r_1 + stride < (19)) {
                out_l_reduction_mem[i_wi_r_1][i_wi_l_1] += out_l_reduction_mem[((i_wi_r_1) + stride)][i_wi_l_1];
              }
            stride /= 2;
            __syncthreads();
            for (; stride > 0; stride /= 2) {
                if (i_wi_r_1 < stride) {
                  out_l_reduction_mem[i_wi_r_1][i_wi_l_1] += out_l_reduction_mem[((i_wi_r_1) + stride)][i_wi_l_1];
                }
              __syncthreads();
            }
            __syncthreads();
          }
          if (i_wi_r_1 == 0) {
            int_res_out[(l_cb_offset_l_1 + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 8 + 0)) / 8) * (96 * 8) + i_wi_l_1)] = out_l_reduction_mem[i_wi_r_1][i_wi_l_1];
          }
          __syncthreads();
        }
        p_cb_offset_l_1 += 8 * (1);
      }
    }
    l_cb_offset_l_1 += (96 * 8) * (8 / 8);
  }
}
