WARNING: Logging before InitGoogleLogging() is written to STDERR
I0819 10:52:13.169203 60258 cuda_tc_executor.cc:105] generatedCuda: 
template<typename T> inline __device__ T floord(T n, T d) {
  return n < 0 ? - (-n + d - 1)/d : n / d;
}
#define if_then_else(cond,a,b) ((cond) ? (a) : (b))

// Halide type handling
typedef int int32;
typedef long int64;
typedef float float32;
typedef double float64;

#define inff __int_as_float(0x7f800000)
#define inf __longlong_as_double(0x7ff0000000000000LL)

// Before CUDA 9, syncwarp is a noop since warps are always synchronized.
#if __CUDACC_VER_MAJOR__ < 9
__device__ void __syncwarp(unsigned mask = 0xFFFFFFFF) {}
#endif

extern "C" {
__global__ void subgraph_16_384_384(int32 N_1, int32 N_3, int32 N_4, float32* pO, const float32* pC_1, const float32* pI_1, const float32* pI_2, const float32* pC_2) {
  int b0 = blockIdx.x; int b1 = blockIdx.y; int b2 = blockIdx.z;
  int t0 = threadIdx.x; int t1 = threadIdx.y; int t2 = threadIdx.z;
  float32 (*O)[1][384][384] = reinterpret_cast<float32 (*)[1][384][384]>(pO);
  const float32 (*C_1) = reinterpret_cast<const float32 (*)>(pC_1);
  const float32 (*I_1)[384][1] = reinterpret_cast<const float32 (*)[384][1]>(pI_1);
  const float32 (*I_2)[1][384] = reinterpret_cast<const float32 (*)[1][384]>(pI_2);
  const float32 (*C_2) = reinterpret_cast<const float32 (*)>(pC_2);
  __shared__ float32 _I_1_0[16][96][1];
  __shared__ float32 _C_1_0[1];
  __shared__ float32 _I_2_0[16][1][65];
  __shared__ float32 _C_2_0[1];
  __syncthreads();
  if (t1 <= 15) {
    _I_2_0[t1][0][t0] = I_2[t1][0][t0 + 64 * b2];
    _I_2_0[t1][0][t0 + 16] = I_2[t1][0][t0 + 64 * b2 + 16];
    _I_2_0[t1][0][t0 + 32] = I_2[t1][0][t0 + 64 * b2 + 32];
    _I_2_0[t1][0][t0 + 48] = I_2[t1][0][t0 + 64 * b2 + 48];
    _I_1_0[t1][t0][0] = I_1[t1][t0 + 96 * b1][0];
    _I_1_0[t1][t0 + 16][0] = I_1[t1][t0 + 96 * b1 + 16][0];
    _I_1_0[t1][t0 + 32][0] = I_1[t1][t0 + 96 * b1 + 32][0];
    _I_1_0[t1][t0 + 48][0] = I_1[t1][t0 + 96 * b1 + 48][0];
    _I_1_0[t1][t0 + 64][0] = I_1[t1][t0 + 96 * b1 + 64][0];
    _I_1_0[t1][t0 + 80][0] = I_1[t1][t0 + 96 * b1 + 80][0];
    if (t1 == 0 && t0 == 0) {
      _C_2_0[0] = C_2[0];
      _C_1_0[0] = C_1[0];
    }
  }
  __syncthreads();
  for (int c3 = 0; c3 <= 15; c3 += 1) {
    for (int c4 = t1; c4 <= 95; c4 += 24) {
      O[c3][0][(96 * b1 + c4)][(t0 + 64 * b2)] = 0.000000f;
      O[c3][0][(96 * b1 + c4)][(t0 + 64 * b2)] = (O[c3][0][(96 * b1 + c4)][(t0 + 64 * b2)] + ((_C_1_0[0] - (_I_1_0[c3][c4][0]*_I_2_0[c3][0][t0]))*_C_2_0[0]));
      O[c3][0][(96 * b1 + c4)][(t0 + 64 * b2 + 16)] = 0.000000f;
      O[c3][0][(96 * b1 + c4)][(t0 + 64 * b2 + 16)] = (O[c3][0][(96 * b1 + c4)][(t0 + 64 * b2 + 16)] + ((_C_1_0[0] - (_I_1_0[c3][c4][0]*_I_2_0[c3][0][t0 + 16]))*_C_2_0[0]));
      O[c3][0][(96 * b1 + c4)][(t0 + 64 * b2 + 32)] = 0.000000f;
      O[c3][0][(96 * b1 + c4)][(t0 + 64 * b2 + 32)] = (O[c3][0][(96 * b1 + c4)][(t0 + 64 * b2 + 32)] + ((_C_1_0[0] - (_I_1_0[c3][c4][0]*_I_2_0[c3][0][t0 + 32]))*_C_2_0[0]));
      O[c3][0][(96 * b1 + c4)][(t0 + 64 * b2 + 48)] = 0.000000f;
      O[c3][0][(96 * b1 + c4)][(t0 + 64 * b2 + 48)] = (O[c3][0][(96 * b1 + c4)][(t0 + 64 * b2 + 48)] + ((_C_1_0[0] - (_I_1_0[c3][c4][0]*_I_2_0[c3][0][t0 + 48]))*_C_2_0[0]));
    }
  }
  __syncthreads();
}
}

grid: CudaDim(1, 4, 6) @0x7ffc73e9a440 block: CudaDim(16, 24, 1) @0x7ffc73e9a480
time:  3596287
