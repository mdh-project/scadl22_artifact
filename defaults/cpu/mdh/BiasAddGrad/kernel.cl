__kernel void bias_add_grad_static_1(__global float  *  restrict in_buf_raw, __global float *  restrict res_g_out_raw, __global float *  restrict int_res_out_raw) {
  const size_t i_wg = get_group_id(0);
  const size_t i_wi = get_local_id(0);
  int_res_out_raw[(i_wg) * (16) + (i_wi)] = 0.0f;
  for (int p_iteration_r_1 = 0; p_iteration_r_1 < (6144); ++p_iteration_r_1) {
    int_res_out_raw[(i_wg) * (16) + (i_wi)] += in_buf_raw[(p_iteration_r_1) * (48)*(16) + i_wg * (16) + i_wi];
  }
}
