//
//  ocl_device.hpp
//  ocal_ocl
//
//  Created by Ari Rasch on 20.09.17.
//  Copyright © 2017 Ari Rasch. All rights reserved.
//

#ifndef ocl_device_hpp
#define ocl_device_hpp


namespace ocal
{

namespace core
{

namespace ocl
{


class device : public abstract::abstract_device< device, cl::Kernel, ocl_event_manager >
{
  // friend class parent
  friend class abstract::abstract_device< device, cl::Kernel, ocl_event_manager >;

  private:
    // enabling easier access to parent's members
    using abstract::abstract_device< device, cl::Kernel, ocl_event_manager >::_platform_id;
    using abstract::abstract_device< device, cl::Kernel, ocl_event_manager >::_device_id;
    using abstract::abstract_device< device, cl::Kernel, ocl_event_manager >::_kernel;
    using abstract::abstract_device< device, cl::Kernel, ocl_event_manager >::_read_buffer_references_to_wait_for;
    using abstract::abstract_device< device, cl::Kernel, ocl_event_manager >::_write_buffer_references_to_wait_for;
    using abstract::abstract_device< device, cl::Kernel, ocl_event_manager >::_thread_configuration;

  public:
    device( const std::string& vendor_name,
            const std::string& device_name
          )
      : abstract_device< device, cl::Kernel, ocl_event_manager >(), _arg_index( 0 )
    {
      cl::Platform platform;
      cl::Device   device;

      // get all platforms
      std::vector<cl::Platform> platforms;
      try
      {
        cl::Platform::get( &platforms );
      }
      catch( cl::Error& err )
      {
        check_opencl_exception( err );
        throw;
      }
      
      // find first platform whose name contains all substrings in "platform_name"
      for( size_t i = 0 ; i < platforms.size(); ++i )
      {
        // get platform name
        std::string actual_vendor_name;
        platforms[ i ].getInfo( CL_PLATFORM_VENDOR, &actual_vendor_name );
        
        if( common::contains_all_sub_strings( actual_vendor_name, vendor_name ) )
        {
          platform = platforms[ i ];
          
          // set opencl platform id
          _platform_id = static_cast<cl_uint>( i );
          
          break; // exit for-loop over platforms
        }
        else if( i == platforms.size() - 1  )
        {
          std::cout << "Vendor with name " << vendor_name << " not found." << std::endl;
          exit( EXIT_FAILURE );
        }
      }
      
      // find first device of platform with id "platform_id" whose name contains all substrings in "device_name"
      std::vector<cl::Device> devices;
      platform.getDevices( CL_DEVICE_TYPE_ALL, &devices );
      for( size_t i = 0 ; i < devices.size() ; ++i )
      {
        // get device name
        std::string actual_device_name;
        devices[ i ].getInfo( CL_DEVICE_NAME, &actual_device_name );
        
        if( common::contains_all_sub_strings( actual_device_name, device_name ) )
        {
          device = devices[ i ];
          
          // set opencl device id
          _device_id = static_cast<cl_uint>( i );
          
          break; // exit for loop over devices
        }
        else if( i == devices.size() - 1 )
        {
          std::cout << "Device with name " << device_name << " not found." << std::endl;
          exit( EXIT_FAILURE );
        }

      }
      
      // set global device and platform id
      unique_device_id_to_opencl_platform_id[ this->unique_id() ] = _platform_id;
      unique_device_id_to_opencl_device_id[ this->unique_id() ] = _device_id;
      
      // if device does not exist then create device
      if( unique_device_id_to_opencl_device.find( this->unique_id() ) == unique_device_id_to_opencl_device.end() )
        unique_device_id_to_opencl_device[ this->unique_id() ] = device;
      
      // create new context for device's platform if context does not already exists
      create_context_if_not_existent( devices, platform, _platform_id );
      
      // create command queues for computations
      try
      {
        // if command queues does not exist then create them
        if( unique_device_id_to_opencl_command_queues.find( this->unique_id() ) == unique_device_id_to_opencl_command_queues.end() )
          for( int i = 0; i < NUM_OPENCL_COMMAND_QUEUES_PER_DEVICE ; ++i )
            unique_device_id_to_opencl_command_queues[ this->unique_id() ][ i ] = cl::CommandQueue( unique_device_id_to_opencl_context( this->unique_id() ),
                                                                                                    device,
                                                                                                    CL_QUEUE_PROFILING_ENABLE,
                                                                                                    &error
                                                                                                  );
      }
      catch( cl::Error& err )
      {
        check_opencl_exception( err );
        throw;
      }
      
      // initialize unique_device_id_to_actual_opencl_command_queue_id if not already done
      if( unique_device_id_to_actual_opencl_command_queue_id.find( this->unique_id() ) == unique_device_id_to_actual_opencl_command_queue_id.end() )
        unique_device_id_to_actual_opencl_command_queue_id[ this->unique_id() ] = 0;
    }
  
  
    device( cl_uint platform_id = 0,
            cl_uint device_id   = 0
          )
      : abstract_device< device, cl::Kernel, ocl_event_manager >(), _arg_index( 0 )
    {
      // set platform/device id
      _platform_id = platform_id;
      _device_id   = device_id;
      
      // set global device and platform id
      unique_device_id_to_opencl_platform_id[ this->unique_id() ] = _platform_id;
      unique_device_id_to_opencl_device_id[ this->unique_id() ]   = _device_id;
      
      // if device alread exists then do nothing
      if( unique_device_id_to_opencl_device.find( this->unique_id() ) != unique_device_id_to_opencl_device.end() )
        return;
      
      // get platform with id "platform_id"
      std::vector<cl::Platform> platforms;
      try
      {
        cl::Platform::get( &platforms );
      }
      catch( cl::Error& err )
      {
        check_opencl_exception( err );
        throw;
      }
      
      if( platform_id >= platforms.size() )
      {
        std::wcerr << "No platform with id " << platform_id << " found." << std::endl;
        exit( EXIT_FAILURE );
      }
      auto platform = platforms[ platform_id ];
      
      // get all of platform's devices
      std::vector<cl::Device> devices;
      platform.getDevices( CL_DEVICE_TYPE_ALL, &devices );

      // set device
      auto& device = devices[ device_id ];
      unique_device_id_to_opencl_device[ this->unique_id() ] = device;
      
      // create new context for device's platform (if it does not already exists)
      create_context_if_not_existent( devices, platform, _platform_id );
      
      // create command queues for computations
      try
      {
        for( int i = 0; i < NUM_OPENCL_COMMAND_QUEUES_PER_DEVICE ; ++i )
          unique_device_id_to_opencl_command_queues[ this->unique_id() ][ i ] = cl::CommandQueue( unique_device_id_to_opencl_context( this->unique_id() ),
                                                                                                  device,
                                                                                                  CL_QUEUE_PROFILING_ENABLE,
                                                                                                  &error
                                                                                                );
      }
      catch( cl::Error& err )
      {
        check_opencl_exception( err );
        throw;
      }

      // initialize unique_device_id_to_actual_opencl_command_queue_id if not already done
      if( unique_device_id_to_actual_opencl_command_queue_id.find( this->unique_id() ) == unique_device_id_to_actual_opencl_command_queue_id.end() )
        unique_device_id_to_actual_opencl_command_queue_id[ this->unique_id() ] = 0;
    }



    template< size_t N, typename device_info_t, typename... device_info_val_t >
    device( common::info_class< N, device_info_t, device_info_val_t... > device_info )
      : abstract_device< device, cl::Kernel, ocl_event_manager >(), _arg_index( 0 )
    {      
      // call "info ctop" with empty platform infos
      device( common::info_class< 0, device_info_t, device_info_val_t... >{},
              device_info
            );
    }


    template< size_t M, typename platform_info_t, typename... platform_info_val_t,
              size_t N, typename device_info_t,   typename... device_info_val_t  
            >
    device( common::info_class< M, platform_info_t, platform_info_val_t... > platform_infos,
            common::info_class< N, device_info_t,   device_info_val_t...   > device_infos
          )
      : abstract_device< device, cl::Kernel, ocl_event_manager >(), _arg_index( 0 )
    {
      cl::Platform platform;
      cl::Device   device;

      // get all platforms
      std::vector<cl::Platform> platforms;
      try
      {
        cl::Platform::get( &platforms );
      }
      catch( cl::Error& err )
      {
        check_opencl_exception( err );
        throw;
      }
      
      // vector for found devices
      std::vector<cl::Device> devices;
  
      // iteration over platforms
      for( size_t i = 0 ; i < platforms.size(); ++i )
      {
        auto scalar_checker = [&]( cl_platform_info info, auto info_val )
                              {
                                // get platform info
                                using info_val_t = decltype( info_val ) ;
                                
                                info_val_t actual_info_val;
                                platforms[ i ].getInfo( info, &actual_info_val );
                                
                                return actual_info_val == info_val;
                              };
        if( !platform_infos.check( scalar_checker ) )
          continue; // Note: goes to next possible platform

        platform     = platforms[ i ];
        _platform_id = static_cast<cl_uint>( i );
        
        // iteration over devices
        devices.clear();
        platform.getDevices( CL_DEVICE_TYPE_ALL, &devices );
        for( size_t i = 0 ; i < devices.size() ; ++i )
        {
          auto scalar_checker = [&]( cl_device_info info, auto info_val )
                                {
                                  // get device info
                                  using info_val_t = decltype( info_val ) ;
                                  
                                  info_val_t actual_info_val;
                                  devices[ i ].getInfo( info, &actual_info_val );
                                  
                                  return actual_info_val == info_val;
                                };
          if( !device_infos.check( scalar_checker ) )
            continue; // Note: goes to next possible device

          device = devices[ i ];
          _device_id = static_cast<cl_uint>( i );
          
          goto device_found; // Note: exits for loop over devices and platforms
        }
      }
      
      // error: no device according to devic_info found
      std::cout << "error: no device according to devic_info found" << std::endl;
      exit( EXIT_FAILURE );
      
      device_found:
        // set global device and platform id
        unique_device_id_to_opencl_platform_id[ this->unique_id() ] = _platform_id;
        unique_device_id_to_opencl_device_id[ this->unique_id() ]   = _device_id;

        // if device does not exist then create device
        if( unique_device_id_to_opencl_device.find( this->unique_id() ) == unique_device_id_to_opencl_device.end() )
          unique_device_id_to_opencl_device[ this->unique_id() ] = device;
    
        // create new context for device's platform if context does not already exists
        create_context_if_not_existent( devices, platform, _platform_id );
    
        // create command queues for computations
        try
        {
          // if command queues does not exist then create them
          if( unique_device_id_to_opencl_command_queues.find( this->unique_id() ) == unique_device_id_to_opencl_command_queues.end() )
            for( int i = 0; i < NUM_OPENCL_COMMAND_QUEUES_PER_DEVICE ; ++i )
              unique_device_id_to_opencl_command_queues[ this->unique_id() ][ i ] = cl::CommandQueue( unique_device_id_to_opencl_context( this->unique_id() ),
                                                                                                      device,
                                                                                                      CL_QUEUE_PROFILING_ENABLE,
                                                                                                      &error
                                                                                                    );
        }
        catch( cl::Error& err )
        {
          check_opencl_exception( err );
          throw;
        }
    
        // initialize unique_device_id_to_actual_opencl_command_queue_id if not already done
        if( unique_device_id_to_actual_opencl_command_queue_id.find( this->unique_id() ) == unique_device_id_to_actual_opencl_command_queue_id.end() )
          unique_device_id_to_actual_opencl_command_queue_id[ this->unique_id() ] = 0;
    }
  
  
    // copy ctor instantiates shallow copy
    device( const device& ) = default;


    ~device()
    {
    // different ocal::devices can point to the same physical device ( -> the ocal:devices have the same unique_id)
//      // erase device's default command queue out of global list
//      unique_device_id_to_default_opencl_command_queue.erase( this->unique_id() );
//      
//      // erase device's command queues for computation out of global list
//      auto command_queues = unique_device_id_to_opencl_command_queues.at( this->unique_id() );      
    }


    // set kernel: case "kernel"
    template< typename T, typename = std::enable_if_t< std::is_same< kernel, typename ::ocal::common::remove_cv_and_references< T >::type >::value > >
    auto& operator()( T& kernel_object )
    {
      auto& ocl_kernel = kernel_object;
      
      auto& abstract_kernel = static_cast< abstract::abstract_kernel< kernel, cl::Kernel > & >( ocl_kernel ); 
      abstract::abstract_device< device, cl::Kernel, ocl_event_manager >::operator()( abstract_kernel );
      
      return *this;
    }


    // set kernel: case "kernel_representation"
    template< typename T, typename... Ts, typename = std::enable_if_t< std::is_base_of< ocl::kernel_representation,  typename ::ocal::common::remove_cv_and_references< T >::type >::value ||
                                                                       std::is_base_of< cuda::kernel_representation, typename ::ocal::common::remove_cv_and_references< T >::type >::value
                                                                     > >
    auto& operator()( const T& kernel_representation, const Ts&... args )
    {
      auto ocl_kernel = kernel( kernel_representation, args... );
      
      auto& abstract_kernel = static_cast< abstract::abstract_kernel< kernel, cl::Kernel > & >( ocl_kernel );
      abstract::abstract_device< device, cl::Kernel, ocl_event_manager >::operator()( abstract_kernel );
      
      return *this;
    }

  
    // set nd_range: case OpenCL
    auto& operator()( nd_range gs, nd_range ls )
    {
      _thread_configuration[ 0 ] = {{ gs.x(), gs.y(), gs.z() }};
      _thread_configuration[ 1 ] = {{ ls.x(), ls.y(), ls.z() }};
      
      return *this;
    }
  

    // set nd_range: case CUDA
    auto& operator()( cuda::dim3 gs, cuda::dim3 bs )
    {
      this->operator()( core::ocl::nd_range{ gs.x() * bs.x(), gs.y() * bs.y(), gs.z() * bs.z() },
                        core::ocl::nd_range{ bs.x()         , bs.y()         , bs.z()          }
                      );
      
      return *this;
    }
    

    // set kernel's input
    template< typename... Ts >
    auto& operator()( const Ts&... args )
    {
      abstract::abstract_device< device, cl::Kernel, ocl_event_manager >::operator()( args... ); 
      
      return *this;
    }
  
  
    template< typename T >
    void information( cl_device_info info, T& info_val )
    {
      unique_device_id_to_opencl_device.at( this->unique_id() ).getInfo( info, &info_val );
    }

    void synchronize()
    {
      for( int i = 0; i < NUM_OPENCL_COMMAND_QUEUES_PER_DEVICE ; ++i )
        unique_device_id_to_opencl_command_queues[ this->unique_id() ][ i ].finish();
    }

    unsigned long long last_runtime()
    {
      _last_event.wait();
      cl_ulong start_time;
      cl_ulong end_time;
      auto err = _last_event.get().getProfilingInfo(CL_PROFILING_COMMAND_START, &start_time);
      if (err != CL_SUCCESS) throw cl::Error(err);
      err = _last_event.get().getProfilingInfo(CL_PROFILING_COMMAND_END, &end_time);
      if (err != CL_SUCCESS) throw cl::Error(err);
      return end_time - start_time;
    }


    std::string name()
    {
      std::string val;
      information(CL_DEVICE_NAME, val);
      return val;
    }


    size_t max_threads_per_group()
    {
      size_t val;
      information(CL_DEVICE_MAX_WORK_GROUP_SIZE, val);
      return val;
    }


    std::vector<size_t> max_group_size()
    {
      cl_uint n_dims;
      information(CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, n_dims);
      std::vector<size_t> vals(n_dims);
      information(CL_DEVICE_MAX_WORK_ITEM_SIZES, vals);
      return vals;
    }


    size_t max_local_or_shared_memory()
    {
      cl_ulong val;
      information(CL_DEVICE_LOCAL_MEM_SIZE, val);
      return static_cast<size_t>(val);
    }
    
  
  private:
    // ----------------------------------------------------------
    //   static polymorphism
    // ----------------------------------------------------------

    int& unique_device_id_to_actual_command_queue_or_stream_id( int unique_device_id )
    {
      return unique_device_id_to_actual_opencl_command_queue_id.at( unique_device_id );
    }


    void start_kernel( int command_queue_id, clEvent_wrapper& event )
    {
      auto command_queue = unique_device_id_to_opencl_command_queues.at( this->unique_id() ).at( command_queue_id );
      
      const nd_range& gs = _thread_configuration[ 0 ];
      const nd_range& ls = _thread_configuration[ 1 ];
      
      try
      {
        // case: OpenCL determines local size
        if( ls.x() == 0 && ls.y() == 0 && ls.z() == 0 )
          command_queue.enqueueNDRangeKernel( _kernel, cl::NullRange, gs.get_opencl_ndrange(), cl::NullRange, NULL, &( event.get() ) );
        else
          command_queue.enqueueNDRangeKernel( _kernel, cl::NullRange, gs.get_opencl_ndrange(), ls.get_opencl_ndrange(), NULL, &( event.get() ) );
      }
      catch( cl::Error& err )
      {
        // reset _arg_index
        _arg_index = 0;

        check_opencl_exception( err );

        throw;
      }

      // reset _arg_index
      _arg_index = 0;
    }


    template< typename T >
    void set_arg( T arg )
    {
      _kernel.setArg( _arg_index++, arg );
    }


    template< typename T >
    void set_arg( local_buffer<T> arg )
    {
      _kernel.setArg( _arg_index++, cl::Local( arg.size() ) );
    }

  
    int num_command_queues_or_streams()
    {
      return NUM_OPENCL_COMMAND_QUEUES_PER_DEVICE;
    }
    
  
  private:
    cl_uint  _arg_index;
};


class ocl_device_info_t // TODO: nötig?
{
  public:
    ocl_device_info_t( cl_device_info device_info )
      : _device_info( device_info )
    {}
  
  
    operator cl_device_info()
    {
      return _device_info;
    }
  
  private:
    cl_device_info _device_info;
};


class ocl_platform_info_t // TODO: nötig?
{
  public:
    ocl_platform_info_t( cl_platform_info platform_info )
      : _platform_info( platform_info )
    {}
  
  
    operator cl_platform_info()
    {
      return _platform_info;
    }
  
  
  private:
    cl_platform_info _platform_info;
};


//template< typename... device_info_val_t, typename... platform_info_val_t, typename device_info_t = ocl_device_info_t, typename platform_info_t = ocl_platform_info_t >
//auto info( std::pair< device_info_t,   device_info_val_t   >... device_infos,
//           std::pair< platform_info_t, platform_info_val_t >... platform_infos
//         )
//{
//  return common::info< device_info_t, std::tuple<device_info_val_t...>, platform_info_t, std::tuple<platform_info_val_t...> >( device_infos..., platform_infos... );
//}


} // namespace "ocl"

} // namespace "core"

} // namespace "ocal"



#endif /* ocl_device_hpp */
