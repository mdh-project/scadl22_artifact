//
//  ocl_nd_range.hpp
//  ocal_ocl
//
//  Created by Ari Rasch on 20.09.17.
//  Copyright © 2017 Ari Rasch. All rights reserved.
//

#ifndef ocl_nd_range_hpp
#define ocl_nd_range_hpp


namespace ocal
{

namespace core
{


#ifndef ocl_thread_configuration_set
#define ocl_thread_configuration_set

namespace ocl
{

class nd_range : public abstract::abstract_thread_configuration
{
  // friend class parent
  friend class abstract::abstract_thread_configuration;

  public:
    nd_range() = default;
  
    // generic ctor
    template< typename... Ts >
    nd_range( const Ts&... args )
      : abstract_thread_configuration( args... )
    {}
  
  
    cl::NDRange get_opencl_ndrange() const
    {
      return cl::NDRange( this->x(), this->y(), this->z() );
    }
};

} // namespace "ocl"

#endif /* ocl_thread_configuration_set */



#ifndef cuda_thread_configuration_set
#define cuda_thread_configuration_set

namespace cuda
{

class dim3 : public abstract::abstract_thread_configuration
{
  // friend class parent
  friend class abstract::abstract_thread_configuration;

  public:
    dim3() = default;
  
    // generic ctor
    template< typename... Ts >
    dim3( const Ts&... args )
      : abstract_thread_configuration( args... )
    {}
};

} // namespace "cuda"

#endif /* cuda_thread_configuration_set */



} // namespace "core"

} // namespace "ocal"


#endif /* ocl_nd_range_hpp */
