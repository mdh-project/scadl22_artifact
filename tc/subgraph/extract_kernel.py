import tensor_comprehensions as tc
import torch
import os
import sys

torch.cuda.device(int(sys.argv[1]))
N_1 = int(sys.argv[2])
N_2 = int(sys.argv[3])
N_3 = int(sys.argv[4])
N_4 = int(sys.argv[5])

if not os.path.isfile("../../results/gpu/tc/subgraph/cache.options"):
	print("Unable to extract kernel from Tensor Comprehensions. Has it not yet been tuned?")
	sys.exit(1)

lang = """
def subgraph(float(1) C_1, float(N_1, N_3, 1) I_1, float(N_1, 1, N_4) I_2, float(1) C_2) -> (O) {
    O(n_1, n_2, n_3, n_4) +=! (C_1(0) - I_1(n_1, n_3, 0) * I_2(n_1, 0, n_4)) * C_2(0) where n_2 in 0:1
}
"""

subgraph = tc.define(lang, cache="../../results/gpu/tc/subgraph/cache", name="subgraph")
C_1, I_1, I_2, C_2 = torch.randn(size=[1]).cuda(), torch.randn(size=[N_1, N_3, 1]).cuda(), torch.randn(size=[N_1, 1, N_4]).cuda(), torch.randn(size=[1]).cuda()
tc.SetDebugFlags(dump_cuda=True)
O = subgraph(C_1, I_1, I_2, C_2)
tc.SetDebugFlags(dump_cuda=False)
torch.cuda.synchronize()