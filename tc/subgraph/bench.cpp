//
// Copyright (c) 2020 Richard Schulze. All rights reserved.
//

#include <vector>
#include "../OCAL/ocal_cuda.hpp"

int main(int argc, const char **argv) {
    if (argc <= 5) {
        std::cout << "usage: kernel_file device_id M N K" << std::endl;
        exit(EXIT_FAILURE);
    }
    std::string kernel_file_name = argv[1];
    int device_id = std::atoi(argv[2]);
    int N_1 = std::atoi(argv[3]);
    int N_2 = std::atoi(argv[4]);
    int N_3 = std::atoi(argv[5]);
    int N_4 = std::atoi(argv[6]);

    // read kernel source and grid/block size
    std::string kernel_source;
    dim3 gs, bs;
    std::ifstream kernel_file(kernel_file_name);
    std::string line;
    int part = 0; //  0 - log output, 1 - kernel, 2 - grid and block size
    while (std::getline(kernel_file, line)) {
        if (line.length() >= 15 && line.compare(line.length() - 15, 15, "generatedCuda: ") == 0) {
            part = 1;
            continue;
        } else if (line.length() >= 6 && line.compare(0, 6, "grid: ") == 0) {
            part = 2;
        }

        if (part == 1) {
            kernel_source += line;
            kernel_source += '\n';
        } else if (part == 2) {
            std::regex rgx(R"(grid: CudaDim\((\d+), (\d+), (\d+)\) \@0x[a-f0-9]{12} block: CudaDim\((\d+), (\d+), (\d+)\) \@0x[a-f0-9]{12})");
            std::smatch match;
            if (std::regex_match(line, match, rgx)) {
                for (int i = 0; i < 6; ++i) {
                    std::stringstream ss(match[1 + i].str());
                    int tmp;
                    if (!(ss >> tmp)) {
                        std::cout << "unable to parse grid or block size: " << ss.str() << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    (i < 3 ? gs : bs).set(i % 3, tmp);
                }
            } else {
                std::cout << "unable to match grid and block size:" << std::endl;
                std::cout << line << std::endl;
                exit(EXIT_FAILURE);
            }
        }

        if (part == 2) {
            part = 0;
        }
    }

    ocal::device<CUDA> gpu(device_id);

    ocal::buffer<float> C_1(1); for (int i = 0; i < C_1.size(); ++i) C_1[i] = (i + 1) % 10;
    ocal::buffer<float> I_1(N_1 * N_3 * 1); for (int i = 0; i < I_1.size(); ++i) I_1[i] = (i + 1) % 10;
    ocal::buffer<float> I_2(N_1 * 1 * N_4); for (int i = 0; i < I_2.size(); ++i) I_2[i] = (i + 1) % 10;
    ocal::buffer<float> C_2(1); for (int i = 0; i < C_2.size(); ++i) C_2[i] = (i + 1) % 10;
    ocal::buffer<float> O(N_1 * N_2 * N_3 * N_4);

    std::vector<unsigned long long> runtimes;
    for (int i = 0; i < 210; ++i) {
        gpu(cuda::source(kernel_source),
            "subgraph_" + std::to_string(N_1) + "_" + std::to_string(N_3) + "_" + std::to_string(N_4),
            std::vector<std::string>({
                                             "--use_fast_math",
                                             "-std=c++11",
                                             "-default-device",
                                             "-DNVRTC_CUB=1"
                                     }))
                (gs, bs)
                (N_1, N_3, N_4, write(O), read(C_1), read(I_1), read(I_2), read(C_2));
        runtimes.push_back(gpu.last_runtime());
    }
    std::cout << "min runtime: " << *std::min_element(runtimes.begin(), runtimes.end()) << std::endl;

//    auto C_1_ptr = C_1.get_host_memory_ptr();
//    auto I_1_ptr = I_1.get_host_memory_ptr();
//    auto I_2_ptr = I_2.get_host_memory_ptr();
//    auto C_2_ptr = C_2.get_host_memory_ptr();
//    auto O_ptr = O.get_host_memory_ptr();
//    for (int n_1 = 0; n_1 < N_1 ; ++n_1) {
//        for (int n_2 = 0; n_2 < N_2 ; ++n_2) {
//            for (int n_3 = 0; n_3 < N_3 ; ++n_3) {
//                for (int n_4 = 0; n_4 < N_4 ; ++n_4) {
//                    float res = (C_1_ptr[0] - I_1_ptr[n_1 * N_3 * 1 + n_3 * 1 + 0] *
//                                              I_2_ptr[n_1 * 1 * N_4 + 0 * N_4 + n_4]) * C_2_ptr[0];
//                    if (res != O_ptr[n_1 * N_2 * N_3 * N_4 + n_2 * N_3 * N_4 + n_3 * N_4 + n_4]) {
//                        std::cout << "incorrect: expected " << res << " got " << O_ptr[n_1 * N_2 * N_3 * N_4 + n_2 * N_3 * N_4 + n_3 * N_4 + n_4] << std::endl;
//                        exit(EXIT_FAILURE);
//                    }
//                }
//            }
//        }
//    }
//    std::cout << "correct" << std::endl;

}