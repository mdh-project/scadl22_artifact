import sys
import time

import torch

import tensor_comprehensions as tc

torch.cuda.device(int(sys.argv[1]))
B_1 = int(sys.argv[2])
B_2 = int(sys.argv[3])
I = int(sys.argv[4])
J = int(sys.argv[5])
K = int(sys.argv[6])

lang = """
def batch_matmul(float(B_1, B_2, I, K) A, float(B_1, B_2, K, J) B) -> (C) {
	C(b_1, b_2, i, j) +=! A(b_1, b_2, i, k) * B(b_1, b_2, k, j)
}
"""

batch_matmul = tc.define(lang, cache="../../results/gpu/tc/BatchMatMul/cache", name="batch_matmul")
A, B = torch.randn(size=[B_1, B_2, I, K]).cuda(), torch.randn(size=[B_1, B_2, K, J]).cuda()
settings = {
    "threads": 8, "generations": 10, "pop_size": 100, "number_elites": 10
}
torch.cuda.synchronize()
start = time.process_time()
batch_matmul.autotune(A, B, cache="../../results/gpu/tc/BatchMatMul/cache",  **settings)
torch.cuda.synchronize()
end = time.process_time()
