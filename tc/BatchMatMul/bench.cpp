//
// Copyright (c) 2020 Richard Schulze. All rights reserved.
//

#include <vector>
#include "../OCAL/ocal_cuda.hpp"

int main(int argc, const char **argv) {
    if (argc <= 5) {
        std::cout << "usage: kernel_file device_id M N K" << std::endl;
        exit(EXIT_FAILURE);
    }
    std::string kernel_file_name = argv[1];
    int device_id = std::atoi(argv[2]);
    int B_1 = std::atoi(argv[3]);
    int B_2 = std::atoi(argv[4]);
    int I = std::atoi(argv[5]);
    int J = std::atoi(argv[6]);
    int K = std::atoi(argv[7]);

    // read kernel source and grid/block size
    std::string kernel_source;
    dim3 gs, bs;
    std::ifstream kernel_file(kernel_file_name);
    std::string line;
    int part = 0; //  0 - log output, 1 - kernel, 2 - grid and block size
    while (std::getline(kernel_file, line)) {
        if (line.length() >= 15 && line.compare(line.length() - 15, 15, "generatedCuda: ") == 0) {
            part = 1;
            continue;
        } else if (line.length() >= 6 && line.compare(0, 6, "grid: ") == 0) {
            part = 2;
        }

        if (part == 1) {
            kernel_source += line;
            kernel_source += '\n';
        } else if (part == 2) {
            std::regex rgx(R"(grid: CudaDim\((\d+), (\d+), (\d+)\) \@0x[a-f0-9]{12} block: CudaDim\((\d+), (\d+), (\d+)\) \@0x[a-f0-9]{12})");
            std::smatch match;
            if (std::regex_match(line, match, rgx)) {
                for (int i = 0; i < 6; ++i) {
                    std::stringstream ss(match[1 + i].str());
                    int tmp;
                    if (!(ss >> tmp)) {
                        std::cout << "unable to parse grid or block size: " << ss.str() << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    (i < 3 ? gs : bs).set(i % 3, tmp);
                }
            } else {
                std::cout << "unable to match grid and block size:" << std::endl;
                std::cout << line << std::endl;
                exit(EXIT_FAILURE);
            }
        }

        if (part == 2) {
            part = 0;
        }
    }

    ocal::device<CUDA> gpu(device_id);

    ocal::buffer<float> A(B_1 * B_2 * I * K); for (int i = 0; i < A.size(); ++i) A[i] = (i + 1) % 10;
    ocal::buffer<float> B(B_1 * B_2 * K * J); for (int i = 0; i < B.size(); ++i) B[i] = (i + 1) % 10;
    ocal::buffer<float> C(B_1 * B_2 * I * J);

    std::vector<unsigned long long> runtimes;
    for (int i = 0; i < 210; ++i) {
        gpu(cuda::source(kernel_source),
            "batch_matmul_" + std::to_string(B_1) + "_" + std::to_string(B_2) + "_" + std::to_string(I) + "_" + std::to_string(J) + "_" + std::to_string(K),
            std::vector<std::string>({
                                             "--use_fast_math",
                                             "-std=c++11",
                                             "-default-device",
                                             "-DNVRTC_CUB=1"
                                     }))
                (gs, bs)
                (B_1, B_2, I, J, K, write(C), read(A), read(B));
        runtimes.push_back(gpu.last_runtime());
    }
    std::cout << "min runtime: " << *std::min_element(runtimes.begin(), runtimes.end()) << std::endl;

//    auto A_ptr = A.get_host_memory_ptr();
//    auto B_ptr = B.get_host_memory_ptr();
//    auto C_ptr = C.get_host_memory_ptr();
//    for (int b_1 = 0; b_1 < B_1 ; ++b_1) {
//        for (int b_2 = 0; b_2 < B_2 ; ++b_2) {
//            for (int i = 0; i < I ; ++i) {
//                for (int j = 0; j < J ; ++j) {
//                    float acc = 0;
//                    for (int k = 0; k < K ; ++k)
//                        acc += A_ptr[b_1 * B_2 * I * K + b_2 * I * K + i * K + k] *
//                               B_ptr[b_1 * B_2 * K * J + b_2 * K * J + k * J + j];
//                    if (acc != C[b_1 * B_2 * I * J + b_2 * I * J + i * J + j]) {
//                        std::cout << "incorrect: expected " << acc << " got " << C_ptr[b_1 * B_2 * I * J + b_2 * I * J + i * J + j] << std::endl;
//                        exit(EXIT_FAILURE);
//                    }
//                }
//            }
//        }
//    }
//    std::cout << "correct" << std::endl;

}