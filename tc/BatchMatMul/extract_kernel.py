import time
import tensor_comprehensions as tc
import torch
import os
import sys

torch.cuda.device(int(sys.argv[1]))
B_1 = int(sys.argv[2])
B_2 = int(sys.argv[3])
I = int(sys.argv[4])
J = int(sys.argv[5])
K = int(sys.argv[6])

if not os.path.isfile("../../results/gpu/tc/BatchMatMul/cache.options"):
	print("Unable to extract kernel from Tensor Comprehensions. Has it not yet been tuned?")
	sys.exit(1)

lang = """
def batch_matmul(float(B_1, B_2, I, K) A, float(B_1, B_2, K, J) B) -> (C) {
	C(b_1, b_2, i, j) +=! A(b_1, b_2, i, k) * B(b_1, b_2, k, j)
}
"""

batch_matmul = tc.define(lang, cache="../../results/gpu/tc/BatchMatMul/cache", name="batch_matmul")
A, B = torch.randn(size=[B_1, B_2, I, K]).cuda(), torch.randn(size=[B_1, B_2, K, J]).cuda()
tc.SetDebugFlags(dump_cuda=True)
C = batch_matmul(A, B)
tc.SetDebugFlags(dump_cuda=False)
torch.cuda.synchronize()
