import tensor_comprehensions as tc
import torch
import os
import sys

torch.cuda.device(int(sys.argv[1]))
I = int(sys.argv[2])
K = int(sys.argv[3])

if not os.path.isfile("../../results/gpu/tc/BiasAddGrad/cache.options"):
	print("Unable to extract kernel from Tensor Comprehensions. Has it not yet been tuned?")
	sys.exit(1)

lang = """
def bias_add_grad(float(K, I) INP) -> (OUT) {
	OUT(i) +=! INP(k, i)
}
"""

bias_add_grad = tc.define(lang, cache="../../results/gpu/tc/BiasAddGrad/cache", name="bias_add_grad")
INP = torch.randn(size=[K, I]).cuda()
tc.SetDebugFlags(dump_cuda=True)
OUT = bias_add_grad(INP)
tc.SetDebugFlags(dump_cuda=False)
torch.cuda.synchronize()
