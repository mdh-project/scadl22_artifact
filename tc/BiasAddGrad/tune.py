import sys
import time

import torch

import tensor_comprehensions as tc

torch.cuda.device(int(sys.argv[1]))
I = int(sys.argv[2])
K = int(sys.argv[3])

lang = """
def bias_add_grad(float(K, I) INP) -> (OUT) {
	OUT(i) +=! INP(k, i)
}
"""

bias_add_grad = tc.define(lang, cache="../../results/gpu/tc/BiasAddGrad/cache", name="bias_add_grad")
INP = torch.randn(size=[K, I]).cuda()
settings = {
    "threads": 8, "generations": 10, "pop_size": 100, "number_elites": 10
}
torch.cuda.synchronize()
start = time.process_time()
bias_add_grad.autotune(INP, cache="../../results/gpu/tc/BiasAddGrad/cache",  **settings)
torch.cuda.synchronize()
end = time.process_time()
