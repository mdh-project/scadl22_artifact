//
// Copyright (c) 2020 Richard Schulze. All rights reserved.
//

#include <vector>
#include "../OCAL/ocal_cuda.hpp"

int main(int argc, const char **argv) {
    if (argc <= 4) {
        std::cout << "usage: kernel_file device_id I K" << std::endl;
        exit(EXIT_FAILURE);
    }
    std::string kernel_file_name = argv[1];
    int device_id = std::atoi(argv[2]);
    int I = std::atoi(argv[3]);
    int K = std::atoi(argv[4]);

    // read kernel source and grid/block size
    std::string kernel_source;
    dim3 gs, bs;
    std::ifstream kernel_file(kernel_file_name);
    std::string line;
    int part = 0; //  0 - log output, 1 - kernel, 2 - grid and block size
    while (std::getline(kernel_file, line)) {
        if (line.length() >= 15 && line.compare(line.length() - 15, 15, "generatedCuda: ") == 0) {
            part = 1;
            continue;
        } else if (line.length() >= 6 && line.compare(0, 6, "grid: ") == 0) {
            part = 2;
        }

        if (part == 1) {
            kernel_source += line;
            kernel_source += '\n';
        } else if (part == 2) {
            std::regex rgx(R"(grid: CudaDim\((\d+), (\d+), (\d+)\) \@0x[a-f0-9]{12} block: CudaDim\((\d+), (\d+), (\d+)\) \@0x[a-f0-9]{12})");
            std::smatch match;
            if (std::regex_match(line, match, rgx)) {
                for (int i = 0; i < 6; ++i) {
                    std::stringstream ss(match[1 + i].str());
                    int tmp;
                    if (!(ss >> tmp)) {
                        std::cout << "unable to parse grid or block size: " << ss.str() << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    (i < 3 ? gs : bs).set(i % 3, tmp);
                }
            } else {
                std::cout << "unable to match grid and block size:" << std::endl;
                std::cout << line << std::endl;
                exit(EXIT_FAILURE);
            }
        }

        if (part == 2) {
            part = 0;
        }
    }

    ocal::device<CUDA> gpu(device_id);

    ocal::buffer<float> INP(K * I); for (int i = 0; i < INP.size(); ++i) INP[i] = (i + 1) % 10;
    ocal::buffer<float> OUT(I);

    std::vector<unsigned long long> runtimes;
    for (int i = 0; i < 210; ++i) {
        gpu(cuda::source(kernel_source),
            "bias_add_grad_" + std::to_string(I) + "_" + std::to_string(K),
            std::vector<std::string>({
                                             "--use_fast_math",
                                             "-std=c++11",
                                             "-default-device",
                                             "-DNVRTC_CUB=1"
                                     }))
                (gs, bs)
                (I, K, write(OUT), read(INP));
        runtimes.push_back(gpu.last_runtime());
    }
    std::cout << "min runtime: " << *std::min_element(runtimes.begin(), runtimes.end()) << std::endl;

//    auto INP_ptr = INP.get_host_memory_ptr();
//    auto OUT_ptr = OUT.get_host_memory_ptr();
//    for (int i = 0; i < I ; ++i) {
//        float acc = 0;
//        for (int k = 0; k < K ; ++k)
//            acc += INP_ptr[k * I + i];
//        if (acc != OUT_ptr[i]) {
//            std::cout << "incorrect: expected " << acc << " got " << OUT_ptr[i] << std::endl;
//            exit(EXIT_FAILURE);
//        }
//    }
//    std::cout << "correct" << std::endl;

}