#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.sh\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    echo "Please execute \"source environment.sh\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

cd $ARTIFACT_ROOT || { echo "Please execute \"source environment.sh\" in the root dir of the artifact (the directory containing the scripts folder) first."; exit 1; }

if [[ -n "$ENABLE_GPU" && "$ENABLE_GPU" -eq 1 ]]; then
    # MDH (BiasAddGrad)
    (
      echo "Benchmarking MDH BiasAddGrad for GPU"
      cd $ARTIFACT_ROOT/mdh/gpu/BiasAddGrad/build &&
      ./bench_md_hom_strided_v3_bias_add_grad --path $ARTIFACT_ROOT/results/gpu/mdh/BiasAddGrad -d 0 --input-size-l-1 768 --input-size-r-1 6144 -g ../gold/md_hom/strided_v3/bias_add_grad/768x6144/gold.tsv --kernel-file-1 $ARTIFACT_ROOT/results/gpu/mdh/BiasAddGrad/kernel.cu
    )

    # MDH (BatchMatMul)
    (
      echo "Benchmarking MDH BatchMatMul for GPU"
      cd $ARTIFACT_ROOT/mdh/gpu/BatchMatMul/build &&
      ./bench_md_hom_strided_v3_batch_matmul --path $ARTIFACT_ROOT/results/gpu/mdh/BatchMatMul -d 0 --input-size-l-1 16 --input-size-l-2 12 --input-size-l-3 384 --input-size-l-4 64 --input-size-r-1 384 -g ../gold/md_hom/strided_v3/batch_matmul/16x12x384x64x384/gold.tsv --kernel-file-1 $ARTIFACT_ROOT/results/gpu/mdh/BatchMatMul/kernel.cu
    )

    # MDH (subgraph)
    (
      echo "Benchmarking MDH subgraph for GPU"
      cd $ARTIFACT_ROOT/mdh/gpu/subgraph/build &&
      ./bench_md_hom_strided_v3_subgraph2 --path $ARTIFACT_ROOT/results/gpu/mdh/subgraph -d 0 --input-size-l-1 16 --input-size-l-2 1 --input-size-l-3 384 --input-size-l-4 384 -g ../gold/md_hom/strided_v3/subgraph2/16x1x384x384/gold.tsv --kernel-file-1 $ARTIFACT_ROOT/results/gpu/mdh/subgraph/kernel.cu
    )

    # TC (BiasAddGrad)
    (
      echo "Benchmarking TC BiasAddGrad for GPU"
      cd $ARTIFACT_ROOT/tc/BiasAddGrad &&
      ./bench $ARTIFACT_ROOT/results/gpu/tc/BiasAddGrad/kernel_info.txt 0 768 6144 | grep -oP "(?<=min runtime: )[0-9]+" > $ARTIFACT_ROOT/results/gpu/tc/BiasAddGrad/runtimes_min
    )

    # TC (BatchMatMul)
    (
      echo "Benchmarking TC BatchMatMul for GPU"
      cd $ARTIFACT_ROOT/tc/BatchMatMul &&
      ./bench $ARTIFACT_ROOT/results/gpu/tc/BatchMatMul/kernel_info.txt 0 16 12 384 64 384 | grep -oP "(?<=min runtime: )[0-9]+" > $ARTIFACT_ROOT/results/gpu/tc/BatchMatMul/runtimes_min
    )

    # TC (subgraph)
    (
      echo "Benchmarking TC subgraph for GPU"
      cd $ARTIFACT_ROOT/tc/subgraph &&
      ./bench $ARTIFACT_ROOT/results/gpu/tc/subgraph/kernel_info.txt 0 16 1 384 384 | grep -oP "(?<=min runtime: )[0-9]+" > $ARTIFACT_ROOT/results/gpu/tc/subgraph/runtimes_min
    )

    # TVM (BiasAddGrad)
    (
      echo "Benchmarking TVM BiasAddGrad for GPU"
      cd $ARTIFACT_ROOT/tvm &&
      python BiasAddGrad.py bench 768 6144 gpu
    )

    # TVM (BatchMatMul)
    (
      echo "Benchmarking TVM BatchMatMul for GPU"
      cd $ARTIFACT_ROOT/tvm &&
      python BatchMatMul.py bench 16 12 384 64 384 gpu
    )

    # TVM (subgraph)
    (
      echo "Benchmarking TVM subgraph for GPU"
      cd $ARTIFACT_ROOT/tvm &&
      python subgraph.py bench 16 1 384 384 gpu
    )

    # TF (BiasAddGrad)
    (
      echo "Benchmarking TF BiasAddGrad for GPU"
      mkdir -p $ARTIFACT_ROOT/results/gpu/tf/BiasAddGrad
      rm -rf $ARTIFACT_ROOT/results/gpu/tf/BiasAddGrad/profiling_data &> /dev/null
      cd $ARTIFACT_ROOT/tf &&
      python BiasAddGrad.py 768 6144 gpu
    )

    # TF (BatchMatMul)
    (
      echo "Benchmarking TF BatchMatMul for GPU"
      mkdir -p $ARTIFACT_ROOT/results/gpu/tf/BatchMatMul
      rm -rf $ARTIFACT_ROOT/results/gpu/tf/BatchMatMul/profiling_data &> /dev/null
      cd $ARTIFACT_ROOT/tf &&
      python BatchMatMul.py 16 12 384 64 384 gpu
    )

    # TF (subgraph)
    (
      echo "Benchmarking TF subgraph for GPU"
      mkdir -p $ARTIFACT_ROOT/results/gpu/tf/subgraph
      rm -rf $ARTIFACT_ROOT/results/gpu/tf/subgraph/profiling_data &> /dev/null
      cd $ARTIFACT_ROOT/tf &&
      python subgraph.py 16 1 384 384 gpu
    )
fi


if [[ -n "$ENABLE_CPU" && "$ENABLE_CPU" -eq 1 ]]; then
    # MDH (BiasAddGrad)
    (
      echo "Benchmarking MDH BiasAddGrad for CPU"
      cd $ARTIFACT_ROOT/mdh/cpu/BiasAddGrad/build &&
      ./bench_md_hom_strided_v3_bias_add_grad --path $ARTIFACT_ROOT/results/cpu/mdh/BiasAddGrad -p 0 -d 0 --input-size-l-1 768 --input-size-r-1 6144 -g ../gold/md_hom/strided_v3/bias_add_grad/768x6144/gold.tsv --kernel-file-1 $ARTIFACT_ROOT/results/cpu/mdh/BiasAddGrad/kernel.cl
    )

    # MDH (BatchMatMul)
    (
      echo "Benchmarking MDH BatchMatMul for CPU"
      cd $ARTIFACT_ROOT/mdh/cpu/BatchMatMul/build &&
      ./bench_md_hom_strided_v3_batch_matmul --path $ARTIFACT_ROOT/results/cpu/mdh/BatchMatMul -p 0 -d 0 --input-size-l-1 16 --input-size-l-2 12 --input-size-l-3 384 --input-size-l-4 64 --input-size-r-1 384 -g ../gold/md_hom/strided_v3/batch_matmul/16x12x384x64x384/gold.tsv --kernel-file-1 $ARTIFACT_ROOT/results/cpu/mdh/BatchMatMul/kernel.cl
    )

    # MDH (subgraph)
    (
      echo "Benchmarking MDH subgraph for CPU"
      cd $ARTIFACT_ROOT/mdh/cpu/subgraph/build &&
      ./bench_md_hom_strided_v3_subgraph2 --path $ARTIFACT_ROOT/results/cpu/mdh/subgraph -p 0 -d 0 --input-size-l-1 16 --input-size-l-2 1 --input-size-l-3 384 --input-size-l-4 384 -g ../gold/md_hom/strided_v3/subgraph2/16x1x384x384/gold.tsv --kernel-file-1 $ARTIFACT_ROOT/results/cpu/mdh/subgraph/kernel.cl
    )

    # TVM (BiasAddGrad)
    (
      echo "Benchmarking TVM BiasAddGrad for CPU"
      cd $ARTIFACT_ROOT/tvm &&
      python BiasAddGrad.py bench 768 6144 cpu
    )

    # TVM (BatchMatMul)
    (
      echo "Benchmarking TVM BatchMatMul for CPU"
      cd $ARTIFACT_ROOT/tvm &&
      python BatchMatMul.py bench 16 12 384 64 384 cpu
    )

    # TVM (subgraph)
    (
      echo "Benchmarking TVM subgraph for CPU"
      cd $ARTIFACT_ROOT/tvm &&
      python subgraph.py bench 16 1 384 384 cpu
    )

    # TF (BiasAddGrad)
    (
      echo "Benchmarking TF BiasAddGrad for CPU"
      mkdir -p $ARTIFACT_ROOT/results/cpu/tf/BiasAddGrad
      rm -rf $ARTIFACT_ROOT/results/cpu/tf/BiasAddGrad/profiling_data &> /dev/null
      cd $ARTIFACT_ROOT/tf &&
      python BiasAddGrad.py 768 6144 cpu
    )

    # TF (BatchMatMul)
    (
      echo "Benchmarking TF BatchMatMul for CPU"
      mkdir -p $ARTIFACT_ROOT/results/cpu/tf/BatchMatMul
      rm -rf $ARTIFACT_ROOT/results/cpu/tf/BatchMatMul/profiling_data &> /dev/null
      cd $ARTIFACT_ROOT/tf &&
      python BatchMatMul.py 16 12 384 64 384 cpu
    )

    # TF (subgraph)
    (
      echo "Benchmarking TF subgraph for CPU"
      mkdir -p $ARTIFACT_ROOT/results/cpu/tf/subgraph
      rm -rf $ARTIFACT_ROOT/results/cpu/tf/subgraph/profiling_data &> /dev/null
      cd $ARTIFACT_ROOT/tf &&
      python subgraph.py 16 1 384 384 cpu
    )
fi


# print results
python collect_results.py | tee results/summary.txt