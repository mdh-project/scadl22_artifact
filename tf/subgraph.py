import os, sys
import json
import tensorflow as tf


N_1 = int(sys.argv[1])
N_2 = int(sys.argv[2])
N_3 = int(sys.argv[3])
N_4 = int(sys.argv[4])
device = sys.argv[5]
if device not in ['cpu', 'gpu']:
    raise ValueError('unknown device "' + device + '"')


class SubgraphModule(tf.Module):
    @tf.function
    def __call__(self, C_1, I_1, I_2, C_2):
        mul = tf.multiply(I_1, I_2)
        exp_dims = tf.expand_dims(mul, axis=1)
        sub = tf.subtract(C_1, exp_dims)
        O = tf.multiply(sub, C_2)
        return O
subgraph_module = SubgraphModule()


with tf.device(f'/{device}:0'):
    C_1 = tf.constant(value=1.0, shape=(), dtype=tf.float32, name='C_1')
    I_1 = tf.constant(value=[(i % 10) + 1 for i in range(0, N_1 * N_3 * 1)], shape=(N_1, N_3, 1), dtype=tf.float32, name='I_1')
    I_2 = tf.constant(value=[(i % 10) + 1 for i in range(0, N_1 * 1 * N_4)], shape=(N_1, 1, N_4), dtype=tf.float32, name='I_2')
    C_2 = tf.constant(value=-10000.0, shape=(), dtype=tf.float32, name='C_2')

    # trace function in SubgraphModule
    subgraph_module(C_1, I_1, I_2, C_2)

    with tf.profiler.experimental.Profile(f'../results/{device}/tf/subgraph/profiling_data') as profiler:
        # warm ups
        for i in range(0, 10):
            subgraph_module(C_1, I_1, I_2, C_2)
        # evaluations
        for i in range(0, 200):
            subgraph_module(C_1, I_1, I_2, C_2)


# read min runtime from trace
timestamp = next(os.walk(f'../results/{device}/tf/subgraph/profiling_data/plugins/profile/'))[1][0]
trace_file_name = [f for f in os.listdir('../results/{}/tf/subgraph/profiling_data/plugins/profile/{}'.format(device, timestamp)) if f.endswith('.trace.json.gz')][0][:-3]
os.system('gunzip ../results/{}/tf/subgraph/profiling_data/plugins/profile/{}/{}.gz'.format(device, timestamp, trace_file_name))
min_times = dict()
device_tid = None
with open('../results/{}/tf/subgraph/profiling_data/plugins/profile/{}/{}'.format(device, timestamp, trace_file_name), 'r') as trace_file:
    trace_json = json.load(trace_file)

    # get execution times and device tid
    for event in trace_json['traceEvents']:
        if 'ph' not in event:
            continue

        if event['ph'] == 'M' and event['name'] == 'thread_name' and 'Compute' in event['args']['name']:
            device_tid = event['tid']
        if event['ph'] == 'X' and (device_tid is None or event['tid'] == device_tid):
            if event['tid'] not in min_times.keys():
                min_times[event['tid']] = dict()
            if event['name'] not in min_times[event['tid']].keys():
                min_times[event['tid']][event['name']] = []
            min_times[event['tid']][event['name']].append(int(event['dur'] * 1000))

if device_tid is None:
    print('no kernel execution found in device trace')
else:
    min_runtime = 0
    for name in min_times[device_tid].keys():
        if len(min_times[device_tid][name]) > 1:
            min_runtime += min(min_times[device_tid][name])

    with open(f'../results/{device}/tf/subgraph/runtimes_min', 'w') as runtime_file:
        runtime_file.write(str(min_runtime))
