import os, sys
import json
import tensorflow as tf

I = int(sys.argv[1])
K = int(sys.argv[2])
device = sys.argv[3]
if device not in ['cpu', 'gpu']:
    raise ValueError('unknown device "' + device + '"')


with tf.device(f'/{device}:0'):
    IN = tf.constant(value=[(i % 10) + 1 for i in range(0, K*I)], shape=(K, I), dtype=tf.float32, name='IN')
    with tf.profiler.experimental.Profile(f'../results/{device}/tf/BiasAddGrad/profiling_data') as profiler:
        # warm ups
        for i in range(0, 10):
            tf.raw_ops.BiasAddGrad(out_backprop=IN)
        # evaluations
        for i in range(0, 200):
            tf.raw_ops.BiasAddGrad(out_backprop=IN)

# read min runtime from trace
timestamp = next(os.walk(f'../results/{device}/tf/BiasAddGrad/profiling_data/plugins/profile/'))[1][0]
trace_file_name = [f for f in os.listdir('../results/{}/tf/BiasAddGrad/profiling_data/plugins/profile/{}'.format(device, timestamp)) if f.endswith('.trace.json.gz')][0][:-3]
os.system('gunzip ../results/{}/tf/BiasAddGrad/profiling_data/plugins/profile/{}/{}.gz'.format(device, timestamp, trace_file_name))
min_times = dict()
device_tid = None
with open('../results/{}/tf/BiasAddGrad/profiling_data/plugins/profile/{}/{}'.format(device, timestamp, trace_file_name), 'r') as trace_file:
    trace_json = json.load(trace_file)

    # get execution times and device tid
    for event in trace_json['traceEvents']:
        if 'ph' not in event:
            continue

        if event['ph'] == 'M' and event['name'] == 'thread_name' and (
                (device == 'gpu' and 'Compute' in event['args']['name']) or
                (device == 'cpu' and event['args']['name'] == 'python')
        ):
            device_tid = event['tid']
        if event['ph'] == 'X' and (device_tid is None or event['tid'] == device_tid):
            if event['tid'] not in min_times.keys():
                min_times[event['tid']] = dict()
            if event['name'] not in min_times[event['tid']].keys():
                min_times[event['tid']][event['name']] = []
            min_times[event['tid']][event['name']].append(int(event['dur'] * 1000))

min_runtime = 0
if device == 'cpu':
    for name in min_times[device_tid].keys():
        if name == 'BiasAddGrad' and len(min_times[device_tid][name]) > 1:
            min_runtime += min(min_times[device_tid][name])
    if min_runtime == 0:
        print('no kernel execution found in device trace')
        sys.exit(0)
else:
    if device_tid is None:
        print('no kernel execution found in device trace')
        sys.exit(0)
    else:
        for name in min_times[device_tid].keys():
            if len(min_times[device_tid][name]) > 1:
                min_runtime += min(min_times[device_tid][name])

with open(f'../results/{device}/tf/BiasAddGrad/runtimes_min', 'w') as runtime_file:
    runtime_file.write(str(min_runtime))
