#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.sh\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.sh\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

cd $ARTIFACT_ROOT || { echo "Please execute \"source environment.sh\" in the root dir of the artifact (the directory containing the scripts folder) first."; exit 1; }
{
  # fix permissions in case files have been uploaded using sftp
  chmod u+x $ARTIFACT_ROOT/use_defaults.sh
  chmod u+x $ARTIFACT_ROOT/tune.sh
  chmod u+x $ARTIFACT_ROOT/bench.sh

  if [[ -n "$ENABLE_GPU" && "$ENABLE_GPU" -eq 1 ]]; then
      # MDH (BiasAddGrad)
      (
        mkdir -p $ARTIFACT_ROOT/mdh/gpu/BiasAddGrad/build &> /dev/null
        cd $ARTIFACT_ROOT/mdh/gpu/BiasAddGrad/build &&
        cmake -DCMAKE_MODULE_PATH=$ARTIFACT_ROOT/mdh/gpu/BiasAddGrad/cmake/Modules ${@:1} .. &&
        make -j `nproc`
      )

      # MDH (BatchMatMul)
      (
        mkdir -p $ARTIFACT_ROOT/mdh/gpu/BatchMatMul/build &> /dev/null
        cd $ARTIFACT_ROOT/mdh/gpu/BatchMatMul/build &&
        cmake -DCMAKE_MODULE_PATH=$ARTIFACT_ROOT/mdh/gpu/BatchMatMul/cmake/Modules ${@:1} .. &&
        make -j `nproc`
      )

      # MDH (subgraph)
      (
        mkdir -p $ARTIFACT_ROOT/mdh/gpu/subgraph/build &> /dev/null
        cd $ARTIFACT_ROOT/mdh/gpu/subgraph/build &&
        cmake -DCMAKE_MODULE_PATH=$ARTIFACT_ROOT/mdh/gpu/subgraph/cmake/Modules ${@:1} .. &&
        make -j `nproc`
      )

      # TC benchmark executable (BiasAddGrad)
      (
        cd $ARTIFACT_ROOT/tc/BiasAddGrad &&
        g++ -lcudart -lnvrtc -lcuda bench.cpp -o bench
      )

      # TC benchmark executable (BatchMatMul)
      (
        cd $ARTIFACT_ROOT/tc/BatchMatMul &&
        g++ -lcudart -lnvrtc -lcuda bench.cpp -o bench
      )

      # TC benchmark executable (subgraph)
      (
        cd $ARTIFACT_ROOT/tc/subgraph &&
        g++ -lcudart -lnvrtc -lcuda bench.cpp -o bench
      )
  fi

  if [[ -n "$ENABLE_CPU" && "$ENABLE_CPU" -eq 1 ]]; then
      # MDH (BiasAddGrad)
      (
        mkdir -p $ARTIFACT_ROOT/mdh/cpu/BiasAddGrad/build &> /dev/null
        cd $ARTIFACT_ROOT/mdh/cpu/BiasAddGrad/build &&
        cmake -DCMAKE_MODULE_PATH=$ARTIFACT_ROOT/mdh/cpu/BiasAddGrad/cmake/Modules ${@:1} .. &&
        make -j `nproc`
      )

      # MDH (BatchMatMul)
      (
        mkdir -p $ARTIFACT_ROOT/mdh/cpu/BatchMatMul/build &> /dev/null
        cd $ARTIFACT_ROOT/mdh/cpu/BatchMatMul/build &&
        cmake -DCMAKE_MODULE_PATH=$ARTIFACT_ROOT/mdh/cpu/BatchMatMul/cmake/Modules ${@:1} .. &&
        make -j `nproc`
      )

      # MDH (subgraph)
      (
        mkdir -p $ARTIFACT_ROOT/mdh/cpu/subgraph/build &> /dev/null
        cd $ARTIFACT_ROOT/mdh/cpu/subgraph/build &&
        cmake -DCMAKE_MODULE_PATH=$ARTIFACT_ROOT/mdh/cpu/subgraph/cmake/Modules ${@:1} .. &&
        make -j `nproc`
      )
  fi

  printf "\n\nBuild successful!\n"
} || {
  printf "\n\nBuild failed!\n"
  exit 1
}