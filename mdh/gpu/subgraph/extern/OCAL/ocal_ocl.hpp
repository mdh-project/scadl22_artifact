//
//  ocal.hpp
//  
//
//  Created by Ari Rasch on 17/10/16.
//
//

#ifndef ocal_hpp
#define ocal_hpp

//#include <vector>
//#include <array>
//#include <string>
//#include <type_traits>
//#include <unordered_map>
//#include <iostream>
//#include <regex>
//#include <sstream>
//#include <cmath>
//#include <numeric>
//
//#include "assert.h"

#include <string>

// set ocl part of OCAL as activated
#define OCAL_OCL


#include <cstdlib>
const std::string PATH_TO_OCAL_KERNEL_DB           = (std::getenv( "PATH_TO_OCAL_KERNEL_DB" )          != NULL) ? std::getenv( "PATH_TO_OCAL_KERNEL_DB"          ) : ".";
const std::string PATH_TO_OCAL_AUTO_TUNING_SCRIPTS = (std::getenv( "PATH_TO_OCAL_AUTO_TUNING_SCRIPTS") != NULL) ? std::getenv( "PATH_TO_OCAL_AUTO_TUNING_SCRIPTS") : ".";
const std::string PATH_TO_OCAL_KERNEL_CACHE        = (std::getenv( "PATH_TO_OCAL_KERNEL_CACHE")        != NULL) ? std::getenv( "PATH_TO_OCAL_KERNEL_CACHE"       ) : ".";

#include "include/core/ocl/ocl.hpp"

// ocal::ocal header
#include "include/ocl_only/ocal_buffer.hpp"
#include "include/ocl_only/ocal_device.hpp"


using namespace ocal::core;

using ocal::common::scalar;
using ocal::common::read;
using ocal::common::write;
using ocal::common::read_write;
using ocal::common::static_parameters; 

using OCL = ocal::core::ocl::device;

using ocal::core::ocl::nd_range;
using ocal::core::cuda::dim3;

// allowing using ocal::kernel instead of ocl::kernel
namespace ocal
{
  using kernel = core::ocl::kernel;
  
  namespace ocl
  {
   
    template< typename T >
    size_t NUM_DEVICES( const T& arg )
    {
      return ::ocal::core::ocl::NUM_DEVICES( arg );
    }
    
  }
}


// syntactic sugar
template< typename... T_lhs, typename... T_rhs >
auto device_info( std::pair<T_lhs,T_rhs>... vals )
{
  return ocal::core::common::info( vals... );
}

template< typename... T_lhs, typename... T_rhs >
auto platform_info( std::pair<T_lhs,T_rhs>... vals )

{
  return ocal::core::common::info( vals... );
}


template< typename... Ts  >
auto pair( Ts... vals )
{
  return std::make_pair( vals... );
}

#endif /* ocal_hpp */
