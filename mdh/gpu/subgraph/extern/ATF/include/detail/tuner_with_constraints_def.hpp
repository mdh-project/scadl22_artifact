//
//  tuner_with_constraints_def.hpp
//  new_atf_lib
//
//  Created by Ari Rasch on 21/03/2017.
//  Copyright © 2017 Ari Rasch. All rights reserved.
//

#ifndef tuner_with_constraints_def_h
#define tuner_with_constraints_def_h

#include <fstream>
#include <limits>
#include <iomanip>
#include <unistd.h>


namespace atf
{


template< typename... TPs >
G_class<TPs...>::G_class( TPs&... tps )
  : _tps( tps... )
{}
  
template< typename... TPs >
auto G_class<TPs...>::tps() const
{
  return _tps;
}


template< typename abort_condition_t >
tuner_with_constraints::tuner_with_constraints( const abort_condition_t& abort_condition, const std::string &logging_file, PROCESS_WRAPPER_TYPE wrapper_type, const std::string &wrapper_command, const is_valid_type &is_valid, const bool& abort_on_error, const progress_callback_t &progress_callback, unsigned long long callback_timeout )
  : _search_space(), _logging_file(logging_file), _wrapper_type(wrapper_type), _wrapper_command(wrapper_command), _socket(0), _port(0), _server(""), _is_wrapper(false), _is_valid(is_valid), _abort_on_error( abort_on_error ), _progress_callback(progress_callback), _callback_timeout(callback_timeout), _number_of_evaluated_configs(), _number_of_invalid_configs(), _evaluations_required_to_find_best_found_result(), _valid_evaluations_required_to_find_best_found_result(), _history(), _generation_time(0)
{
  _abort_condition = std::unique_ptr<abort_condition_t>(new abort_condition_t(abort_condition));

  // when using number of evaluations as abort condition and user specified to stop after 0 evaluations, then set abort
  // condition to nullptr and create new abort condition when starting to tune with search space size as number of
  // evaluations
  cond::evaluations* evaluations_abort = dynamic_cast<cond::evaluations*>(_abort_condition.get());
  if (evaluations_abort != nullptr && evaluations_abort->num_evaluations() == 0)
    _abort_condition = nullptr;

  _history.emplace_back( 0, 0,
                         std::chrono::high_resolution_clock::now(),
                         configuration{},
                         std::numeric_limits<unsigned long long>::max()
                       );

    if (_progress_callback) {
        _progress_callback();
        _last_callback = std::chrono::high_resolution_clock::now();
    }
}

// first application of operator(): IS
template< typename... Ts, typename... range_ts, typename... callables >
tuner_with_constraints& tuner_with_constraints::operator()( tp_t<Ts,range_ts,callables>&... tps )
{
  return this->operator()( G(tps...) );
}

// first application of operator(): IS (required due to ambiguity)
template< typename T, typename range_t, typename callable >
tuner_with_constraints& tuner_with_constraints::operator()( tp_t<T,range_t,callable>& tp )
{
  return this->operator()( G(tp) );
}


// second case
template< typename... Ts, typename... G_CLASSES >
tuner_with_constraints& tuner_with_constraints::operator()( G_class<Ts...> G_class, G_CLASSES... G_classes )
{
  auto start = std::chrono::high_resolution_clock::now();

  const size_t num_trees = sizeof...(G_CLASSES) + 1;
  _search_space.append_new_trees( num_trees );

  insert_tp_names_in_search_space( G_class, G_classes... );

  auto& res = generate_config_trees<num_trees>( G_class, G_classes... );

  auto end = std::chrono::high_resolution_clock::now();
  _generation_time += std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();

  return res;
}


template< typename callable >
configuration tuner_with_constraints::operator()( const callable& program ) // func must take config_t and return a value for which "<" is defined.
{
  unsigned long long program_runtime = std::numeric_limits<unsigned long long>::max();
  unsigned long long compile_time = 0;
  int error_code = 0;

  if (_is_wrapper) {
    auto config = _search_space[0];
    compile_time = 0;
    error_code = 0;
    try
    {
      if (_is_valid && !_is_valid(config)) throw std::runtime_error("configuration not valid accourding to _is_valid");
      program_runtime = program( config, &compile_time, &error_code );
    }
    catch( ... )
    {
//      abort();
    }

    // send data
    bool send_compile_time = true;
    bool send_error_code = true;
    bool send_runtime = true;

    // notify server what values to receive
    uint16_t send_mask = 0;
    if (send_compile_time) send_mask |= 1;
    if (send_error_code) send_mask |= 2;
    if (send_runtime) send_mask |= 4;
    send_mask = htons(send_mask);
    if (send(_socket, &send_mask, sizeof(send_mask), 0) < 0) {
      std::cerr << "error while sending send_mask: " << strerror(errno) << std::endl;
      exit(1);
    }

    if (send_compile_time) {
      // send compile time
      __uint64_t compile_time_64 = htobe64(compile_time);
      if (send(_socket, &compile_time_64, sizeof(compile_time_64), 0) < 0) {
        std::cerr << "error while sending compile_time: " << strerror(errno) << std::endl;
        exit(1);
      }
    }

    if (send_error_code) {
      uint32_t error_code_32 = htonl(static_cast<uint32_t>(std::abs(error_code)));
      if (send(_socket, &error_code_32, sizeof(error_code_32), 0) < 0) {
        std::cerr << "error while sending absolute error code: " << strerror(errno) << std::endl;
        exit(1);
      }
      uint32_t error_sign_32 = error_code < 0 ? htonl(1) : htonl(0);
      if (send(_socket, &error_sign_32, sizeof(error_sign_32), 0) < 0) {
        std::cerr << "error while sending error code sign: " << strerror(errno) << std::endl;
        exit(1);
      }
    }

    if (send_runtime) {
      // send runtime
      __uint64_t runtime_64 = htobe64(program_runtime);
      if (send(_socket, &runtime_64, sizeof(runtime_64), 0) < 0) {
        std::cerr << "error while sending runtime: " << strerror(errno) << std::endl;
        exit(1);
      }
    }

    // close connection
    close(_socket);
    return config;
  }

  std::cout << "\nsearch space size: " << _search_space.num_configs() << std::endl << std::endl;

  // if no abort condition is specified then iterate over the whole search space.
  if( _abort_condition == NULL )
    _abort_condition = std::unique_ptr<cond::abort>( new cond::evaluations( _search_space.num_configs() ) );

  // execute first callback
  if (_progress_callback) {
    _progress_callback();
    _last_callback = std::chrono::high_resolution_clock::now();
  }

  // open file for verbose logging
  std::ofstream csv_file;
  bool write_header = true;
  if (!_logging_file.empty()) {
    csv_file.open(_logging_file, std::ofstream::out | std::ofstream::trunc);
  }

  auto start = std::chrono::system_clock::now();

  initialize( _search_space );

  while( !_abort_condition->stop( *this ) )
  {
    auto config = get_next_config();

    ++_number_of_evaluated_configs;
    compile_time = 0;
    error_code = 0;
    auto collection_start = std::chrono::system_clock::now();
    try
    {
      if (_is_valid && !_is_valid(config)) throw std::runtime_error("configuration not valid accourding to _is_valid");
      if (_wrapper_type == NONE) {
        program_runtime = program( config, &compile_time, &error_code );
      } else {
        program_runtime = execute_wrapper( config, &compile_time, &error_code );
      }
    }
    catch( ... )
    {
      ++_number_of_invalid_configs;

      if( _abort_on_error )
        abort();
      else
        program_runtime = std::numeric_limits<unsigned long long>::max();
    }
    auto collection_end = std::chrono::system_clock::now();

    auto current_best_result = std::get<4>( _history.back() );
    if( program_runtime < current_best_result  )
    {
      _evaluations_required_to_find_best_found_result = this->number_of_evaluated_configs();
      _valid_evaluations_required_to_find_best_found_result = this->number_of_valid_evaluated_configs();
      _history.emplace_back( this->number_of_evaluated_configs(),
                             this->number_of_valid_evaluated_configs(),
                             std::chrono::high_resolution_clock::now(),
                             config,
                             program_runtime
                           );
    }

    report_result( program_runtime ); // TODO: refac "program_runtime" -> "program_cost"

    if (!_logging_file.empty()) {
      if (write_header) {
        csv_file << "collection_start;collection_end;valid;error_code;compile_time;run_time";
        for (const auto &tp : config) {
          if (tp.first.find("NOT_USED") == std::string::npos)
            csv_file << ";" << tp.first;
        }
        write_header = false;
      }
      auto collection_start_time_t = std::chrono::system_clock::to_time_t(collection_start);
      auto start_ms = std::chrono::duration_cast<std::chrono::milliseconds>(collection_start.time_since_epoch()) % 1000;
      csv_file << std::endl
               << std::put_time(std::localtime(&collection_start_time_t), "%F %T");
      csv_file << '.' << std::setfill('0') << std::setw(3) << start_ms.count();
      auto collection_end_time_t = std::chrono::system_clock::to_time_t(collection_end);
      auto end_ms = std::chrono::duration_cast<std::chrono::milliseconds>(collection_end.time_since_epoch()) % 1000;
      csv_file << ";" << std::put_time(std::localtime(&collection_end_time_t), "%F %T");
      csv_file << '.' << std::setfill('0') << std::setw(3) << end_ms.count();
      csv_file << ";" << (program_runtime < std::numeric_limits<unsigned long long>::max())
               << ";" << error_code
               << ";" << compile_time
               << ";" << program_runtime;
      for (const auto &tp : config) {
        if (tp.first.find("NOT_USED") == std::string::npos)
          csv_file << ";" << tp.second.value();
      }
    }

    std::cout << std::endl << "evaluated configs: " << this->number_of_evaluated_configs() << " , valid configs: " << this->number_of_valid_evaluated_configs() << " , program cost: " << program_runtime << " , current best result: " << this->best_measured_result() << std::endl << std::endl;

    if (_progress_callback &&
        (program_runtime < current_best_result || std::chrono::duration_cast<std::chrono::seconds>(std::chrono::high_resolution_clock::now() - _last_callback).count() >= _callback_timeout)) {
      // if result improved or callback has not been called in a while -> call callback
      _progress_callback();
      _last_callback = std::chrono::high_resolution_clock::now();
    }
  }

  finalize();

  if (!_logging_file.empty()) {
    csv_file.close();
  }

  std::cout << "\nnumber of evaluated configs: " << this->number_of_evaluated_configs() << " , number of valid configs: " << this->number_of_valid_evaluated_configs() << " , number of invalid configs: " << _number_of_invalid_configs << " , evaluations required to find best found result: " << _evaluations_required_to_find_best_found_result << " , valid evaluations required to find best found result: " << _valid_evaluations_required_to_find_best_found_result << std::endl;

  auto end = std::chrono::system_clock::now();
  auto runtime_in_sec = std::chrono::duration_cast<std::chrono::seconds>( end - start ).count();
  std::cout << std::endl << "total runtime for tuning = " << runtime_in_sec << "sec" << std::endl;


  // output
  auto        best_config = best_configuration();
  std::string seperator = "";
  std::cout << "\nbest configuration: [ ";
  for( const auto& tp : best_config )
  {
    auto tp_value = tp.second;
    std::cout << seperator << tp_value.name() << " = " << tp_value.value();
    seperator = " ; ";
  }
  std::cout << " ] with cost: " << this->best_measured_result() << std::endl << std::endl;

  // store best found result in file
  std::stringstream tp_names;
  std::stringstream tp_vals;
  for( auto& tp : best_config )
  {
    tp_names << tp.first  << ";";
    tp_vals  << tp.second << ";";
  }

//  std::ofstream outfile;
//  outfile.open("results.csv", std::ofstream::app ); // TODO: "/Users/arirasch/results.csv"
//  outfile << "best_measured_result" << ";" << "number_of_valid_evaluated_configs" << ";" << "evaluations_required_to_find_best_found_result" << ";" << "valid_evaluations_required_to_find_best_found_result" ";" << tp_names.str() << std::endl;
//  outfile << this->best_measured_result() << ";" << this->number_of_valid_evaluated_configs() << ";" << _evaluations_required_to_find_best_found_result << ";" << _valid_evaluations_required_to_find_best_found_result << ";" << tp_vals.str() << std::endl;
//  outfile.close();

  // make final call to callback
  if (_progress_callback) {
    _progress_callback();
    _last_callback = std::chrono::high_resolution_clock::now();
  }

  auto best_configuration = std::get<3>( _history.back() );
  return best_configuration;
}
template< typename callable >
configuration tuner_with_constraints::operator()( callable& program ) // func must take config_t and return a value for which "<" is defined.
{
  unsigned long long program_runtime = std::numeric_limits<unsigned long long>::max();
  unsigned long long compile_time = 0;
  int error_code = 0;

  if (_is_wrapper) {
    auto config = _search_space[0];
    compile_time = 0;
    error_code = 0;
    try
    {
      if (_is_valid && !_is_valid(config)) throw std::runtime_error("configuration not valid accourding to _is_valid");
      program_runtime = program( config, &compile_time, &error_code );
    }
    catch( ... )
    {
//      abort();
    }

    // send data
    bool send_compile_time = true;
    bool send_error_code = true;
    bool send_runtime = true;

    // notify server what values to receive
    uint16_t send_mask = 0;
    if (send_compile_time) send_mask |= 1;
    if (send_error_code) send_mask |= 2;
    if (send_runtime) send_mask |= 4;
    send_mask = htons(send_mask);
    if (send(_socket, &send_mask, sizeof(send_mask), 0) < 0) {
      std::cerr << "error while sending send_mask: " << strerror(errno) << std::endl;
      exit(1);
    }

    if (send_compile_time) {
      // send compile time
      __uint64_t compile_time_64 = htobe64(compile_time);
      if (send(_socket, &compile_time_64, sizeof(compile_time_64), 0) < 0) {
        std::cerr << "error while sending compile_time: " << strerror(errno) << std::endl;
        exit(1);
      }
    }

    if (send_error_code) {
      uint32_t error_code_32 = htonl(static_cast<uint32_t>(std::abs(error_code)));
      if (send(_socket, &error_code_32, sizeof(error_code_32), 0) < 0) {
        std::cerr << "error while sending absolute error code: " << strerror(errno) << std::endl;
        exit(1);
      }
      uint32_t error_sign_32 = error_code < 0 ? htonl(1) : htonl(0);
      if (send(_socket, &error_sign_32, sizeof(error_sign_32), 0) < 0) {
        std::cerr << "error while sending error code sign: " << strerror(errno) << std::endl;
        exit(1);
      }
    }

    if (send_runtime) {
      // send runtime
      __uint64_t runtime_64 = htobe64(program_runtime);
      if (send(_socket, &runtime_64, sizeof(runtime_64), 0) < 0) {
        std::cerr << "error while sending runtime: " << strerror(errno) << std::endl;
        exit(1);
      }
    }

    // close connection
    close(_socket);
    return config;
  }

  std::cout << "\nsearch space size: " << _search_space.num_configs() << std::endl << std::endl;

  // if no abort condition is specified then iterate over the whole search space.
  if( _abort_condition == NULL )
    _abort_condition = std::unique_ptr<cond::abort>( new cond::evaluations( _search_space.num_configs() ) );

  // execute first callback
  if (_progress_callback) {
      _progress_callback();
      _last_callback = std::chrono::high_resolution_clock::now();
  }

  // open file for verbose logging
  std::ofstream csv_file;
  bool write_header = true;
  if (!_logging_file.empty()) {
    csv_file.open(_logging_file, std::ofstream::out | std::ofstream::trunc);
  }

  auto start = std::chrono::system_clock::now();

  initialize( _search_space );

  while( !_abort_condition->stop( *this ) )
  {
    auto config = get_next_config();

    ++_number_of_evaluated_configs;
    compile_time = 0;
    error_code = 0;
    auto collection_start = std::chrono::system_clock::now();
    try
    {
      if (_is_valid && !_is_valid(config)) throw std::runtime_error("configuration not valid accourding to _is_valid");
      if (_wrapper_type == NONE) {
        program_runtime = program( config, &compile_time, &error_code );
      } else {
        program_runtime = execute_wrapper( config, &compile_time, &error_code );
      }
    }
    catch( ... )
    {
      ++_number_of_invalid_configs;

      if( _abort_on_error )
        abort();
      else
        program_runtime = std::numeric_limits<unsigned long long>::max();
    }
    auto collection_end = std::chrono::system_clock::now();

    auto current_best_result = std::get<4>( _history.back() );
    if( program_runtime < current_best_result  )
    {
      _evaluations_required_to_find_best_found_result = this->number_of_evaluated_configs();
      _valid_evaluations_required_to_find_best_found_result = this->number_of_valid_evaluated_configs();
      _history.emplace_back( this->number_of_evaluated_configs(),
                             this->number_of_valid_evaluated_configs(),
                             std::chrono::high_resolution_clock::now(),
                             config,
                             program_runtime
                           );
    }

    report_result( program_runtime ); // TODO: refac "program_runtime" -> "program_cost"

    if (!_logging_file.empty()) {
      if (write_header) {
        csv_file << "collection_start;collection_end;valid;error_code;compile_time;run_time";
        for (const auto &tp : config) {
          if (tp.first.find("NOT_USED") == std::string::npos)
            csv_file << ";" << tp.first;
        }
        write_header = false;
      }
      auto collection_start_time_t = std::chrono::system_clock::to_time_t(collection_start);
      auto start_ms = std::chrono::duration_cast<std::chrono::milliseconds>(collection_start.time_since_epoch()) % 1000;
      csv_file << std::endl
               << std::put_time(std::localtime(&collection_start_time_t), "%F %T");
      csv_file << '.' << std::setfill('0') << std::setw(3) << start_ms.count();
      auto collection_end_time_t = std::chrono::system_clock::to_time_t(collection_end);
      auto end_ms = std::chrono::duration_cast<std::chrono::milliseconds>(collection_end.time_since_epoch()) % 1000;
      csv_file << ";" << std::put_time(std::localtime(&collection_end_time_t), "%F %T");
      csv_file << '.' << std::setfill('0') << std::setw(3) << end_ms.count();
      csv_file << ";" << (program_runtime < std::numeric_limits<unsigned long long>::max())
              << ";" << error_code
              << ";" << compile_time
               << ";" << program_runtime;
      for (const auto &tp : config) {
        if (tp.first.find("NOT_USED") == std::string::npos)
          csv_file << ";" << tp.second.value();
      }
    }

    std::cout << std::endl << "evaluated configs: " << this->number_of_evaluated_configs() << " , valid configs: " << this->number_of_valid_evaluated_configs() << " , program cost: " << program_runtime << " , current best result: " << this->best_measured_result() << std::endl << std::endl;

    if (_progress_callback &&
        (program_runtime < current_best_result || std::chrono::duration_cast<std::chrono::seconds>(std::chrono::high_resolution_clock::now() - _last_callback).count() >= _callback_timeout)) {
      // if result improved or callback has not been called in a while -> call callback
      _progress_callback();
      _last_callback = std::chrono::high_resolution_clock::now();
    }
  }

  finalize();

  if (!_logging_file.empty()) {
    csv_file.close();
  }

  std::cout << "\nnumber of evaluated configs: " << this->number_of_evaluated_configs() << " , number of valid configs: " << this->number_of_valid_evaluated_configs() << " , number of invalid configs: " << _number_of_invalid_configs << " , evaluations required to find best found result: " << _evaluations_required_to_find_best_found_result << " , valid evaluations required to find best found result: " << _valid_evaluations_required_to_find_best_found_result << std::endl;

  auto end = std::chrono::system_clock::now();
  auto runtime_in_sec = std::chrono::duration_cast<std::chrono::seconds>( end - start ).count();
  std::cout << std::endl << "total runtime for tuning = " << runtime_in_sec << "sec" << std::endl;


  // output
  auto        best_config = best_configuration();
  std::string seperator = "";
  std::cout << "\nbest configuration: [ ";
  for( const auto& tp : best_config )
  {
    auto tp_value = tp.second;
    std::cout << seperator << tp_value.name() << " = " << tp_value.value();
    seperator = " ; ";
  }
  std::cout << " ] with cost: " << this->best_measured_result() << std::endl << std::endl;

  // store best found result in file
  std::stringstream tp_names;
  std::stringstream tp_vals;
  for( auto& tp : best_config )
  {
    tp_names << tp.first  << ";";
    tp_vals  << tp.second << ";";
  }

//  std::ofstream outfile;
//  outfile.open("results.csv", std::ofstream::app ); // TODO: "/Users/arirasch/results.csv"
//  outfile << "best_measured_result" << ";" << "number_of_valid_evaluated_configs" << ";" << "evaluations_required_to_find_best_found_result" << ";" << "valid_evaluations_required_to_find_best_found_result" ";" << tp_names.str() << std::endl;
//  outfile << this->best_measured_result() << ";" << this->number_of_valid_evaluated_configs() << ";" << _evaluations_required_to_find_best_found_result << ";" << _valid_evaluations_required_to_find_best_found_result << ";" << tp_vals.str() << std::endl;
//  outfile.close();

  // make final call to callback
  if (_progress_callback) {
    _progress_callback();
    _last_callback = std::chrono::high_resolution_clock::now();
  }

  auto best_configuration = std::get<3>( _history.back() );
  return best_configuration;
}


template< typename... Ts, typename... rest_tp_tuples >
void tuner_with_constraints::insert_tp_names_in_search_space( G_class<Ts...> tp_tuple, rest_tp_tuples... tuples )
{
  insert_tp_names_of_one_tree_in_search_space ( tp_tuple, std::make_index_sequence<sizeof...(Ts)>{} );
  
  insert_tp_names_in_search_space( tuples... );
}


template< typename... Ts, size_t... Is>
void tuner_with_constraints::insert_tp_names_of_one_tree_in_search_space ( G_class<Ts...> tp_tuple, std::index_sequence<Is...> ) // TODO refac: insert_tp_names_of_one_tree_in_search_space  -> insert_tp_names_of_one_tree_in_search_space
{
  insert_tp_names_of_one_tree_in_search_space ( std::get<Is>( tp_tuple.tps() )... );
}

template< typename T, typename range_t, typename callable, typename... Ts >
void tuner_with_constraints::insert_tp_names_of_one_tree_in_search_space ( tp_t<T,range_t,callable>& tp, Ts&... tps )
{
  if (std::find(_search_space.names().begin(), _search_space.names().end(), tp.name()) == _search_space.names().end()) {
    _unconstrained_search_space_size *= tp.get_range_ptr()->size();
  }

  _search_space.add_name( tp.name() );
  
  insert_tp_names_of_one_tree_in_search_space ( tps... );
}


template< size_t TREE_ID, typename... Ts, typename... rest_tp_tuples>
tuner_with_constraints& tuner_with_constraints::generate_config_trees( G_class<Ts...> tp_tuple, rest_tp_tuples... tuples )
{
  return generate_config_trees<TREE_ID>( tp_tuple, std::make_index_sequence<sizeof...(Ts)>{}, tuples... );
}


template< size_t TREE_ID, typename... Ts, size_t... Is, typename... rest_tp_tuples >
tuner_with_constraints& tuner_with_constraints::generate_config_trees( G_class<Ts...> tp_tuple, std::index_sequence<Is...>, rest_tp_tuples... tuples )
{
  // fill generated config tree
  const size_t TREE_DEPTH = sizeof...(Is);
  _threads.emplace_back( [=](){ generate_single_config_tree< TREE_ID, TREE_DEPTH >( std::get<Is>( tp_tuple.tps() )... ); } );

  return generate_config_trees< TREE_ID - 1 >( tuples... );
}


template< size_t TREE_ID >
tuner_with_constraints& tuner_with_constraints::generate_config_trees()
{
  for( auto& thread : _threads )
    thread.join();
  
  _threads.clear();

  return *this;
}


template< size_t TREE_ID, size_t TREE_DEPTH, typename T, typename range_t, typename callable, typename... Ts, std::enable_if_t<( TREE_DEPTH > 0 )>* >
void tuner_with_constraints::generate_single_config_tree( tp_t<T,range_t,callable>& tp, Ts&... tps )
{
  if (_is_wrapper) {
    T value;
    std::stringstream ss(_wrapper_configuration[tp.name()]);
    if (!(ss >> value)) {
      std::cerr << "failed to parse value \"" << ss.str() << "\" for tp " << tp.name() << std::endl;
      exit(EXIT_FAILURE);
    }
    auto value_tp_pair = std::make_pair( value, static_cast<void*>( tp._act_elem.get() ) );
    generate_single_config_tree< TREE_ID, TREE_DEPTH-1 >( tps..., value_tp_pair );
  } else {
    T value;
    while( tp.get_next_value(value) )
    {
      auto value_tp_pair = std::make_pair( value, static_cast<void*>( tp._act_elem.get() ) );
      generate_single_config_tree< TREE_ID, TREE_DEPTH-1 >( tps..., value_tp_pair );
    }
  }
}


template< size_t TREE_ID, size_t TREE_DEPTH, typename... Ts, std::enable_if_t<( TREE_DEPTH == 0 )>*  >
void tuner_with_constraints::generate_single_config_tree( const Ts&... values )
{
  _search_space.tree( _search_space.num_trees() -  TREE_ID ).insert( values... ); // read TREE_IDs from right to left!
  //print_path( tps... );
}


// TODO: loeschen
template< typename T, typename... Ts >
void tuner_with_constraints::print_path(T val, Ts... tps)
{
  std::cout << val << " ";
  print_path(tps...);
}


} // namespace "atf"

#endif /* tuner_with_constraints_def_h */
