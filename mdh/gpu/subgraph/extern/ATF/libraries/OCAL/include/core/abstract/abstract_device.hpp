//
//  abstract_device.hpp
//  ocal
//
//  Created by Ari Rasch on 25.10.17.
//  Copyright © 2017 Ari Rasch. All rights reserved.
//

#ifndef abstract_device_h
#define abstract_device_h


namespace ocal
{

namespace core
{

namespace abstract
{


template< typename child_t, typename kernel_t, typename event_manager_t >
class abstract_device
{
  public:
    abstract_device()
      : _platform_id( 0 ), _device_id( 0 ), _kernel(), _read_buffer_references_to_wait_for(), _write_buffer_references_to_wait_for(), _thread_configuration{ { {{0,0,0}}, {{0,0,0}} } }
    {}
  
    // copy ctor instantiates shallow copy
    abstract_device( const abstract_device& ) = default;


    virtual ~abstract_device()
    {
    // different ocal::devices can point to the same physical device ( -> the ocal:devices have the same unique_id)
//      // erase device's default command queue out of global list
//      unique_device_id_to_default_opencl_command_queue.erase( this->unique_id() );
//
//      // erase device's command queues for computation out of global list
//      auto command_queues = unique_device_id_to_opencl_command_queues.at( this->unique_id() );
    }


    template< typename kernel_child_t >
    auto& operator()( abstract_kernel< kernel_child_t, kernel_t >& kernel )
    {
      // compile kernel for device (if not already done)
      if( !kernel.contains_kernel_for_device( this->unique_id() ) )
        kernel.build_kernel_for_device( this->unique_id() );
      
      // get compiled kernel
      this->_kernel = kernel.get( this->unique_id() );
      
      // set kernel's thread configuration (e.g., determined via auto-tuning)
      if( kernel.thread_configuration_set( this->unique_id() ) )
        _thread_configuration = kernel.thread_configuration( this->unique_id() );
      
      return *this;
    }
  

    // set kernel's input -- fundamental type
    template< typename T, typename... Ts, typename = std::enable_if_t< std::is_fundamental<T>::value > >
    auto& operator()( const T& val, const Ts&... inputs )
    {
      // set kernel argument
      this->set_arg( val );

      this->operator()( inputs... );
      
      return *this;
    }


    // set kernel's input -- explicit scalar type
    template< typename T, typename... Ts >
    auto& operator()( ::ocal::common::scalar_class<T> scalar, const Ts&... inputs )
    {
      // set kernel argument
      this->set_arg( scalar );

      this->operator()( inputs... );

      return *this;
    }
    
  
    // set kernel input -- buffer types
    template< typename T, typename... Ts >
    auto& operator()( ::ocal::common::read_class<T> read_buffer, const Ts&... inputs )
    {
      // get the actual buffer out of the buffer wrapper
      auto& buffer = read_buffer.get();
      
      // set kernel argument
      this->set_arg( buffer.get_device_buffer_with_read_access( this->unique_id(),
                                                                this->unique_device_id_to_actual_command_queue_or_stream_id( this->unique_id() )
                                                              ) );
      
      this->operator()( inputs... );
      
      return *this;
    }
  
  
    template< typename T, typename... Ts >
    auto& operator()( ::ocal::common::write_class<T> write_buffer, const Ts&... inputs )
    {
      // get the actual buffer out of the buffer wrapper
      auto& buffer = write_buffer.get();
    
      this->set_arg( buffer.get_device_buffer_with_write_access( this->unique_id(),
                                                                 this->unique_device_id_to_actual_command_queue_or_stream_id( this->unique_id() )
                                                               ) );
      
      // causes adding the kernel event later to the buffer class to ensure that the computations on write_buffer are finished on this device after accessing the device's buffer again
      this->add_write_buffer_to_synchronization_list( buffer );
      
      this->operator()( inputs... );
      
      return *this;
    }


    template< typename T, typename... Ts >
    auto& operator()( ::ocal::common::read_write_class<T> read_write_buffer, const Ts&... inputs )
    {
      // get the actual buffer out of the buffer wrapper
      auto& buffer = read_write_buffer.get();
    
      // set kernel argument
      this->set_arg( buffer.get_device_buffer_with_read_write_access( this->unique_id(),
                                                                      this->unique_device_id_to_actual_command_queue_or_stream_id( this->unique_id() )
                                                                    ) );


      // causes adding the kernel event later to the buffer class to ensure that the computations on write_buffer are finished on this device after accessing the device's buffer again
      this->add_write_buffer_to_synchronization_list( buffer );
      
      this->operator()( inputs... );
      
      return *this;
    }
  
  
    template< template <typename> class T, typename T_hlpr, typename... Ts, typename = std::enable_if_t< std::is_base_of< local_or_shared_buffer<T_hlpr>, T<T_hlpr> >::value > >
    auto& operator()( T<T_hlpr> local_or_shared_buffer, const Ts&... inputs )
    {
      // set kernel argument
      this->set_arg( local_or_shared_buffer );
      
      this->operator()( inputs... );
      
      return *this;
    }
  

    // IA
    void operator()()
    {
      // get command queue
      auto& actual_command_queue_or_stream_id = this->unique_device_id_to_actual_command_queue_or_stream_id( this->unique_id() );

      // start kernel
      _last_event = typename event_manager_t::event_type(this->unique_id(), actual_command_queue_or_stream_id, false, false);
      this->start_kernel( actual_command_queue_or_stream_id, _last_event );

      // add the events to the buffers
      for( auto& buffer : _read_buffer_references_to_wait_for )
        buffer.get().set_event_device_buffer_of_device_is_read( _last_event, this->unique_id() );

      for( auto& buffer : _write_buffer_references_to_wait_for )
        buffer.get().set_event_device_buffer_of_device_is_written( _last_event, this->unique_id() );
        
      // increment command queue id
      actual_command_queue_or_stream_id = ( actual_command_queue_or_stream_id + 1 ) % this->num_command_queues_or_streams();

      // reset events
      _read_buffer_references_to_wait_for.clear();
      _write_buffer_references_to_wait_for.clear();
    }

    void synchronize() {
        // pointer on me as child
        auto me_as_child = dynamic_cast< child_t* >( this );

        // set kernel argument
        me_as_child->synchronize();
    }

    typename event_manager_t::event_type &last_event() {
      return _last_event;
    }


    unsigned long long last_runtime()
    {
      // pointer on me as child
      auto me_as_child = dynamic_cast< child_t* >( this );

      // set kernel argument
      return me_as_child->last_runtime();
    }


    std::string name()
    {
      // pointer on me as child
      auto me_as_child = dynamic_cast< child_t* >( this );

      // set kernel argument
      return me_as_child->name();
    }


    size_t max_threads_per_group()
    {
      // pointer on me as child
      auto me_as_child = dynamic_cast< child_t* >( this );

      // set kernel argument
      return me_as_child->max_threads_per_group();
    }


    std::vector<size_t> max_group_size()
    {
      // pointer on me as child
      auto me_as_child = dynamic_cast< child_t* >( this );

      // set kernel argument
      return me_as_child->max_group_size();
    }


    size_t max_local_or_shared_memory()
    {
      // pointer on me as child
      auto me_as_child = dynamic_cast< child_t* >( this );

      // set kernel argument
      return me_as_child->max_local_or_shared_memory();
    }


  // ----------------------------------------------------------
  //   helper
  // ----------------------------------------------------------

  protected:
  
    // unique device id (over platforms) -- uses a straightforwards coding based on prime factors
    int unique_id() const
    {
      return std::pow( 2, _platform_id ) * std::pow( 3, _device_id );
    }


    // ----------------------------------------------------------
    //   helper for synchronization
    // ----------------------------------------------------------
  
    void add_read_buffer_to_synchronization_list( event_manager_t& buffer )
    {
      _read_buffer_references_to_wait_for.emplace_back( buffer );
    }

  
    void add_write_buffer_to_synchronization_list( event_manager_t& buffer )
    {
      _write_buffer_references_to_wait_for.emplace_back( buffer );
    }
  

    // ----------------------------------------------------------
    //   static polymorphism
    // ----------------------------------------------------------
  
    template< typename... Ts >
    int& unique_device_id_to_actual_command_queue_or_stream_id( const Ts&... args )
    {
      // pointer on me as child
      auto me_as_child = dynamic_cast< child_t* >( this );
  
      // run child class' function
      return me_as_child->unique_device_id_to_actual_command_queue_or_stream_id( args... );
    }
    

    template< typename... Ts >
    void start_kernel( Ts&... args )
    {
      // pointer on me as child
      auto me_as_child = dynamic_cast< child_t* >( this );
  
      // run child class' function
      me_as_child->start_kernel( args... );
    }


    template< typename... Ts >
    void set_arg( const Ts&... args )
    {
      // pointer on me as child
      auto me_as_child = dynamic_cast< child_t* >( this );
  
      // set kernel argument
      me_as_child->set_arg( args... );
    }
  
  
    int num_command_queues_or_streams() 
    {
      // pointer on me as child
      auto me_as_child = dynamic_cast< child_t* >( this );
  
      // set kernel argument
      return me_as_child->num_command_queues_or_streams();
    }

  
  protected:
    int                                                      _platform_id;                          // the OpenCL platform id
    int                                                      _device_id;                            // the OpenCL device   id
    kernel_t                                                 _kernel;
    std::vector< std::reference_wrapper< event_manager_t > > _read_buffer_references_to_wait_for;  // TODO: löschen
    std::vector< std::reference_wrapper< event_manager_t > > _write_buffer_references_to_wait_for; // Note: event_manager_t is parent class of buffer, and the reference wrapper allows using buffer as event_manager_t
    std::array< std::array<int,3>, 2 >                       _thread_configuration;
    typename event_manager_t::event_type                     _last_event;
};


} // namespace "abstract"

} // namespace "core"

} // namespace "ocal"


#endif /* abstract_device_h */
