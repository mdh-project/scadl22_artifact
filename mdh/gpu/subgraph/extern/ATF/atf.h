//
//  atf.h
//  new_atf_lib
//
//  Created by Ari Rasch on 28/10/2016.
//  Copyright © 2016 Ari Rasch. All rights reserved.
//

#ifndef atf_h
#define atf_h

#include "include/abort_conditions.hpp"
#include "include/range.hpp"
#include "include/tp.hpp"
#include "include/coordinate_space.hpp"

#include "include/auc_bandit.hpp"
#include "include/differential_evolution.hpp"
#include "include/open_tuner.hpp"
#include "include/open_tuner_flat.hpp"
#include "include/open_tuner_on_coord_space.hpp"
#include "include/exhaustive.hpp"
#include "include/particle_swarm.hpp"
#include "include/pattern_search.hpp"
#include "include/random_search.hpp"
#include "include/round_robin.hpp"
#include "include/annealing.hpp"
#include "include/annealing_tree.hpp"
#include "include/simulated_annealing_opentuner.hpp"
#include "include/torczon.hpp"

#include "include/operators.hpp"
#include "include/predicates.hpp"

#if defined(OCAL_OCL_WRAPPER) || defined(OCAL_CUDA_WRAPPER)
#include "include/ocal_wrapper.hpp"
#include "include/ocal_md_hom_wrapper.hpp"
#endif
#include "include/cpp_cf.hpp"
#include "include/bash_cf.hpp"


#endif /* atf_h */
