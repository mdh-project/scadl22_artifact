#include "tclap/CmdLine.h"
#include "data_util.hpp"
#include <iostream>

int main(int argc, const char **argv) {
    TCLAP::CmdLine cmd("Data utility program.");
    TCLAP::UnlabeledValueArg<std::string> command_arg("command", "The command to execute.", true, "", "string");
    cmd.add(command_arg);
    TCLAP::UnlabeledMultiArg<std::string> args_arg("args", "The command arguments.", false, "string");
    cmd.add(args_arg);

    try {
        cmd.parse(argc, argv);
    } catch (TCLAP::ArgException &e) {
        std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
    }

    if (command_arg.getValue() == "data_directory") {
        if (args_arg.getValue().empty() || args_arg.getValue().size() > 6) {
            std::cerr << "usage for command " << command_arg.getValue() << ": [parts...]" << std::endl;
            exit(EXIT_FAILURE);
        }
        std::cout << data_directory(args_arg.getValue());
    } else {
        std::cerr << "unknown command: " << command_arg.getValue() << std::endl;
        exit(EXIT_FAILURE);
    }
}