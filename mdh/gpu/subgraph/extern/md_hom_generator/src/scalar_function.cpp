//
// Created by Richard Schulze on 01.11.2017.
//

#include "scalar_function.hpp"

namespace md_hom {

scalar_function::scalar_function(const std::function<std::string(bool)> &function_body) : _function_body(function_body) {
}

const std::string scalar_function::function_body(bool reduce) const {
    return _function_body(reduce);
}

}
