//
// Created by Richard Schulze on 06.11.17.
//

#ifndef MD_BLAS_RESULT_BUFFER_WRAPPER_HPP
#define MD_BLAS_RESULT_BUFFER_WRAPPER_HPP

#include <sstream>
#include <iostream>

#include "result_buffer.hpp"
#include "macros.hpp"
#include "types.hpp"


namespace md_hom {
namespace generator {


template<unsigned int L_DIMS, unsigned int R_DIMS, typename TOs>
class result_buffer_wrapper {
public:
    result_buffer_wrapper(const TOs &result_buffer, const macros<L_DIMS, R_DIMS> &macros, const std::vector<dimension_t> &dimension_order, bool runtime_inputs, const std::vector<dimension_t> &skip_post_processing, bool cuda)
            : _result_buffer(result_buffer), _macros(macros), _dimension_order(dimension_order), _runtime_inputs(runtime_inputs), _skip_post_processing(skip_post_processing), _cuda(cuda) {
        _buffer_macro_name_pattern = "K%d_RES_%c_BUFFER_%s";
        _buffer_def_macro_name_pattern = "K%d_RES_%c_BUFFER_DEF_%s";
        _res_macro_name_pattern = "K%d%s_%c_CB_RES_DEST_%s";
        _fu_res_macro_name_pattern = "K%d_FU%s_%c_CB_RES_DEST_%s";
        _reduction_mem_macro_name_pattern = "K%d_%c_REDUCTION_MEM_%s";
        _kernel_res_macro_name_pattern = "K%d%s_%c_KERNEL_RES_%s";
        _kernel_res_buffer_macro_name_pattern = "K%d_KERNEL_RES_BUFFER_%s";
        _fu_kernel_res_macro_name_pattern = "K%d_FU%s_%c_KERNEL_RES_%s";

        for (LEVEL level : {LEVEL::PRIVATE, LEVEL::LOCAL, LEVEL::GLOBAL}) {
            std::vector<std::string> params;
            for (int i = 0; i < L_DIMS + sub_levels(level, true, ORDER::DESCENDING).size() * R_DIMS; ++i) {
                params.push_back("%s");
            }
            _buffer_macro_call_pattern[LEVEL_ID(level)] = stringf("%s(%s)", _buffer_macro_name_pattern, concat(params, ", "));
        }
        _res_macro_call_pattern = stringf("%s(", _res_macro_name_pattern);
        _fu_res_macro_call_pattern = stringf("%s(", _fu_res_macro_name_pattern);
        _reduction_mem_macro_call_pattern = _reduction_mem_macro_name_pattern + "(";
        _kernel_res_macro_call_pattern = _kernel_res_macro_name_pattern + "(";
        _kernel_res_buffer_macro_call_pattern = _kernel_res_buffer_macro_name_pattern + "(";
        _fu_kernel_res_macro_call_pattern = _fu_kernel_res_macro_name_pattern + "(";
        for (int i = 0; i < L_DIMS; ++i) {
            if (i > 0) {
                _res_macro_call_pattern.append(", ");
                _fu_res_macro_call_pattern.append(", ");
                _reduction_mem_macro_call_pattern.append(", ");
                _kernel_res_macro_call_pattern.append(", ");
                _kernel_res_buffer_macro_call_pattern.append(", ");
                _fu_kernel_res_macro_call_pattern.append(", ");
            }
            _res_macro_call_pattern.append("%s");
            _fu_res_macro_call_pattern.append("%s");
            _reduction_mem_macro_call_pattern.append("%s");
            _kernel_res_macro_call_pattern.append("%s");
            _kernel_res_buffer_macro_call_pattern.append("%s");
            _fu_kernel_res_macro_call_pattern.append("%s");
        }
        for (int i = 0; i < R_DIMS; ++i) {
            if (L_DIMS > 0 || i > 0) {
                _reduction_mem_macro_call_pattern.append(", ");
                _kernel_res_buffer_macro_call_pattern.append(", ");
            }
            _reduction_mem_macro_call_pattern.append("%s");
            _kernel_res_buffer_macro_call_pattern.append("%s");
        }
        _res_macro_call_pattern.append(")");
        _fu_res_macro_call_pattern.append(")");
        _reduction_mem_macro_call_pattern.append(")");
        _kernel_res_macro_call_pattern.append(")");
        _kernel_res_buffer_macro_call_pattern.append(")");
        _fu_kernel_res_macro_call_pattern.append(")");

        bool r_dim_found = false;
        for (const auto &dim : _dimension_order) {
            if (dim.type == DIM_TYPE::R) {
                r_dim_found = true;
            } else if (r_dim_found) {
                _l_dims_after_first_r_dim.push_back(dim);
            }
        }
    }

    const std::string buffer_macro_name(const std::string &output_name, unsigned int kernel, LEVEL level) const {
        return stringf(_buffer_macro_name_pattern, kernel, level, upper_case(output_name));
    }

    const std::string buffer_def_macro_name(const std::string &output_name, unsigned int kernel, LEVEL level) const {
        return stringf(_buffer_def_macro_name_pattern, kernel, level, upper_case(output_name));
    }

    const std::string res_macro_name(const std::string &output_name, unsigned int kernel, LEVEL level, char *phase) const {
        std::string p3_prefix = "";
        for (const auto &dim : dim_range(L_DIMS, 0)) {
            bool dim_in_phase3 = false;
            bool my_level_in_phase3 = false;
            for (LEVEL p_level : parent_levels(level, true)) {
                if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(p_level, dim)] == 3) {
                    dim_in_phase3 = true;
                    break;
                }
            }
            my_level_in_phase3 = phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(level, dim)] == 3;

            if (dim_in_phase3) {
                if (my_level_in_phase3) {
                    p3_prefix += "_P3";
                } else {
                    p3_prefix += "_PP3";
                }
                if (level == LEVEL::PRIVATE && phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dim)] == 2) {
                    p3_prefix += stringf("_IN_COMPLETE_%c_CB", parent_level(parent_level(level)));
                }
                p3_prefix += stringf("_%c_%d", dim.type, dim.nr);
            }
        }
        return stringf(_res_macro_name_pattern, kernel, p3_prefix, level, upper_case(output_name));
    }

    const std::string fu_res_macro_name(const std::string &output_name, unsigned int kernel, LEVEL level, char *phase) const {
        std::string p3_prefix = "";
        for (const auto &dim : dim_range(L_DIMS, 0)) {
            bool dim_in_phase3 = false;
            bool my_level_in_phase3 = false;
            for (LEVEL p_level : parent_levels(level, true)) {
                if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(p_level, dim)] == 3) {
                    dim_in_phase3 = true;
                    break;
                }
            }
            my_level_in_phase3 = phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(level, dim)] == 3;

            if (dim_in_phase3) {
                if (my_level_in_phase3) {
                    p3_prefix += "_P3";
                } else {
                    p3_prefix += "_PP3";
                }
                if (level == LEVEL::PRIVATE && phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dim)] == 2) {
                    p3_prefix += stringf("_IN_COMPLETE_%c_CB", parent_level(parent_level(level)));
                }
                p3_prefix += stringf("_%c_%d", dim.type, dim.nr);
            }
        }
        return stringf(_fu_res_macro_name_pattern, kernel, p3_prefix, level, upper_case(output_name));
    }

    const std::string reduction_mem_macro_name(const std::string &output_name, unsigned int kernel, LEVEL level) const {
        return stringf(_reduction_mem_macro_name_pattern, kernel, level, upper_case(output_name));
    }

    const std::string kernel_res_macro_name(const std::string &output_name, unsigned int kernel, LEVEL level, char *phase) const {
        std::string p3_prefix = "";
        for (const auto &dim : dim_range(L_DIMS, 0)) {
            bool dim_in_phase3 = false;
            bool my_level_in_phase3 = false;
            for (LEVEL p_level : parent_levels(level, true)) {
                if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(p_level, dim)] == 3) {
                    dim_in_phase3 = true;
                    break;
                }
            }
            my_level_in_phase3 = phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(level, dim)] == 3;

            if (dim_in_phase3) {
                if (my_level_in_phase3) {
                    p3_prefix += "_P3";
                } else {
                    p3_prefix += "_PP3";
                }
                if (level == LEVEL::PRIVATE && phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dim)] == 2) {
                    p3_prefix += stringf("_IN_COMPLETE_%c_CB", parent_level(parent_level(level)));
                }
                p3_prefix += stringf("_%c_%d", dim.type, dim.nr);
            }
        }
        return stringf(_kernel_res_macro_name_pattern, kernel, p3_prefix, level, upper_case(output_name));
    }

    const std::string fu_kernel_res_macro_name(const std::string &output_name, unsigned int kernel, LEVEL level, char *phase) const {
        std::string p3_prefix = "";
        for (const auto &dim : dim_range(L_DIMS, 0)) {
            bool dim_in_phase3 = false;
            bool my_level_in_phase3 = false;
            for (LEVEL p_level : parent_levels(level, true)) {
                if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(p_level, dim)] == 3) {
                    dim_in_phase3 = true;
                    break;
                }
            }
            my_level_in_phase3 = phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(level, dim)] == 3;

            if (dim_in_phase3) {
                if (my_level_in_phase3) {
                    p3_prefix += "_P3";
                } else {
                    p3_prefix += "_PP3";
                }
                if (level == LEVEL::PRIVATE && phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dim)] == 2) {
                    p3_prefix += stringf("_IN_COMPLETE_%c_CB", parent_level(parent_level(level)));
                }
                p3_prefix += stringf("_%c_%d", dim.type, dim.nr);
            }
        }
        return stringf(_fu_kernel_res_macro_name_pattern, kernel, p3_prefix, level, upper_case(output_name));
    }

    const std::string call_buffer_macro(const std::string &output_name, unsigned int kernel, LEVEL level, std::vector<std::string> params) const {
        std::string call = stringf_p(_buffer_macro_call_pattern[LEVEL_ID(level)], kernel, level, upper_case(output_name));
        for (const auto& param : params) {
            call = stringf_p(call, param);
        }
        return call;
    }

    const std::string call_res_macro(const std::string &output_name, unsigned int kernel, LEVEL level, char *phase, std::vector<std::string> params) const {
        std::string p3_prefix = "";
        if (phase != nullptr) {
            for (const auto &dim : dim_range(L_DIMS, 0)) {
                bool dim_in_phase3 = false;
                bool my_level_in_phase3 = false;
                for (LEVEL p_level : parent_levels(level, true)) {
                    if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(p_level, dim)] == 3) {
                        dim_in_phase3 = true;
                        break;
                    }
                }
                my_level_in_phase3 = phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(level, dim)] == 3;

                if (dim_in_phase3) {
                    if (my_level_in_phase3) {
                        p3_prefix += "_P3";
                    } else {
                        p3_prefix += "_PP3";
                    }
                    if (level == LEVEL::PRIVATE &&
                        phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dim)] == 2) {
                        p3_prefix += stringf("_IN_COMPLETE_%c_CB", parent_level(parent_level(level)));
                    }
                    p3_prefix += stringf("_%c_%d", dim.type, dim.nr);
                }
            }
        }
        std::string call = stringf_p(_res_macro_call_pattern, kernel, p3_prefix, level, upper_case(output_name));
        for (const auto& param : params) {
            call = stringf_p(call, param);
        }
        return call;
    }

    const std::string call_fu_res_macro(const std::string &output_name, unsigned int kernel, LEVEL level, char *phase, std::vector<std::string> params) const {
        std::string p3_prefix = "";
        if (phase != nullptr) {
            for (const auto &dim : dim_range(L_DIMS, 0)) {
                bool dim_in_phase3 = false;
                bool my_level_in_phase3 = false;
                for (LEVEL p_level : parent_levels(level, true)) {
                    if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(p_level, dim)] == 3) {
                        dim_in_phase3 = true;
                        break;
                    }
                }
                my_level_in_phase3 = phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(level, dim)] == 3;

                if (dim_in_phase3) {
                    if (my_level_in_phase3) {
                        p3_prefix += "_P3";
                    } else {
                        p3_prefix += "_PP3";
                    }
                    if (level == LEVEL::PRIVATE &&
                        phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dim)] == 2) {
                        p3_prefix += stringf("_IN_COMPLETE_%c_CB", parent_level(parent_level(level)));
                    }
                    p3_prefix += stringf("_%c_%d", dim.type, dim.nr);
                }
            }
        }
        std::string call = stringf_p(_fu_res_macro_call_pattern, kernel, p3_prefix, level, upper_case(output_name));
        for (const auto& param : params) {
            call = stringf_p(call, param);
        }
        return call;
    }

    const std::string call_reduction_mem_macro(const std::string &output_name, unsigned int kernel, LEVEL level, std::vector<std::string> params) const {
        std::string call = stringf_p(_reduction_mem_macro_call_pattern, kernel, level, upper_case(output_name));
        for (const auto& param : params) {
            call = stringf_p(call, param);
        }
        return call;
    }

    const std::string call_kernel_res_buffer_macro(const std::string &output_name, unsigned int kernel, std::vector<std::string> params) const {
        std::string call = stringf_p(_kernel_res_buffer_macro_call_pattern, kernel, upper_case(output_name));
        for (const auto& param : params) {
            call = stringf_p(call, param);
        }
        return call;
    }

    const std::string call_kernel_res_macro(const std::string &output_name, unsigned int kernel, LEVEL level, char *phase, std::vector<std::string> params) const {
        std::string p3_prefix = "";
        if (phase != nullptr) {
            for (const auto &dim : dim_range(L_DIMS, 0)) {
                bool dim_in_phase3 = false;
                bool my_level_in_phase3 = false;
                for (LEVEL p_level : parent_levels(level, true)) {
                    if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(p_level, dim)] == 3) {
                        dim_in_phase3 = true;
                        break;
                    }
                }
                my_level_in_phase3 = phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(level, dim)] == 3;

                if (dim_in_phase3) {
                    if (my_level_in_phase3) {
                        p3_prefix += "_P3";
                    } else {
                        p3_prefix += "_PP3";
                    }
                    if (level == LEVEL::PRIVATE &&
                        phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dim)] == 2) {
                        p3_prefix += stringf("_IN_COMPLETE_%c_CB", parent_level(parent_level(level)));
                    }
                    p3_prefix += stringf("_%c_%d", dim.type, dim.nr);
                }
            }
        }
        std::string call = stringf_p(_kernel_res_macro_call_pattern, kernel, p3_prefix, level, upper_case(output_name));
        for (const auto& param : params) {
            call = stringf_p(call, param);
        }
        return call;
    }

    const std::string call_fu_kernel_res_macro(const std::string &output_name, unsigned int kernel, LEVEL level, char *phase, std::vector<std::string> params) const {
        std::string p3_prefix = "";
        if (phase != nullptr) {
            for (const auto &dim : dim_range(L_DIMS, 0)) {
                bool dim_in_phase3 = false;
                bool my_level_in_phase3 = false;
                for (LEVEL p_level : parent_levels(level, true)) {
                    if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(p_level, dim)] == 3) {
                        dim_in_phase3 = true;
                        break;
                    }
                }
                my_level_in_phase3 = phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(level, dim)] == 3;

                if (dim_in_phase3) {
                    if (my_level_in_phase3) {
                        p3_prefix += "_P3";
                    } else {
                        p3_prefix += "_PP3";
                    }
                    if (level == LEVEL::PRIVATE &&
                        phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dim)] == 2) {
                        p3_prefix += stringf("_IN_COMPLETE_%c_CB", parent_level(parent_level(level)));
                    }
                    p3_prefix += stringf("_%c_%d", dim.type, dim.nr);
                }
            }
        }
        std::string call = stringf_p(_fu_kernel_res_macro_call_pattern, kernel, p3_prefix, level, upper_case(output_name));
        for (const auto& param : params) {
            call = stringf_p(call, param);
        }
        return call;
    }

    template<typename T>
    std::string replace_vars_in_index_function(const T &buffer, unsigned int dim_id, const std::vector<std::string> &params) const {
        std::string index = buffer.index_functions()[dim_id];
        const std::regex dim_regex("([lr])([1-9][0-9]*)");
        std::smatch match;
        while (std::regex_search(index, match, dim_regex)) {
            index = match.prefix().str() + params[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(match[1].str() == "l" ? L(std::atoi(match[2].str().c_str())) : R(std::atoi(match[2].str().c_str())))] + match.suffix().str();
        }
        return index;
    }

    template<typename T>
    std::vector<std::string> replace_vars_in_index_function(const T &buffer, const std::vector<unsigned int> &dim_id, const std::vector<std::vector<std::string>> &params) const {
        std::vector<std::string> indices;
        if (dim_id.empty() || params.empty()) return indices;
        for (size_t i = 0; i < std::max(dim_id.size(), params.size()); ++i) {
            indices.push_back(replace_vars_in_index_function(buffer, dim_id[std::min(i, dim_id.size() - 1)], params[std::min(i, params.size() - 1)]));
        }
        return indices;
    }

    template<typename T>
    std::string replace_vars_in_size_function(const T &buffer, unsigned int dim_id, const std::vector<std::string> &params) const {
        std::string size = buffer.dimension_sizes()[dim_id];
        const std::regex dim_regex("([LR])([1-9][0-9]*)");
        std::smatch match;
        while (std::regex_search(size, match, dim_regex)) {
            size = match.prefix().str() + params[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(match[1].str() == "L" ? L(std::atoi(match[2].str().c_str())) : R(std::atoi(match[2].str().c_str())))] + match.suffix().str();
        }
        return size;
    }

    const std::string definitions(unsigned int kernel) const {
        std::stringstream code;

        auto l_standard_indices = make_standard_indices(L_DIMS);
        code << "// -------------------- result buffer --------------------" << std::endl;
        code << std::endl << "// check which levels are used";
        for (LEVEL res_level : {LEVEL::PRIVATE, LEVEL::LOCAL, LEVEL::GLOBAL}) {
            code << std::endl << stringf("#if %s", concat(multi_stringf("%c_CB_RES_DEST_LEVEL == %s", V(LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE), V(long_level(res_level))), " || "));
            code << std::endl << stringf("#define K%d_%c_LEVEL_HAS_RESULTS", kernel, res_level);
            code << std::endl << "#endif";
        }
        code << std::endl;

        code << std::endl << "// determine pointer type for private result cache blocks";
        for (LEVEL res_level : {LEVEL::PRIVATE, LEVEL::LOCAL, LEVEL::GLOBAL}) {
            code << std::endl << stringf("#%s P_CB_RES_DEST_LEVEL == %s", res_level == LEVEL::PRIVATE ? "if" : "elif", long_level(res_level));
            code << std::endl << stringf("#define P_CB_RES_POINTER_TYPE __%s", lower_case(long_level(res_level)));
        }
        code << std::endl << "#endif";

        for_each(_result_buffer, [&](const result_buffer_class &output) {
            code << std::endl;
            code << stringf("// -------------------- %s --------------------", output.name()) << std::endl;

            for (LEVEL res_level : {LEVEL::PRIVATE, LEVEL::LOCAL, LEVEL::GLOBAL}) {
                auto prefix_levels = parent_levels(res_level, false, ORDER::DESCENDING);
                auto suffix_levels = sub_levels(res_level, true, ORDER::ASCENDING);

                auto buffer_indices = make_standard_indices(
                        static_cast<unsigned int>(L_DIMS + suffix_levels.size() * R_DIMS));
                std::vector<std::string> buffer_concatenation(L_DIMS + R_DIMS);
                std::vector<std::string> buffer_def_concatenation(L_DIMS + R_DIMS);
                if (!_l_dims_after_first_r_dim.empty() && res_level != LEVEL::GLOBAL) {
                    for (const auto &dim : _l_dims_after_first_r_dim) {
                        for (const auto c_level : level_range(LEVEL::LOCAL, res_level)) {
                            buffer_concatenation[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(dim)] += stringf("K%d_%c_RES_%c_%s_LOOP_ORDER_PREFIX_%c_%d()", kernel, c_level, res_level, upper_case(output.name()), dim.type, dim.nr);
                            buffer_def_concatenation[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(dim)] += stringf("K%d_%c_RES_%c_%s_DEF_LOOP_ORDER_PREFIX_%c_%d()", kernel, c_level, res_level, upper_case(output.name()), dim.type, dim.nr);
                        }
                    }
                }

                code << std::endl << stringf("// ------ %s ------", long_level(res_level));
                code << std::endl << stringf("#ifdef K%d_%c_LEVEL_HAS_RESULTS", kernel, res_level);

                if (L_DIMS > 0) {
                    // prefix
                    if (res_level != LEVEL::GLOBAL) {
                        code << std::endl << stringf("// construct prefix for res_%c_%s", lower_case(res_level), output.name());
                        if (!_l_dims_after_first_r_dim.empty()) {
                            for (const auto p_level : parent_levels(res_level, false, ASCENDING)) {
                                code << std::endl << stringf("#%s defined(K%d_%c_LEVEL_HAS_RESULTS)",
                                                             p_level == parent_level(res_level) ? "if" : "elif",
                                                             kernel, p_level);
                                for (const auto c_level : level_range(LEVEL::GLOBAL, parent_level(res_level))) {
                                    if (c_level < p_level) {
                                        for (const auto &dim : _l_dims_after_first_r_dim) {
                                            code << std::endl << stringf("#define K%d_%c_RES_%c_%s_LOOP_ORDER_PREFIX_%c_%d()",
                                                                         kernel, sub_level(c_level), res_level, upper_case(output.name()), dim.type, dim.nr);
                                            code << std::endl << stringf("#define K%d_%c_RES_%c_%s_DEF_LOOP_ORDER_PREFIX_%c_%d()",
                                                                         kernel, sub_level(c_level), res_level, upper_case(output.name()), dim.type, dim.nr);
                                        }
                                    } else {
                                        code << std::endl << stringf("#if %c_CB_RES_DEST_LEVEL != %s", c_level, upper_case(long_level(res_level)));
                                        for (const auto &dim : _l_dims_after_first_r_dim) {
                                            code << std::endl << stringf("#define K%d_%c_RES_%c_%s_LOOP_ORDER_PREFIX_%c_%d() [%s]",
                                                                         kernel, sub_level(c_level), res_level, upper_case(output.name()), dim.type, dim.nr,
                                                                         loop_generator<L_DIMS, R_DIMS>::loop_variable(sub_level(c_level), dim));
                                            code << std::endl << stringf("#define K%d_%c_RES_%c_%s_DEF_LOOP_ORDER_PREFIX_%c_%d() [%s + (%s > 0) + (%s > 0)]",
                                                                         kernel, sub_level(c_level), res_level, upper_case(output.name()), dim.type, dim.nr,
                                                                         _macros.num_steps(kernel, sub_level(c_level), dim),
                                                                         _macros.num_extra_cached_iterations(kernel, sub_level(c_level), dim),
                                                                         _macros.num_extra_elements(kernel, sub_level(c_level), dim));
                                        }
                                        code << std::endl << "#else";
                                        for (const auto &dim : _l_dims_after_first_r_dim) {
                                            code << std::endl << stringf("#define K%d_%c_RES_%c_%s_LOOP_ORDER_PREFIX_%c_%d()",
                                                                         kernel, sub_level(c_level), res_level, upper_case(output.name()), dim.type, dim.nr);
                                            code << std::endl << stringf("#define K%d_%c_RES_%c_%s_DEF_LOOP_ORDER_PREFIX_%c_%d()",
                                                                         kernel, sub_level(c_level), res_level, upper_case(output.name()), dim.type, dim.nr);
                                        }
                                        code << std::endl << "#endif";
                                    }
                                }
                            }
                            code << std::endl << "#endif";
                        }
                    }
                    std::string current_prefix = "";
                    std::string current_def_prefix = "";
                    for (LEVEL prefix_level : prefix_levels) {
                        current_prefix = stringf_p("%s_%c_PREFIX_%c_%d()", buffer_macro_name(output.name(), kernel, res_level),
                                                   prefix_level);
                        current_def_prefix = stringf_p("%s_%c_PREFIX_%c_%d()", buffer_def_macro_name(output.name(), kernel, res_level),
                                                       prefix_level);

                        code << std::endl << stringf("#if %c_CB_RES_DEST_LEVEL == %s", prefix_level, long_level(res_level));
                        for (const auto &dim : dim_range(L_DIMS, 0)) {
                            code << std::endl << stringf("#define %s %s",
                                                         stringf(current_prefix, dim.type, dim.nr),
                                                         stringf("[%c_step_l_%d]",
                                                                 lower_case(sub_level(prefix_level)),
                                                                 dim.nr
                                                         ));
                        }
                        for (const auto &dim : dim_range(L_DIMS, 0)) {
                            code << std::endl << stringf("#define %s %s",
                                                         stringf(current_def_prefix, dim.type, dim.nr),
                                                         stringf("[%s + (%s > 0) + (%s > 0)]",
                                                                 _macros.num_steps(kernel, sub_level(prefix_level), dim),
                                                                 _macros.num_extra_cached_iterations(kernel, sub_level(prefix_level), dim),
                                                                 _macros.num_extra_elements(kernel, sub_level(prefix_level), dim)
                                                         ));
                        }
                        code << std::endl << "#else";
                        for (const auto &dim : dim_range(L_DIMS, 0)) {
                            code << std::endl << stringf("#define %s", stringf(current_prefix, dim.type, dim.nr));
                        }
                        for (const auto &dim : dim_range(L_DIMS, 0)) {
                            code << std::endl << stringf("#define %s", stringf(current_def_prefix, dim.type, dim.nr));
                        }
                        code << std::endl << "#endif";

                        for (const auto dim : dim_range(L_DIMS, 0)) {
                            buffer_concatenation[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(dim)] += stringf(current_prefix,
                                                                                                    dim.type, dim.nr);
                            buffer_def_concatenation[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(dim)] += stringf(current_def_prefix,
                                                                                                        dim.type, dim.nr);
                        }
                    }

                    // L-dim addressing & sizing
                    for (const auto dim : dim_range(L_DIMS, 0)) {
                        buffer_concatenation[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(dim)] += stringf("[%s]", buffer_indices[dim.nr - 1]);
                        buffer_def_concatenation[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(dim)] += stringf("[%s]", _macros.cb_size(kernel, res_level, dim));
                    }
                }

                std::string suffix = stringf_p("%s_SUFFIX_%c_%d", buffer_macro_name(output.name(), kernel, res_level));
                std::string def_suffix = stringf_p("%s_SUFFIX_%c_%d", buffer_def_macro_name(output.name(), kernel, res_level));
                if (R_DIMS > 0) {
                    // suffix
                    code << std::endl << stringf("// construct suffix for res_%c_%s", lower_case(res_level), output.name());
                    auto params = make_standard_indices(static_cast<unsigned int>(suffix_levels.size()));
                    for (LEVEL suffix_level : suffix_levels) {
                        code << std::endl << stringf("%s %c_CB_RES_DEST_LEVEL == %s",
                                                     suffix_level == LEVEL::PRIVATE ? "#if  " : "#elif",
                                                     suffix_level, long_level(res_level));
                        if (suffix_level <= LEVEL::LOCAL) {
                            code << std::endl << stringf("#if L_REDUCTION >= %s", long_level(suffix_level));
                        }
                        auto relevant_levels = level_range(res_level, suffix_level);
                        if (res_level == LEVEL::GLOBAL) {
                            if (suffix_level == LEVEL::GLOBAL) relevant_levels.push_back(LEVEL::LOCAL);
                            for (const auto dim : dim_range(0, R_DIMS)) {
                                code << std::endl << stringf(
                                        "#define %s(%s) [%s]",
                                        stringf(suffix, dim.type, dim.nr),
                                        concat(params, ", "),
                                        make_flat_index(
                                                multi_stringf("(%s)", make_standard_indices(relevant_levels.size())),
                                                multi_stringf("%s", _macros.num_fu(V(kernel),
                                                                                   relevant_levels,
                                                                                   V(dim)))
                                        )
                                );
                            }

                            for (const auto dim : dim_range(0, R_DIMS)) {
                                code << std::endl << stringf(
                                        "#define %s [%s]",
                                        stringf(def_suffix, dim.type, dim.nr),
                                        concat(multi_stringf("%s",
                                                             _macros.num_fu(V(kernel), relevant_levels,
                                                                            V(dim))), " * ")
                                );
                            }
                        } else {
                            for (const auto dim : dim_range(0, R_DIMS)) {
                                std::string indexing = "";
                                int i = 0;
                                for (LEVEL l : level_range(res_level, suffix_level)) {
                                    indexing += stringf(
                                            "[(%s)]",
                                            params[i]);
                                    ++i;
                                }
                                code << std::endl << stringf("#define %s(%s) %s",
                                                             stringf(suffix, dim.type, dim.nr),
                                                             concat(params, ", "),
                                                             indexing);
                            }
                            for (const auto dim : dim_range(0, R_DIMS)) {
                                std::string size = "";
                                int i = 0;
                                for (LEVEL l : level_range(res_level, suffix_level)) {
                                    size += stringf(
                                            "[%s]",
                                            _macros.num_fu(kernel, l, dim));
                                    ++i;
                                }
                                code << std::endl << stringf("#define %s %s",
                                                             stringf(def_suffix, dim.type, dim.nr),
                                                             size);
                            }
                        }
                        if (suffix_level <= LEVEL::LOCAL) {
                            code << std::endl << "#else";
                            if (contains(relevant_levels, LEVEL::LOCAL)) {
                                relevant_levels.erase(std::remove(relevant_levels.begin(), relevant_levels.end(), LEVEL::LOCAL), relevant_levels.end());
                            }
                            if (res_level == LEVEL::GLOBAL) {
                                for (const auto dim : dim_range(0, R_DIMS)) {
                                    code << std::endl << stringf(
                                            "#define %s(%s) [%s]",
                                            stringf(suffix, dim.type, dim.nr),
                                            concat(params, ", "),
                                            make_flat_index(
                                                    multi_stringf("(%s)", make_standard_indices(relevant_levels.size())),
                                                    multi_stringf("%s", _macros.num_fu(V(kernel),
                                                                                       relevant_levels,
                                                                                       V(dim)))
                                            )
                                    );
                                }

                                for (const auto dim : dim_range(0, R_DIMS)) {
                                    code << std::endl << stringf(
                                            "#define %s [%s]",
                                            stringf(def_suffix, dim.type, dim.nr),
                                            concat(multi_stringf("%s",
                                                                 _macros.num_fu(V(kernel), relevant_levels,
                                                                                V(dim))), " * ")
                                    );
                                }
                            } else {
                                for (const auto dim : dim_range(0, R_DIMS)) {
                                    std::string indexing = "";
                                    int i = 0;
                                    for (LEVEL l : relevant_levels) {
                                        indexing += stringf(
                                                "[(%s)]",
                                                params[i]);
                                        ++i;
                                    }
                                    code << std::endl << stringf("#define %s(%s) %s",
                                                                 stringf(suffix, dim.type, dim.nr),
                                                                 concat(params, ", "),
                                                                 indexing);
                                }
                                for (const auto dim : dim_range(0, R_DIMS)) {
                                    std::string size = "";
                                    int i = 0;
                                    for (LEVEL l : relevant_levels) {
                                        size += stringf(
                                                "[%s]",
                                                _macros.num_fu(kernel, l, dim));
                                        ++i;
                                    }
                                    code << std::endl << stringf("#define %s %s",
                                                                 stringf(def_suffix, dim.type, dim.nr),
                                                                 size);
                                }
                            }
                            code << std::endl << "#endif";
                        }
                    }
                    for (const auto dim : dim_range(0, R_DIMS)) {
                        buffer_concatenation[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(dim)] += stringf("%s(%s)",
                                                                                                stringf(suffix, dim.type,
                                                                                                        dim.nr),
                                                                                                concat(std::vector<std::string>(
                                                                                                        buffer_indices.begin() +
                                                                                                        L_DIMS +
                                                                                                        (dim.nr - 1) *
                                                                                                        suffix_levels.size(),
                                                                                                        buffer_indices.begin() +
                                                                                                        L_DIMS + (dim.nr) *
                                                                                                                 suffix_levels.size()),
                                                                                                       ", "));
                        buffer_def_concatenation[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(dim)] += stringf(def_suffix, dim.type,
                                                                                                    dim.nr);
                    }
                    code << std::endl << "#endif";
                }

                // buffer abstraction
                code << std::endl << stringf("// buffer abstraction for res_%c_%s", lower_case(res_level), output.name());
                std::string buffer_abstraction;
                if (res_level == LEVEL::GLOBAL) {
                    code << std::endl << stringf("#define K%d_RES_%c_%s_BUFFER_NAME() res_g_%s",
                                                 kernel, res_level, upper_case(output.name()), output.name());
                    if (_cuda) {
                        buffer_abstraction = stringf("CONCAT_IN_DESCENDING_OCL_ORDER(%s)", concat(buffer_concatenation, ", "));
                    } else {
                        buffer_abstraction = "[FLAT_INDEX_IN_DESCENDING_OCL_ORDER(";
                        for (const auto dim : dim_range(L_DIMS, 0)) {
                            buffer_abstraction += stringf(
                                    "%s%s",
                                    dim.nr > 1 ? ", " : "",
                                    buffer_indices[dim.nr - 1]);
                        }
                        for (const auto dim : dim_range(0, R_DIMS)) {
                            buffer_abstraction += stringf("%s%s(%s)",
                                                          L_DIMS > 0 || dim.nr > 1 ? ", " : "",
                                                          stringf(suffix, dim.type, dim.nr),
                                                          concat(std::vector<std::string>(buffer_indices.begin() + L_DIMS + (dim.nr - 1) * suffix_levels.size(), buffer_indices.begin() + L_DIMS + (dim.nr) * suffix_levels.size()), ", "));
                        }
                        for (const auto dim : dim_range(L_DIMS, 0)) {
                            buffer_abstraction += stringf(", %s", _macros.cb_size(kernel, LEVEL::GLOBAL, dim));
                        }
                        for (const auto dim : dim_range(0, R_DIMS)) {
                            buffer_abstraction += stringf(", %s", stringf(def_suffix, dim.type, dim.nr));
                        }
                        buffer_abstraction += ")]";
                    }
                    code << std::endl << stringf("#define %s K%d_RES_%c_%s_BUFFER_NAME()%s",
                                                 call_buffer_macro(output.name(), kernel, res_level, buffer_indices),
                                                 kernel, res_level, upper_case(output.name()),
                                                 buffer_abstraction);
                } else {
                    code << std::endl << stringf("#define K%d_RES_%c_%s_BUFFER_NAME() res_%c_%s",
                                                 kernel, res_level, upper_case(output.name()), lower_case(res_level), output.name());
                    code << std::endl << stringf("#define %s K%d_RES_%c_%s_BUFFER_NAME()%s",
                                                 buffer_def_macro_name(output.name(), kernel, res_level),
                                                 kernel, res_level, upper_case(output.name()),
                                                 stringf("CONCAT_IN_DESCENDING_OCL_ORDER(%s)", concat(buffer_def_concatenation, ", ")));
                    buffer_abstraction = stringf("CONCAT_IN_DESCENDING_OCL_ORDER(%s)", concat(buffer_concatenation, ", "));
                    code << std::endl << stringf("#define %s K%d_RES_%c_%s_BUFFER_NAME()%s",
                                                 call_buffer_macro(output.name(), kernel, res_level, buffer_indices),
                                                 kernel, res_level, upper_case(output.name()),
                                                 buffer_abstraction);
                }
                code << std::endl << "#endif";
                code << std::endl;
            }

            code << std::endl << "// determine memory destination for results";
            for (LEVEL res_level : {LEVEL::PRIVATE, LEVEL::LOCAL, LEVEL::GLOBAL}) {
                for (LEVEL dest_level : {LEVEL::PRIVATE, LEVEL::LOCAL, LEVEL::GLOBAL}) {
                    code << std::endl << stringf("%s %c_CB_RES_DEST_LEVEL == %s",
                                                 dest_level == LEVEL::PRIVATE ? "#if  " : "#elif",
                                                 res_level,
                                                 long_level(dest_level)).c_str();
                    if (R_DIMS > 0 && dest_level == LEVEL::GLOBAL) {
                        code << std::endl << stringf("#if (%s) || L_REDUCTION < %s",
                                                     concat(multi_stringf("%s == 1", _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS))), " && "),
                                                     upper_case(long_level(res_level)));
                        iterate_all_phase_combinations_for_index_conversion<L_DIMS, R_DIMS>(
                                res_level,
                                dim_range(L_DIMS, 0),
                                [&](char *phase) {
                                    code << std::endl << stringf("#define %s %s",
                                                                 call_res_macro(output.name(), kernel, res_level, phase, l_standard_indices),
                                                                 call_kernel_res_macro(output.name(), kernel, LEVEL::PRIVATE, phase, l_standard_indices)).c_str();
                                });
                        code << std::endl << "#else";
                    }
                    if (R_DIMS == 0 && dest_level == LEVEL::GLOBAL) {
                        iterate_all_phase_combinations_for_index_conversion<L_DIMS, R_DIMS>(
                                res_level,
                                dim_range(L_DIMS, 0),
                                [&](char *phase) {
                                    code << std::endl << stringf("#define %s %s",
                                                                 call_res_macro(output.name(), kernel, res_level, phase, l_standard_indices),
                                                                 call_kernel_res_macro(output.name(), kernel, LEVEL::PRIVATE, phase, l_standard_indices)).c_str();
                                });
                    } else {
                        iterate_all_phase_combinations_for_index_conversion<L_DIMS, R_DIMS>(
                                res_level,
                                dim_range(L_DIMS, 0),
                                [&](char *phase) {
                                    auto l_standard_indices_filled = l_standard_indices;
                                    for (int i = 0; i < L_DIMS; ++i) {
                                        for (auto sub : sub_levels(dest_level, false, ASCENDING)) {
                                            l_standard_indices_filled[i] = _macros.index_conversion(kernel, sub, L(i + 1), l_standard_indices_filled[i], phase);
                                        }
                                    }
                                    for (int i = 1; i <= R_DIMS; ++i) {
                                        for (auto sub : sub_levels(dest_level, true, ORDER::DESCENDING)) {
                                            l_standard_indices_filled.push_back(_macros.fu_id(kernel, sub, R(i)));
                                        }
                                    }
                                    code << std::endl << stringf("#define %s %s",
                                                                 call_res_macro(output.name(), kernel, res_level, phase, l_standard_indices),
                                                                 call_buffer_macro(output.name(), kernel, dest_level, l_standard_indices_filled)).c_str();
                                });
                    }
                    if (R_DIMS > 0 && dest_level == LEVEL::GLOBAL) {
                        code << std::endl << "#endif";
                    }
                }
                code << std::endl << "#endif" << std::endl;
            }
            for (LEVEL res_level : {LEVEL::PRIVATE, LEVEL::LOCAL, LEVEL::GLOBAL}) {
                for (LEVEL dest_level : {LEVEL::PRIVATE, LEVEL::LOCAL, LEVEL::GLOBAL}) {
                    code << std::endl << stringf("%s %c_CB_RES_DEST_LEVEL == %s",
                                                 dest_level == LEVEL::PRIVATE ? "#if  " : "#elif",
                                                 res_level,
                                                 long_level(dest_level)).c_str();
                    if (R_DIMS > 0 && dest_level == LEVEL::GLOBAL) {
                        code << std::endl << stringf("#if (%s) || L_REDUCTION < %s",
                                                     concat(multi_stringf("%s == 1", _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS))), " && "),
                                                     upper_case(long_level(res_level)));
                        iterate_all_phase_combinations_for_index_conversion<L_DIMS, R_DIMS>(
                                res_level,
                                dim_range(L_DIMS, 0),
                                [&](char *phase) {
                                    code << std::endl << stringf("#define %s %s",
                                                                 call_fu_res_macro(output.name(), kernel, res_level, phase, l_standard_indices),
                                                                 call_fu_kernel_res_macro(output.name(), kernel, LEVEL::PRIVATE, phase, l_standard_indices)).c_str();
                                });
                        code << std::endl << "#else";
                    }
                    if (R_DIMS == 0 && dest_level == LEVEL::GLOBAL) {
                        iterate_all_phase_combinations_for_index_conversion<L_DIMS, R_DIMS>(
                                res_level,
                                dim_range(L_DIMS, 0),
                                [&](char *phase) {
                                    code << std::endl << stringf("#define %s %s",
                                                                 call_fu_res_macro(output.name(), kernel, res_level, phase, l_standard_indices),
                                                                 call_fu_kernel_res_macro(output.name(), kernel, LEVEL::PRIVATE, phase, l_standard_indices)).c_str();
                                });
                    } else {
                        iterate_all_phase_combinations_for_index_conversion<L_DIMS, R_DIMS>(
                                res_level,
                                dim_range(L_DIMS, 0),
                                [&](char *phase) {
                                    auto l_standard_indices_filled = l_standard_indices;
                                    for (int i = 0; i < L_DIMS; ++i) {
                                        for (auto sub : sub_levels(dest_level, false, ASCENDING)) {
                                            l_standard_indices_filled[i] = _macros.fu_index_conversion(kernel, sub, L(i + 1), l_standard_indices_filled[i], phase);
                                        }
                                    }
                                    for (int i = 1; i <= R_DIMS; ++i) {
                                        for (auto sub : sub_levels(dest_level, true, ORDER::DESCENDING)) {
                                            l_standard_indices_filled.push_back(_macros.fu_id(kernel, sub, R(i)));
                                        }
                                    }
                                    code << std::endl << stringf("#define %s %s",
                                                                 call_fu_res_macro(output.name(), kernel, res_level, phase, l_standard_indices),
                                                                 call_buffer_macro(output.name(), kernel, dest_level, l_standard_indices_filled)).c_str();
                                });
                    }
                    if (R_DIMS > 0 && dest_level == LEVEL::GLOBAL) {
                        code << std::endl << "#endif";
                    }
                }
                code << std::endl << "#endif" << std::endl;
            }

            if (R_DIMS > 0) {
                // TODO only one per result type instead of result buffer
                code << std::endl << "// check if additional local memory for reduction is needed";
                auto params = make_standard_indices(L_DIMS + R_DIMS);
                for (LEVEL reduction_level : {LEVEL::LOCAL}) {
                    code << std::endl << stringf("#if (%s) && (%s)",
                            concat(multi_stringf("%s > 1", _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS))), " || "),
                            concat(multi_stringf("(%c_REDUCTION == %s && %c_CB_RES_DEST_LEVEL < %s)", V(reduction_level), long_level(level_range(LEVEL::PRIVATE, LEVEL::GLOBAL)), level_range(LEVEL::PRIVATE, LEVEL::GLOBAL), V(long_level(reduction_level))), " || "));
                    code << std::endl << stringf("#define %s_NAME() %s_%c_reduction_mem", reduction_mem_macro_name(output.name(), kernel, reduction_level),
                                                 lower_case(output.name()), lower_case(reduction_level));
                    code << std::endl << stringf("#define %s %s_NAME()CONCAT_IN_DESCENDING_OCL_ORDER(",
                                                 call_reduction_mem_macro(output.name(), kernel, reduction_level,
                                                                          make_standard_indices(L_DIMS + R_DIMS)),
                                                 reduction_mem_macro_name(output.name(), kernel, reduction_level));
    #ifdef REDUCTION_COPY_COMPLETE
                    for (unsigned int l_dim = 1; l_dim <= L_DIMS; ++l_dim) {
                        code << stringf("%s%s[%s]",
                                        l_dim > 1 ? ", " : "",
                                        (!_l_dims_after_first_r_dim.empty() && std::find(_l_dims_after_first_r_dim.begin(), _l_dims_after_first_r_dim.end(), L(l_dim)) != _l_dims_after_first_r_dim.end())
                                        ? stringf("[l_step_l_%d]", l_dim) : "",
                                        params[l_dim - 1]);
                    }
                    for (unsigned int r_dim = 1; r_dim <= R_DIMS; ++r_dim) {
                        code << stringf("%s[(%s)]",
                                        L_DIMS > 0 || r_dim > 1 ? ", " : "",
                                        params[L_DIMS + r_dim - 1]);
                    }
    #else
                    code << make_standard_params(L_DIMS + R_DIMS, "[%%val%%]");
    #endif
                    code << ")";
                    code << std::endl << "#endif";
                }
            }

            // kernel_res abstraction
            code << std::endl << std::endl << "// buffer abstraction for kernel_res buffer";
            if (kernel == 1 && R_DIMS > 0) {
                code << std::endl << stringf("#if %s", concat(multi_stringf("%s == 1",
                                                                            _macros.num_fu(V(kernel), V(LEVEL::GLOBAL),
                                                                                           dim_range(0, R_DIMS))), " && "));
                code << std::endl << "// if result is final result, use requested ordering of L-dimensions";
            }
            std::vector<std::string> indices;
            std::vector<std::string> sizes;
            for (int i = 0; i < output.index_functions().size(); ++i) {
                if (output.dimension_sizes().size() > i && !output.dimension_sizes()[i].empty()) {
                    sizes.emplace_back(stringf("(%s)", replace_vars_in_size_function(output, i, _macros.cb_size(V(kernel), V(LEVEL::GLOBAL), dim_range(L_DIMS, R_DIMS)))));
                } else {
                    sizes.emplace_back(stringf("((%s) - (%s) + 1)",
                                               replace_vars_in_index_function(output, i, multi_stringf("(%s - 1)", _macros.cb_size(V(kernel), V(LEVEL::GLOBAL), dim_range(L_DIMS, R_DIMS)))),
                                               replace_vars_in_index_function(output, i, repeat<std::string>("0", L_DIMS + R_DIMS))));
                }
                indices.push_back(replace_vars_in_index_function(output, i, make_standard_indices(L_DIMS + R_DIMS)));
            }
            if (_cuda)
                code << std::endl << stringf("#define K%d_KERNEL_RES_BUFFER_%s(%s) %s%s",
                                             kernel, upper_case(output.name()),
                                             make_standard_params(L_DIMS + R_DIMS),
                                             kernel == 1 ? stringf("int_res_%s", output.name()) : output.name(),
                                             concat(wrap(indices, "[", "]")));
            else
                code << std::endl << stringf("#define K%d_KERNEL_RES_BUFFER_%s(%s) %s[%s]",
                                             kernel, upper_case(output.name()),
                                             make_standard_params(L_DIMS + R_DIMS),
                                             kernel == 1 ? stringf("int_res_%s", output.name()) : output.name(),
                                             make_flat_index(indices, sizes));
            if (kernel == 1 && R_DIMS > 0) {
                code << std::endl << "#else";
                code << std::endl << "// else order in descending OpenCL dimension order";
                if (_cuda)
                    code << std::endl << stringf("#define K%d_KERNEL_RES_BUFFER_%s(%s) int_res_%s%s",
                                                 kernel, upper_case(output.name()),
                                                 make_standard_params(L_DIMS + R_DIMS),
                                                 output.name(),
                                                 concat(wrap(multi_stringf("DESCENDING_DIMS_%d(%s)", uint_range(L_DIMS + R_DIMS - 1, 0), V(make_standard_params(L_DIMS + R_DIMS))), "[", "]")));
                else
                    code << std::endl << stringf("#define K%d_KERNEL_RES_BUFFER_%s(%s) int_res_%s[FLAT_INDEX_IN_DESCENDING_OCL_ORDER(%s, %s)]",
                                                 kernel, upper_case(output.name()),
                                                 make_standard_params(L_DIMS + R_DIMS),
                                                 output.name(),
                                                 make_standard_params(L_DIMS + R_DIMS),
                                                 concat(join(_macros.cb_size(V(kernel), V(LEVEL::GLOBAL), dim_range(L_DIMS, 0)), _macros.num_fu(V(kernel), V(LEVEL::GLOBAL), dim_range(0, R_DIMS))), ", "));
                code << std::endl << "#endif";
            }
            code << std::endl;
            for (LEVEL level : {LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE}) {
                iterate_all_phase_combinations_for_index_conversion<L_DIMS, R_DIMS>(
                        level,
                        dim_range(L_DIMS, 0),
                        [&](char *phase) {
                            auto offset_indices = make_standard_indices(L_DIMS);
                            if (level != LEVEL::GLOBAL) {
                                offset_indices = _macros.index_conversion(V(kernel), V(level), dim_range(L_DIMS, 0), offset_indices, V(phase));
                            }
                            code << std::endl << stringf("#define %s %s(%s)",
                                                         call_kernel_res_macro(output.name(), kernel, level, phase, make_standard_indices(L_DIMS)),
                                                         level == LEVEL::GLOBAL
                                                         ? stringf("K%d_KERNEL_RES_BUFFER_%s", kernel, upper_case(output.name()))
                                                         : kernel_res_macro_name(output.name(), kernel, parent_level(level), phase),
                                                         level == LEVEL::GLOBAL
                                                         ? concat(join(offset_indices, _macros.fu_id(V(kernel), V(LEVEL::GLOBAL), dim_range(0, R_DIMS))), ", ")
                                                         : concat(offset_indices, ", "));
                        });
            }
            for (LEVEL level : {LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE}) {
                iterate_all_phase_combinations_for_index_conversion<L_DIMS, R_DIMS>(
                        level,
                        dim_range(L_DIMS, 0),
                        [&](char *phase) {
                            auto offset_indices = make_standard_indices(L_DIMS);
                            if (level != LEVEL::GLOBAL) {
                                offset_indices = _macros.fu_index_conversion(V(kernel), V(level), dim_range(L_DIMS, 0), offset_indices, V(phase));
                            }
                            code << std::endl << stringf("#define %s %s(%s)",
                                                         call_fu_kernel_res_macro(output.name(), kernel, level, phase, make_standard_indices(L_DIMS)),
                                                         level == LEVEL::GLOBAL
                                                         ? stringf("K%d_KERNEL_RES_BUFFER_%s", kernel, upper_case(output.name()))
                                                         : fu_kernel_res_macro_name(output.name(), kernel, parent_level(level), phase),
                                                         level == LEVEL::GLOBAL
                                                         ? concat(join(offset_indices, _macros.fu_id(V(kernel), V(LEVEL::GLOBAL), dim_range(0, R_DIMS))), ", ")
                                                         : concat(offset_indices, ", "));
                        });
            }
        });
        return code.str();
    }

    const std::string variable_declarations(unsigned int kernel) const {
        std::stringstream code;
        code << "// declare variables for result memory";
        for_each(_result_buffer, [&](const result_buffer_class &output) {
            for (LEVEL res_level : {LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE}) {
                if (res_level == LEVEL::GLOBAL) {
                    if (_cuda) {
                        std::vector<std::string> sizes;
                        for (const auto dim : dim_range(L_DIMS, 0)) {
                            sizes.push_back(stringf("[%s]", _macros.cb_size(kernel, LEVEL::GLOBAL, dim)));
                        }
                        for (const auto dim : dim_range(0, R_DIMS)) {
                            sizes.push_back(stringf("%s_SUFFIX_%c_%d", buffer_def_macro_name(output.name(), kernel, res_level), dim.type, dim.nr));
                        }
                        code << std::endl << stringf(
                                "%s (*res_g_%s)%s = reinterpret_cast<%s (*)%s>(res_g_%s_raw);",
                                output.data_type(),
                                output.name(),
                                L_DIMS + R_DIMS > 1 ? concat(multi_stringf("DESCENDING_DIMS_%d(%s)", uint_range(L_DIMS + R_DIMS - 2, 0), V(concat(sizes, ", ")))) : "",
                                output.data_type(),
                                L_DIMS + R_DIMS > 1 ? concat(multi_stringf("DESCENDING_DIMS_%d(%s)", uint_range(L_DIMS + R_DIMS - 2, 0), V(concat(sizes, ", ")))) : "",
                                output.name()
                        );
                        if (kernel == 1) {
                            code << std::endl << stringf("#if %s", R_DIMS == 0 ? "1" : concat(multi_stringf("%s == 1", _macros.num_fu(V(kernel), V(LEVEL::GLOBAL), dim_range(0, R_DIMS))), " && "));
                        }
                        sizes.clear();
                        for (int i = 1; i < output.index_functions().size(); ++i) {
                            if (output.dimension_sizes().size() > i && !output.dimension_sizes()[i].empty()) {
                                sizes.emplace_back(stringf("(%s)", replace_vars_in_size_function(output, i, _macros.cb_size(V(kernel), V(LEVEL::GLOBAL), dim_range(L_DIMS, R_DIMS)))));
                            } else {
                                sizes.emplace_back(stringf("((%s) - (%s) + 1)",
                                                           replace_vars_in_index_function(output, i, multi_stringf("(%s - 1)", _macros.cb_size(V(kernel), V(LEVEL::GLOBAL), dim_range(L_DIMS, R_DIMS)))),
                                                           replace_vars_in_index_function(output, i, repeat<std::string>("0", L_DIMS + R_DIMS))));
                            }
                        }
                        code << std::endl << stringf(
                                "%s (*%s)%s = reinterpret_cast<%s (*)%s>(%s);",
                                output.data_type(),
                                kernel == 1 ? stringf("int_res_%s", output.name()) : stringf("%s", output.name()),
                                concat(wrap(sizes, "[", "]")),
                                output.data_type(),
                                concat(wrap(sizes, "[", "]")),
                                kernel == 1 ? stringf("int_res_%s_raw", output.name()) : stringf("%s_raw", output.name())
                        );
                        if (kernel == 1) {
                            code << std::endl << "#else";
                            code << std::endl << stringf(
                                    "%s (*int_res_%s)%s = reinterpret_cast<%s (*)%s>(int_res_%s_raw);",
                                    output.data_type(),
                                    output.name(),
                                    L_DIMS + R_DIMS > 1 ? concat(wrap(multi_stringf("DESCENDING_DIMS_%d(%s)", uint_range(L_DIMS + R_DIMS - 2, 0), V(concat(join(_macros.cb_size(V(kernel), V(LEVEL::GLOBAL), dim_range(L_DIMS, 0)), _macros.num_fu(V(kernel), V(LEVEL::GLOBAL), dim_range(0, R_DIMS))), ", "))), "[", "]")) : "",
                                    output.data_type(),
                                    L_DIMS + R_DIMS > 1 ? concat(wrap(multi_stringf("DESCENDING_DIMS_%d(%s)", uint_range(L_DIMS + R_DIMS - 2, 0), V(concat(join(_macros.cb_size(V(kernel), V(LEVEL::GLOBAL), dim_range(L_DIMS, 0)), _macros.num_fu(V(kernel), V(LEVEL::GLOBAL), dim_range(0, R_DIMS))), ", "))), "[", "]")) : "",
                                    output.name()
                            );
                            code << std::endl << "#endif";
                        }
                    } else {
                        code << std::endl << stringf(
                                "__global %s * const restrict res_g_%s = res_g_%s_raw;",
                                output.data_type(),
                                output.name(),
                                output.name()
                        );
                        if (kernel == 1) {
                            code << std::endl << stringf(
                                    "__global %s * const restrict int_res_%s = int_res_%s_raw;",
                                    output.data_type(),
                                    output.name(),
                                    output.name()
                            );
                        } else {
                            code << std::endl << stringf(
                                    "__global %s * const restrict %s = %s_raw;",
                                    output.data_type(),
                                    output.name(),
                                    output.name()
                            );
                        }
                    }
                } else {
                    code << std::endl << stringf("// ------ %s ------", long_level(res_level));
                    code << std::endl << stringf("#ifdef K%d_%c_LEVEL_HAS_RESULTS", kernel, res_level);
                    code << std::endl << stringf("%s%s %s;", res_level == LEVEL::LOCAL ? (_cuda ? "__shared__ " : "__local ") : (_cuda ? "" : "__private "), output.data_type(), buffer_def_macro_name(output.name(), kernel, res_level));
                    code << std::endl << "#endif";
                }
            }
            if (R_DIMS > 0) {
                code << std::endl << std::endl << "// declare variables for reduction memory (if needed)";
                for (LEVEL reduction_level : {LEVEL::LOCAL}) {
                    code << std::endl << stringf("#if (%s) && (%s)",
                            concat(multi_stringf("%s > 1", _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS))), " || "),
                            concat(multi_stringf("(%c_REDUCTION == %s && %c_CB_RES_DEST_LEVEL < %s)", V(reduction_level), long_level(level_range(LEVEL::PRIVATE, LEVEL::GLOBAL)), level_range(LEVEL::PRIVATE, LEVEL::GLOBAL), V(long_level(reduction_level))), " || "));
    #ifdef REDUCTION_COPY_COMPLETE
                    code << std::endl << stringf("%s %s %s_NAME()CONCAT_IN_DESCENDING_OCL_ORDER(",
                                                 _cuda ? "__shared__" : "__local",
                                                 output.data_type(),
                                                 reduction_mem_macro_name(output.name(), kernel, reduction_level));
                    for (unsigned int l_dim = 1; l_dim <= L_DIMS; ++l_dim) {
                        code << (l_dim > 1 ? ", " : "");
                        if (!_l_dims_after_first_r_dim.empty() && std::find(_l_dims_after_first_r_dim.begin(), _l_dims_after_first_r_dim.end(), L(l_dim)) != _l_dims_after_first_r_dim.end()) {
                            code << stringf("[%s + (%s > 0) + (%s > 0)]",
                                            _macros.num_steps(kernel, reduction_level, L(l_dim)),
                                            _macros.num_extra_cached_iterations(kernel, reduction_level, L(l_dim)),
                                            _macros.num_extra_elements(kernel, reduction_level, L(l_dim))
                            );
                        }
                        code << "[" << _macros.cb_size(kernel, reduction_level, L(l_dim)) << "]";
                    }
                    for (unsigned int r_dim = 1; r_dim <= R_DIMS; ++r_dim) {
                        code << (L_DIMS > 0 || r_dim > 1 ? ", " : "") << "["
                             << _macros.num_fu(kernel, reduction_level, R(r_dim)) << "]";
                    }
    #else
                    if (_cuda) {
                        code << std::endl << stringf("#if %s", shuffle_reduction_condition(kernel));
                        code << std::endl << stringf("%s %s %s_NAME()CONCAT_IN_DESCENDING_OCL_ORDER(",
                                                     _cuda ? "__shared__" : "__local",
                                                     output.data_type(),
                                                     reduction_mem_macro_name(output.name(), kernel, reduction_level));
                        code << concat(multi_stringf("[(%s + 31 ) / 32]", _macros.num_fu(V(kernel), V(reduction_level), dim_range(L_DIMS, R_DIMS))), ", ");
                        code << ");";
                        code << std::endl << "#else";
                    }
                    code << std::endl << stringf("%s %s %s_NAME()CONCAT_IN_DESCENDING_OCL_ORDER(",
                                                 _cuda ? "__shared__" : "__local",
                                                 output.data_type(),
                                                 reduction_mem_macro_name(output.name(), kernel, reduction_level));
                    code << concat(wrap(_macros.num_fu(V(kernel), V(reduction_level), dim_range(L_DIMS, R_DIMS)), "[", "]"), ", ");
                    code << ");";
                    if (_cuda) {
                        code << std::endl << "#endif";
                    }
    #endif
                    code << std::endl << "#endif";
                }
            }
        });
        return code.str();
    }

    const std::string reduction(unsigned int kernel, LEVEL fu_level, LEVEL cb_level, char *phase, bool *first_iteration, const std::vector<std::string> &input_names) const {
        std::stringstream code;

        if (fu_level == LEVEL::PRIVATE) {
            code << "// ---------- reduction --------------------";
            code << std::endl << "// will never be necessary on this level";
            code << std::endl << stringf("// because K%d_P_NUM_FU_R == 1 for OpenCL", kernel);
            code << std::endl << "// ---------- end of reduction -------------";
        } else {
            code << stringf("#if %s", concat(multi_stringf("%s > 1", _macros.num_fu(V(kernel), V(fu_level), dim_range(0, R_DIMS))), " || ")) << std::endl;
            code << wrap_in_active_condition(kernel, fu_level, phase, reduction_impl(kernel, fu_level, cb_level, phase, first_iteration, input_names)).c_str() << std::endl;
            code << "#endif";
        }
        return code.str();
    }

    const std::string write_back_code(unsigned int kernel, LEVEL level, char *phase, bool *first_iteration) const {
        std::stringstream code;

        std::vector<std::string> num_elems_to_reduce(R_DIMS);
        for (const auto r_dim : dim_range(0, R_DIMS)) {
            if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(level, r_dim)] == 3) {
                num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(r_dim) - L_DIMS] = stringf("P3_NUM_ACTIVE_WI_R_%d", r_dim.nr);
            } else if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(level, r_dim)] == 2) {
                LEVEL first_complete_level = LEVEL::GLOBAL;
                for (const auto p_level : parent_levels(level, false, ORDER::ASCENDING)) {
                    if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(p_level, r_dim)] == 1) {
                        first_complete_level = p_level;
                        break;
                    }
                }
                num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(r_dim) - L_DIMS] = stringf("NUM_ACTIVE_WI_IN_COMPLETE_%c_CB_R_%d", first_complete_level,
                                                                                                 r_dim.nr);
            } else {
                num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(r_dim) - L_DIMS] = stringf("NUM_ACTIVE_WI_R_%d", r_dim.nr);
            }
        }

        if (L_DIMS > 0 && level != LEVEL::PRIVATE) {
            std::stringstream flat_write_back_code;
            // flat write back
            code << std::endl << stringf("#if %c_CB_RES_DEST_LEVEL >= %s%s", level, upper_case(long_level(LEVEL::LOCAL)),
#ifdef REDUCTION_COPY_COMPLETE
                                         R_DIMS > 0 ? stringf(" || (L_REDUCTION == %s && (%s))", upper_case(long_level(level)), concat(multi_stringf("%s > 1", _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS))), " || ")) : ""
#else
                                         ""
#endif
            );
            code << std::endl << "// flat write back";
            // check if in first iteration if necessary
            bool first_iteration_in_parent_level = true;
            for (const auto dim : dim_range(0, R_DIMS)) {
                first_iteration_in_parent_level = first_iteration_in_parent_level && first_iteration[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dim)];
            }
            bool reduce = false;
            if (R_DIMS > 0 && !first_iteration_in_parent_level) {
                reduce = true;
            }
            // wait for WIs to finish if synchronization was not done by reduction
            if (R_DIMS > 0)
                flat_write_back_code << std::endl << stringf("#if L_REDUCTION > %s || (%s)", upper_case(long_level(level)), concat(multi_stringf("%s == 1", _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS))), " && "));
            // barrier now also necessary when caching is enabled, because caching code does no longer add a barrier after processing the cache block
//            flat_write_back_code << std::endl << stringf("#if %c_CB_RES_DEST_LEVEL <= %s && CACHE_%c_CB == 0", level, long_level(level), level);
            flat_write_back_code << std::endl << stringf("#if %c_CB_RES_DEST_LEVEL <= %s", level, long_level(level));
            flat_write_back_code << std::endl << stringf("%s%s", level == LEVEL::PRIVATE ? "// " : "", barrier(level, _cuda));
            for (LEVEL p_level : parent_levels(level, false, ORDER::ASCENDING)) {
                flat_write_back_code << std::endl << stringf("#elif %c_CB_RES_DEST_LEVEL == %s", level, long_level(p_level));
                flat_write_back_code << std::endl << barrier(p_level, _cuda);
            }
            flat_write_back_code << std::endl << "#endif"; // for #if of barrier
            if (R_DIMS > 0)
                flat_write_back_code << std::endl << "#endif";
            // TODO completely flat write back
            auto loops = loop_generator<L_DIMS, R_DIMS>(kernel, _macros, _runtime_inputs, V(LEVEL::LOCAL), level == LEVEL::GLOBAL ? dim_range(L_DIMS, 0) : _l_dims_after_first_r_dim, false, nullptr, false, _skip_post_processing);
            flat_write_back_code << std::endl << loops.from(
                    kernel, LEVEL::LOCAL, level == LEVEL::GLOBAL || _l_dims_after_first_r_dim.empty() ? L(1) : _l_dims_after_first_r_dim.front(),
                    [&] (auto inner_level, const auto &inner_dimension, char *inner_phase, bool *first_iteration) -> std::string {
                        auto l_indices = multi_stringf("l_index_%c_%d", lower_case(dim_range_types(L_DIMS, 0)), dim_range_nrs(L_DIMS, 0));
#ifdef REDUCTION_COPY_COMPLETE
                        auto reduction_mem_indices = l_indices;
                        auto reduction_mem_indices_with_r_dims = l_indices;
                        for (int i = 0; i < R_DIMS; ++i) {
                            reduction_mem_indices.push_back("0");
                            reduction_mem_indices_with_r_dims.push_back(std::string("l_fu_r_") + std::to_string(i + 1));
                        }
#endif
                        auto res_l_indices = l_indices;
                        auto res_l_indices_with_r_dims = l_indices;
                        for (int i = 0; i < R_DIMS * 2; ++i) {
                            res_l_indices.push_back("0");
                            if (i % 2 == 0)
                                res_l_indices_with_r_dims.push_back(std::string("l_fu_r_") + std::to_string((i / 2) + 1));
                            else
                                res_l_indices_with_r_dims.push_back("0");
                        }
                        auto res_g_indices = l_indices;
                        for (const LEVEL conv_level : level_range(LEVEL::LOCAL, LEVEL::LOCAL)) {
                            res_g_indices = _macros.index_conversion(V(kernel), V(conv_level), dim_range(L_DIMS, 0), res_g_indices, V(inner_phase));
                        }
                        auto res_g_indices_with_r_dims = res_g_indices;
                        for (int i = 0; i < R_DIMS; ++i) {
                            res_g_indices.push_back(std::string("i_wg_r_") + std::to_string(i + 1));
                            res_g_indices.push_back("0");
                            res_g_indices.push_back("0");
                            res_g_indices_with_r_dims.push_back(std::string("i_wg_r_") + std::to_string(i + 1));
                            res_g_indices_with_r_dims.push_back(std::string("l_fu_r_") + std::to_string(i + 1));
                            res_g_indices_with_r_dims.push_back("0");
                        }
                        auto kernel_res_indices = l_indices;
                        for (const LEVEL conv_level : level_range(LEVEL::LOCAL, LEVEL::LOCAL)) {
                            kernel_res_indices = _macros.index_conversion(V(kernel), V(conv_level), dim_range(L_DIMS, 0), kernel_res_indices, V(inner_phase));
                        }
                        for (int i = 0; i < R_DIMS; ++i) {
                            kernel_res_indices.push_back(std::string("i_wg_r_") + std::to_string(i + 1));
                        }
                        auto indices = multi_stringf("l_index_%c_%d", lower_case(split_dim_range(std::get<0>(_result_buffer).dimension_order()).first), split_dim_range(std::get<0>(_result_buffer).dimension_order()).second);
                        auto indices_descending = multi_stringf("DESCENDING_L_DIMS_%d(%s)",
                                                                uint_range(std::get<0>(_result_buffer).dimension_order().size() - 1, 0),
                                                                V(concat(multi_stringf("l_index_%c_%d", lower_case(dim_range_types(L_DIMS, 0)), dim_range_nrs(L_DIMS, 0)), ", "))
                        );
                        auto indices_with_r_dims_descending = multi_stringf("DESCENDING_DIMS_%d(%s)",
                                                                uint_range(L_DIMS + R_DIMS - 1, 0),
                                                                V(concat(join(multi_stringf("l_index_%c_%d", lower_case(dim_range_types(L_DIMS, 0)), dim_range_nrs(L_DIMS, 0)),
                                                                              multi_stringf("l_fu_%c_%d", lower_case(dim_range_types(0, R_DIMS)), dim_range_nrs(0, R_DIMS))), ", "))
                        );
                        std::vector<std::string> dim_sizes;
                        for (const auto &dim : std::get<0>(_result_buffer).dimension_order()) {
                            dim_sizes.push_back(_macros.num_processed_elements(kernel, LEVEL::LOCAL, dim, inner_phase));
                        }
                        std::vector<std::string> dim_sizes_descending = multi_stringf("DESCENDING_L_DIMS_%d(%s)",
                                                                                      uint_range(L_DIMS - 1, 0),
                                                                                      V(concat(_macros.num_processed_elements(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0), V(inner_phase)), ", "))
                        );
                        std::vector<std::string> dim_sizes_with_r_dims_descending = multi_stringf("DESCENDING_DIMS_%d(%s)",
                                                                                                  uint_range(L_DIMS + R_DIMS - 1, 0),
                                                                                                  V(concat(join(_macros.num_processed_elements(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0), V(inner_phase)), num_elems_to_reduce), ", "))
                        );
                        bool used_dimension_in_phase3 = false;
                        for (const auto &dim : dim_range(L_DIMS, 0)) {
                            if (inner_phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(LEVEL::LOCAL, dim)] == 3) {
                                used_dimension_in_phase3 = true;
                            }
                        }
                        std::string num_elements_to_write = concat(_macros.num_processed_elements(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0), V(inner_phase)), " * ");
                        bool static_num_elems_to_write = true;
                        for (const auto &dim : dim_range(L_DIMS, 0)) {
                            for (LEVEL p_level : parent_levels(LEVEL::LOCAL, true)) {
                                if (inner_phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(p_level, dim)] == 3) {
                                    static_num_elems_to_write = false;
                                    break;
                                }
                            }
                            if (_runtime_inputs && inner_phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(LEVEL::LOCAL, dim)] == 2) {
                                static_num_elems_to_write = false;
                                break;
                            }
                            if (!static_num_elems_to_write) break;
                        }

                        std::string buffer_assignments;
                        for_each(_result_buffer, [&](const result_buffer_class &output) {
                            std::string pattern = R"(#if %c_CB_RES_DEST_LEVEL == LOCAL
  %s
#elif %c_CB_RES_DEST_LEVEL == GLOBAL
  #if (%s) || L_REDUCTION <= %s
  %s
  #else
  %s
  #endif
#endif
)";
                            std::string lhs_after_reduction = stringf(pattern, parent_level(level),
                                                                       this->call_buffer_macro(output.name(), kernel, LEVEL::LOCAL, res_l_indices),
                                                                       parent_level(level),
                                                                       R_DIMS == 0 ? "1" : concat(multi_stringf("%s == 1", _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS))), " && "),
                                                                       long_level(level),
                                                                       this->call_kernel_res_buffer_macro(output.name(), kernel, kernel_res_indices),
                                                                       this->call_buffer_macro(output.name(), kernel, LEVEL::GLOBAL, res_g_indices)
                            );
                            std::string lhs_before_reduction = stringf(pattern, parent_level(level),
                                                                       this->call_buffer_macro(output.name(), kernel, LEVEL::LOCAL, R_DIMS == 0 || level == LEVEL::GLOBAL ? res_l_indices : res_l_indices_with_r_dims),
                                                                       parent_level(level),
                                                                       R_DIMS == 0 ? "1" : concat(multi_stringf("%s == 1", _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS))), " && "),
                                                                       long_level(level),
                                                                       this->call_kernel_res_buffer_macro(output.name(), kernel, kernel_res_indices),
                                                                       this->call_buffer_macro(output.name(), kernel, LEVEL::GLOBAL, R_DIMS == 0 || level == LEVEL::GLOBAL ? res_g_indices : res_g_indices_with_r_dims));
                            pattern = R"(
#if %c_CB_RES_DEST_LEVEL < LOCAL
  %s;
#elif %c_CB_RES_DEST_LEVEL == LOCAL
  %s;
#elif %c_CB_RES_DEST_LEVEL == GLOBAL
  %s;
#endif)";
                            std::string rhs_after_reduction = stringf(pattern, level,
#ifdef REDUCTION_COPY_COMPLETE
                                                                      this->call_reduction_mem_macro(output.name(), kernel, LEVEL::LOCAL, reduction_mem_indices),
#else
                                                                      this->call_buffer_macro(output.name(), kernel, LEVEL::LOCAL, res_l_indices),
#endif
                                                                      level,
                                                                      this->call_buffer_macro(output.name(), kernel, LEVEL::LOCAL, res_l_indices),
                                                                      level,
                                                                      this->call_buffer_macro(output.name(), kernel, LEVEL::GLOBAL, res_g_indices));
                            std::string rhs_before_reduction = stringf(pattern, level,
#ifdef REDUCTION_COPY_COMPLETE
                                                                       this->call_reduction_mem_macro(output.name(), kernel, LEVEL::LOCAL, R_DIMS == 0 || level == LEVEL::GLOBAL ? reduction_mem_indices : reduction_mem_indices_with_r_dims),
#else
                                                                       this->call_buffer_macro(output.name(), kernel, LEVEL::LOCAL, R_DIMS == 0 || level == LEVEL::GLOBAL ? res_l_indices : res_l_indices_with_r_dims),
#endif
                                                                       level,
                                                                       this->call_buffer_macro(output.name(), kernel, LEVEL::LOCAL, R_DIMS == 0 || level == LEVEL::GLOBAL ? res_l_indices : res_l_indices_with_r_dims),
                                                                       level,
                                                                       this->call_buffer_macro(output.name(), kernel, LEVEL::GLOBAL, R_DIMS == 0 || level == LEVEL::GLOBAL ? res_g_indices : res_g_indices_with_r_dims));
                            if (reduce) {
                                buffer_assignments += stringf(R"(
#if L_REDUCTION <= %s
%s
#else
%s
#endif)", long_level(level), output.reduce_and_store(lhs_after_reduction, rhs_after_reduction), output.reduce_and_store(lhs_before_reduction, rhs_before_reduction));
                            } else {
                                buffer_assignments += stringf(R"(
#if L_REDUCTION <= %s
%s
#else
%s
#endif)", long_level(level), stringf("%s = %s", lhs_after_reduction, rhs_after_reduction), stringf("%s = %s", lhs_before_reduction, rhs_before_reduction));
                            }
                        });
                        return stringf(R"(%s
%sfor (size_t step = 0; step < (NUM_ELEMS_TO_WRITE) / %s; ++step) {
  const size_t flat_index = %s + step * %s;
  #if %s && %s
%s
  #else
%s
  #endif
%s
}
%sif (%s < (NUM_ELEMS_TO_WRITE) %% %s) {
  const size_t flat_index = %s + ((NUM_ELEMS_TO_WRITE) / %s) * %s;
  #if %s && %s
%s
  #else
%s
  #endif
%s
}%s
#undef NUM_ELEMS_TO_WRITE
#ifdef STATIC_NUM_ELEMS_TO_WRITE
#undef STATIC_NUM_ELEMS_TO_WRITE
#endif)",
                                       R_DIMS > 0
                                       ? stringf("#if L_REDUCTION > %s\n"
                                                 "#define NUM_ELEMS_TO_WRITE (%s * %s)\n"
                                                 "#if %s%s\n"
                                                 "#define STATIC_NUM_ELEMS_TO_WRITE NUM_ELEMS_TO_WRITE\n"
                                                 "#endif\n"
                                                 "#else\n"
                                                 "#define NUM_ELEMS_TO_WRITE (%s)\n"
                                                 "%s"
                                                 "#endif",
                                                 long_level(level),
                                                 num_elements_to_write, concat(num_elems_to_reduce, " * "),
                                                 concat(multi_stringf("defined(STATIC_%s)", num_elems_to_reduce), " && "),
                                                 !static_num_elems_to_write ? " && 0" : "",
                                                 num_elements_to_write,
                                                 static_num_elems_to_write ? "#define STATIC_NUM_ELEMS_TO_WRITE NUM_ELEMS_TO_WRITE\n" : ""
                                       )
                                       : stringf("#define NUM_ELEMS_TO_WRITE (%s)%s", num_elements_to_write, static_num_elems_to_write ? "\n#define STATIC_NUM_ELEMS_TO_WRITE NUM_ELEMS_TO_WRITE" : ""),
                                       used_dimension_in_phase3
                                       ? ""
                                       : stringf("#if !defined(STATIC_NUM_ELEMS_TO_WRITE) || (STATIC_NUM_ELEMS_TO_WRITE) / %s > 0\n", _macros.flat_num_wi(kernel, LEVEL::LOCAL)),
                                       _macros.flat_num_wi(kernel, LEVEL::LOCAL),
                                       _macros.flat_wi_id(kernel, LEVEL::LOCAL), _macros.flat_num_wi(kernel, LEVEL::LOCAL),
                                       R_DIMS == 0 ? "1" : concat(multi_stringf("%s == 1", _macros.num_fu(V(kernel), V(LEVEL::GLOBAL), dim_range(0, R_DIMS))), " && "),
                                       R_DIMS == 0 ? "1" : stringf("L_REDUCTION <= %s", long_level(level)),
                                       indent(resolve_flat_index("flat_index", indices, dim_sizes), 1),
                                       indent(R_DIMS == 0 ? resolve_flat_index("flat_index", indices_descending, dim_sizes_descending) : stringf("#if L_REDUCTION <= %s\n%s\n#else\n%s\n#endif", long_level(level), resolve_flat_index("flat_index", indices_descending, dim_sizes_descending), resolve_flat_index("flat_index", indices_with_r_dims_descending, dim_sizes_with_r_dims_descending)), 1),
                                       buffer_assignments,
                                       used_dimension_in_phase3
                                       ? ""
                                       : stringf("#endif\n#if !defined(STATIC_NUM_ELEMS_TO_WRITE) || (STATIC_NUM_ELEMS_TO_WRITE) %% %s > 0\n", _macros.flat_num_wi(kernel, LEVEL::LOCAL)),
                                       _macros.flat_wi_id(kernel, LEVEL::LOCAL),
                                       _macros.flat_num_wi(kernel, LEVEL::LOCAL),
                                       _macros.flat_wi_id(kernel, LEVEL::LOCAL),
                                       _macros.flat_num_wi(kernel, LEVEL::LOCAL),
                                       _macros.flat_num_wi(kernel, LEVEL::LOCAL),
                                       R_DIMS == 0 ? "1" : concat(multi_stringf("%s == 1", _macros.num_fu(V(kernel), V(LEVEL::GLOBAL), dim_range(0, R_DIMS))), " && "),
                                       R_DIMS == 0 ? "1" : stringf("L_REDUCTION <= %s", long_level(level)),
                                       indent(resolve_flat_index("flat_index", indices, dim_sizes), 1),
                                       indent(R_DIMS == 0 ? resolve_flat_index("flat_index", indices_descending, dim_sizes_descending) : stringf("#if L_REDUCTION <= %s\n%s\n#else\n%s\n#endif", long_level(level), resolve_flat_index("flat_index", indices_descending, dim_sizes_descending), resolve_flat_index("flat_index", indices_with_r_dims_descending, dim_sizes_with_r_dims_descending)), 1),
                                       buffer_assignments,
                                       used_dimension_in_phase3 ? "" : "\n#endif");
                    }, true, phase
            );
            flat_write_back_code << std::endl << stringf("#if %c_CB_RES_DEST_LEVEL <= %s", level, long_level(level));
#ifdef REDUCTION_COPY_COMPLETE
            flat_write_back_code << std::endl << barrier(LEVEL::LOCAL, _cuda);
#else
            flat_write_back_code << std::endl << stringf("%s%s", level == LEVEL::PRIVATE ? "// " : "", barrier(level, _cuda));
#endif
            for (LEVEL p_level : parent_levels(level, false, ORDER::ASCENDING)) {
                flat_write_back_code << std::endl << stringf("#elif %c_CB_RES_DEST_LEVEL == %s", level, long_level(p_level));
                flat_write_back_code << std::endl << barrier(p_level, _cuda);
            }
            flat_write_back_code << std::endl << "#endif"; // for #if of barrier
            // wrap in active condition
            if (R_DIMS > 0 && level != LEVEL::PRIVATE) {
                code << std::endl << wrap_in_active_condition(kernel, level == LEVEL::GLOBAL ? LEVEL::LOCAL : level, phase, flat_write_back_code.str());
            } else {
                code << std::endl << flat_write_back_code.str();
            }
            code << std::endl << "#else";
            code << std::endl << "// structured write back";
        }

        // structured write back
        bool first_iteration_in_parent_level = true;
        for (const auto dim : dim_range(0, R_DIMS)) {
            first_iteration_in_parent_level = first_iteration_in_parent_level && first_iteration[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dim)];
        }
        std::string structured_write_back_code;
        auto params = _macros.iteration_to_index_conversion(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), iterations_loop_variable(LEVEL::PRIVATE, dim_range(L_DIMS, 0)));
        auto write_back_code = [&] (auto inner_level, const auto &inner_dimension, char *inner_phase, bool *first_iteration) -> std::string {
            std::string assignment_code;
            for_each(_result_buffer, [&](const result_buffer_class &output) {
                if (!assignment_code.empty()) assignment_code += "\n";
                assignment_code += stringf("%s = %s;",
                                           level == LEVEL::GLOBAL
                                           ? this->call_fu_kernel_res_macro(output.name(), kernel, LEVEL::PRIVATE, inner_phase, params)
                                           : this->call_fu_res_macro(output.name(), kernel, parent_level(level), inner_phase, params),
                                           this->call_fu_res_macro(output.name(), kernel, level, inner_phase, params));
            });
            if (R_DIMS > 0) {
                std::string reduce_code;
                for_each(_result_buffer, [&](const result_buffer_class &output) {
                    if (!reduce_code.empty()) reduce_code += "\n";
                    reduce_code += stringf("#if %c_CB_RES_DEST_LEVEL < %s%s\n%s;\n#else\n%s;\n#endif",
                                           level, upper_case(long_level(LEVEL::LOCAL)),
                                           stringf(" && (L_REDUCTION == %s && (%s))", upper_case(long_level(level)), concat(multi_stringf("%s > 1", _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS))), " || ")),
                                           output.reduce_and_store(
                                                   level == LEVEL::GLOBAL
                                                   ? this->call_fu_kernel_res_macro(output.name(), kernel, LEVEL::PRIVATE, inner_phase, params)
                                                   : this->call_fu_res_macro(output.name(), kernel, parent_level(level), inner_phase, params),
#ifdef REDUCTION_COPY_COMPLETE
                                                   this->call_reduction_mem_macro(output.name(), kernel, LEVEL::LOCAL, join(_macros.fu_index_conversion(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), params, V(phase)), _macros.fu_id(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS))))
#else
                                                   this->call_fu_res_macro(output.name(), kernel, level, inner_phase, params)
#endif
                                           ),
                                           output.reduce_and_store(
                                                   level == LEVEL::GLOBAL
                                                   ? this->call_fu_kernel_res_macro(output.name(), kernel, LEVEL::PRIVATE, inner_phase, params)
                                                   : this->call_fu_res_macro(output.name(), kernel, parent_level(level), inner_phase, params),
                                                   this->call_fu_res_macro(output.name(), kernel, level, inner_phase, params)
                                           )
                    );
                });
                // necessary to check wether values have to be read from reduction memory
                return wrap_in_iterations_loop<L_DIMS, R_DIMS>(
                        kernel, LEVEL::PRIVATE, dim_range(L_DIMS, 0),
                        first_iteration_in_parent_level ? assignment_code : reduce_code,
                        true, inner_phase);
            } else {
                return wrap_in_iterations_loop<L_DIMS, R_DIMS>(kernel, LEVEL::PRIVATE, dim_range(L_DIMS, 0), assignment_code, true, inner_phase);
            }
        };
        // determine which cb loops are necessary in L-DIMs
        if (L_DIMS > 0) {
            switch (level) {
                case LEVEL::PRIVATE: {
                    auto loops = loop_generator<L_DIMS, R_DIMS>(kernel, _macros, _runtime_inputs, V(LEVEL::PRIVATE), _l_dims_after_first_r_dim, false, nullptr, false, _skip_post_processing);
                    structured_write_back_code = loops.from(kernel, LEVEL::PRIVATE, _l_dims_after_first_r_dim.empty() ? L(1) : _l_dims_after_first_r_dim.front(), write_back_code, true, phase);
                    break;
                }
                case LEVEL::LOCAL: {
                    auto loops = loop_generator<L_DIMS, R_DIMS>(kernel, _macros, _runtime_inputs, V(LEVEL::LOCAL), _l_dims_after_first_r_dim, false, nullptr, false, _skip_post_processing);
                    auto inner_loops = loop_generator<L_DIMS, R_DIMS>(kernel, _macros, _runtime_inputs, V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), false, nullptr, false, _skip_post_processing);
                    structured_write_back_code = loops.from(
                            kernel, LEVEL::LOCAL, _l_dims_after_first_r_dim.empty() ? L(1) : _l_dims_after_first_r_dim.front(),
                            [&] (auto inner_level, const auto &inner_dimension, char *inner_phase, bool *first_iteration) -> std::string {
                                return inner_loops.from(kernel, LEVEL::PRIVATE, L(1), write_back_code, true, inner_phase);
                            }, true, phase
                    );
                    break;
                }
                case LEVEL::GLOBAL: {
                    auto loops = loop_generator<L_DIMS, R_DIMS>(kernel, _macros, _runtime_inputs, V(LEVEL::LOCAL, LEVEL::PRIVATE), dim_range(L_DIMS, 0), false, nullptr, false, _skip_post_processing);
                    structured_write_back_code = loops.from(kernel, LEVEL::LOCAL, L(1), write_back_code, true, phase);
                    break;
                }
            }
        } else {
            structured_write_back_code = write_back_code(level, L(1) /*will not be used*/, phase, first_iteration);
        }
        // check if reduction was already performed or not all WI in R dimensions were active
        if (R_DIMS > 0) {
            structured_write_back_code = stringf("#if L_REDUCTION <= %s\nif (%s)\n#elif %s\nif (true\n%s\n)\n#endif\n{\n%s\n}",
                                                 upper_case(long_level(level)),
                                                 concat(multi_stringf("%s == 0", _macros.fu_id(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS))), " && "),
                                                 concat(multi_stringf("(!defined(STATIC_%s) || STATIC_%s < %s)", num_elems_to_reduce, num_elems_to_reduce, _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS))), " || "),
                                                 concat(multi_stringf("#if !defined(STATIC_%s) || STATIC_%s < %s\n    && (%s < %s)\n#endif", num_elems_to_reduce, num_elems_to_reduce, _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)), _macros.fu_id(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)), num_elems_to_reduce), "\n"),
                                                 indent(structured_write_back_code, 1));
        }
        // wrap in active condition
        if (R_DIMS > 0) {
            structured_write_back_code = wrap_in_active_condition(kernel, level == LEVEL::GLOBAL ? LEVEL::LOCAL : level, phase, structured_write_back_code);
        }
        // check for moved ifs
        std::string moved_ifs_condition;
        if (level == LEVEL::PRIVATE) {
            for (const auto dim : _dimension_order) {
                if (dim.type == DIM_TYPE::R) break;

                bool dim_in_phase3 = false;
                LEVEL dim_top_level_in_phase3;
                for (LEVEL p_level : parent_levels(level, true)) {
                    if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(p_level, dim)] == 3) {
                        dim_in_phase3 = true;
                        dim_top_level_in_phase3 = p_level;
                        break;
                    }
                }
                if (dim_in_phase3) {
                    if (!moved_ifs_condition.empty()) moved_ifs_condition += " && ";
                    if (dim_top_level_in_phase3 == LEVEL::PRIVATE) {
                        moved_ifs_condition.append(stringf("(%s < (%s + %s - 1) / %s)",
                                                           _macros.fu_id(kernel, parent_level(level), dim),
                                                           _macros.num_extra_elements(kernel, level, dim, phase),
                                                           _macros.num_fu(kernel, level, dim),
                                                           _macros.num_fu(kernel, level, dim)));
                    } else {
                        moved_ifs_condition.append(stringf("(%s < %s - %s * %s)",
                                                           _macros.fu_id(kernel, parent_level(level), dim),
                                                           _macros.num_extra_elements(kernel, dim_top_level_in_phase3, dim),
                                                           _macros.fu_id(kernel, parent_level(dim_top_level_in_phase3), dim),
                                                           _macros.num_fu(kernel, dim_top_level_in_phase3, dim)));
                    }
                }

            }
        }
        if (!moved_ifs_condition.empty()) structured_write_back_code = stringf("if (%s) {\n%s\n}", moved_ifs_condition, indent(structured_write_back_code, 1));
#ifdef REDUCTION_COPY_COMPLETE
        // wait for all FUs if values have been copied from separate reduction memory
        structured_write_back_code.append(stringf("\n#if %c_CB_RES_DEST_LEVEL == %s", level, long_level(level)));
        structured_write_back_code.append("\n");
        structured_write_back_code.append(barrier(LEVEL::LOCAL, _cuda));
        structured_write_back_code.append("\n#endif");
#endif
        code << std::endl << structured_write_back_code;

        if (L_DIMS > 0 && level != LEVEL::PRIVATE) {
            code << std::endl << "#endif"; // for #if flat write back
        }

        return stringf("// move results upwards in memory hierarchy\n#if %s%s\n%s\n#endif",
#ifdef REDUCTION_COPY_COMPLETE
                       level != LEVEL::GLOBAL ? stringf("%c_CB_RES_DEST_LEVEL > %c_CB_RES_DEST_LEVEL", parent_level(level), level) : "",
                       R_DIMS > 0 ? stringf("%s(L_REDUCTION == %s && (%s) && (%c_CB_RES_DEST_LEVEL < LOCAL || %c_CB_RES_DEST_LEVEL == GLOBAL))", level != LEVEL::GLOBAL ? " || " : "", upper_case(long_level(level)), concat(multi_stringf("%s > 1", _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS))), " || "), level, level) : "",
#else
                       R_DIMS > 0
                       ? (level != LEVEL::GLOBAL ? stringf("(%c_CB_RES_DEST_LEVEL > %c_CB_RES_DEST_LEVEL || (L_REDUCTION == %s && (%s) && %c_CB_RES_DEST_LEVEL == GLOBAL))", parent_level(level), level, upper_case(long_level(level)), concat(multi_stringf("%s > 1", _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS))), " || "), level) : "")
                       : (level != LEVEL::GLOBAL ? stringf("%c_CB_RES_DEST_LEVEL > %c_CB_RES_DEST_LEVEL", parent_level(level), level) : ""),
                       R_DIMS > 0 ? stringf("%s(L_REDUCTION != %s || (%s) || %c_CB_RES_DEST_LEVEL >= LOCAL)", level != LEVEL::GLOBAL ? " && " : "", upper_case(long_level(level)), concat(multi_stringf("%s == 1", _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS))), " && "), level) : "",
#endif
                       code.str()
        );
    }

    const std::string reset_reduction_memory_space(unsigned int kernel) const {
        std::stringstream code;

        code << stringf("#if %s || L_CB_RES_DEST_LEVEL < GLOBAL || P_CB_RES_DEST_LEVEL < GLOBAL", concat(multi_stringf("%s > 1", _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS))), " || "));
        code << std::endl << "// check if reduction can be done with all FUs";
        if (_runtime_inputs) {
            code << std::endl << concat(multi_stringf(
                    R"(#if L_REDUCTION == PRIVATE
#if %s == 0 && %s == 0
#define NUM_ACTIVE_WI_%c_%d (%s)
#define STATIC_NUM_ACTIVE_WI_%c_%d NUM_ACTIVE_WI_%c_%d
#else
#define NUM_ACTIVE_WI_%c_%d (%s)
#define STATIC_NUM_ACTIVE_WI_%c_%d NUM_ACTIVE_WI_%c_%d
#endif
size_t NUM_ACTIVE_WI_IN_COMPLETE_G_CB_%c_%d = %s;
if (%s == 0 && %s == 0) {
  NUM_ACTIVE_WI_IN_COMPLETE_G_CB_%c_%d = %s;
}
#define P3_NUM_ACTIVE_WI_%c_%d (%s < %s / %s ? %s : (%s %% %s))
#elif L_REDUCTION == LOCAL
#if %s == 0 && %s == 0
#define NUM_ACTIVE_WI_%c_%d (%s)
#define STATIC_NUM_ACTIVE_WI_%c_%d NUM_ACTIVE_WI_%c_%d
#else
#define NUM_ACTIVE_WI_%c_%d (%s)
#define STATIC_NUM_ACTIVE_WI_%c_%d NUM_ACTIVE_WI_%c_%d
#endif
size_t NUM_ACTIVE_WI_IN_COMPLETE_G_CB_%c_%d = %s;
if (%s == 0 && %s == 0) {
  NUM_ACTIVE_WI_IN_COMPLETE_G_CB_%c_%d = %s;
}
#define P3_NUM_ACTIVE_WI_%c_%d (%s < %s / %s ? %s : (%s %% %s))
#elif L_REDUCTION == GLOBAL
size_t NUM_ACTIVE_WI_%c_%d = %s;
if (%s == 0 && %s == 0) {
  NUM_ACTIVE_WI_%c_%d = %s < %s / %s ? %s : (%s %% %s);
}
#endif)",
                    _macros.num_steps(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0)),
                    _macros.num_extra_cached_iterations(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0)),
                    dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0), _macros.num_extra_elements(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0)),
                    dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0), dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0),
                    dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0), _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                    dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0), dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0),

                    dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0), _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                    _macros.num_steps_in_incomplete_cb(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), V(LEVEL::GLOBAL)),
                    _macros.num_extra_cached_iterations_in_incomplete_cb(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), V(LEVEL::GLOBAL)),
                    dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0),
                    _macros.num_extra_elements_in_incomplete_cb(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), V(LEVEL::GLOBAL)),

                    dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0),
                    _macros.fu_id(V(kernel), V(LEVEL::GLOBAL), dim_range(L_DIMS, 0)),
                    _macros.num_extra_elements(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                    _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                    _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                    _macros.num_extra_elements(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                    _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),


                    _macros.num_steps(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0)),
                    _macros.num_extra_cached_iterations(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0)),
                    dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0), _macros.num_extra_elements(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0)),
                    dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0), dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0),
                    dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0), _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                    dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0), dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0),

                    dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0), _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                    _macros.num_steps_in_incomplete_cb(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), V(LEVEL::GLOBAL)),
                    _macros.num_extra_cached_iterations_in_incomplete_cb(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), V(LEVEL::GLOBAL)),
                    dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0),
                    _macros.num_extra_elements_in_incomplete_cb(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), V(LEVEL::GLOBAL)),

                    dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0),
                    _macros.fu_id(V(kernel), V(LEVEL::GLOBAL), dim_range(L_DIMS, 0)),
                    _macros.num_extra_elements(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                    _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                    _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                    _macros.num_extra_elements(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                    _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),


                    dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0), _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                    _macros.num_steps(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                    _macros.num_extra_cached_iterations(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                    dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0),
                    _macros.fu_id(V(kernel), V(LEVEL::GLOBAL), dim_range(L_DIMS, 0)),
                    _macros.num_extra_elements(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                    _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                    _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                    _macros.num_extra_elements(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                    _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0))
            ), "\n");
            code << std::endl << concat(multi_stringf(
                    R"(#if L_REDUCTION == PRIVATE
#if %s == 0 && %s == 0
#define NUM_ACTIVE_WI_%c_%d (%s)
#define STATIC_NUM_ACTIVE_WI_%c_%d NUM_ACTIVE_WI_%c_%d
#else
#define NUM_ACTIVE_WI_%c_%d (%s)
#define STATIC_NUM_ACTIVE_WI_%c_%d NUM_ACTIVE_WI_%c_%d
#endif
size_t NUM_ACTIVE_WI_IN_COMPLETE_G_CB_%c_%d = %s;
if (%s == 0 && %s == 0) {
  NUM_ACTIVE_WI_IN_COMPLETE_G_CB_%c_%d = %s;
}
#define P3_NUM_ACTIVE_WI_%c_%d (%s < %s / %s ? %s : (%s %% %s))
#elif L_REDUCTION == LOCAL
size_t NUM_ACTIVE_WI_%c_%d = %s;
if (%s == 0 && %s == 0) {
  NUM_ACTIVE_WI_%c_%d = %s < %s / %s ? %s : (%s %% %s);
}
#elif L_REDUCTION == GLOBAL
size_t NUM_ACTIVE_WI_%c_%d = %s;
if (%s == 0 && %s == 0) {
  NUM_ACTIVE_WI_%c_%d = %s < %s / %s ? %s : (%s %% %s);
}
#endif)",
                    _macros.num_steps(V(kernel), V(LEVEL::PRIVATE), dim_range(0, R_DIMS)),
                    _macros.num_extra_cached_iterations(V(kernel), V(LEVEL::PRIVATE), dim_range(0, R_DIMS)),
                    dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS), _macros.num_extra_elements(V(kernel), V(LEVEL::PRIVATE), dim_range(0, R_DIMS)),
                    dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS), dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS),
                    dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS), _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS), dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS),

                    dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS), _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    _macros.num_steps_in_incomplete_cb(V(kernel), V(LEVEL::PRIVATE), dim_range(0, R_DIMS), V(LEVEL::GLOBAL)),
                    _macros.num_extra_cached_iterations_in_incomplete_cb(V(kernel), V(LEVEL::PRIVATE), dim_range(0, R_DIMS), V(LEVEL::GLOBAL)),
                    dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS),
                    _macros.num_extra_elements_in_incomplete_cb(V(kernel), V(LEVEL::PRIVATE), dim_range(0, R_DIMS), V(LEVEL::GLOBAL)),

                    dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS),
                    _macros.fu_id(V(kernel), V(LEVEL::GLOBAL), dim_range(0, R_DIMS)),
                    _macros.num_extra_elements(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    _macros.num_extra_elements(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),


                    dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS), _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    _macros.num_steps(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    _macros.num_extra_cached_iterations(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS),
                    _macros.fu_id(V(kernel), V(LEVEL::GLOBAL), dim_range(0, R_DIMS)),
                    _macros.num_extra_elements(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    _macros.num_extra_elements(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),


                    dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS), _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    _macros.num_steps(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    _macros.num_extra_cached_iterations(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS),
                    _macros.fu_id(V(kernel), V(LEVEL::GLOBAL), dim_range(0, R_DIMS)),
                    _macros.num_extra_elements(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    _macros.num_extra_elements(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS))
            ), "\n");
        } else {
            code << std::endl << concat(multi_stringf(
                    R"(#if L_REDUCTION == PRIVATE
#if %s == 0 && %s == 0
#define NUM_ACTIVE_WI_%c_%d (%s)
#define STATIC_NUM_ACTIVE_WI_%c_%d NUM_ACTIVE_WI_%c_%d
#else
#define NUM_ACTIVE_WI_%c_%d (%s)
#define STATIC_NUM_ACTIVE_WI_%c_%d NUM_ACTIVE_WI_%c_%d
#endif
#if %s == 0 && %s == 0
#define NUM_ACTIVE_WI_IN_COMPLETE_G_CB_%c_%d (%s)
#define STATIC_NUM_ACTIVE_WI_IN_COMPLETE_G_CB_%c_%d NUM_ACTIVE_WI_IN_COMPLETE_G_CB_%c_%d
#else
#define NUM_ACTIVE_WI_IN_COMPLETE_G_CB_%c_%d (%s)
#define STATIC_NUM_ACTIVE_WI_IN_COMPLETE_G_CB_%c_%d NUM_ACTIVE_WI_IN_COMPLETE_G_CB_%c_%d
#endif
#define P3_NUM_ACTIVE_WI_%c_%d (%s < %s / %s ? %s : (%s %% %s))
#elif L_REDUCTION == LOCAL
#if %s == 0 && %s == 0
#define NUM_ACTIVE_WI_%c_%d (%s)
#define STATIC_NUM_ACTIVE_WI_%c_%d NUM_ACTIVE_WI_%c_%d
#else
#define NUM_ACTIVE_WI_%c_%d (%s)
#define STATIC_NUM_ACTIVE_WI_%c_%d NUM_ACTIVE_WI_%c_%d
#endif
#if %s == 0 && %s == 0
#define NUM_ACTIVE_WI_IN_COMPLETE_G_CB_%c_%d (%s)
#define STATIC_NUM_ACTIVE_WI_IN_COMPLETE_G_CB_%c_%d NUM_ACTIVE_WI_IN_COMPLETE_G_CB_%c_%d
#else
#define NUM_ACTIVE_WI_IN_COMPLETE_G_CB_%c_%d (%s)
#define STATIC_NUM_ACTIVE_WI_IN_COMPLETE_G_CB_%c_%d NUM_ACTIVE_WI_IN_COMPLETE_G_CB_%c_%d
#endif
#define P3_NUM_ACTIVE_WI_%c_%d (%s < %s / %s ? %s : (%s %% %s))
#elif L_REDUCTION == GLOBAL
#if %s == 0 && %s == 0
#define NUM_ACTIVE_WI_%c_%d (%s < %s / %s ? %s : (%s %% %s))
#else
#define NUM_ACTIVE_WI_%c_%d (%s)
#define STATIC_NUM_ACTIVE_WI_%c_%d NUM_ACTIVE_WI_%c_%d
#endif
#endif)",
                    _macros.num_steps(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0)),
                    _macros.num_extra_cached_iterations(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0)),
                    dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0), _macros.num_extra_elements(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0)),
                    dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0), dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0),
                    dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0), _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                    dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0), dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0),

                    _macros.num_steps_in_incomplete_cb(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), V(LEVEL::GLOBAL)),
                    _macros.num_extra_cached_iterations_in_incomplete_cb(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), V(LEVEL::GLOBAL)),
                    dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0),
                    _macros.num_extra_elements_in_incomplete_cb(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), V(LEVEL::GLOBAL)),
                    dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0), dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0),
                    dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0), _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                    dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0), dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0),

                    dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0),
                    _macros.fu_id(V(kernel), V(LEVEL::GLOBAL), dim_range(L_DIMS, 0)),
                    _macros.num_extra_elements(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                    _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                    _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                    _macros.num_extra_elements(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                    _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),


                    _macros.num_steps(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0)),
                    _macros.num_extra_cached_iterations(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0)),
                    dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0), _macros.num_extra_elements(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0)),
                    dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0), dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0),
                    dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0), _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                    dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0), dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0),

                    _macros.num_steps_in_incomplete_cb(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), V(LEVEL::GLOBAL)),
                    _macros.num_extra_cached_iterations_in_incomplete_cb(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), V(LEVEL::GLOBAL)),
                    dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0),
                    _macros.num_extra_elements_in_incomplete_cb(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), V(LEVEL::GLOBAL)),
                    dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0), dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0),
                    dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0), _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                    dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0), dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0),

                    dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0),
                    _macros.fu_id(V(kernel), V(LEVEL::GLOBAL), dim_range(L_DIMS, 0)),
                    _macros.num_extra_elements(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                    _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                    _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                    _macros.num_extra_elements(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                    _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),


                    _macros.num_steps(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                    _macros.num_extra_cached_iterations(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                    dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0),
                    _macros.fu_id(V(kernel), V(LEVEL::GLOBAL), dim_range(L_DIMS, 0)),
                    _macros.num_extra_elements(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                    _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                    _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                    _macros.num_extra_elements(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                    _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                    dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0), _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                    dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0), dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0)
            ), "\n");
            code << std::endl << concat(multi_stringf(
                    R"(#if L_REDUCTION == PRIVATE
#if %s == 0 && %s == 0
#define NUM_ACTIVE_WI_%c_%d (%s)
#define STATIC_NUM_ACTIVE_WI_%c_%d NUM_ACTIVE_WI_%c_%d
#else
#define NUM_ACTIVE_WI_%c_%d (%s)
#define STATIC_NUM_ACTIVE_WI_%c_%d NUM_ACTIVE_WI_%c_%d
#endif
#if %s == 0 && %s == 0
#define NUM_ACTIVE_WI_IN_COMPLETE_G_CB_%c_%d (%s)
#define STATIC_NUM_ACTIVE_WI_IN_COMPLETE_G_CB_%c_%d NUM_ACTIVE_WI_IN_COMPLETE_G_CB_%c_%d
#else
#define NUM_ACTIVE_WI_IN_COMPLETE_G_CB_%c_%d (%s)
#define STATIC_NUM_ACTIVE_WI_IN_COMPLETE_G_CB_%c_%d NUM_ACTIVE_WI_IN_COMPLETE_G_CB_%c_%d
#endif
#define P3_NUM_ACTIVE_WI_%c_%d (%s < %s / %s ? %s : (%s %% %s))
#elif L_REDUCTION == LOCAL
#if %s == 0 && %s == 0
#define NUM_ACTIVE_WI_%c_%d (%s < %s / %s ? %s : (%s %% %s))
#else
#define NUM_ACTIVE_WI_%c_%d (%s)
#define STATIC_NUM_ACTIVE_WI_%c_%d NUM_ACTIVE_WI_%c_%d
#endif
#elif L_REDUCTION == GLOBAL
#if %s == 0 && %s == 0
#define NUM_ACTIVE_WI_%c_%d (%s < %s / %s ? %s : (%s %% %s))
#else
#define NUM_ACTIVE_WI_%c_%d (%s)
#define STATIC_NUM_ACTIVE_WI_%c_%d NUM_ACTIVE_WI_%c_%d
#endif
#endif)",
                    _macros.num_steps(V(kernel), V(LEVEL::PRIVATE), dim_range(0, R_DIMS)),
                    _macros.num_extra_cached_iterations(V(kernel), V(LEVEL::PRIVATE), dim_range(0, R_DIMS)),
                    dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS), _macros.num_extra_elements(V(kernel), V(LEVEL::PRIVATE), dim_range(0, R_DIMS)),
                    dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS), dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS),
                    dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS), _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS), dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS),

                    _macros.num_steps_in_incomplete_cb(V(kernel), V(LEVEL::PRIVATE), dim_range(0, R_DIMS), V(LEVEL::GLOBAL)),
                    _macros.num_extra_cached_iterations_in_incomplete_cb(V(kernel), V(LEVEL::PRIVATE), dim_range(0, R_DIMS), V(LEVEL::GLOBAL)),
                    dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS),
                    _macros.num_extra_elements_in_incomplete_cb(V(kernel), V(LEVEL::PRIVATE), dim_range(0, R_DIMS), V(LEVEL::GLOBAL)),
                    dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS), dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS),
                    dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS), _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS), dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS),

                    dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS),
                    _macros.fu_id(V(kernel), V(LEVEL::GLOBAL), dim_range(0, R_DIMS)),
                    _macros.num_extra_elements(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    _macros.num_extra_elements(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),


                    _macros.num_steps(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    _macros.num_extra_cached_iterations(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS),
                    _macros.fu_id(V(kernel), V(LEVEL::GLOBAL), dim_range(0, R_DIMS)),
                    _macros.num_extra_elements(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    _macros.num_extra_elements(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS), _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS), dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS),


                    _macros.num_steps(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    _macros.num_extra_cached_iterations(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS),
                    _macros.fu_id(V(kernel), V(LEVEL::GLOBAL), dim_range(0, R_DIMS)),
                    _macros.num_extra_elements(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    _macros.num_extra_elements(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS), _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                    dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS), dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS)
            ), "\n");
        }
        code << std::endl << "#endif";

        return code.str();
    }
private:
    const TOs &_result_buffer;
    const macros<L_DIMS, R_DIMS> &_macros;
    const std::vector<dimension_t> _dimension_order;
    const bool                     _runtime_inputs;
    const std::vector<dimension_t> _skip_post_processing;
    const bool                     _cuda;
    std::vector<dimension_t> _l_dims_after_first_r_dim;

    std::string _buffer_macro_name_pattern;
    std::string _buffer_def_macro_name_pattern;
    std::string _res_macro_name_pattern;
    std::string _fu_res_macro_name_pattern;
    std::string _buffer_macro_call_pattern[3]; // per level
    std::string _res_macro_call_pattern;
    std::string _fu_res_macro_call_pattern;

    std::string _reduction_mem_macro_name_pattern;
    std::string _reduction_mem_macro_call_pattern;

    std::string _kernel_res_macro_name_pattern;
    std::string _fu_kernel_res_macro_name_pattern;
    std::string _kernel_res_buffer_macro_name_pattern;
    std::string _kernel_res_macro_call_pattern;
    std::string _fu_kernel_res_macro_call_pattern;
    std::string _kernel_res_buffer_macro_call_pattern;

    std::string shuffle_reduction_condition(unsigned int kernel) const {
        if (kernel == 1) {
            return stringf(
                    "(defined(USE_SHUFFLE_REDUCTIONS) && USE_SHUFFLE_REDUCTIONS == 1 && ((%s) == %d) && (((%s) & (%s - 1)) == 0) && (%s))",
                    concat(multi_stringf("OCL_DIM_%c_%d", dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS)), " + "),
                    ((R_DIMS - 1) * R_DIMS) / 2,
                    concat(_macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)), " * "),
                    concat(_macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)), " * "),
                    concat(multi_stringf("((INPUT_SIZE_%c_%d %% (%s * %s) == 0) && ((INPUT_SIZE_%c_%d / (%s * %s)) %% (L_CB_SIZE_%c_%d / %s) == 0) && (L_CB_SIZE_%c_%d %% %s == 0) && ((L_CB_SIZE_%c_%d / %s) %% P_CB_SIZE_%c_%d == 0))",
                                         dim_range_types(L_DIMS, R_DIMS), dim_range_nrs(L_DIMS, R_DIMS),
                                         _macros.num_fu(V(kernel), V(LEVEL::GLOBAL), dim_range(L_DIMS, R_DIMS)),
                                         _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, R_DIMS)),
                                         dim_range_types(L_DIMS, R_DIMS), dim_range_nrs(L_DIMS, R_DIMS),
                                         _macros.num_fu(V(kernel), V(LEVEL::GLOBAL), dim_range(L_DIMS, R_DIMS)),
                                         _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, R_DIMS)),
                                         dim_range_types(L_DIMS, R_DIMS), dim_range_nrs(L_DIMS, R_DIMS),
                                         _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, R_DIMS)),
                                         dim_range_types(L_DIMS, R_DIMS), dim_range_nrs(L_DIMS, R_DIMS),
                                         _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, R_DIMS)),
                                         dim_range_types(L_DIMS, R_DIMS), dim_range_nrs(L_DIMS, R_DIMS),
                                         _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, R_DIMS)),
                                         dim_range_types(L_DIMS, R_DIMS), dim_range_nrs(L_DIMS, R_DIMS)), " && ")
            );
        } else {
            return stringf(
                    "(defined(USE_SHUFFLE_REDUCTIONS) && USE_SHUFFLE_REDUCTIONS == 1 && ((%s) == %d) && (((%s) & (%s - 1)) == 0)%s && (%s))",
                    concat(multi_stringf("OCL_DIM_%c_%d", dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS)), " + "),
                    ((R_DIMS - 1) * R_DIMS) / 2,
                    concat(_macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)), " * "),
                    concat(_macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)), " * "),
                    L_DIMS == 0
                    ? ""
                    : stringf(" && (%s)",
                              concat(multi_stringf("((INPUT_SIZE_%c_%d %% (%s * %s) == 0) && ((INPUT_SIZE_%c_%d / (%s * %s)) %% (L_CB_SIZE_%c_%d / %s) == 0) && (L_CB_SIZE_%c_%d %% %s == 0) && ((L_CB_SIZE_%c_%d / %s) %% P_CB_SIZE_%c_%d == 0))",
                                                   dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0),
                                                   _macros.num_fu(V(kernel), V(LEVEL::GLOBAL), dim_range(L_DIMS, 0)),
                                                   _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                                                   dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0),
                                                   _macros.num_fu(V(kernel), V(LEVEL::GLOBAL), dim_range(L_DIMS, 0)),
                                                   _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                                                   dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0),
                                                   _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                                                   dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0),
                                                   _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                                                   dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0),
                                                   _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(L_DIMS, 0)),
                                                   dim_range_types(L_DIMS, 0), dim_range_nrs(L_DIMS, 0)), " && ")),
                    concat(multi_stringf("((NUM_WG_%c_%d %% %s == 0) && ((NUM_WG_%c_%d / %s) %% (L_CB_SIZE_%c_%d / %s) == 0) && (L_CB_SIZE_%c_%d %% %s == 0) && ((L_CB_SIZE_%c_%d / %s) %% P_CB_SIZE_%c_%d == 0))",
                                         dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS),
                                         _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                                         dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS),
                                         _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                                         dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS),
                                         _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                                         dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS),
                                         _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                                         dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS),
                                         _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                                         dim_range_types(0, R_DIMS), dim_range_nrs(0, R_DIMS)), " && ")
            );
        }
    }

    std::string wrap_in_active_condition(unsigned int kernel, LEVEL level, char *phase,
                                         const std::string &body, const std::string &precondition = "") const {
        if (level == LEVEL::GLOBAL) level = LEVEL::LOCAL; // active condition for local level is same as for local level, because global level only has a single FU (the device)

        std::stringstream code;
        auto active_condition = [&](auto dim) {
            return stringf(
                    "%s < (%s + %s - 1) / %s",
                    _macros.fu_id(kernel, parent_level(level), dim),
                    _macros.num_extra_elements(kernel, level, dim, phase),
                    _macros.num_fu(kernel, level, dim),
                    _macros.num_fu(kernel, level, dim)
            );
        };

        bool parent_level_in_phase2 = false;
        for (const auto dim : dim_range(0, R_DIMS)) {
            parent_level_in_phase2 = parent_level_in_phase2 || phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dim)] == 2;
        }
        if (_runtime_inputs && (level == LEVEL::LOCAL || parent_level_in_phase2)) {
            std::string condition_for_all_r_dims;
            for (const auto dim : dim_range(0, R_DIMS)) {
                if (level != LEVEL::LOCAL && phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dim)] != 2) continue;
                if (!condition_for_all_r_dims.empty()) condition_for_all_r_dims.append(" && ");
                condition_for_all_r_dims.append(stringf("((%s != 0 || %s != 0 || %s == 0) || %s)",
                           _macros.num_steps(kernel, level, dim, phase),
                           _macros.num_extra_cached_iterations(kernel, level, dim, phase),
                           _macros.num_extra_elements(kernel, level, dim, phase),
                           active_condition(dim)
                ));
            }
            return stringf("if (%s%s%s) {\n%s\n}",
                           precondition,
                           precondition.empty() ? "" : " && ",
                           condition_for_all_r_dims,
                           indent(body, 1));
        } else {
            if (precondition.empty()) {
                code << "#if ";
                for (const auto dim : dim_range(0, R_DIMS)) {
                    if (dim.nr > 1) code << " || ";
                    if (phase != nullptr && phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dim)] == 3) {
                        code << "1";
                    } else {
                        code << stringf("(%s == 0 && %s == 0 && %s > 0)",
                                        _macros.num_steps(kernel, level, dim, phase),
                                        _macros.num_extra_cached_iterations(kernel, level, dim, phase),
                                        _macros.num_extra_elements(kernel, level, dim, phase));
                    }
                }
                code << std::endl;
                code << "if (true" << std::endl;
                code << "#endif" << std::endl;

                for (const auto dim : dim_range(0, R_DIMS)) {
                    if (phase != nullptr && phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dim)] == 3) {
                        code << stringf("    && (%s < %s - %s * %s)",
                                        _macros.fu_id(kernel, parent_level(level), dim),
                                        _macros.num_extra_elements(kernel, parent_level(level), dim),
                                        _macros.fu_id(kernel, parent_level(parent_level(level)), dim),
                                        _macros.num_fu(kernel, parent_level(level), dim)
                        ).c_str() << std::endl;
                    } else {
                        code << stringf("#if %s == 0 && %s == 0 && %s > 0",
                                        _macros.num_steps(kernel, level, dim, phase),
                                        _macros.num_extra_cached_iterations(kernel, level, dim, phase),
                                        _macros.num_extra_elements(kernel, level, dim, phase)
                        ).c_str() << std::endl;
                        code << stringf("    && (%s)", active_condition(dim)).c_str() << std::endl;
                        code << "#endif" << std::endl;
                    }
                }

                code << "#if ";
                for (const auto dim : dim_range(0, R_DIMS)) {
                    if (dim.nr > 1) code << " || ";
                    if (phase != nullptr && phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dim)] == 3) {
                        code << "1";
                    } else {
                        code << stringf("(%s == 0 && %s == 0 && %s > 0)",
                                        _macros.num_steps(kernel, level, dim, phase),
                                        _macros.num_extra_cached_iterations(kernel, level, dim, phase),
                                        _macros.num_extra_elements(kernel, level, dim, phase));
                    }
                }
                code << std::endl;
                code << ")" << std::endl;
                code << "#endif" << std::endl;

                code << "{" << std::endl;
                code << indent(body, 1).c_str() << std::endl;
                code << "}";
            } else {
                code << "#if ";
                for (const auto dim : dim_range(0, R_DIMS)) {
                    if (dim.nr > 1) code << " || ";
                    if (phase != nullptr && phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dim)] == 3) {
                        code << "1";
                    } else {
                        code << stringf("(%s == 0 && %s == 0 && %s > 0)",
                                        _macros.num_steps(kernel, level, dim, phase),
                                        _macros.num_extra_cached_iterations(kernel, level, dim, phase),
                                        _macros.num_extra_elements(kernel, level, dim, phase));
                    }
                }
                code << std::endl;
                code << stringf("if ((%s)", precondition) << std::endl;
                code << "#endif" << std::endl;

                for (const auto dim : dim_range(0, R_DIMS)) {
                    if (phase != nullptr && phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dim)] == 3) {
                        code << stringf("    && (%s < %s - %s * %s)",
                                        _macros.fu_id(kernel, parent_level(level), dim),
                                        _macros.num_extra_elements(kernel, parent_level(level), dim),
                                        _macros.fu_id(kernel, parent_level(parent_level(level)), dim),
                                        _macros.num_fu(kernel, parent_level(level), dim)
                        ).c_str() << std::endl;
                    } else {
                        code << stringf("#if %s == 0 && %s == 0 && %s > 0",
                                        _macros.num_steps(kernel, level, dim, phase),
                                        _macros.num_extra_cached_iterations(kernel, level, dim, phase),
                                        _macros.num_extra_elements(kernel, level, dim, phase)
                        ).c_str() << std::endl;
                        code << stringf("    && (%s)", active_condition(dim)).c_str() << std::endl;
                        code << "#endif" << std::endl;
                    }
                }

                code << "#if ";
                for (const auto dim : dim_range(0, R_DIMS)) {
                    if (dim.nr > 1) code << " || ";
                    if (phase != nullptr && phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dim)] == 3) {
                        code << "1";
                    } else {
                        code << stringf("(%s == 0 && %s == 0 && %s > 0)",
                                        _macros.num_steps(kernel, level, dim, phase),
                                        _macros.num_extra_cached_iterations(kernel, level, dim, phase),
                                        _macros.num_extra_elements(kernel, level, dim, phase));
                    }
                }
                code << std::endl;
                code << ")" << std::endl;
                code << "#else" << std::endl;
                code << stringf("if (%s)", precondition) << std::endl;
                code << "#endif" << std::endl;

                code << "{" << std::endl;
                code << indent(body, 1).c_str() << std::endl;
                code << "}";
            }
            return code.str();
        }
    }

    const std::string reduction_impl(unsigned int kernel, LEVEL fu_level, LEVEL cb_level, char *phase, bool *first_iteration, const std::vector<std::string> &input_names) const {
        std::stringstream code;

#ifdef REDUCTION_COPY_COMPLETE
        std::string moved_ifs_condition;
        if (cb_level == LEVEL::PRIVATE) {
            for (const auto dim : _dimension_order) {
                if (dim.type == DIM_TYPE::R) break;

                bool dim_in_phase3 = false;
                LEVEL dim_top_level_in_phase3;
                for (LEVEL p_level : parent_levels(LEVEL::PRIVATE, true)) {
                    if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(p_level, dim)] == 3) {
                        dim_in_phase3 = true;
                        dim_top_level_in_phase3 = p_level;
                        break;
                    }
                }
                if (dim_in_phase3) {
                    if (!moved_ifs_condition.empty()) moved_ifs_condition += " && ";
                    if (dim_top_level_in_phase3 == LEVEL::PRIVATE) {
                        moved_ifs_condition.append(stringf("(%s < (%s + %s - 1) / %s)",
                                                           _macros.fu_id(kernel, parent_level(LEVEL::PRIVATE), dim),
                                                           _macros.num_extra_elements(kernel, LEVEL::PRIVATE, dim, phase),
                                                           _macros.num_fu(kernel, LEVEL::PRIVATE, dim),
                                                           _macros.num_fu(kernel, LEVEL::PRIVATE, dim)));
                    } else {
                        moved_ifs_condition.append(stringf("(%s < %s - %s * %s)",
                                                           _macros.fu_id(kernel, parent_level(LEVEL::PRIVATE), dim),
                                                           _macros.num_extra_elements(kernel, dim_top_level_in_phase3, dim),
                                                           _macros.fu_id(kernel, parent_level(dim_top_level_in_phase3), dim),
                                                           _macros.num_fu(kernel, dim_top_level_in_phase3, dim)));
                    }
                }

            }
        }

        auto l_params = _macros.iteration_to_index_conversion(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), iterations_loop_variable(LEVEL::PRIVATE, dim_range(L_DIMS, 0)));
        auto params_reduction_mem = _macros.fu_index_conversion(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), l_params, V(phase));
        for (int i = 0; i < R_DIMS; ++i) {
            params_reduction_mem.push_back(_macros.fu_id(kernel, fu_level, R(i + 1)));
        }

        code << "// wait for all FUs to finish computation";
        for (LEVEL p_level : parent_levels(fu_level, true, ORDER::ASCENDING)) {
            // barrier now also necessary when cahing is enabled, because caching code does no longer add a barrier after processing the cache block
//            code << std::endl << stringf("%s %c_CB_RES_DEST_LEVEL == %s%s",
//                                         p_level == fu_level ? "#if  " : "#elif",
//                                         cb_level, long_level(p_level),
//                                         p_level == fu_level && cb_level == LEVEL::LOCAL ? stringf(
//                                                 " && %s",
//#ifdef INDIVIDUAL_INPUT_CACHING
//                                                 concat(multi_stringf("%s_CACHE_%c_CB == 0", upper_case(input_names), V(p_level)), " && ")
//#else
//                                                 stringf("CACHE_%c_CB == 0", p_level)
//#endif
//                                         ): "");
            code << std::endl << stringf("%s %c_CB_RES_DEST_LEVEL == %s",
                                         p_level == fu_level ? "#if  " : "#elif",
                                         cb_level, long_level(p_level));

            code << std::endl << barrier(p_level, _cuda);
        }
        code << std::endl << "#endif"; // for #if of barrier
        code << std::endl << stringf("#if %c_CB_RES_DEST_LEVEL < %s", cb_level, long_level(fu_level));
        std::string copy_body;
        auto copy_body_callback = [&] (auto inner_level, const auto &inner_dimension, char *inner_phase, bool *first_iteration) -> std::string {
            return stringf(
                    "%s = %s;",
                    this->call_reduction_mem_macro(output.name(), kernel, fu_level, params_reduction_mem),
                    this->call_fu_res_macro(output.name(), kernel, inner_level, inner_phase, l_params));
        };
        if (L_DIMS > 0) {
            switch (cb_level) {
                case LEVEL::PRIVATE: {
                    auto loops = loop_generator<L_DIMS, R_DIMS>(kernel, _macros, _runtime_inputs, V(LEVEL::PRIVATE), _l_dims_after_first_r_dim, false, nullptr, false, _skip_post_processing);
                    copy_body = loops.from(
                            kernel, LEVEL::PRIVATE, _l_dims_after_first_r_dim.empty() ? L(1) : _l_dims_after_first_r_dim.front(),
                            [&](auto inner_level, const auto &inner_dimension, char *inner_phase, bool *first_iteration) -> std::string {
                                return wrap_in_iterations_loop<L_DIMS, R_DIMS>(kernel, LEVEL::PRIVATE, dim_range(L_DIMS, 0), copy_body_callback(
                                        cb_level, inner_dimension, inner_phase, first_iteration
                                ), true, inner_phase);
                            }, true, phase);
                    break;
                }
                case LEVEL::LOCAL: {
                    auto loops = loop_generator<L_DIMS, R_DIMS>(kernel, _macros, _runtime_inputs, V(LEVEL::LOCAL), _l_dims_after_first_r_dim, false, nullptr, false, _skip_post_processing);
                    auto inner_loops = loop_generator<L_DIMS, R_DIMS>(kernel, _macros, _runtime_inputs, V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), false, nullptr, false, _skip_post_processing);
                    copy_body = loops.from(
                            kernel, LEVEL::LOCAL, _l_dims_after_first_r_dim.empty() ? L(1) : _l_dims_after_first_r_dim.front(),
                            [&] (auto inner_level, const auto &inner_dimension, char *inner_phase, bool *first_iteration) -> std::string {
                                return inner_loops.from(
                                        kernel, LEVEL::PRIVATE, L(1),
                                        [&](auto inner_level2, const auto &inner_dimension2, char *inner_phase2, bool *first_iteration2) -> std::string {
                                            return wrap_in_iterations_loop<L_DIMS, R_DIMS>(kernel, LEVEL::PRIVATE, dim_range(L_DIMS, 0), copy_body_callback(
                                                    cb_level, inner_dimension2, inner_phase2, first_iteration2
                                            ), true, inner_phase2);
                                        }, true, inner_phase);
                            }, true, phase);
                    break;
                }
                case LEVEL::GLOBAL: {
                    auto loops = loop_generator<L_DIMS, R_DIMS>(kernel, _macros, _runtime_inputs, V(LEVEL::LOCAL, LEVEL::PRIVATE), dim_range(L_DIMS, 0), false, nullptr, false, _skip_post_processing);
                    copy_body = loops.from(
                            kernel, LEVEL::LOCAL, L(1),
                            [&](auto inner_level, const auto &inner_dimension, char *inner_phase, bool *first_iteration) -> std::string {
                                return wrap_in_iterations_loop<L_DIMS, R_DIMS>(kernel, LEVEL::PRIVATE, dim_range(L_DIMS, 0), copy_body_callback(
                                        cb_level, inner_dimension, inner_phase, first_iteration
                                ), true, inner_phase);
                            }, true, phase);
                    break;
                }
            }
        } else {
            copy_body = copy_body_callback(cb_level, L(1), phase, nullptr);
        }
        if (!moved_ifs_condition.empty())
            code << std::endl << stringf("if (%s) {", moved_ifs_condition);
        code << std::endl << indent(copy_body, !moved_ifs_condition.empty());
        if (!moved_ifs_condition.empty())
            code << std::endl << "}";
        code << std::endl << "// wait for all values to be copied into reduction memory";
        code << std::endl << barrier(fu_level, _cuda);
        code << std::endl << "#endif"; // for #if of copying into reduction memory
        std::vector<dimension_t> previous_r_dims;
        for (const auto &r_dim : reverse(_dimension_order)) {
            if (r_dim.type != DIM_TYPE::R) continue;

            auto params_reduction_mem_strided = _macros.fu_index_conversion(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), l_params, V(phase));
            for (int i = 0; i < R_DIMS; ++i) {
                if (i + 1 == r_dim.nr)
                    params_reduction_mem_strided.push_back(stringf("%s + stride", _macros.fu_id(kernel, fu_level, R(i + 1))));
                else
                    params_reduction_mem_strided.push_back(stringf("%s", _macros.fu_id(kernel, fu_level, R(i + 1))));
            }

            auto loop_body_add_callback = [&] (auto inner_level, const auto &inner_dimension, char *inner_phase, bool *first_iteration) -> std::string {
                std::string loop_body;
                loop_body.append(stringf("#if %c_CB_RES_DEST_LEVEL < %s\n", cb_level, long_level(fu_level)));
                loop_body.append("// reduction in separate memory location\n");
                loop_body.append(stringf(
                        "%s += %s;\n",
                        this->call_reduction_mem_macro(output.name(), kernel, fu_level, params_reduction_mem),
                        this->call_reduction_mem_macro(output.name(), kernel, fu_level, params_reduction_mem_strided)));
                unsigned int p_level_nr = 0;
                for (LEVEL p_level : parent_levels(fu_level, true, ORDER::ASCENDING)) {
                    auto params = _macros.iteration_to_index_conversion(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), iterations_loop_variable(LEVEL::PRIVATE, dim_range(L_DIMS, 0)));
                    auto params_strided = _macros.iteration_to_index_conversion(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), iterations_loop_variable(LEVEL::PRIVATE, dim_range(L_DIMS, 0)));
                    for (int i = 0; i < L_DIMS; ++i) {
                        for (auto sub : sub_levels(p_level, false, ASCENDING)) {
                            params[i] = _macros.fu_index_conversion(kernel, sub, L(i + 1), params[i], inner_phase);
                            params_strided[i] = _macros.fu_index_conversion(kernel, sub, L(i + 1), params_strided[i], inner_phase);
                        }
                    }
                    for (int i = 1; i <= R_DIMS; ++i) {
                        if (p_level != fu_level) {
                            for (auto l : level_range(p_level, parent_level(fu_level))) {
                                params.push_back(_macros.fu_id(kernel, l, R(i)));
                                params_strided.push_back(_macros.fu_id(kernel, l, R(i)));
                            }
                        }
                        params.push_back(_macros.fu_id(kernel, fu_level, R(i)));
                        if (i == r_dim.nr) {
                            params_strided.push_back(stringf("%s + stride", _macros.fu_id(kernel, fu_level, R(i))));
                        } else {
                            params_strided.push_back(_macros.fu_id(kernel, fu_level, R(i)));
                        }
                        for (auto l : level_range(sub_level(fu_level), LEVEL::PRIVATE)) {
                            params.push_back(_macros.fu_id(kernel, l, R(i)));
                            params_strided.push_back(_macros.fu_id(kernel, l, R(i)));
                        }
                    }
                    loop_body.append(stringf("#elif %c_CB_RES_DEST_LEVEL == %s\n", cb_level, long_level(p_level)));
                    loop_body.append(stringf("%s += %s;\n",
                                             this->call_buffer_macro(output.name(), kernel, p_level, params),
                                             this->call_buffer_macro(output.name(), kernel, p_level, params_strided)));
                    ++p_level_nr;
                }
                loop_body.append("#endif\n"); // for #if of reduction memory selection
                return loop_body;
            };
            auto loop_body_reset_callback = [&] (auto inner_level, const auto &inner_dimension, char *inner_phase, bool *first_iteration) -> std::string {
                std::string loop_body;
                loop_body.append(stringf("#if %c_CB_RES_DEST_LEVEL < %s\n", cb_level, long_level(fu_level)));
                loop_body.append("// reduction in separate memory location\n");
                loop_body.append(stringf("%s = 0;\n", this->call_reduction_mem_macro(output.name(), kernel, fu_level, params_reduction_mem)));
                unsigned int p_level_nr = 0;
                for (LEVEL p_level : parent_levels(fu_level, true, ORDER::ASCENDING)) {
                    auto params = _macros.iteration_to_index_conversion(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), iterations_loop_variable(LEVEL::PRIVATE, dim_range(L_DIMS, 0)));
                    for (int i = 0; i < L_DIMS; ++i) {
                        for (auto sub : sub_levels(p_level, false, ASCENDING)) {
                            params[i] = _macros.fu_index_conversion(kernel, sub, L(i + 1), params[i], inner_phase);
                        }
                    }
                    for (int i = 1; i <= R_DIMS; ++i) {
                        if (p_level != fu_level) {
                            for (auto l : level_range(p_level, parent_level(fu_level))) {
                                params.push_back(_macros.fu_id(kernel, l, R(i)));
                            }
                        }
                        params.push_back(_macros.fu_id(kernel, fu_level, R(i)));
                        for (auto l : level_range(sub_level(fu_level), LEVEL::PRIVATE)) {
                            params.push_back(_macros.fu_id(kernel, l, R(i)));
                        }
                    }
                    loop_body.append(stringf("#elif %c_CB_RES_DEST_LEVEL == %s\n", cb_level, long_level(p_level)));
                    loop_body.append(stringf("%s = 0;\n", this->call_buffer_macro(output.name(), kernel, p_level, params)));
                    ++p_level_nr;
                }
                loop_body.append("#endif\n"); // for #if of reduction memory selection
                return loop_body;
            };
            std::string loop_body_add;
            std::string loop_body_reset;
            if (L_DIMS > 0) {
                switch (cb_level) {
                    case LEVEL::PRIVATE: {
                        auto loops = loop_generator<L_DIMS, R_DIMS>(kernel, _macros, _runtime_inputs, V(LEVEL::PRIVATE), _l_dims_after_first_r_dim, false, nullptr, false, _skip_post_processing);
                        loop_body_add = loops.from(
                                kernel, LEVEL::PRIVATE, _l_dims_after_first_r_dim.empty() ? L(1) : _l_dims_after_first_r_dim.front(),
                                [&](auto inner_level, const auto &inner_dimension, char *inner_phase, bool *first_iteration) -> std::string {
                                    return wrap_in_iterations_loop<L_DIMS, R_DIMS>(kernel, LEVEL::PRIVATE, dim_range(L_DIMS, 0), loop_body_add_callback(
                                            fu_level, inner_dimension, inner_phase, first_iteration
                                    ), true, inner_phase);
                                }, true, phase);
                        loop_body_reset = loops.from(
                                kernel, LEVEL::PRIVATE, _l_dims_after_first_r_dim.empty() ? L(1) : _l_dims_after_first_r_dim.front(),
                                [&](auto inner_level, const auto &inner_dimension, char *inner_phase, bool *first_iteration) -> std::string {
                                    return wrap_in_iterations_loop<L_DIMS, R_DIMS>(kernel, LEVEL::PRIVATE, dim_range(L_DIMS, 0), loop_body_reset_callback(
                                            fu_level, inner_dimension, inner_phase, first_iteration
                                    ), true, inner_phase);
                                }, true, phase);
                        break;
                    }
                    case LEVEL::LOCAL: {
                        auto loops = loop_generator<L_DIMS, R_DIMS>(kernel, _macros, _runtime_inputs, V(LEVEL::LOCAL), _l_dims_after_first_r_dim, false, nullptr, false, _skip_post_processing);
                        auto inner_loops = loop_generator<L_DIMS, R_DIMS>(kernel, _macros, _runtime_inputs, V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), false, nullptr, false, _skip_post_processing);
                        loop_body_add = loops.from(
                                kernel, LEVEL::LOCAL, _l_dims_after_first_r_dim.empty() ? L(1) : _l_dims_after_first_r_dim.front(),
                                [&] (auto inner_level, const auto &inner_dimension, char *inner_phase, bool *first_iteration) -> std::string {
                                    return inner_loops.from(
                                            kernel, LEVEL::PRIVATE, L(1),
                                            [&](auto inner_level2, const auto &inner_dimension2, char *inner_phase2, bool *first_iteration2) -> std::string {
                                                return wrap_in_iterations_loop<L_DIMS, R_DIMS>(kernel, LEVEL::PRIVATE, dim_range(L_DIMS, 0), loop_body_add_callback(
                                                        fu_level, inner_dimension2, inner_phase2, first_iteration2
                                                ), true, inner_phase2);
                                            }, true, inner_phase);
                                }, true, phase);
                        loop_body_reset = loops.from(
                                kernel, LEVEL::LOCAL, _l_dims_after_first_r_dim.empty() ? L(1) : _l_dims_after_first_r_dim.front(),
                                [&] (auto inner_level, const auto &inner_dimension, char *inner_phase, bool *first_iteration) -> std::string {
                                    return inner_loops.from(
                                            kernel, LEVEL::PRIVATE, L(1),
                                            [&](auto inner_level2, const auto &inner_dimension2, char *inner_phase2, bool *first_iteration2) -> std::string {
                                                return wrap_in_iterations_loop<L_DIMS, R_DIMS>(kernel, LEVEL::PRIVATE, dim_range(L_DIMS, 0), loop_body_reset_callback(
                                                        fu_level, inner_dimension2, inner_phase2, first_iteration2
                                                ), true, inner_phase2);
                                            }, true, inner_phase);
                                }, true, phase);
                        break;
                    }
                    case LEVEL::GLOBAL: {
                        auto loops = loop_generator<L_DIMS, R_DIMS>(kernel, _macros, _runtime_inputs, V(LEVEL::LOCAL, LEVEL::PRIVATE), dim_range(L_DIMS, 0), false, nullptr, false, _skip_post_processing);
                        loop_body_add = loops.from(
                                kernel, LEVEL::LOCAL, L(1),
                                [&](auto inner_level, const auto &inner_dimension, char *inner_phase, bool *first_iteration) -> std::string {
                                    return wrap_in_iterations_loop<L_DIMS, R_DIMS>(kernel, LEVEL::PRIVATE, dim_range(L_DIMS, 0), loop_body_add_callback(
                                            fu_level, inner_dimension, inner_phase, first_iteration
                                    ), true, inner_phase);
                                }, true, phase);
                        loop_body_reset = loops.from(
                                kernel, LEVEL::LOCAL, L(1),
                                [&](auto inner_level, const auto &inner_dimension, char *inner_phase, bool *first_iteration) -> std::string {
                                    return wrap_in_iterations_loop<L_DIMS, R_DIMS>(kernel, LEVEL::PRIVATE, dim_range(L_DIMS, 0), loop_body_reset_callback(
                                            fu_level, inner_dimension, inner_phase, first_iteration
                                    ), true, inner_phase);
                                }, true, phase);
                        break;
                    }
                }
            } else {
                loop_body_add = loop_body_add_callback(fu_level, r_dim, phase, nullptr);
                loop_body_reset = loop_body_reset_callback(fu_level, r_dim, phase, nullptr);
            }

            code << std::endl << stringf("#if %s > 1", _macros.num_fu(kernel, fu_level, r_dim));
            code << std::endl << stringf("// reduction in dimension %c_%d", r_dim.type, r_dim.nr);
            code << std::endl << "{";
            code << std::endl << indent(stringf("#if (%s & (%s - 1)) != 0",
                                                _macros.num_fu(kernel, fu_level, r_dim),
                                                _macros.num_fu(kernel, fu_level, r_dim)), 1);

            code << std::endl << indent("// pre processing: reduce number of values to largest possible power of 2", 1);
            code << std::endl << indent(stringf("size_t stride = 1 << (int)floor(log2((float) %s));",
                                                _macros.num_fu(kernel, fu_level, r_dim)), 1);
            if (!moved_ifs_condition.empty())
                code << std::endl << indent(stringf("if (%s) {", moved_ifs_condition), 1);
            code << std::endl << indent(stringf("if (%s < stride && %s + stride < %s%s) {",
                                                _macros.fu_id(kernel, fu_level, r_dim),
                                                _macros.fu_id(kernel, fu_level, r_dim),
                                                _macros.num_fu(kernel, fu_level, r_dim),
                                                concat(multi_stringf(" && %s == 0", _macros.fu_id(V(kernel), V(fu_level), previous_r_dims)))), 1 + !moved_ifs_condition.empty());
            code << std::endl << indent(loop_body_add, 2 + !moved_ifs_condition.empty());
            code << std::endl << indent("}", 1 + !moved_ifs_condition.empty());
            if (!moved_ifs_condition.empty())
                code << std::endl << indent("}", 1);
            code << std::endl << indent("stride /= 2;", 1);
            code << std::endl << indent(stringf("#if %c_CB_RES_DEST_LEVEL <= %s", cb_level, long_level(fu_level)), 1);
            code << std::endl << indent(barrier(fu_level, _cuda), 1);
            for (LEVEL p_level : parent_levels(fu_level, false, ORDER::ASCENDING)) {
                code << std::endl << indent(stringf("#elif %c_CB_RES_DEST_LEVEL == %s", cb_level, long_level(p_level)), 1);
                code << std::endl << indent(barrier(p_level, _cuda), 1);
            }
            code << std::endl << indent("#endif", 1); // for #if of barrier
            code << std::endl << indent("#else", 1);
            code << std::endl << indent(stringf("size_t stride = %s / 2;",
                                                _macros.num_fu(kernel, fu_level, r_dim)), 1);
            code << std::endl << indent("#endif", 1); // for #if number of values to reduce is a power of 2

            code << std::endl << indent("// perform reduction", 1);
            code << std::endl << indent("for (; stride > 0; stride /= 2) {", 1);
            if (!moved_ifs_condition.empty())
                code << std::endl << indent(stringf("if (%s) {", moved_ifs_condition), 2);
            code << std::endl << indent(stringf("if (%s < stride%s) {", _macros.fu_id(kernel, fu_level, r_dim),
                                                concat(multi_stringf(" && %s == 0", _macros.fu_id(V(kernel), V(fu_level), previous_r_dims)))), 2 + !moved_ifs_condition.empty());
            code << std::endl << indent(loop_body_add, 3 + !moved_ifs_condition.empty());
            code << std::endl << indent("}", 2 + !moved_ifs_condition.empty());
            if (cb_level == LEVEL::PRIVATE) {
                code << std::endl << indent(stringf("#if %c_CB_RES_DEST_LEVEL == %c_CB_RES_DEST_LEVEL", parent_level(cb_level), cb_level), 2 + !moved_ifs_condition.empty());
                code << std::endl << indent("else {", 2 + !moved_ifs_condition.empty());
                code << std::endl << indent(loop_body_reset, 3 + !moved_ifs_condition.empty());
                code << std::endl << indent("}", 2 + !moved_ifs_condition.empty());
                code << std::endl << indent("#endif", 2 + !moved_ifs_condition.empty());
            }
            if (!moved_ifs_condition.empty())
                code << std::endl << indent("}", 2);
            code << std::endl << indent(stringf("#if %c_CB_RES_DEST_LEVEL <= %s", cb_level, long_level(fu_level)), 2);
            code << std::endl << indent(barrier(fu_level, _cuda), 2);
            for (LEVEL p_level : parent_levels(fu_level, false, ORDER::ASCENDING)) {
                code << std::endl << indent(stringf("#elif %c_CB_RES_DEST_LEVEL == %s", cb_level, long_level(p_level)), 2);
                code << std::endl << indent(barrier(p_level, _cuda), 2);
            }
            code << std::endl << indent("#endif", 2); // for #if of barrier
            code << std::endl << indent("}", 1);

            code << std::endl << indent(stringf("#if %c_CB_RES_DEST_LEVEL <= %s", cb_level, long_level(fu_level)), 1);
            code << std::endl << indent(barrier(fu_level, _cuda), 1);
            for (LEVEL p_level : parent_levels(fu_level, false, ORDER::ASCENDING)) {
                code << std::endl << indent(stringf("#elif %c_CB_RES_DEST_LEVEL == %s", cb_level, long_level(p_level)), 1);
                code << std::endl << indent(barrier(p_level, _cuda), 1);
            }
            code << std::endl << indent("#endif", 1); // for #if of barrier
            code << std::endl << "}";
            code << std::endl << indent("#endif", 1); // for #if num_fu > 1

            previous_r_dims.push_back(r_dim);
            next_r_dims.erase(next_r_dims.begin());
        }
#else
        bool first_iteration_in_parent_level = true;
        for (const auto dim : dim_range(0, R_DIMS)) {
            first_iteration_in_parent_level = first_iteration_in_parent_level && first_iteration[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(cb_level), dim)];
        }
        bool reduce = false;
        if (R_DIMS > 0 && !first_iteration_in_parent_level) {
            reduce = true;
        }
        // barrier to wait for all FUs to complete processing
        code << stringf("#if %c_CB_RES_DEST_LEVEL < %s", cb_level, long_level(fu_level));
        code << std::endl << "// reduction in separate memory location";
        if (cb_level == LEVEL::PRIVATE) {
            code << std::endl << "// no barrier necessary";
        } else {
            code << std::endl << barrier(fu_level, _cuda);
        }
        for (LEVEL p_level : parent_levels(fu_level, true, ORDER::ASCENDING)) {
            code << std::endl << stringf("#elif %c_CB_RES_DEST_LEVEL == %s", cb_level, long_level(p_level));
            code << std::endl << barrier(p_level, _cuda);
        }
        code << std::endl << "#endif"; // for #if of reduction memory selection

        auto loop_body_callback = [&] (auto inner_level, const auto &inner_dimension, char *inner_phase, bool *first_iteration) -> std::string {
            std::stringstream code;

            std::string moved_ifs_condition;
            for (const auto dim : _dimension_order) {
                if (dim.type == DIM_TYPE::R) continue;

                bool dim_in_phase3 = false;
                LEVEL dim_top_level_in_phase3;
                for (LEVEL p_level : parent_levels(LEVEL::PRIVATE, true)) {
                    if (inner_phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(p_level, dim)] == 3) {
                        dim_in_phase3 = true;
                        dim_top_level_in_phase3 = p_level;
                        break;
                    }
                }
                if (dim_in_phase3) {
                    if (!moved_ifs_condition.empty()) moved_ifs_condition += " && ";
                    if (dim_top_level_in_phase3 == LEVEL::PRIVATE) {
                        moved_ifs_condition.append(stringf("(%s < (%s + %s - 1) / %s)",
                                                           _macros.fu_id(kernel, parent_level(LEVEL::PRIVATE), dim),
                                                           _macros.num_extra_elements(kernel, LEVEL::PRIVATE, dim, inner_phase),
                                                           _macros.num_fu(kernel, LEVEL::PRIVATE, dim),
                                                           _macros.num_fu(kernel, LEVEL::PRIVATE, dim)));
                    } else {
                        moved_ifs_condition.append(stringf("(%s < %s - %s * %s)",
                                                           _macros.fu_id(kernel, parent_level(LEVEL::PRIVATE), dim),
                                                           _macros.num_extra_elements(kernel, dim_top_level_in_phase3, dim),
                                                           _macros.fu_id(kernel, parent_level(dim_top_level_in_phase3), dim),
                                                           _macros.num_fu(kernel, dim_top_level_in_phase3, dim)));
                    }
                }
            }

            std::vector<std::string> mask_active_wi;
            for (const auto dim : dim_range(L_DIMS, 0)) {
                if (inner_phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(fu_level, dim)] == 3) {
                    mask_active_wi.push_back(stringf("P3_NUM_ACTIVE_WI_%c_%d", dim.type, dim.nr));
                } else if (inner_phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(fu_level, dim)] == 2) {
                    LEVEL first_complete_level = LEVEL::GLOBAL;
                    for (const auto p_level : parent_levels(cb_level, false, ORDER::ASCENDING)) {
                        if (inner_phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(p_level, dim)] == 1) {
                            first_complete_level = p_level;
                            break;
                        }
                    }
                    mask_active_wi.push_back(stringf("NUM_ACTIVE_WI_IN_COMPLETE_%c_CB_%c_%d", first_complete_level, dim.type, dim.nr));
                } else {
                    mask_active_wi.push_back(stringf("NUM_ACTIVE_WI_%c_%d", dim.type, dim.nr));
                }
            }
            std::vector<std::string> num_elems_to_reduce(R_DIMS);
            for (const auto dim : dim_range(0, R_DIMS)) {
                if (inner_phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(cb_level), dim)] == 3) {
                    num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(dim) - L_DIMS] = stringf("P3_NUM_ACTIVE_WI_%c_%d", dim.type, dim.nr);
                    mask_active_wi.push_back(stringf("P3_NUM_ACTIVE_WI_%c_%d", dim.type, dim.nr));
                } else if (inner_phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(cb_level), dim)] == 2) {
                    LEVEL first_complete_level = LEVEL::GLOBAL;
                    for (const auto p_level : parent_levels(cb_level, false, ORDER::ASCENDING)) {
                        if (inner_phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(p_level, dim)] == 1) {
                            first_complete_level = p_level;
                            break;
                        }
                    }
                    num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(dim) - L_DIMS] = stringf("NUM_ACTIVE_WI_IN_COMPLETE_%c_CB_%c_%d", first_complete_level, dim.type, dim.nr);
                    mask_active_wi.push_back(stringf("NUM_ACTIVE_WI_IN_COMPLETE_%c_CB_%c_%d", first_complete_level, dim.type, dim.nr));
                } else {
                    num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(dim) - L_DIMS] = stringf("NUM_ACTIVE_WI_%c_%d", dim.type, dim.nr);
                    mask_active_wi.push_back(stringf("NUM_ACTIVE_WI_%c_%d", dim.type, dim.nr));
                }
            }

            auto params_reduction_mem = _macros.fu_id(V(kernel), V(fu_level), dim_range(L_DIMS, R_DIMS));
            auto params_reduction_mem_after_shuffle = params_reduction_mem;
            for (int i = 0; i < R_DIMS; ++i)
                params_reduction_mem_after_shuffle[L_DIMS + i] = stringf("(%s / 32)", params_reduction_mem_after_shuffle[L_DIMS + i]);

            std::string copy_code;
            copy_code.append(stringf("#if %c_CB_RES_DEST_LEVEL < %s\n", cb_level, long_level(fu_level)));
            copy_code.append("// copy values for reduction in separate memory location\n");
            if (!moved_ifs_condition.empty()) {
                copy_code.append(stringf("if (%s)\n", moved_ifs_condition));
            }
            copy_code.append("{\n");
            copy_code.append(indent(stringf("#if %s\n", concat(
                    multi_stringf("(!defined(STATIC_%s) || STATIC_%s < %s)", num_elems_to_reduce, num_elems_to_reduce, _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS))),
                    " || ")), 1));
            copy_code.append(indent("if (true\n", 1));
            copy_code.append(indent(concat(
                    multi_stringf("#if !defined(STATIC_%s) || STATIC_%s < %s\n    && (%s < %s)\n#endif",
                                  num_elems_to_reduce, num_elems_to_reduce, _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)),
                                  _macros.fu_id(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS)), num_elems_to_reduce),
                    "\n"), 1));
            copy_code.append(indent("\n)\n", 1));
            copy_code.append(indent("#endif\n", 1));
            copy_code.append(indent("{\n", 1));

            // shuffle reduction
            if (_cuda) {
                copy_code.append(indent(stringf("\n#if %s", shuffle_reduction_condition(kernel)), 2));
                std::vector<dimension_t> previous_r_dims;
                for (const auto &r_dim : reverse(_dimension_order)) {
                    if (r_dim.type != DIM_TYPE::R) continue;

                    copy_code.append(indent(stringf("\n// shuffle reduction in dimension %c_%d", r_dim.type, r_dim.nr), 2));

                    if (!previous_r_dims.empty())
                        copy_code.append(indent(stringf("\nif (%s)", concat(multi_stringf("%s == 0", _macros.fu_id(V(kernel), V(fu_level), previous_r_dims)), " && ")), 2));
                    copy_code.append(indent("\n{", 2));

                    copy_code.append(indent(stringf("\n#if !defined(STATIC_%s) || (STATIC_%s %% 32 != 0 && STATIC_%s < %s)",
                                                    num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(r_dim) - L_DIMS],
                                                    num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(r_dim) - L_DIMS],
                                                    num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(r_dim) - L_DIMS],
                                                    _macros.num_fu(kernel, fu_level, r_dim)), 3));
                    copy_code.append(indent(stringf("\n#if !defined(STATIC_%s)", num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(r_dim) - L_DIMS]), 3));
                    copy_code.append(indent(stringf("\nif (%s %% 32 != 0 && %s < %s)",
                                                    num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(r_dim) - L_DIMS],
                                                    num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(r_dim) - L_DIMS],
                                                    _macros.num_fu(kernel, fu_level, r_dim)), 3));
                    copy_code.append(indent("\n#endif", 3));
                    copy_code.append(indent(stringf("\nif (%s >= %s && %s < (%s + 31) / 32 * 32)",
                                                    _macros.fu_id(kernel, fu_level, r_dim),
                                                    num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(r_dim) - L_DIMS],
                                                    _macros.fu_id(kernel, fu_level, r_dim),
                                                    num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(r_dim) - L_DIMS]
                    ), 3));
                    for_each(_result_buffer, [&](const result_buffer_class &output) {
                        copy_code.append(indent(stringf(
                                "\n%s = 0;",
                                this->call_fu_res_macro(output.name(), kernel, cb_level, inner_phase, _macros.iteration_to_index_conversion(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), iterations_loop_variable(LEVEL::PRIVATE, dim_range(L_DIMS, 0))))
                        ), 3));
                    });
                    copy_code.append(indent("\n#endif", 3));

                    copy_code.append(indent("\n{", 3));
                    copy_code.append(indent(stringf("\n#ifdef STATIC_%s", num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(r_dim) - L_DIMS]), 4));
                    copy_code.append(indent(stringf("\n#if STATIC_%s < 32", num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(r_dim) - L_DIMS]), 4));
                    copy_code.append(indent(stringf("\nsize_t stride = 1 << ((int) ceil(log2((float) STATIC_%s)) - 1);", num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(r_dim) - L_DIMS]), 4));
                    copy_code.append(indent("\n#else", 4));
                    copy_code.append(indent("\nsize_t stride = 16;", 4));
                    copy_code.append(indent("\n#endif", 4));
                    copy_code.append(indent("\n#else", 4));
                    copy_code.append(indent(stringf("\nsize_t stride = min(1 << ((int) ceil(log2((float) %s)) - 1), 16);", num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(r_dim) - L_DIMS]), 4));
                    copy_code.append(indent("\n#endif", 4));

                    copy_code.append(indent(stringf("\n#if %s",concat(multi_stringf("defined(STATIC_%s)", mask_active_wi), " && ")), 4));
                    copy_code.append(indent(stringf("\n#if %s < 32", concat(mask_active_wi, " * ")), 4));
                    copy_code.append(indent(stringf("\nunsigned mask = (1 << (%s)) - 1;", concat(mask_active_wi, " * ")), 4));
                    copy_code.append(indent("\n#else", 4));
                    copy_code.append(indent("\nunsigned mask = 0xffffffff;", 4));
                    copy_code.append(indent("\n#endif", 4));
                    copy_code.append(indent("\n#else", 4));
                    copy_code.append(indent("\nunsigned mask;", 4));
                    copy_code.append(indent(stringf("\nif (%s < 32)", concat(mask_active_wi, " * ")), 4));
                    copy_code.append(indent(stringf("\nmask = (1 << (%s)) - 1;", concat(mask_active_wi, " * ")), 5));
                    copy_code.append(indent("\nelse", 4));
                    copy_code.append(indent("\nmask = 0xffffffff;", 5));
                    copy_code.append(indent("\n#endif", 4));

                    copy_code.append(indent("\nfor (;stride > 0; stride /= 2)", 4));
                    for_each(_result_buffer, [&](const result_buffer_class &output) {
                        copy_code.append(indent(stringf(
                                "\n%s;",
                                output.reduce_and_store(
                                        this->call_fu_res_macro(output.name(), kernel, cb_level, inner_phase, _macros.iteration_to_index_conversion(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), iterations_loop_variable(LEVEL::PRIVATE, dim_range(L_DIMS, 0)))),
                                        stringf("__shfl_down_sync(mask, %s, stride)", this->call_fu_res_macro(output.name(), kernel, cb_level, inner_phase, _macros.iteration_to_index_conversion(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), iterations_loop_variable(LEVEL::PRIVATE, dim_range(L_DIMS, 0)))))
                                )
                        ), 5));
                    });
                    copy_code.append(indent("\n}", 3));
                    copy_code.append(indent("\n}", 2));

                    copy_code.append(indent(stringf("\n#define AFTER_SHUFFLE_%s ((%s + 31) / 32)",
                                                    num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(r_dim) - L_DIMS],
                                                    num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(r_dim) - L_DIMS]), 2));
                    copy_code.append(indent(stringf("\n#ifdef STATIC_%s",
                                                    num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(r_dim) - L_DIMS]), 2));
                    copy_code.append(indent(stringf("\n#define STATIC_AFTER_SHUFFLE_%s AFTER_SHUFFLE_%s",
                                                    num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(r_dim) - L_DIMS],
                                                    num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(r_dim) - L_DIMS]), 2));
                    copy_code.append("\n#endif\n");

                    previous_r_dims.push_back(r_dim);
                }
                copy_code.append(indent("\n#endif", 2));
                copy_code.append("\n");
                copy_code.append("\n");
            }

            for_each(_result_buffer, [&](const result_buffer_class &output) {
                if (_cuda) {
                    copy_code.append(indent(stringf("#if %s\n", shuffle_reduction_condition(kernel)), 2));
                    copy_code.append(indent(stringf("#if (%s)\n", concat(multi_stringf("(!defined(STATIC_AFTER_SHUFFLE_%s) || STATIC_AFTER_SHUFFLE_%s > 1)", num_elems_to_reduce, num_elems_to_reduce), " || ")), 2));
                    copy_code.append(indent(stringf("#if (%s)\n", concat(multi_stringf("!defined(STATIC_AFTER_SHUFFLE_%s)", num_elems_to_reduce), " || ")), 2));
                    copy_code.append(indent("if (true\n", 2));
                    for (int i = 0; i < R_DIMS; ++i) {
                        copy_code.append(indent(stringf("#if !defined(STATIC_AFTER_SHUFFLE_%s)\n", num_elems_to_reduce[i]), 2));
                        copy_code.append(indent(stringf("    && (AFTER_SHUFFLE_%s > 1)\n", num_elems_to_reduce[i]), 2));
                        copy_code.append(indent("#endif\n", 2));
                    }
                    copy_code.append(")\n#endif\n");
                    copy_code.append(indent(stringf("if (%s)\n  ", concat(multi_stringf("(%s %% 32 == 0)", _macros.fu_id(V(kernel), V(fu_level), dim_range(0, R_DIMS))), " && ")), 2));
                    copy_code.append(indent(stringf(
                            "%s = %s;\n",
                            this->call_reduction_mem_macro(output.name(), kernel, fu_level, params_reduction_mem_after_shuffle),
                            this->call_fu_res_macro(output.name(), kernel, cb_level, inner_phase, _macros.iteration_to_index_conversion(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), iterations_loop_variable(LEVEL::PRIVATE, dim_range(L_DIMS, 0))))), 2));
                    copy_code.append("\n    #endif\n");
                    copy_code.append(indent(stringf("#if (%s)\n", concat(multi_stringf("(!defined(STATIC_AFTER_SHUFFLE_%s) || STATIC_AFTER_SHUFFLE_%s == 1)", num_elems_to_reduce, num_elems_to_reduce), " && ")), 2));
                    copy_code.append(indent(stringf("#if (%s)\n", concat(multi_stringf("!defined(STATIC_AFTER_SHUFFLE_%s)", num_elems_to_reduce), " || ")), 2));
                    copy_code.append(indent("if (true\n", 2));
                    for (int i = 0; i < R_DIMS; ++i) {
                        copy_code.append(indent(stringf("#if !defined(STATIC_AFTER_SHUFFLE_%s)\n", num_elems_to_reduce[i]), 2));
                        copy_code.append(indent(stringf("    && (AFTER_SHUFFLE_%s == 1)\n", num_elems_to_reduce[i]), 2));
                        copy_code.append(indent("#endif\n", 2));
                    }
                    copy_code.append(")\n#endif\n");
                    copy_code.append(indent(stringf(
                            "#if %c_CB_RES_DEST_LEVEL != %c_CB_RES_DEST_LEVEL\n",
                            parent_level(cb_level),
                            cb_level
                    ), 2));
                    copy_code.append(indent(stringf("if (%s)\n  ", concat(multi_stringf("(%s == 0)", _macros.fu_id(V(kernel), V(fu_level), dim_range(0, R_DIMS))), " && ")), 2));
                    if (reduce) {
                        copy_code.append(indent(stringf("%s;\n",
                                                        output.reduce_and_store(
                                                                this->call_fu_res_macro(output.name(), kernel, parent_level(cb_level), inner_phase, _macros.iteration_to_index_conversion(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), iterations_loop_variable(LEVEL::PRIVATE, dim_range(L_DIMS, 0)))),
                                                                this->call_fu_res_macro(output.name(), kernel, cb_level, inner_phase, _macros.iteration_to_index_conversion(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), iterations_loop_variable(LEVEL::PRIVATE, dim_range(L_DIMS, 0)))))), 2));
                    } else {
                        copy_code.append(indent(stringf(
                                "%s = %s;\n",
                                this->call_fu_res_macro(output.name(), kernel, parent_level(cb_level), inner_phase, _macros.iteration_to_index_conversion(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), iterations_loop_variable(LEVEL::PRIVATE, dim_range(L_DIMS, 0)))),
                                this->call_fu_res_macro(output.name(), kernel, cb_level, inner_phase, _macros.iteration_to_index_conversion(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), iterations_loop_variable(LEVEL::PRIVATE, dim_range(L_DIMS, 0))))), 2));
                    }
                    copy_code.append("\n    #endif");
                    copy_code.append("\n    #endif");
                    copy_code.append(indent("\n#else\n", 2));
                }
                copy_code.append(indent(stringf(
                        "%s = %s;\n",
                        this->call_reduction_mem_macro(output.name(), kernel, fu_level, params_reduction_mem),
                        this->call_fu_res_macro(output.name(), kernel, cb_level, inner_phase, _macros.iteration_to_index_conversion(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), iterations_loop_variable(LEVEL::PRIVATE, dim_range(L_DIMS, 0))))), 2));
                if (_cuda) {
                    copy_code.append(indent("\n#endif\n", 2));
                }
                copy_code.append(indent("}\n", 1));
                if (_cuda) {
                    copy_code.append(indent(stringf(
                            "#if %c_CB_RES_DEST_LEVEL == %c_CB_RES_DEST_LEVEL\n",
                            parent_level(cb_level),
                            cb_level
                    ), 1));
                    copy_code.append(indent(stringf("if (%s)\n  ", concat(multi_stringf("(%s != 0)", _macros.fu_id(V(kernel), V(fu_level), dim_range(0, R_DIMS))), " || ")), 1));
                    copy_code.append(indent("#endif\n", 1));
                }
                copy_code.append(indent(stringf(
                        "%s = 0;\n",
                        this->call_fu_res_macro(output.name(), kernel, cb_level, inner_phase, _macros.iteration_to_index_conversion(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), iterations_loop_variable(LEVEL::PRIVATE, dim_range(L_DIMS, 0))))), 1));
                copy_code.append("}\n");
            });
            copy_code.append(barrier(fu_level, _cuda));
            if (_cuda) {
                copy_code.append("\n#else\n");
                copy_code.append(stringf("\n#if %s", shuffle_reduction_condition(kernel)));
                for (const auto r_dim : dim_range(0, R_DIMS)) {
                    copy_code.append(stringf("\n#define AFTER_SHUFFLE_%s %s",
                                             num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(r_dim) - L_DIMS],
                                             num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(r_dim) - L_DIMS]));
                }
                for (const auto r_dim : dim_range(0, R_DIMS)) {
                    copy_code.append(stringf("\n#ifdef STATIC_%s",
                                             num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(r_dim) - L_DIMS]));
                    copy_code.append(stringf("\n#define STATIC_AFTER_SHUFFLE_%s AFTER_SHUFFLE_%s",
                                             num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(r_dim) - L_DIMS],
                                             num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(r_dim) - L_DIMS]));
                    copy_code.append("\n#endif\n");
                }
                copy_code.append("\n#endif\n");
            }
            copy_code.append("\n#endif");
            code << std::endl << copy_code;

            for (int shuffle = 0; shuffle <= (_cuda ? 1 : 0); ++shuffle) {
                if (shuffle) {
                    for (const auto r_dim : dim_range(0, R_DIMS))
                        num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(r_dim) - L_DIMS] = stringf("AFTER_SHUFFLE_%s", num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(r_dim) - L_DIMS]);
                }
                if (_cuda) {
                    if (!shuffle)
                        code << std::endl << stringf("#if !(%s)", shuffle_reduction_condition(kernel));
                    else {
                        code << std::endl << stringf("#elif (%s)", concat(multi_stringf("(!defined(STATIC_%s) || STATIC_%s > 1)", num_elems_to_reduce, num_elems_to_reduce), " || "));
                        code << std::endl << stringf("#if (%s)", concat(multi_stringf("!defined(STATIC_%s)", num_elems_to_reduce), " || "));
                        code << std::endl << "if (false";
                        for (int i = 0; i < R_DIMS; ++i) {
                            code << std::endl << stringf("#if !defined(STATIC_%s)", num_elems_to_reduce[i]);
                            code << std::endl << stringf("    || (%s > 1)", num_elems_to_reduce[i]);
                            code << std::endl << "#endif";
                        }
                        code << std::endl << ")";
                        code << std::endl << "#endif";
                        code << std::endl << "{";
                    }
                }

                std::vector<dimension_t> next_r_dims;
                for (const auto &r_dim : reverse(_dimension_order)) {
                    if (r_dim.type != DIM_TYPE::R) continue;
                    next_r_dims.push_back(r_dim);
                }
                std::vector<dimension_t> previous_r_dims;
                for (const auto &r_dim : reverse(_dimension_order)) {
                    if (r_dim.type != DIM_TYPE::R) continue;

                    auto params_reduction_mem_strided = _macros.fu_id(V(kernel), V(fu_level), dim_range(L_DIMS, 0));
                    for (int i = 1; i <= R_DIMS; ++i) {
                        if (shuffle) {
                            if (i == r_dim.nr)
                                params_reduction_mem_strided.push_back(stringf("((%s) + stride)", params_reduction_mem_after_shuffle[L_DIMS + i - 1]));
                            else
                                params_reduction_mem_strided.push_back(params_reduction_mem_after_shuffle[L_DIMS + i - 1]);
                        } else {
                            if (i == r_dim.nr)
                                params_reduction_mem_strided.push_back(stringf("((%s) + stride)", params_reduction_mem[L_DIMS + i - 1]));
                            else
                                params_reduction_mem_strided.push_back(params_reduction_mem[L_DIMS + i - 1]);
                        }
                    }

                    std::string reduction_step_code;
                    reduction_step_code.append(stringf("#if %c_CB_RES_DEST_LEVEL < %s\n", cb_level, long_level(fu_level)));
                    reduction_step_code.append("// reduction in separate memory location\n");
                    for_each(_result_buffer, [&](const result_buffer_class &output) {
                        reduction_step_code.append(stringf(
                                "%s;\n",
                                output.reduce_and_store(
                                        this->call_reduction_mem_macro(output.name(), kernel, fu_level, params_reduction_mem),
                                        this->call_reduction_mem_macro(output.name(), kernel, fu_level, params_reduction_mem_strided)
                                )
                        ));
                    });
                    unsigned int p_level_nr = 0;
                    for (LEVEL p_level : parent_levels(fu_level, true, ORDER::ASCENDING)) {
                        auto params = _macros.iteration_to_index_conversion(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), iterations_loop_variable(LEVEL::PRIVATE, dim_range(L_DIMS, 0)));
                        auto params_strided = _macros.iteration_to_index_conversion(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), iterations_loop_variable(LEVEL::PRIVATE, dim_range(L_DIMS, 0)));
                        for (int i = 0; i < L_DIMS; ++i) {
                            for (auto sub : sub_levels(p_level, false, ASCENDING)) {
                                params[i] = _macros.fu_index_conversion(kernel, sub, L(i + 1), params[i], inner_phase);
                                params_strided[i] = _macros.fu_index_conversion(kernel, sub, L(i + 1), params_strided[i], inner_phase);
                            }
                        }
                        for (int i = 1; i <= R_DIMS; ++i) {
                            if (p_level != fu_level) {
                                for (auto l : level_range(p_level, parent_level(fu_level))) {
                                    params.push_back(_macros.fu_id(kernel, l, R(i)));
                                    params_strided.push_back(_macros.fu_id(kernel, l, R(i)));
                                }
                            }
                            params.push_back(_macros.fu_id(kernel, fu_level, R(i)));
                            if (i == r_dim.nr) {
                                params_strided.push_back(stringf("%s + stride", _macros.fu_id(kernel, fu_level, R(i))));
                            } else {
                                params_strided.push_back(_macros.fu_id(kernel, fu_level, R(i)));
                            }
                            for (auto l : level_range(sub_level(fu_level), LEVEL::PRIVATE)) {
                                params.push_back(_macros.fu_id(kernel, l, R(i)));
                                params_strided.push_back(_macros.fu_id(kernel, l, R(i)));
                            }
                        }
                        reduction_step_code.append(stringf("#elif %c_CB_RES_DEST_LEVEL == %s\n", cb_level, long_level(p_level)));
                        for_each(_result_buffer, [&](const result_buffer_class &output) {
                            reduction_step_code.append(stringf(
                                    "%s;\n",
                                    output.reduce_and_store(
                                            this->call_buffer_macro(output.name(), kernel, p_level, params),
                                            this->call_buffer_macro(output.name(), kernel, p_level, params_strided)
                                    )
                            ));
                        });
                        ++p_level_nr;
                    }
                    reduction_step_code.append("#endif\n"); // for #if of reduction memory selection
                    std::string reset_code;
                    for (LEVEL p_level : {LEVEL::LOCAL, LEVEL::GLOBAL}) {
                        auto params = _macros.iteration_to_index_conversion(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), iterations_loop_variable(LEVEL::PRIVATE, dim_range(L_DIMS, 0)));
                        for (int i = 0; i < L_DIMS; ++i) {
                            for (auto sub : sub_levels(p_level, false, ASCENDING)) {
                                params[i] = _macros.fu_index_conversion(kernel, sub, L(i + 1), params[i], inner_phase);
                            }
                        }
                        for (int i = 1; i <= R_DIMS; ++i) {
                            for (auto l : level_range(p_level, LEVEL::PRIVATE)) {
                                params.push_back(_macros.fu_id(kernel, l, R(i)));
                            }
                        }
                        reset_code.append(stringf("%s %c_CB_RES_DEST_LEVEL == %s\n", p_level == LEVEL::LOCAL ? "#if" : "#elif", cb_level, long_level(p_level)));
                        for_each(_result_buffer, [&](const result_buffer_class &output) {
                            reset_code.append(stringf("%s = 0;\n", this->call_buffer_macro(output.name(), kernel, p_level, params)));
                        });
                    }
                    reset_code.append("#endif\n"); // for #if of reduction memory selection

                    code << std::endl << stringf("#if %s > 1", _macros.num_fu(kernel, fu_level, r_dim));
                    code << std::endl << stringf("// reduction in dimension %c_%d", r_dim.type, r_dim.nr);
                    code << std::endl << "{";
                    code << std::endl << indent(stringf("#if !defined(STATIC_%s) || (STATIC_%s & (STATIC_%s - 1)) != 0", num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(r_dim) - L_DIMS], num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(r_dim) - L_DIMS], num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(r_dim) - L_DIMS]), 1);
                    code << std::endl << indent("// pre processing: reduce number of values to largest possible power of 2", 1);
                    code << std::endl << indent(stringf("size_t stride = 1 << (int) floor(log2((float) %s));", num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(r_dim) - L_DIMS]), 1);
                    if (!moved_ifs_condition.empty())
                        code << std::endl << indent(stringf("if (%s) {", moved_ifs_condition), 1);
                    for (const auto next_r_dim : next_r_dims) {
                        code << std::endl << indent(stringf("#if !defined(STATIC_%s) || (STATIC_%s < %s)",
                                                            num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(next_r_dim) - L_DIMS],
                                                            num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(next_r_dim) - L_DIMS],
                                                            _macros.num_fu(kernel, fu_level, next_r_dim)
                        ), 1 + !moved_ifs_condition.empty());
                        code << std::endl << indent(stringf("if (%s < %s)",
                                                            _macros.fu_id(kernel, fu_level, next_r_dim),
                                                            num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(next_r_dim) - L_DIMS]), 1 + !moved_ifs_condition.empty());
                        code << std::endl << indent("#endif", 1 + !moved_ifs_condition.empty());
                    }
                    code << std::endl << indent(stringf("if (%s < stride && %s + stride < %s%s) {",
                                                        _macros.fu_id(kernel, fu_level, r_dim),
                                                        _macros.fu_id(kernel, fu_level, r_dim),
                                                        num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(r_dim) - L_DIMS],
                                                        concat(multi_stringf(" && %s == 0", _macros.fu_id(V(kernel), V(fu_level), previous_r_dims)))), 2 + !moved_ifs_condition.empty());
                    code << std::endl << indent(reduction_step_code, 3 + !moved_ifs_condition.empty());


                    code << std::endl << indent("}", 2 + !moved_ifs_condition.empty());
                    if (!moved_ifs_condition.empty())
                        code << std::endl << indent("}", 1);
                    code << std::endl << indent("stride /= 2;", 1);
                    code << std::endl << indent(stringf("#if %c_CB_RES_DEST_LEVEL <= %s", cb_level, long_level(fu_level)), 1);
                    code << std::endl << indent(barrier(fu_level, _cuda), 1);
                    for (LEVEL p_level : parent_levels(fu_level, false, ORDER::ASCENDING)) {
                        code << std::endl << indent(stringf("#elif %c_CB_RES_DEST_LEVEL == %s", cb_level, long_level(p_level)), 1);
                        code << std::endl << indent(barrier(p_level, _cuda), 1);
                    }
                    code << std::endl << indent("#endif", 1); // for #if of barrier
                    code << std::endl << indent("#else", 1);
                    code << std::endl << indent(stringf("size_t stride = %s / 2;", num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(r_dim) - L_DIMS]), 1);
                    code << std::endl << indent("#endif", 1); // for #if number of values to reduce is a power of 2

                    code << std::endl << indent("// perform reduction", 1);
                    code << std::endl << indent("for (; stride > 0; stride /= 2) {", 1);
                    if (!moved_ifs_condition.empty())
                        code << std::endl << indent(stringf("if (%s) {", moved_ifs_condition), 2);
                    for (const auto next_r_dim : next_r_dims) {
                        code << std::endl << indent(stringf("#if !defined(STATIC_%s) || (STATIC_%s < %s)",
                                                            num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(next_r_dim) - L_DIMS],
                                                            num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(next_r_dim) - L_DIMS],
                                                            _macros.num_fu(kernel, fu_level, next_r_dim)
                        ), 2 + !moved_ifs_condition.empty());
                        code << std::endl << indent(stringf("if (%s < %s)",
                                                            _macros.fu_id(kernel, fu_level, next_r_dim),
                                                            num_elems_to_reduce[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(next_r_dim) - L_DIMS]), 2 + !moved_ifs_condition.empty());
                        code << std::endl << indent("#endif", 2 + !moved_ifs_condition.empty());
                    }
                    code << std::endl << indent(stringf("if (%s < stride%s) {", _macros.fu_id(kernel, fu_level, r_dim),
                                                        concat(multi_stringf(" && %s == 0", _macros.fu_id(V(kernel), V(fu_level), previous_r_dims)))), 3 + !moved_ifs_condition.empty());
                    code << std::endl << indent(reduction_step_code, 4 + !moved_ifs_condition.empty());
                    code << std::endl << indent("}", 3 + !moved_ifs_condition.empty());
                    if (cb_level == LEVEL::PRIVATE) {
                        code << std::endl << indent(stringf("#if %c_CB_RES_DEST_LEVEL == %c_CB_RES_DEST_LEVEL", parent_level(cb_level), cb_level), 3 + !moved_ifs_condition.empty());
                        code << std::endl << indent(stringf("else if (%s >= 2 * stride%s) {", _macros.fu_id(kernel, fu_level, r_dim), concat(multi_stringf(" && %s == 0", _macros.fu_id(V(kernel), V(fu_level), previous_r_dims)))), 3 + !moved_ifs_condition.empty());
                        code << std::endl << indent(reset_code, 4 + !moved_ifs_condition.empty());
                        code << std::endl << indent("}", 3 + !moved_ifs_condition.empty());
                        code << std::endl << indent("#endif", 3 + !moved_ifs_condition.empty());
                    }
                    if (!moved_ifs_condition.empty())
                        code << std::endl << indent("}", 2);
                    code << std::endl << indent(stringf("#if %c_CB_RES_DEST_LEVEL <= %s", cb_level, long_level(fu_level)), 2);
                    code << std::endl << indent(barrier(fu_level, _cuda), 2);
                    for (LEVEL p_level : parent_levels(fu_level, false, ORDER::ASCENDING)) {
                        code << std::endl << indent(stringf("#elif %c_CB_RES_DEST_LEVEL == %s", cb_level, long_level(p_level)), 2);
                        code << std::endl << indent(barrier(p_level, _cuda), 2);
                    }
                    code << std::endl << indent("#endif", 2); // for #if of barrier
                    code << std::endl << indent("}", 1);
                    if (cb_level == LEVEL::PRIVATE) {
                        code << std::endl << indent(stringf("#if %c_CB_RES_DEST_LEVEL == %c_CB_RES_DEST_LEVEL", parent_level(cb_level), cb_level), 1);
                        code << std::endl << indent(stringf("if (%s == 1%s%s) {", _macros.fu_id(kernel, fu_level, r_dim), concat(multi_stringf(" && %s == 0", _macros.fu_id(V(kernel), V(fu_level), previous_r_dims))), moved_ifs_condition.empty() ? "" : stringf(" && (%s)", moved_ifs_condition)), 1);
                        code << std::endl << indent(reset_code, 2);
                        code << std::endl << indent("}", 1);
                        code << std::endl << indent("#endif", 1);
                    }
                    code << std::endl << indent(stringf("#if %c_CB_RES_DEST_LEVEL <= %s", cb_level, long_level(fu_level)), 1);
                    code << std::endl << indent(barrier(fu_level, _cuda), 1);
                    for (LEVEL p_level : parent_levels(fu_level, false, ORDER::ASCENDING)) {
                        code << std::endl << indent(stringf("#elif %c_CB_RES_DEST_LEVEL == %s", cb_level, long_level(p_level)), 1);
                        code << std::endl << indent(barrier(p_level, _cuda), 1);
                    }
                    code << std::endl << indent("#endif", 1); // for #if of barrier
                    code << std::endl << "}";
                    code << std::endl << "#endif"; // for #if num_fu > 1

                    previous_r_dims.push_back(r_dim);
                    next_r_dims.erase(next_r_dims.begin());
                }

                code << std::endl << stringf("#if %c_CB_RES_DEST_LEVEL < %s", cb_level, long_level(fu_level));
                code << std::endl << "// copy reduced values from reduction memory to cache block result memory";
                if (shuffle) {
                    code << std::endl << stringf("#if (%s)", concat(multi_stringf("(!defined(STATIC_%s) || STATIC_%s > 1)", num_elems_to_reduce, num_elems_to_reduce), " || "));
                    code << std::endl << stringf("#if (%s)", concat(multi_stringf("!defined(STATIC_%s)", num_elems_to_reduce), " || "));
                    code << std::endl << "if (true";
                    for (int i = 0; i < R_DIMS; ++i) {
                        code << std::endl << stringf("#if !defined(STATIC_%s)", num_elems_to_reduce[i]);
                        code << std::endl << stringf("    || (%s > 1)", num_elems_to_reduce[i]);
                        code << std::endl << "#endif";
                    }
                    code << std::endl << ")\n#endif\n";
                    code << std::endl << stringf("if (%s%s%s) {", concat(multi_stringf("%s == 0", _macros.fu_id(V(kernel), V(fu_level), dim_range(0, R_DIMS))), " && "), !moved_ifs_condition.empty() ? " && " : "", moved_ifs_condition);
                    for_each(_result_buffer, [&](const result_buffer_class &output) {
                        if (reduce) {
                            code << std::endl << indent(stringf("%s;",
                                                                output.reduce_and_store(
                                                                        this->call_fu_res_macro(output.name(), kernel, parent_level(cb_level), inner_phase, _macros.iteration_to_index_conversion(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), iterations_loop_variable(LEVEL::PRIVATE, dim_range(L_DIMS, 0)))),
                                                                        this->call_reduction_mem_macro(output.name(), kernel, fu_level, params_reduction_mem_after_shuffle)
                                                                )
                            ), 1);
                        } else {
                            code << std::endl << indent(stringf("%s = %s;",
                                                                this->call_fu_res_macro(output.name(), kernel, parent_level(cb_level), inner_phase, _macros.iteration_to_index_conversion(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), iterations_loop_variable(LEVEL::PRIVATE, dim_range(L_DIMS, 0)))),
                                                                this->call_reduction_mem_macro(output.name(), kernel, fu_level, params_reduction_mem_after_shuffle)
                            ), 1);
                        }
                    });
                    code << std::endl << "}";
                    code << std::endl << "#endif";
                } else {
                    code << std::endl << stringf("if (%s%s%s) {", concat(multi_stringf("%s == 0", _macros.fu_id(V(kernel), V(fu_level), dim_range(0, R_DIMS))), " && "), !moved_ifs_condition.empty() ? " && " : "", moved_ifs_condition);
                    for_each(_result_buffer, [&](const result_buffer_class &output) {
                        if (reduce) {
                            code << std::endl << indent(stringf("%s;",
                                                                output.reduce_and_store(
                                                                        this->call_fu_res_macro(output.name(), kernel, parent_level(cb_level), inner_phase, _macros.iteration_to_index_conversion(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), iterations_loop_variable(LEVEL::PRIVATE, dim_range(L_DIMS, 0)))),
                                                                        this->call_reduction_mem_macro(output.name(), kernel, fu_level, params_reduction_mem)
                                                                )
                            ), 1);
                        } else {
                            code << std::endl << indent(stringf("%s = %s;",
                                                                this->call_fu_res_macro(output.name(), kernel, parent_level(cb_level), inner_phase, _macros.iteration_to_index_conversion(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), iterations_loop_variable(LEVEL::PRIVATE, dim_range(L_DIMS, 0)))),
                                                                this->call_reduction_mem_macro(output.name(), kernel, fu_level, params_reduction_mem)
                            ), 1);
                        }
                    });
                    code << std::endl << "}";
                }

                code << std::endl << stringf("#if %c_CB_RES_DEST_LEVEL == LOCAL", parent_level(cb_level));
                code << std::endl << barrier(LEVEL::LOCAL, _cuda);
                code << std::endl << stringf("#elif %c_CB_RES_DEST_LEVEL == GLOBAL", parent_level(cb_level));
                code << std::endl << barrier(LEVEL::GLOBAL, _cuda);
                code << std::endl << "#endif"; // for #if of barrier
                code << std::endl << "#endif";
            }
            if (_cuda) {
                code << std::endl << "}";
                code << std::endl << "#endif";
            }

            return code.str();
        };
        if (L_DIMS > 0) {
            switch (cb_level) {
                case LEVEL::PRIVATE: {
                    auto loops = loop_generator<L_DIMS, R_DIMS>(kernel, _macros, _runtime_inputs, V(LEVEL::PRIVATE), _l_dims_after_first_r_dim, false, &_l_dims_after_first_r_dim, true, _skip_post_processing);
                    code << std::endl << loops.from(
                            kernel, LEVEL::PRIVATE, _l_dims_after_first_r_dim.empty() ? L(1) : _l_dims_after_first_r_dim.front(),
                            [&](auto inner_level, const auto &inner_dimension, char *inner_phase, bool *inner_first_iteration) -> std::string {
                                return wrap_in_iterations_loop<L_DIMS, R_DIMS>(kernel, LEVEL::PRIVATE, dim_range(L_DIMS, 0), loop_body_callback(
                                        fu_level, inner_dimension, inner_phase, inner_first_iteration
                                ), true, inner_phase);
                            }, true, phase, first_iteration);
                    break;
                }
                case LEVEL::LOCAL: {
                    auto loops = loop_generator<L_DIMS, R_DIMS>(kernel, _macros, _runtime_inputs, V(LEVEL::LOCAL), _l_dims_after_first_r_dim, false, &_l_dims_after_first_r_dim, true, _skip_post_processing);
                    auto inner_loops = loop_generator<L_DIMS, R_DIMS>(kernel, _macros, _runtime_inputs, V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), false, &_l_dims_after_first_r_dim, true, _skip_post_processing);
                    code << std::endl << loops.from(
                            kernel, LEVEL::LOCAL, _l_dims_after_first_r_dim.empty() ? L(1) : _l_dims_after_first_r_dim.front(),
                            [&] (auto inner_level, const auto &inner_dimension, char *inner_phase, bool *inner_first_iteration) -> std::string {
                                return inner_loops.from(
                                        kernel, LEVEL::PRIVATE, L(1),
                                        [&](auto inner_level2, const auto &inner_dimension2, char *inner_phase2, bool *inner_first_iteration2) -> std::string {
                                            return wrap_in_iterations_loop<L_DIMS, R_DIMS>(kernel, LEVEL::PRIVATE, dim_range(L_DIMS, 0), loop_body_callback(
                                                    fu_level, inner_dimension2, inner_phase2, inner_first_iteration2
                                            ), true, inner_phase2);
                                        }, true, inner_phase, inner_first_iteration);
                            }, true, phase, first_iteration);
                    break;
                }
                case LEVEL::GLOBAL: {
                    auto loops = loop_generator<L_DIMS, R_DIMS>(kernel, _macros, _runtime_inputs, V(LEVEL::LOCAL, LEVEL::PRIVATE), dim_range(L_DIMS, 0), false, &_l_dims_after_first_r_dim, true, _skip_post_processing);
                    code << std::endl << loops.from(
                            kernel, LEVEL::LOCAL, L(1),
                            [&](auto inner_level, const auto &inner_dimension, char *inner_phase, bool *inner_first_iteration) -> std::string {
                                return wrap_in_iterations_loop<L_DIMS, R_DIMS>(kernel, LEVEL::PRIVATE, dim_range(L_DIMS, 0), loop_body_callback(
                                        fu_level, inner_dimension, inner_phase, inner_first_iteration
                                ), true, inner_phase);
                            }, true, phase, first_iteration);
                    break;
                }
            }
        } else {
            code << std::endl << loop_body_callback(fu_level, R(1), phase, first_iteration);
        }
#endif

        return code.str();
    }

};

}
}

#endif //MD_BLAS_RESULT_BUFFER_WRAPPER_HPP
