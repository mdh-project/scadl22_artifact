//
// Created by Richard Schulze on 06.11.17.
//

#ifndef MD_BLAS_RESULT_BUFFER_HPP
#define MD_BLAS_RESULT_BUFFER_HPP

#include <string>
#include <functional>

namespace md_hom {

extern std::pair<std::function<std::string(const std::string&, const std::string&)>, std::function<std::string(const std::string&, const std::string&)>> PLUS;

class result_buffer_class {
public:
    result_buffer_class(const std::string &data_type,
                        const std::pair<std::function<std::string(const std::string&, const std::string&)>, std::function<std::string(const std::string&, const std::string&)>> &op,
                        const std::string &name,
                        const std::vector<std::string> &index_functions,
                        const std::vector<std::string> &dimension_sizes);

    const std::string name() const {
        return _name;
    }

    const std::vector<std::string> index_functions() const {
        return _index_functions;
    }

    const std::vector<std::string> dimension_sizes() const {
        return _dimension_sizes;
    }

    const std::vector<dimension_t> dimension_order() const {
        return _dimension_order;
    }

    const std::string &data_type() const {
        return _data_type;
    }

    std::string reduce(const std::string &lhs, const std::string &rhs) const {
        return _op.first(lhs, rhs);
    }

    std::string reduce_and_store(const std::string &lhs, const std::string &rhs) const {
        return _op.second(lhs, rhs);
    }
private:
    const std::string              _name;
    const std::vector<std::string> _index_functions;
    const std::vector<std::string> _dimension_sizes;
          std::vector<dimension_t> _dimension_order;
    const std::pair<std::function<std::string(const std::string&, const std::string&)>, std::function<std::string(const std::string&, const std::string&)>> _op;
    const std::string              _data_type;
};


result_buffer_class result_buffer(const std::string &data_type,
                                  const std::pair<std::function<std::string(const std::string&, const std::string&)>, std::function<std::string(const std::string&, const std::string&)>> &op,
                                  const std::string &name,
                                  const std::vector<std::string> &index_functions,
                                  const std::vector<std::string> &dimension_sizes = {});

result_buffer_class result_buffer(const std::string &data_type,
                                  const std::pair<std::function<std::string(const std::string&, const std::string&)>, std::function<std::string(const std::string&, const std::string&)>> &op,
                                  const std::string &name,
                                  const std::string &index_function,
                                  const std::vector<std::string> &dimension_sizes = {});



}

#endif //MD_BLAS_RESULT_BUFFER_HPP
