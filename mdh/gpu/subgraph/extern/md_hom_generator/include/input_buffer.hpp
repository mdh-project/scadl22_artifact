//
// Created by Richard Schulze on 30.10.2017.
//

#ifndef MD_BLAS_INPUT_BUFFER_HPP
#define MD_BLAS_INPUT_BUFFER_HPP

#include <string>
#include <vector>

#include "types.hpp"

namespace md_hom {

class input_buffer_class {
public:
    input_buffer_class(const std::string &data_type,
                       const std::string &name,
                       const std::vector<std::vector<std::string>> &index_functions,
                       const std::vector<std::vector<std::string>> &global_index_functions,
                       const std::vector<std::string> &global_dimension_sizes);

    const std::string &data_type() const;
    const std::string &name() const;
    const std::vector<std::vector<std::string>> &index_functions() const;
    const std::vector<std::vector<std::string>> &global_index_functions() const;
    const std::vector<std::string> &global_dimension_sizes() const;
    const std::vector<std::vector<dimension_t>> &used_dimensions() const;
private:
    const std::string                           _name;
    const std::vector<std::vector<std::string>> _index_functions;
    const std::vector<std::vector<std::string>> _global_index_functions;
    const std::vector<std::string>              _global_dimension_sizes;
    std::vector<std::vector<dimension_t>>       _used_dimensions;
    const std::string                           _data_type;
};

input_buffer_class input_buffer(const std::string &data_type,
                                const std::string &name,
                                const std::vector<std::vector<std::string>> &index_functions,
                                const std::vector<std::vector<std::string>> &global_index_functions = {},
                                const std::vector<std::string> &global_dimension_sizes = {});

input_buffer_class input_buffer(const std::string &data_type,
                                const std::string &name,
                                const std::vector<std::string> &index_functions,
                                const std::vector<std::vector<std::string>> &global_index_functions = {},
                                const std::vector<std::string> &global_dimension_sizes = {});

input_buffer_class input_buffer(const std::string &data_type,
                                const std::string &name,
                                const std::string &index_function,
                                const std::vector<std::vector<std::string>> &global_index_functions = {},
                                const std::vector<std::string> &global_dimension_sizes = {});

input_buffer_class input_buffer(const std::string &data_type,
                                const std::string &name,
                                const std::vector<dimension_t> &used_dimensions);


}
#endif //MD_BLAS_INPUT_BUFFER_HPP
