void add_arguments(TCLAP::CmdLine &cmd) {

}

std::vector<std::string> inputs_str(int case_nr, int input_size_l_1, int input_size_l_2, int input_size_l_3, int input_size_l_4) {
    return {std::to_string(input_size_l_1) + "x" + std::to_string(input_size_l_3) + "x1",
            std::to_string(input_size_l_1) + "x1x" + std::to_string(input_size_l_4)};
}

std::vector<std::string> outputs_str(int case_nr, int input_size_l_1, int input_size_l_2, int input_size_l_3, int input_size_l_4) {
    return {std::to_string(input_size_l_1) + "x" + std::to_string(input_size_l_2) + "x" + std::to_string(input_size_l_3) + "x" + std::to_string(input_size_l_4)};
}

void generation_data_path(std::vector<std::string> &path) {

}

void gold_data_path(std::vector<std::string> &path) {

}

void tuning_data_path(std::vector<std::string> &path) {

}

void benchmark_data_path(std::vector<std::string> &path) {

}