float               c_1 = 1.0f;
ocal::buffer<float> mul_i_1;
ocal::buffer<float> mul_i_2;
float               c_2 = -10000.0f;
ocal::buffer<float> o;

void init_data(const std::vector<int> &input_size_l_1, const std::vector<int> &input_size_l_2, const std::vector<int> &input_size_l_3, const std::vector<int> &input_size_l_4) {
    int max_mul_i_1_size = 0;
    int max_mul_i_2_size = 0;
    int max_o_size = 0;
    for (int i = 0; i < input_size_l_1.size(); ++i) {
        max_mul_i_1_size = std::max(max_mul_i_1_size, input_size_l_1[i] * input_size_l_3[i] * 1);
        max_mul_i_2_size = std::max(max_mul_i_2_size, input_size_l_1[i] * 1 * input_size_l_4[i]);
        max_o_size = std::max(max_o_size, input_size_l_1[i] * input_size_l_2[i] * input_size_l_3[i] * input_size_l_4[i]);
    }
    mul_i_1 = ocal::buffer<float>(max_mul_i_1_size); for (int i = 0; i < mul_i_1.size(); ++i) mul_i_1[i] = data_sequence<float>(i);
    mul_i_2 = ocal::buffer<float>(max_mul_i_2_size); for (int i = 0; i < mul_i_2.size(); ++i) mul_i_2[i] = data_sequence<float>(i);
    o = ocal::buffer<float>(max_o_size); for (int i = 0; i < o.size(); ++i) o[i] = 0;
}

auto inputs(int case_nr) {
    return std::make_tuple(c_1, read(mul_i_1), read(mul_i_2), c_2);
}

auto outputs(int case_nr) {
    return std::make_tuple(write(o));
}