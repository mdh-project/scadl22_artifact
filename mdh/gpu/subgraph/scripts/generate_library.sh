#!/usr/bin/env bash

output_directory=$1
output_name=$2
config_json=$3
kernel_file_1=$4
kernel_file_2=$5

output_name_upper=${output_name^^}
header_file=$output_directory/${output_name}.hpp
backend="cuda"
kernel_name="subgraph2"

# check if second kernel is executed
has_second_kernel=false

# calculate global and local sizes
gs_1=(1 1 1)
ls_1=(1 1 1)
gs_2=(1 1 1)
ls_2=(1 1 1)
OCL_DIM_L_1=`jq --raw-output ".OCL_DIM_L_1" "$config_json"`
NUM_WG_L_1=`jq --raw-output ".NUM_WG_L_1" "$config_json"`
NUM_WI_L_1=`jq --raw-output ".NUM_WI_L_1" "$config_json"`
if [ $OCL_DIM_L_1 -lt 2 ]; then
  gs_1[$OCL_DIM_L_1]=$((gs_1[OCL_DIM_L_1] * NUM_WG_L_1 * NUM_WI_L_1))
  ls_1[$OCL_DIM_L_1]=$((ls_1[OCL_DIM_L_1] *              NUM_WI_L_1))
  gs_2[$OCL_DIM_L_1]=$((gs_2[OCL_DIM_L_1] *              NUM_WI_L_1))
  ls_2[$OCL_DIM_L_1]=$((ls_2[OCL_DIM_L_1] *              NUM_WI_L_1))
else
  gs_1[2]=$((gs_1[2] * NUM_WG_L_1 * NUM_WI_L_1))
  ls_1[2]=$((ls_1[2] *              NUM_WI_L_1))
  gs_2[2]=$((gs_2[2] *              NUM_WI_L_1))
  ls_2[2]=$((ls_2[2] *              NUM_WI_L_1))
fi
OCL_DIM_L_2=`jq --raw-output ".OCL_DIM_L_2" "$config_json"`
NUM_WG_L_2=`jq --raw-output ".NUM_WG_L_2" "$config_json"`
NUM_WI_L_2=`jq --raw-output ".NUM_WI_L_2" "$config_json"`
if [ $OCL_DIM_L_2 -lt 2 ]; then
  gs_1[$OCL_DIM_L_2]=$((gs_1[OCL_DIM_L_2] * NUM_WG_L_2 * NUM_WI_L_2))
  ls_1[$OCL_DIM_L_2]=$((ls_1[OCL_DIM_L_2] *              NUM_WI_L_2))
  gs_2[$OCL_DIM_L_2]=$((gs_2[OCL_DIM_L_2] *              NUM_WI_L_2))
  ls_2[$OCL_DIM_L_2]=$((ls_2[OCL_DIM_L_2] *              NUM_WI_L_2))
else
  gs_1[2]=$((gs_1[2] * NUM_WG_L_2 * NUM_WI_L_2))
  ls_1[2]=$((ls_1[2] *              NUM_WI_L_2))
  gs_2[2]=$((gs_2[2] *              NUM_WI_L_2))
  ls_2[2]=$((ls_2[2] *              NUM_WI_L_2))
fi
OCL_DIM_L_3=`jq --raw-output ".OCL_DIM_L_3" "$config_json"`
NUM_WG_L_3=`jq --raw-output ".NUM_WG_L_3" "$config_json"`
NUM_WI_L_3=`jq --raw-output ".NUM_WI_L_3" "$config_json"`
if [ $OCL_DIM_L_3 -lt 2 ]; then
  gs_1[$OCL_DIM_L_3]=$((gs_1[OCL_DIM_L_3] * NUM_WG_L_3 * NUM_WI_L_3))
  ls_1[$OCL_DIM_L_3]=$((ls_1[OCL_DIM_L_3] *              NUM_WI_L_3))
  gs_2[$OCL_DIM_L_3]=$((gs_2[OCL_DIM_L_3] *              NUM_WI_L_3))
  ls_2[$OCL_DIM_L_3]=$((ls_2[OCL_DIM_L_3] *              NUM_WI_L_3))
else
  gs_1[2]=$((gs_1[2] * NUM_WG_L_3 * NUM_WI_L_3))
  ls_1[2]=$((ls_1[2] *              NUM_WI_L_3))
  gs_2[2]=$((gs_2[2] *              NUM_WI_L_3))
  ls_2[2]=$((ls_2[2] *              NUM_WI_L_3))
fi
OCL_DIM_L_4=`jq --raw-output ".OCL_DIM_L_4" "$config_json"`
NUM_WG_L_4=`jq --raw-output ".NUM_WG_L_4" "$config_json"`
NUM_WI_L_4=`jq --raw-output ".NUM_WI_L_4" "$config_json"`
if [ $OCL_DIM_L_4 -lt 2 ]; then
  gs_1[$OCL_DIM_L_4]=$((gs_1[OCL_DIM_L_4] * NUM_WG_L_4 * NUM_WI_L_4))
  ls_1[$OCL_DIM_L_4]=$((ls_1[OCL_DIM_L_4] *              NUM_WI_L_4))
  gs_2[$OCL_DIM_L_4]=$((gs_2[OCL_DIM_L_4] *              NUM_WI_L_4))
  ls_2[$OCL_DIM_L_4]=$((ls_2[OCL_DIM_L_4] *              NUM_WI_L_4))
else
  gs_1[2]=$((gs_1[2] * NUM_WG_L_4 * NUM_WI_L_4))
  ls_1[2]=$((ls_1[2] *              NUM_WI_L_4))
  gs_2[2]=$((gs_2[2] *              NUM_WI_L_4))
  ls_2[2]=$((ls_2[2] *              NUM_WI_L_4))
fi

# calculate res_g size

# create directory if not exists
mkdir -p "$output_directory"

# create library header
rm "$header_file" || touch "$header_file"

# add includes
echo "#include "\<ocal_cuda.hpp\>"" >> "$header_file"

# add configuration defines with output_name prefix to header
echo "" >> "$header_file"
jq --raw-output "to_entries[] | \"#define ${output_name_upper}_\(.key) \(.value)\"" "$config_json" >> "$header_file"

# add global and local sizes
cat >> "$header_file" <<- EOM

// global and local size for first kernel
ocl::nd_range ${output_name}_gs_1{${gs_1[0]}, ${gs_1[1]}, ${gs_1[2]}};
ocl::nd_range ${output_name}_ls_1{${ls_1[0]}, ${ls_1[1]}, ${ls_1[2]}};
EOM
if [ "$has_second_kernel" = true ]; then
  cat >> "$header_file" <<- EOM

// global and local size for second kernel
ocl::nd_range ${output_name}_gs_2{${gs_2[0]}, ${gs_2[1]}, ${gs_2[2]}};
ocl::nd_range ${output_name}_ls_2{${ls_2[0]}, ${ls_2[1]}, ${ls_2[2]}};
EOM
fi

# preprocess first kernel and add to header
echo "" >> "$header_file"
echo "ocal::kernel ${output_name}_kernel_1(${backend}::source(R\"(" >> "$header_file"
## make temporary copy of kernel file
temp_file_1=$(mktemp --suffix ".c")
trap "rm -f $temp_file_1" 0 2 3 15
## parse json configuration
jq --raw-output 'to_entries[] | "#define \(.key) \(.value)"' "$config_json" > "$temp_file_1"
## add kernel code
printf "\n\n" >> "$temp_file_1"
cat "$kernel_file_1" >> "$temp_file_1"
## preprocess
gcc -E "$temp_file_1" | sed '/^$/d' | sed '/^\w*#.*$/d'  >> "$header_file"
echo ")\"), \"${kernel_name}_1\");" >> "$header_file"

if [ "$has_second_kernel" = true ]; then
  # preprocess second kernel and add to header
  echo "" >> "$header_file"
  echo "ocal::kernel ${output_name}_kernel_2(${backend}::source(R\"(" >> "$header_file"
  ## make temporary copy of kernel file
  temp_file_2=$(mktemp --suffix ".c")
  trap "rm -f $temp_file_2" 0 2 3 15
  ## parse json configuration
  jq --raw-output 'to_entries[] | "#define \(.key) \(.value)"' "$config_json" > "$temp_file_2"
  ## add kernel code
  printf "\n\n" >> "$temp_file_2"
  cat "$kernel_file_2" >> "$temp_file_2"
  ## preprocess
  gcc -E "$temp_file_2" | sed '/^$/d' | sed '/^\w*#.*$/d'  >> "$header_file"
  echo ")\"), \"${kernel_name}_2\");" >> "$header_file"
fi

# add library function header
cat >> "$header_file" <<- EOM

void ${output_name}(ocal::device<CUDA> &ocal_device, const float &c_1, ocal::buffer<float> &mul_i_1, ocal::buffer<float> &mul_i_2, const float &c_2, ocal::buffer<float> &o) {
    // allocate memory for intermediate results
EOM

# add buffers for intermediate results
if [ "$has_second_kernel" = true ]; then
  cat >> "$header_file" <<- EOM
    ocal::buffer<float> int_res_o(${output_name_upper}_INPUT_SIZE_L_1 * ${output_name_upper}_INPUT_SIZE_L_2 * ${output_name_upper}_INPUT_SIZE_L_3 * ${output_name_upper}_INPUT_SIZE_L_4);
EOM
fi

if [ -n "$res_g_size" ]; then
  cat >> "$header_file" <<- EOM
    ocal::buffer<float> res_g_o(${res_g_size});
EOM
else
  cat >> "$header_file" <<- EOM
    ocal::buffer<float> res_g_o(1);
EOM
fi

# add kernel executions
if [ "$has_second_kernel" = false ]; then
  cat >> "$header_file" <<- EOM

    // execute first kernel
    ocal_device(${output_name}_kernel_1)
               (${output_name}_gs_1, ${output_name}_ls_1)
    (read(c_1), read(mul_i_1), read(mul_i_2), read(c_2), write(res_g_o), write(o));
EOM
else
  cat >> "$header_file" <<- EOM

    // execute first kernel
    ocal_device(${output_name}_kernel_1)
               (${output_name}_gs_1, ${output_name}_ls_1)
    (read(c_1), read(mul_i_1), read(mul_i_2), read(c_2), write(res_g_o), write(int_res_o));

    // execute second kernel
    ocal_device(${output_name}_kernel_2)
               (${output_name}_gs_2, ${output_name}_ls_2)
    (read(int_res_o), write(res_g_o), write(o));
EOM
fi

# add synchronization
if [ "$has_second_kernel" = true ] || [ -n "$res_g_size" ]; then
  cat >> "$header_file" <<- EOM

    ocal_device.synchronize();
}
EOM
fi