ocal::buffer<float> in;
ocal::buffer<float> out;

void init_data(const std::vector<int> &input_size_l_1, const std::vector<int> &input_size_r_1) {
    int max_in_size = 0;
    int max_out_size = 0;
    for (int i = 0; i < input_size_l_1.size(); ++i) {
        max_in_size = std::max(max_in_size, input_size_r_1[i] * input_size_l_1[i]);
        max_out_size = std::max(max_out_size, input_size_l_1[i]);
    }
    in = ocal::buffer<float>(max_in_size); for (int i = 0; i < in.size(); ++i) in[i] = data_sequence<float>(i);
    out = ocal::buffer<float>(max_out_size); for (int i = 0; i < out.size(); ++i) out[i] = 0;
}

auto inputs(int case_nr) {
    return std::make_tuple(read(in));
}

auto outputs(int case_nr) {
    return std::make_tuple(write(out));
}