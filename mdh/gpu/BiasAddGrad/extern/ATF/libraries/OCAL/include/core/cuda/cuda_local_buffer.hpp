//
//  cuda_local_buffer.hpp
//  ocal
//
//  Created by Ari Rasch on 18.10.17.
//  Copyright © 2017 Ari Rasch. All rights reserved.
//

#ifndef cuda_local_buffer_h
#define cuda_local_buffer_h


namespace ocal
{

namespace core
{

namespace cuda
{


template< typename T >
class shared_buffer : public abstract::local_or_shared_buffer< T >
{
//  // friend class parent
//  friend class abstract::local_or_shared_buffer< T >;
  
  public:
    template< typename... Ts >
    shared_buffer( Ts... args )
      : abstract::local_or_shared_buffer< T >( args... )
    {}
};


} // namespace "cuda"

} // namespace "core"

} // namespace "ocal"


#endif /* cuda_local_buffer_h */
