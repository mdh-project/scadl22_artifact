//
//  ocl_helper.hpp
//  ocal_ocl
//
//  Created by Ari Rasch on 20.09.17.
//  Copyright © 2017 Ari Rasch. All rights reserved.
//

#ifndef ocl_helper_hpp
#define ocl_helper_hpp


namespace ocal
{

namespace core
{

namespace ocl
{


cl_int error;
extern cl_uint PLATFORM_ID_OF_HOST_DEVICE; // Note: allows defining PLATFORM_ID_OF_HOST_DEVICE later

const std::string get_error_string( const cl_int& error )
{
  switch(error)
  {
    // run-time and JIT compiler errors
    case 0: return "CL_SUCCESS";
    case -1: return "CL_DEVICE_NOT_FOUND";
    case -2: return "CL_DEVICE_NOT_AVAILABLE";
    case -3: return "CL_COMPILER_NOT_AVAILABLE";
    case -4: return "CL_MEM_OBJECT_ALLOCATION_FAILURE";
    case -5: return "CL_OUT_OF_RESOURCES";
    case -6: return "CL_OUT_OF_HOST_MEMORY";
    case -7: return "CL_PROFILING_INFO_NOT_AVAILABLE";
    case -8: return "CL_MEM_COPY_OVERLAP";
    case -9: return "CL_IMAGE_FORMAT_MISMATCH";
    case -10: return "CL_IMAGE_FORMAT_NOT_SUPPORTED";
    case -11: return "CL_BUILD_PROGRAM_FAILURE";
    case -12: return "CL_MAP_FAILURE";
    case -13: return "CL_MISALIGNED_SUB_BUFFER_OFFSET";
    case -14: return "CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST";
    case -15: return "CL_COMPILE_PROGRAM_FAILURE";
    case -16: return "CL_LINKER_NOT_AVAILABLE";
    case -17: return "CL_LINK_PROGRAM_FAILURE";
    case -18: return "CL_DEVICE_PARTITION_FAILED";
    case -19: return "CL_KERNEL_ARG_INFO_NOT_AVAILABLE";

    // compile-time errors
    case -30: return "CL_INVALID_VALUE";
    case -31: return "CL_INVALID_DEVICE_TYPE";
    case -32: return "CL_INVALID_PLATFORM";
    case -33: return "CL_INVALID_DEVICE";
    case -34: return "CL_INVALID_CONTEXT";
    case -35: return "CL_INVALID_QUEUE_PROPERTIES";
    case -36: return "CL_INVALID_COMMAND_QUEUE";
    case -37: return "CL_INVALID_HOST_PTR";
    case -38: return "CL_INVALID_MEM_OBJECT";
    case -39: return "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR";
    case -40: return "CL_INVALID_IMAGE_SIZE";
    case -41: return "CL_INVALID_SAMPLER";
    case -42: return "CL_INVALID_BINARY";
    case -43: return "CL_INVALID_BUILD_OPTIONS";
    case -44: return "CL_INVALID_PROGRAM";
    case -45: return "CL_INVALID_PROGRAM_EXECUTABLE";
    case -46: return "CL_INVALID_KERNEL_NAME";
    case -47: return "CL_INVALID_KERNEL_DEFINITION";
    case -48: return "CL_INVALID_KERNEL";
    case -49: return "CL_INVALID_ARG_INDEX";
    case -50: return "CL_INVALID_ARG_VALUE";
    case -51: return "CL_INVALID_ARG_SIZE";
    case -52: return "CL_INVALID_KERNEL_ARGS";
    case -53: return "CL_INVALID_WORK_DIMENSION";
    case -54: return "CL_INVALID_WORK_GROUP_SIZE";
    case -55: return "CL_INVALID_WORK_ITEM_SIZE";
    case -56: return "CL_INVALID_GLOBAL_OFFSET";
    case -57: return "CL_INVALID_EVENT_WAIT_LIST";
    case -58: return "CL_INVALID_EVENT";
    case -59: return "CL_INVALID_OPERATION";
    case -60: return "CL_INVALID_GL_OBJECT";
    case -61: return "CL_INVALID_BUFFER_SIZE";
    case -62: return "CL_INVALID_MIP_LEVEL";
    case -63: return "CL_INVALID_GLOBAL_WORK_SIZE";
    case -64: return "CL_INVALID_PROPERTY";
    case -65: return "CL_INVALID_IMAGE_DESCRIPTOR";
    case -66: return "CL_INVALID_COMPILER_OPTIONS";
    case -67: return "CL_INVALID_LINKER_OPTIONS";
    case -68: return "CL_INVALID_DEVICE_PARTITION_COUNT";

    // extension errors
    case -1000: return "CL_INVALID_GL_SHAREGROUP_REFERENCE_KHR";
    case -1001: return "CL_PLATFORM_NOT_FOUND_KHR";
    case -1002: return "CL_INVALID_D3D10_DEVICE_KHR";
    case -1003: return "CL_INVALID_D3D10_RESOURCE_KHR";
    case -1004: return "CL_D3D10_RESOURCE_ALREADY_ACQUIRED_KHR";
    case -1005: return "CL_D3D10_RESOURCE_NOT_ACQUIRED_KHR";
    
    default: return "Unknown OpenCL error";
  }
}


void check_opencl_exception( cl::Error& err )
{
  std::cout << "Error string: " << err.what()                     << std::endl;
  std::cout << "Error code  : " << get_error_string( err.err() )  << std::endl << std::endl;
}


// returns the platform id of the platform that shares its memory with the host
cl_uint get_platform_id_of_host_platform()
{
  // get all platforms
  std::vector<cl::Platform> platforms;
  try
  {
    cl::Platform::get( &platforms );
  }
  catch( cl::Error& err )
  {
    check_opencl_exception( err );
    throw;
  }

  // find first device of platform with id "platform_id" whose name contains all substrings in "device_name"
  std::vector<cl::Device> devices;
  for( cl_uint p_id = 0 ; p_id < platforms.size() ; ++p_id )
  {
    // get devices for platform
    platforms[ p_id ].getDevices( CL_DEVICE_TYPE_ALL, &devices );
    
    for( cl_uint d_id = 0 ; d_id < devices.size() ; ++d_id )
    {
      // get device name
      cl_bool device_uses_host_memory;
      devices[ d_id ].getInfo( CL_DEVICE_HOST_UNIFIED_MEMORY, &device_uses_host_memory ); 
      
      if( device_uses_host_memory )
        return p_id;
    }
    
    return 0; // Note: use first platform if "CL_DEVICE_HOST_UNIFIED_MEMORY" is not set for the system's platforms
  }

  // should never be reached
  throw std::exception();
  return -1;
}


cl::Context& unique_device_id_to_opencl_context( int unique_device_id )
{
  auto  platform_id = unique_device_id_to_opencl_platform_id.at( unique_device_id );
  auto& context     = opencl_platform_id_to_context.at( platform_id );
  
  return context;
}


auto get_random_unique_device_id_for_platform_id( const cl_uint& platform_id )
{
  for( const auto& elem : unique_device_id_to_opencl_platform_id )
    if( elem.second == platform_id )
      return elem.first;
  
  assert( false && "should never be reached" );
  return -1;
}


cl::CommandQueue unique_device_id_to_actual_opencl_command_queue( int unique_device_id )
{
  auto actual_opencl_command_queue_id = unique_device_id_to_actual_opencl_command_queue_id.at( unique_device_id );
  auto opencl_command_queue           = unique_device_id_to_opencl_command_queues.at( unique_device_id )[ actual_opencl_command_queue_id ];
  
  return opencl_command_queue;
}


// required to have a uniform interface for event managment
class clEvent_wrapper : public common::event_wrapper
{
  public:
    clEvent_wrapper( int = 0 /*device_id*/, int = 0 /*command_queue_id*/, bool disable_timing = true, bool record_event = true ) // Note: OpenCL does not require enqueuing events in a command queue of a specific device
      : _event()
    {}
  
  
    auto& get()
    {
      return _event;
    }
  
  
    void wait() const
    {
      _event.wait();
    }


    STATUS status() const
    {
      cl_int status = -1;
      cl_int err = _event.getInfo(CL_EVENT_COMMAND_EXECUTION_STATUS, &status);
      if (err != CL_INVALID_EVENT && status != CL_COMPLETE) {
          return RUNNING;
      } else {
          return COMPLETE;
      }
    }
  
  
  private:
    cl::Event _event;
};


template< typename T >
class opencl_host_buffer_wrapper
{
  public:
    opencl_host_buffer_wrapper( size_t size, cl::Buffer& host_buffer )
      : _size( size ), _host_memory( host_buffer ), _ptr_to_host_memory(), _command_queue_of_host()
    {
      auto random_unique_device_id_of_host_platform = get_random_unique_device_id_for_platform_id( PLATFORM_ID_OF_HOST_DEVICE );
      _command_queue_of_host                        = unique_device_id_to_actual_opencl_command_queue( random_unique_device_id_of_host_platform );

      _ptr_to_host_memory = (T*) _command_queue_of_host.enqueueMapBuffer( _host_memory,
                                                                          CL_TRUE,
                                                                          CL_MAP_READ | CL_MAP_WRITE,
                                                                          0,               // offset
                                                                          _size * sizeof( T )
                                                                        );
    }


    opencl_host_buffer_wrapper( const opencl_host_buffer_wrapper& ) = default;
  
    ~opencl_host_buffer_wrapper()
    {
      _command_queue_of_host.enqueueUnmapMemObject( _host_memory,
                                                    _ptr_to_host_memory
                                                  );
    }
  
  
    // copy assignment operator -- implemented only for assigning the same object -> causes flushing the buffer
    opencl_host_buffer_wrapper& operator=( const opencl_host_buffer_wrapper& other )
    {
      //assert( &(this->_host_memory) == &(other._host_memory) ); //&& this->_ptr_to_host_memory == other._ptr_to_host_memory && &(this->_command_queue_of_host) == &(other._command_queue_of_host) );
      
      this->flush();
      
      return *this;
    }
  
  
    void flush()
    {
      // unmap
      _command_queue_of_host.enqueueUnmapMemObject( _host_memory,
                                                    _ptr_to_host_memory
                                                  );

      // map
      auto random_unique_device_id_of_host_platform = get_random_unique_device_id_for_platform_id( PLATFORM_ID_OF_HOST_DEVICE );
           _command_queue_of_host                   = unique_device_id_to_actual_opencl_command_queue( random_unique_device_id_of_host_platform );

      _ptr_to_host_memory = (T*) _command_queue_of_host.enqueueMapBuffer( _host_memory,
                                                                          CL_TRUE,
                                                                          CL_MAP_READ | CL_MAP_WRITE,
                                                                          0,               // offset
                                                                          _size * sizeof( T )
                                                                        );
    }


    // access functions
    operator T* ()
    {
      return _ptr_to_host_memory;
    }


    T* get()
    {
      return _ptr_to_host_memory;
    }

  

    T& operator*()
    {
      return *_ptr_to_host_memory;
    }


    T& operator[]( size_t i )
    {
      return _ptr_to_host_memory[ i ];
    }
  
  
  private:
    size_t           _size;
    cl::Buffer       _host_memory;
    T*               _ptr_to_host_memory;
    cl::CommandQueue _command_queue_of_host;
};


void create_context_if_not_existent( const std::vector<cl::Device>& devices, const cl::Platform& platform, const cl_uint& platform_id )
{
  // when context already exists then do nothing
  if( opencl_platform_id_to_context.find( platform_id ) != opencl_platform_id_to_context.end() )
    return;
  
  // create context for all of platform's devices
  cl_context_properties props[] = { CL_CONTEXT_PLATFORM,
                                    reinterpret_cast<cl_context_properties>( platform() ),
                                    0
                                  };
  auto context = cl::Context( devices, props );
  
  opencl_platform_id_to_context[ platform_id ] = context;
}


std::string device_name_of( int unique_device_id )
{
  auto opencl_device = unique_device_id_to_opencl_device.at( unique_device_id );
  
  std::string device_name;
  opencl_device.getInfo( CL_DEVICE_NAME, &device_name );
  
  return device_name;
}


std::string vendor_name_of( int unique_device_id )
{
  auto opencl_device = unique_device_id_to_opencl_device.at( unique_device_id );
  
  std::string vendor_name;
  opencl_device.getInfo( CL_DEVICE_VENDOR, &vendor_name );
  
  return vendor_name;
}


size_t NUM_DEVICES( const std::string& vendor_name )
{
  cl::Platform platform;

  // get all platforms
  std::vector<cl::Platform> platforms;
  try
  {
    cl::Platform::get( &platforms );
  }
  catch( cl::Error& err )
  {
    check_opencl_exception( err );
    throw;
  }

  // find first platform whose name contains all substrings in "platform_name"
  for( size_t i = 0 ; i < platforms.size(); ++i )
  {
    // get platform name
    std::string actual_vendor_name;
    platforms[ i ].getInfo( CL_PLATFORM_VENDOR, &actual_vendor_name );
    
    if( common::contains_all_sub_strings( actual_vendor_name, vendor_name ) )
    {
      platform = platforms[ i ];
      
      break; // exit for-loop over platforms
    }
  }

  // find first device of platform with id "platform_id" whose name contains all substrings in "device_name"
  std::vector<cl::Device> devices;
  platform.getDevices( CL_DEVICE_TYPE_ALL, &devices );

  return devices.size();
}


size_t NUM_DEVICES( const cl_uint& platform_id )
{
  // get platform with id "platform_id"
  std::vector<cl::Platform> platforms;
  try
  {
    cl::Platform::get( &platforms );
  }
  catch( cl::Error& err )
  {
    check_opencl_exception( err );
    throw;
  }
  
  if( platform_id >= platforms.size() )
  {
    std::wcerr << "No platform with id " << platform_id << " found." << std::endl;
    exit( EXIT_FAILURE );
  }
  auto platform = platforms[ platform_id ];
  
  // get all of platform's devices
  std::vector<cl::Device> devices;
  platform.getDevices( CL_DEVICE_TYPE_ALL, &devices );
  
  return devices.size();
}


void ocl_synchronize()
{
  for( auto& elem : unique_device_id_to_opencl_command_queues )
  {
    auto& command_queue_map = elem.second;
    
    for( auto& elem : command_queue_map )
    {
      auto& queue = elem.second;
      queue.finish();
    }
  }
}


} // namespace "ocl"

} // namespace "core"

} // namespace "ocal"


#endif /* ocl_helper_hpp */

