//
//  helper.hpp
//  ocal
//
//  Created by Ari Rasch on 09.11.17.
//  Copyright © 2017 Ari Rasch. All rights reserved.
//

#ifndef common_helper_hpp
#define common_helper_hpp


namespace ocal
{

namespace common
{


template< typename T >
struct remove_cv_and_references
{
  using type = typename std::remove_reference< typename std::remove_cv<T>::type >::type;
};


//// used for indirection
//template< typename T >
//class pointer_wrapper
//{
//  public:
//    pointer_wrapper( size_t, std::shared_ptr<T> ptr ) // Note: size of memory pointed to not required
//      : _ptr( ptr )
//    {}
//
//
//    pointer_wrapper() = delete;
//
//
//    pointer_wrapper& operator=( std::shared_ptr<T> other)
//    {
//      _ptr = other;
//
//      return *this;
//    }
//
//
//    operator T*() const
//    {
//      return _ptr.get();
//    }
//
//
//    T** operator&()
//    {
//      return &( _ptr.get() );
//    }
//
//
//
//    T* get() const
//    {
//      return _ptr.get();
//    }
//
//    void flush() const
//    {} // Note: nothing to do
//
//
//  private:
//    std::shared_ptr<T> _ptr;
//};


// used for indirection
template< typename T >
class pointer_wrapper : public std::shared_ptr<T>
{
  public:
    pointer_wrapper( size_t, std::shared_ptr<T> ptr ) // Note: size of memory pointed to not required
      : std::shared_ptr<T>( ptr )
    {}

    template< typename deleter >
    pointer_wrapper( size_t, std::shared_ptr<T> ptr, deleter d ) // Note: size of memory pointed to not required
      : std::shared_ptr<T>( ptr, d )
    {}


    operator T*() const
    {
      return this->get();
    }


    void flush() const
    {} // Note: nothing to do
};

class hash {
public:
    std::size_t operator()(const std::string& value) {
        constexpr size_t chunk_size = std::pow(2, 31);
        if (value.length() < chunk_size) {
            return std::hash<std::string>{}(value);
        } else {
            // std::hash segfaults when string has length >= 2^31
            // instead hash chunks of size 2^31 and concatenate hashes, then hash again
            std::stringstream ss;
            for (int i = 0; i < value.length() / chunk_size; ++i) {
                ss << std::hash<std::string>{}(value.substr(i * chunk_size, chunk_size));
            }
            return hash{}(ss.str());
        }
    }
};


} // namespace "common"

} // namespace "ocal"


#endif /* common_helper_hpp */
