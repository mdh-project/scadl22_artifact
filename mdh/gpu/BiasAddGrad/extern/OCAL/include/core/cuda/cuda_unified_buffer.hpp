//
//  cuda_unified_buffer.hpp
//  ocal
//
//  Created by Ari Rasch on 18.10.17.
//  Copyright © 2017 Ari Rasch. All rights reserved.
//

#ifndef cuda_unified_buffer_h
#define cuda_unified_buffer_h


namespace ocal
{

namespace core
{

namespace cuda
{


template< typename T >
class unified_buffer : public abstract::abstract_unified_buffer< unified_buffer<T>,                  // (required for static polymorphism)
                                                                 std::shared_ptr<T>,                 // buffer_t
                                                                 CUevent_wrapper,                    // even_t
                                                                 ::ocal::common::pointer_wrapper<T>, // host_memory_ptr_t
                                                                 T
                                                               >
{
  // friend class parent
  friend class abstract::abstract_unified_buffer< unified_buffer<T>,                  // (required for static polymorphism)
                                                  std::shared_ptr<T>,                 // buffer_t
                                                  CUevent_wrapper,                    // even_t
                                                  ::ocal::common::pointer_wrapper<T>, // host_memory_ptr_t
                                                  T
                                                >;

  private:
    // enabling easier access to parent's members ( Note: these are available after ctors )
    using abstract::abstract_unified_buffer< unified_buffer<T>, std::shared_ptr<T>, CUevent_wrapper, ::ocal::common::pointer_wrapper<T>, T >::_size;
    using abstract::abstract_unified_buffer< unified_buffer<T>, std::shared_ptr<T>, CUevent_wrapper, ::ocal::common::pointer_wrapper<T>, T >::_platform_id_of_host;
    using abstract::abstract_unified_buffer< unified_buffer<T>, std::shared_ptr<T>, CUevent_wrapper, ::ocal::common::pointer_wrapper<T>, T >::_platform_or_stream_id_to_opencl_or_cuda_buffer; // Note: has to be declared before "_host_memory" due to host buffer creation of unified buffers
    using abstract::abstract_unified_buffer< unified_buffer<T>, std::shared_ptr<T>, CUevent_wrapper, ::ocal::common::pointer_wrapper<T>, T >::_host_memory;
    using abstract::abstract_unified_buffer< unified_buffer<T>, std::shared_ptr<T>, CUevent_wrapper, ::ocal::common::pointer_wrapper<T>, T >::_ptr_to_host_memory;
    using abstract::abstract_unified_buffer< unified_buffer<T>, std::shared_ptr<T>, CUevent_wrapper, ::ocal::common::pointer_wrapper<T>, T >::_platform_or_stream_id_to_dirty_flag;
    using abstract::abstract_unified_buffer< unified_buffer<T>, std::shared_ptr<T>, CUevent_wrapper, ::ocal::common::pointer_wrapper<T>, T >::_host_is_dirty;


  public:
    unified_buffer( size_t size = 0 )
      : abstract::abstract_unified_buffer< unified_buffer<T>, std::shared_ptr<T>, CUevent_wrapper, ::ocal::common::pointer_wrapper<T>, T >( size,
                                                                                                                                            [&]() -> auto& // Note: creates initial device buffer which is used as host memory
                                                                                                                                            {
                                                                                                                                              // create device buffer
                                                                                                                                              auto ptr_to_managed_cuda_buffer = &_platform_or_stream_id_to_opencl_or_cuda_buffer[ _platform_id_of_host ];
                                                              
                                                                                                                                              CUDA_SAFE_CALL( cuMemAllocManaged( reinterpret_cast< CUdeviceptr* >( ptr_to_managed_cuda_buffer ), size * sizeof(T), CU_MEM_ATTACH_GLOBAL ) );
                                                                                                                                             
                                                                                                                                              return _platform_or_stream_id_to_opencl_or_cuda_buffer.at( _platform_id_of_host );
                                                                                                                                            },
                                                                                                                                            0 // Note: CUDA's host platform has id 0
                                                                                                                                          )
    {}
  
  
    unified_buffer( size_t size, T init_value )
      : abstract::abstract_unified_buffer< unified_buffer<T>, std::shared_ptr<T>, CUevent_wrapper, ::ocal::common::pointer_wrapper<T>, T >( size,
                                                                                                                                            [&]() -> auto& // Note: creates initial device buffer which is used as host memory
                                                                                                                                            {
                                                                                                                                              // create device buffer
                                                                                                                                              auto ptr_to_managed_cuda_buffer = &_platform_or_stream_id_to_opencl_or_cuda_buffer[ _platform_id_of_host ];
                                                              
                                                                                                                                              CUDA_SAFE_CALL( cuMemAllocManaged( reinterpret_cast< CUdeviceptr* >( ptr_to_managed_cuda_buffer ), size * sizeof(T), CU_MEM_ATTACH_GLOBAL ) );
                                                                                                                                             
                                                                                                                                              return _platform_or_stream_id_to_opencl_or_cuda_buffer.at( _platform_id_of_host );
                                                                                                                                            },
                                                                                                                                            0 // Note: CUDA's host platform has id 0
                                                                                                                                          )
    {
      if( init_value == 0 )
        std::memset( _ptr_to_host_memory.get(), 0, _size * sizeof(T) );
      
      else
        for( int i = 0 ; i < _size ; ++i )
          this->operator[]( i ) = init_value;
    }
  
  
    unified_buffer( const std::vector<T>& vec )
      : abstract::abstract_unified_buffer< unified_buffer<T>, std::shared_ptr<T>, CUevent_wrapper, ::ocal::common::pointer_wrapper<T>, T >( vec.size(),
                                                                                                                                            [&]() -> auto& // Note: creates initial device buffer which is used as host memory
                                                                                                                                            {
                                                                                                                                              // create device buffer
                                                                                                                                              auto ptr_to_managed_cuda_buffer = &_platform_or_stream_id_to_opencl_or_cuda_buffer[ _platform_id_of_host ];
                                                              
                                                                                                                                              CUDA_SAFE_CALL( cuMemAllocManaged( reinterpret_cast< CUdeviceptr* >( ptr_to_managed_cuda_buffer ), vec.size() * sizeof(T), CU_MEM_ATTACH_GLOBAL ) );
                                                                                                                                             
                                                                                                                                              return _platform_or_stream_id_to_opencl_or_cuda_buffer.at( _platform_id_of_host );
                                                                                                                                            },
                                                                                                                                            0 // Note: CUDA's host platform has id 0
                                                                                                                                          )
    {
      this->set_content( vec.data() );
    }
  

    //unified_buffer()                            = default;  // Note: buffer are initialized with input size
    unified_buffer( const unified_buffer<T>&  ) = default; // Note: required to put buffers in std::vector
    unified_buffer(       unified_buffer<T>&& ) = default;

    unified_buffer& operator=( const unified_buffer<T>&  ) = default;
    unified_buffer& operator=(       unified_buffer<T>&& ) = default;

  
    ~unified_buffer()
    {
      // free managed CUDA memory
      auto& ptr_to_managed_cuda_buffer = _platform_or_stream_id_to_opencl_or_cuda_buffer[ _platform_id_of_host ];
      cuMemFree( reinterpret_cast< CUdeviceptr >( ptr_to_managed_cuda_buffer.get() ) );
    }
  

    // ----------------------------------------------------------
    //   get host memory pointer
    // ----------------------------------------------------------

    T* get_host_memory_ptr()
    {
      this->update_host_memory();
    
      // set only host buffer as up to date
      this->set_all_platform_buffers_as_dirty();
      this->set_host_memory_as_up_to_date();
      
      return _ptr_to_host_memory;
    }
  
  
    // ----------------------------------------------------------
    //   set/get host memory (static polymorphism)
    // ----------------------------------------------------------
  
    void set_content( T* new_content )
    {
      // wait for critical events to finish
      this->sync_read_events_on_host_memory();
      this->sync_write_events_on_host_memory();
      
      // copy data
      std::memcpy( _ptr_to_host_memory, new_content, _size * sizeof( T ) );
      
      // set only host buffer as up to date
      this->set_all_platform_buffers_as_dirty();
      this->set_host_memory_as_up_to_date();
    }


    std::vector<T> get_content()
    {
      this->update_host_memory();
    
      // declare result vector
      std::vector<T> content;
      content.reserve( _size );
      
      // copy data
      content.insert( content.end(), &_ptr_to_host_memory[ 0 ], &_ptr_to_host_memory[ _size ] );
  
      // set only host buffer as up to date
      this->set_all_platform_buffers_as_dirty();
      this->set_host_memory_as_up_to_date();
      
      return content;
    }


    // ---------------------------------------------------------------------------
    //   implicit cast to CUdeviceptr (enables compatibility to CUDA-Libraries)
    // ---------------------------------------------------------------------------

    operator T*()
    {
      return _host_memory.get();
    }
  

  private:

    // --------------------------------------------------------------------------------------
    //   native Buffer management: creation and H2D/D2H data transfers (static polymorphism)
    // --------------------------------------------------------------------------------------

    void create_platform_buffer_if_not_existant( int platform_id )
    {
      // create device buffer if it does not already exist
      if( _platform_or_stream_id_to_opencl_or_cuda_buffer.find( platform_id ) == _platform_or_stream_id_to_opencl_or_cuda_buffer.end() )
      {
        assert( false && "should nerver be reached" ); // Note: CUDA has only (NVIDIA-)context and thus manages only one unified_buffer (see CUDA UVM)
      }
    }
  
  
    void copy_data_from_platform_buffer_to_platform_buffer( int /*device_id_of_source*/, int /*device_id_of_destination*/ )
    {
      assert( false && "should nerver be reached" ); // Note: CUDA has only (NVIDIA-)context and thus manages only one unified_buffer (see CUDA UVM)
    }

  
    // access over unique device id
    void copy_data_from_host_memory_to_platform_buffer( int /*unique_device_id*/ )
    {
      assert( false && "should nerver be reached" ); // Note: CUDA has only (NVIDIA-)context and thus manages only one unified_buffer (see CUDA UVM)
    }



    void copy_data_from_platform_buffer_to_host_memory( int /*unique_device_id*/ )
    {
      assert( false && "should nerver be reached" ); // Note: CUDA has only (NVIDIA-)context and thus manages only one unified_buffer (see CUDA UVM)
    }
  
  
    // ----------------------------------------------------------
    //   overrides
    // ----------------------------------------------------------

    void update_platform_buffer( int=0 ) override // Note: platform if not required in CUDA
    {
      for( auto& device_id : unique_device_id_to_cuda_context )
      {
        set_cuda_context( device_id.first );

        // device synchronization: required by CUDA in case of using UVM
        CUDA_SAFE_CALL( cuCtxSynchronize() );
      }
    }


    void sync_read_events_on_device_buffer( int unique_device_id ) override
    {
//      // set cuda context
//      set_cuda_context( unique_device_id );  // TODO: really necessary?

      auto& events = common::event_manager< CUevent_wrapper >::_unique_device_id_to_read_events_on_device_buffer[ unique_device_id ]; // Note: "[...]" instead of ".at" to generate event vector when accessed the first time
      
      // device synchronization: required by CUDA in case of using UVM
      //CUDA_SAFE_CALL( cuCtxSynchronize() );
      this->update_platform_buffer();

      // wait for events
      for( auto& event : events )
        event.wait();
      
      events.clear();
    }
  
  
    void sync_write_events_on_device_buffer( int unique_device_id ) override
    {
//      // set cuda context
//      set_cuda_context( unique_device_id );  // TODO: really necessary?

      auto& events = common::event_manager< CUevent_wrapper >::_unique_device_id_to_write_events_on_device_buffer[ unique_device_id ]; // Note: "[...]" instead of ".at" to generate event vector when accessed the first time
      
      // device synchronization: required by CUDA in case of using UVM
      //CUDA_SAFE_CALL( cuCtxSynchronize() );
      this->update_platform_buffer();

      // wait for events
      for( auto& event : events )
        event.wait();
      
      events.clear();
    }
  
  
    void sync_read_events_on_host_memory() override
    {
      auto& events = common::event_manager< CUevent_wrapper >::_read_events_on_host_memory;
      
      // device synchronization: required by CUDA in case of using UVM
      //CUDA_SAFE_CALL( cuCtxSynchronize() );
      this->update_platform_buffer();

      // wait for events
      for( auto& event : events )
        event.wait();
      
      events.clear();
    }
  
  
    void sync_write_events_on_host_memory() override
    {
      auto& events = common::event_manager< CUevent_wrapper >::_write_events_on_host_memory;
      
      // device synchronization: required by CUDA in case of using UVM
      //CUDA_SAFE_CALL( cuCtxSynchronize() );
      this->update_platform_buffer();

      // wait for events
      for( auto& event : events )
        event.wait();
      
      events.clear();
    }
  
    // ----------------------------------------------------------
    //   helper
    // ----------------------------------------------------------

    int get_platform_id( int /*unique_device_id*/ )
    {
      return 0; // Note: CUDA has only (NVIDIA-)context and thus manages only one unified_buffer (see CUDA UVM)
    }
};


} // namespace "cuda"

} // namespace "core"

} // namespace "ocal"


#endif /* cuda_unified_buffer_h */
