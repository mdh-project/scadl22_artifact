//
//  ocl_buffer.hpp
//  ocal_ocl
//
//  Created by Ari Rasch on 20.09.17.
//  Copyright © 2017 Ari Rasch. All rights reserved.
//


#ifndef ocl_unified_buffer_hpp
#define ocl_unified_buffer_hpp


namespace ocal
{

namespace core
{

namespace ocl
{


template< typename T >
class unified_buffer : public abstract::abstract_unified_buffer< unified_buffer<T>,             // (required for static polymorphism)
                                                                 cl::Buffer,                    // buffer_t
                                                                 clEvent_wrapper,               // event_t
                                                                 opencl_host_buffer_wrapper<T>, // host_memory_ptr_t
                                                                 T
                                                               >
{
  // friend class parent
  friend class abstract::abstract_unified_buffer< unified_buffer<T>,             // (required for static polymorphism)
                                                  cl::Buffer,                    // buffer_t
                                                  clEvent_wrapper,               // event_t
                                                  opencl_host_buffer_wrapper<T>, // host_memory_ptr_t
                                                  T
                                                >;

  private:
    // enabling easier access to parent's members ( Note: these are available after ctors )
    using abstract::abstract_unified_buffer< unified_buffer<T>, cl::Buffer, clEvent_wrapper, opencl_host_buffer_wrapper<T>, T >::_size;
    using abstract::abstract_unified_buffer< unified_buffer<T>, cl::Buffer, clEvent_wrapper, opencl_host_buffer_wrapper<T>, T >::_platform_id_of_host;
    using abstract::abstract_unified_buffer< unified_buffer<T>, cl::Buffer, clEvent_wrapper, opencl_host_buffer_wrapper<T>, T >::_platform_or_stream_id_to_opencl_or_cuda_buffer; // Note: has to be declared before "_host_memory" due to host buffer creation of unified buffers
    using abstract::abstract_unified_buffer< unified_buffer<T>, cl::Buffer, clEvent_wrapper, opencl_host_buffer_wrapper<T>, T >::_host_memory;
    using abstract::abstract_unified_buffer< unified_buffer<T>, cl::Buffer, clEvent_wrapper, opencl_host_buffer_wrapper<T>, T >::_ptr_to_host_memory;
    using abstract::abstract_unified_buffer< unified_buffer<T>, cl::Buffer, clEvent_wrapper, opencl_host_buffer_wrapper<T>, T >::_platform_or_stream_id_to_dirty_flag;
    using abstract::abstract_unified_buffer< unified_buffer<T>, cl::Buffer, clEvent_wrapper, opencl_host_buffer_wrapper<T>, T >::_host_is_dirty;
  
  
  public:
    unified_buffer( size_t size = 0 )
      : abstract::abstract_unified_buffer< unified_buffer<T>, cl::Buffer, clEvent_wrapper, opencl_host_buffer_wrapper<T>, T >( size,
                                                                                                                               [&]() -> auto& // Note: creates initial device buffer which is used as host memory
                                                                                                                               {
                                                                                                                                 // get platform
                                                                                                                                 std::vector<cl::Platform> platforms;
                                                                                                                                 try
                                                                                                                                 {
                                                                                                                                   cl::Platform::get( &platforms );
                                                                                                                                 }
                                                                                                                                 catch( cl::Error& err )
                                                                                                                                 {
                                                                                                                                   check_opencl_exception( err );
                                                                                                                                   throw;
                                                                                                                                 }
                                                                                                                                 auto platform = platforms[ ocl::PLATFORM_ID_OF_HOST_DEVICE ];
       
                                                                                                                                 // get devices
                                                                                                                                 std::vector<cl::Device> devices;
                                                                                                                                 platform.getDevices( CL_DEVICE_TYPE_ALL, &devices );
           
                                                                                                                                 // create new context for device's platform if context does not already exists
                                                                                                                                 create_context_if_not_existent( devices, platform, ocl::PLATFORM_ID_OF_HOST_DEVICE );
              
                                                                                                                                 // create device buffer
                                                                                                                                 _platform_or_stream_id_to_opencl_or_cuda_buffer[ ocl::PLATFORM_ID_OF_HOST_DEVICE ] = cl::Buffer( opencl_platform_id_to_context.at( ocl::PLATFORM_ID_OF_HOST_DEVICE ),
                                                                                                                                                                                                                                  CL_MEM_READ_WRITE | CL_MEM_ALLOC_HOST_PTR,      // Note: "CL_MEM_ALLOC_HOST_PTR" causes memory to be allocated in host memory
                                                                                                                                                                                                                                  size * sizeof( T )
                                                                                                                                                                                                                                );

                                                                                                                                 return _platform_or_stream_id_to_opencl_or_cuda_buffer.at( ocl::PLATFORM_ID_OF_HOST_DEVICE );
                                                                                                                               },
                                                                                                                               ocl::PLATFORM_ID_OF_HOST_DEVICE // Note: set platform id of host
                                                                                                                             )
    {}
  
  
    unified_buffer( size_t size, T init_value )
      : abstract::abstract_unified_buffer< unified_buffer<T>, cl::Buffer, clEvent_wrapper, opencl_host_buffer_wrapper<T>, T >( size,
                                                                                                                               [&]() -> auto& // Note: creates initial device buffer which is used as host memory
                                                                                                                               {
                                                                                                                                 // get platform
                                                                                                                                 std::vector<cl::Platform> platforms;
                                                                                                                                 try
                                                                                                                                 {
                                                                                                                                   cl::Platform::get( &platforms );
                                                                                                                                 }
                                                                                                                                 catch( cl::Error& err )
                                                                                                                                 {
                                                                                                                                   check_opencl_exception( err );
                                                                                                                                   throw;
                                                                                                                                 }
                                                                                                                                 auto platform = platforms[ ocl::PLATFORM_ID_OF_HOST_DEVICE ];
       
                                                                                                                                 // get devices
                                                                                                                                 std::vector<cl::Device> devices;
                                                                                                                                 platform.getDevices( CL_DEVICE_TYPE_ALL, &devices );
           
                                                                                                                                 // create new context for device's platform if context does not already exists
                                                                                                                                 create_context_if_not_existent( devices, platform, ocl::PLATFORM_ID_OF_HOST_DEVICE );
              
                                                                                                                                 // create device buffer
                                                                                                                                 _platform_or_stream_id_to_opencl_or_cuda_buffer[ ocl::PLATFORM_ID_OF_HOST_DEVICE ] = cl::Buffer( opencl_platform_id_to_context.at( ocl::PLATFORM_ID_OF_HOST_DEVICE ),
                                                                                                                                                                                                                                  CL_MEM_READ_WRITE | CL_MEM_ALLOC_HOST_PTR,      // Note: "CL_MEM_ALLOC_HOST_PTR" causes memory to be allocated in host memory
                                                                                                                                                                                                                                  size * sizeof( T )
                                                                                                                                                                                                                                );

                                                                                                                                 return _platform_or_stream_id_to_opencl_or_cuda_buffer.at( ocl::PLATFORM_ID_OF_HOST_DEVICE );
                                                                                                                               },
                                                                                                                               ocl::PLATFORM_ID_OF_HOST_DEVICE // Note: set platform id of host
                                                                                                                             )
    {
      if( init_value == 0 )
        std::memset( _ptr_to_host_memory.get(), 0, _size * sizeof(T) );

      else
        for( int i = 0 ; i < _size ; ++i )
          this->operator[]( i ) = init_value;      
    }
  
  
    unified_buffer( const std::vector<T>& vec )
      : abstract::abstract_unified_buffer< unified_buffer<T>, cl::Buffer, clEvent_wrapper, opencl_host_buffer_wrapper<T>, T >( vec.size(),
                                                                                                                               [&]() -> auto& // Note: creates initial device buffer which is used as host memory
                                                                                                                               {
                                                                                                                                 // get platform
                                                                                                                                 std::vector<cl::Platform> platforms;
                                                                                                                                 try
                                                                                                                                 {
                                                                                                                                   cl::Platform::get( &platforms );
                                                                                                                                 }
                                                                                                                                 catch( cl::Error& err )
                                                                                                                                 {
                                                                                                                                   check_opencl_exception( err );
                                                                                                                                   throw;
                                                                                                                                 }
                                                                                                                                 auto platform = platforms[ ocl::PLATFORM_ID_OF_HOST_DEVICE ];
       
                                                                                                                                 // get devices
                                                                                                                                 std::vector<cl::Device> devices;
                                                                                                                                 platform.getDevices( CL_DEVICE_TYPE_ALL, &devices );
           
                                                                                                                                 // create new context for device's platform if context does not already exists
                                                                                                                                 create_context_if_not_existent( devices, platform, ocl::PLATFORM_ID_OF_HOST_DEVICE );
              
                                                                                                                                 // create device buffer
                                                                                                                                 _platform_or_stream_id_to_opencl_or_cuda_buffer[ ocl::PLATFORM_ID_OF_HOST_DEVICE ] = cl::Buffer( opencl_platform_id_to_context.at( ocl::PLATFORM_ID_OF_HOST_DEVICE ),
                                                                                                                                                                                                                                  CL_MEM_READ_WRITE | CL_MEM_ALLOC_HOST_PTR,      // Note: "CL_MEM_ALLOC_HOST_PTR" causes memory to be allocated in host memory
                                                                                                                                                                                                                                  vec.size() * sizeof( T )
                                                                                                                                                                                                                                );

                                                                                                                                 return _platform_or_stream_id_to_opencl_or_cuda_buffer.at( ocl::PLATFORM_ID_OF_HOST_DEVICE );
                                                                                                                               },
                                                                                                                               ocl::PLATFORM_ID_OF_HOST_DEVICE // Note: set platform id of host
                                                                                                                             )
    {
      this->set_content( vec.data() );
    }
  
    //unified_buffer()                            = delete;  // Note: buffer are initialized with input size
    unified_buffer( const unified_buffer<T>&  ) = default; // Note: required to put buffers in std::vector
    unified_buffer(       unified_buffer<T>&& ) = default;

    unified_buffer& operator=( const unified_buffer<T>&  ) = default;
    unified_buffer& operator=(       unified_buffer<T>&& ) = default;


    // ----------------------------------------------------------
    //   get host memory pointer
    // ----------------------------------------------------------

    T* get_host_memory_ptr()
    {
      this->update_host_memory();
    
      // set only host buffer as up to date
      this->set_all_platform_buffers_as_dirty();
      this->set_host_memory_as_up_to_date();
      
      return _ptr_to_host_memory;
    }


    // ----------------------------------------------------------
    //   set/get host memory (static polymorphism)
    // ----------------------------------------------------------

    void set_content( T* new_content )
    {
      // wait for critical events to finish
      this->sync_read_events_on_host_memory();
      this->sync_write_events_on_host_memory();

      // copy data
      std::memcpy( _ptr_to_host_memory, new_content, _size * sizeof( T ) );

      // set only host buffer as up to date
      this->set_all_platform_buffers_as_dirty();
      this->set_host_memory_as_up_to_date();
    }

  
    std::vector<T> get_content()
    {
      // update host memory
      this->update_host_memory();
      
      // declare result vector
      std::vector<T> content;
      content.reserve( _size );
      
      // copy data
      content.insert( content.end(), &_ptr_to_host_memory[ 0 ], &_ptr_to_host_memory[ _size ] );

      return content;
    }
    

    // ------------------------------------------------------------------------------
    //   implicit cast to cl::Buffer (enables compatibility to OpenCL-Libraries)
    // ------------------------------------------------------------------------------

    operator cl::Buffer()
    {
      return _host_memory;
    }


  private:

    // ---------------------------------------------------------------------------------------
    //   native Buffer management: creation and H2D/D2H data transfers (static polymorphism)
    // ---------------------------------------------------------------------------------------

    void create_platform_buffer_if_not_existent( int platform_id )
    {
      // create device buffer if it does not already exist
      if( _platform_or_stream_id_to_opencl_or_cuda_buffer.find( platform_id ) == _platform_or_stream_id_to_opencl_or_cuda_buffer.end() )
      {
        _platform_or_stream_id_to_opencl_or_cuda_buffer[ platform_id ] = cl::Buffer( opencl_platform_id_to_context.at( platform_id ),
                                                                                     CL_MEM_READ_WRITE | CL_MEM_ALLOC_HOST_PTR,      // Note: "CL_MEM_ALLOC_HOST_PTR" causes memory to be allocated in host memory
                                                                                     _size * sizeof( T )
                                                                                   );
        _platform_or_stream_id_to_dirty_flag[ platform_id ] = true; // Note: buffer does not exist and is read so it is dirty
      }
    }
  
  
    void copy_data_from_platform_buffer_to_platform_buffer( int platform_id_of_source, int platform_id_of_destination )
    {
      assert( (platform_id_of_source != platform_id_of_destination) && "makes no sense" );
      
      // get device ids
      auto device_id_of_source      = get_random_unique_device_id_for_platform_id( platform_id_of_source      );
      auto device_id_of_destination = get_random_unique_device_id_for_platform_id( platform_id_of_destination );
    
      assert( _platform_or_stream_id_to_opencl_or_cuda_buffer.find( platform_id_of_source      ) != _platform_or_stream_id_to_opencl_or_cuda_buffer.end() );  // assert device buffers exists
      assert( _platform_or_stream_id_to_opencl_or_cuda_buffer.find( platform_id_of_destination ) != _platform_or_stream_id_to_opencl_or_cuda_buffer.end() );  // assert device buffers exists
      assert( this->is_platform_buffer_up_to_date( platform_id_of_source )                                                                                  );  // assert source buffer is up to date
      
      // get default command queue of device "unique_device_id"
      auto command_queue_of_source      = unique_device_id_to_actual_opencl_command_queue( device_id_of_source      );
      auto command_queue_of_destination = unique_device_id_to_actual_opencl_command_queue( device_id_of_destination );
      
      // get pointer to host memory
      auto source_ptr      = command_queue_of_source.enqueueMapBuffer( _platform_or_stream_id_to_opencl_or_cuda_buffer.at( platform_id_of_source ),
                                                                       CL_TRUE,
                                                                       CL_MAP_READ,
                                                                       0,                   // offset
                                                                       _size * sizeof( T )
                                                                     );
      
      auto destination_ptr = command_queue_of_destination.enqueueMapBuffer( _platform_or_stream_id_to_opencl_or_cuda_buffer.at( platform_id_of_destination ),
                                                                            CL_TRUE,
                                                                            CL_MAP_WRITE,
                                                                            0,              // offset
                                                                            _size * sizeof( T )
                                                                          );
      
      // mem copy
      std::memcpy( destination_ptr, source_ptr, _size * sizeof( T ) ); // TODO: durch enqueWriteBuffer ersetzen testen (ptr auf source holen und mit "command_queue_of_destination.enqueWriteBuffer(...)" Daten kopieren
      
      // free pointer to host memory
      command_queue_of_source.enqueueUnmapMemObject( _platform_or_stream_id_to_opencl_or_cuda_buffer.at( platform_id_of_source ),
                                                     source_ptr
                                                   );
      command_queue_of_destination.enqueueUnmapMemObject( _platform_or_stream_id_to_opencl_or_cuda_buffer.at( platform_id_of_destination ),
                                                          destination_ptr
                                                        );
      
   }
  
  
    // access over unique device id
    void copy_data_from_host_memory_to_platform_buffer( int platform_id )
    {
      assert( _platform_or_stream_id_to_opencl_or_cuda_buffer.find( PLATFORM_ID_OF_HOST_DEVICE) != _platform_or_stream_id_to_opencl_or_cuda_buffer.end() );
      assert( _platform_or_stream_id_to_opencl_or_cuda_buffer.find( platform_id               ) != _platform_or_stream_id_to_opencl_or_cuda_buffer.end() );
      
      // copy data from host to device
      this->copy_data_from_platform_buffer_to_platform_buffer( _platform_id_of_host, platform_id );
    }


    void copy_data_from_platform_buffer_to_host_memory( int platform_id )
    {
      assert( _platform_or_stream_id_to_opencl_or_cuda_buffer.find( platform_id                ) != _platform_or_stream_id_to_opencl_or_cuda_buffer.end() );  // assert device buffer exists
      assert( _platform_or_stream_id_to_opencl_or_cuda_buffer.find( PLATFORM_ID_OF_HOST_DEVICE ) != _platform_or_stream_id_to_opencl_or_cuda_buffer.end() );
      
      // copy data from device to host
      this->copy_data_from_platform_buffer_to_platform_buffer( platform_id, _platform_id_of_host );
    }
  
  
    // ----------------------------------------------------------
    //   overrides
    // ----------------------------------------------------------

    void sync_write_events_on_host_memory() override
    {
      this->sync_write_events_of_platform( ocl::unique_device_id_to_opencl_platform_id, _platform_id_of_host );
    }
  
    virtual void update_platform_buffer( int platform_id ) override
    {
      // if device buffer is already up to date
      if( this->is_platform_buffer_up_to_date( platform_id ) )
      {
        // wait for critical events to finish
        this->sync_write_events_of_platform( ocl::unique_device_id_to_opencl_platform_id, platform_id );

        return;
      }
      

      // if host memory is up to date then copy data from host memory to device buffer, otherwise: copy data from D2H and then from H2D
      if( this->is_host_memory_up_to_date() )
      {
        this->sync_write_events_of_platform( ocl::unique_device_id_to_opencl_platform_id, _platform_id_of_host );
        this->sync_write_events_of_platform( ocl::unique_device_id_to_opencl_platform_id, platform_id );
        
        this->copy_data_from_host_memory_to_platform_buffer( platform_id );
        
        this->sync_write_events_of_platform( ocl::unique_device_id_to_opencl_platform_id, platform_id );
      }
      else
      {
        // find clean buffer
        auto source_platform_id = this->get_platform_id_of_clean_platform_buffer();
        
        // get a random device of the platform with id "source_platform_id"
        this->copy_data_from_platform_buffer_to_platform_buffer( source_platform_id, platform_id );
      }
      
      // manage dirty flags
      this->set_platform_buffer_as_up_to_date( platform_id );
      this->set_host_memory_as_up_to_date();
    }
  
  
    // ----------------------------------------------------------
    //   helper (static polymorphism)
    // ----------------------------------------------------------

    int get_platform_id( int unique_device_id )
    {
      return ocl::unique_device_id_to_opencl_platform_id.at( unique_device_id );
    }
  
    template< typename T_arg >
    void sync_write_events_of_platform( T_arg unique_device_id_to_platform_id, int platform_id )
    {
      for( auto& elem : this->_unique_device_id_to_write_events_on_device_buffer )
      {
        auto& device_id = elem.first;
        
        if( unique_device_id_to_platform_id.at( device_id ) == platform_id )
          this->sync_write_events_on_device_buffer( device_id );
      }
    }
};


} // namespace "ocl"

} // namespace "core"

} // namespace "ocal"


#endif /* ocl_unified_buffer_hpp */
