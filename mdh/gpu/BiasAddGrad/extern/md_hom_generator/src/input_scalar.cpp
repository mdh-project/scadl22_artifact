//
// Created by Richard Schulze on 30.01.18.
//

#include "input_scalar.hpp"

namespace md_hom {

input_scalar_class::input_scalar_class(const std::string &data_type, const std::string &name) :
        _name(name), _data_type(data_type) {
}

const std::string &input_scalar_class::name() const {
    return _name;
}

const std::string &input_scalar_class::data_type() const {
    return _data_type;
}

input_scalar_class input_scalar(const std::string &data_type, const std::string &name) {
    return input_scalar_class(data_type, name);
}

}