//
// Created by Richard Schulze on 06.11.17.
//

#ifndef MD_BLAS_INPUT_WRAPPER_HPP
#define MD_BLAS_INPUT_WRAPPER_HPP


namespace md_hom {
namespace generator {

class input_wrapper {
public:
    virtual ~input_wrapper() = default;

    virtual std::string input_name() const = 0;
    virtual const std::vector<std::vector<std::string>> &index_functions() const = 0;
    virtual const std::vector<std::vector<dimension_t>> &used_dimensions() const = 0;
    virtual const std::vector<dimension_t> &distinct_used_dimensions() const = 0;
    virtual std::string macro_name(unsigned int kernel, int value_id, LEVEL level, char *phase) const = 0;
    virtual std::string call_macro(unsigned int kernel, int value_id, LEVEL level, char *phase,
                                   const std::vector<std::string> &parameters) const = 0;
    virtual std::string fu_macro_name(unsigned int kernel, int value_id, LEVEL level, char *phase) const = 0;
    virtual std::string call_fu_macro(unsigned int kernel, int value_id, LEVEL level, char *phase,
                                      const std::vector<std::string> &parameters) const = 0;
    virtual bool has_definitions(unsigned int kernel) const = 0;
    virtual std::string definitions(unsigned int kernel) const = 0;
    virtual bool supports_caching() const = 0;
    virtual std::string caching_variable_declaration(unsigned int kernel, LEVEL level) const {
        return "";
    }
    virtual std::string caching_copy_code(unsigned int kernel, LEVEL level, char *phase, std::map<dimension_t, std::vector<LEVEL>> &skipped_loops, bool dbl_buffering) const {
        return "";
    }
    virtual std::string replace_vars_in_index_function(unsigned int value_id, unsigned int dim_id, const std::vector<std::string> &params) const {
        return "";
    }
    virtual std::vector<std::string> replace_vars_in_index_function(const std::vector<unsigned int> &value_id, const std::vector<unsigned int> &dim_id, const std::vector<std::vector<std::string>> &params) const {
        return {};
    }
};

}
}

#endif //MD_BLAS_INPUT_WRAPPER_HPP
