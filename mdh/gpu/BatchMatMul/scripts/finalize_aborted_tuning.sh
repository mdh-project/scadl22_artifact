#!/usr/bin/env bash

results_dir=$1
csv_file=$results_dir/tuning_log.csv
config_json=$results_dir/tuned_configuration

# get log line number of best config
min_runtime=`cut -d";" -f 6 $results_dir/tuning_log.csv | tail -n +2 | sort -n | head -n 1`
log_line=`grep -n ";$min_runtime;" $results_dir/tuning_log.csv | cut -d":" -f 1 | head -n 1`

# get csv config data
csv_header=`cat $csv_file | head -n 1 | cut -d";" -f7-`
csv_data=`cat $csv_file | head -n $((log_line)) | tail -n -1 | cut -d";" -f7-`

# convert csv data to json
IFS=';' read -ra split_header <<< "$csv_header"
IFS=';' read -ra split_data <<< "$csv_data"
echo "{" > $config_json
for (( i=0; i<${#split_header[@]}; i++ )); do
  [ $i -gt 0 ] && echo "," >> $config_json
  printf "    \"${split_header[i]}\": ${split_data[i]}" >> $config_json
done
printf "\n}" >> $config_json