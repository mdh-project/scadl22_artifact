#include "data_util/include/data_util.hpp"
#include "md_hom_generator/md_hom_generator.hpp"
#include "tclap/CmdLine.h"

#include "meta_info.cpp"

int main(int argc, const char **argv) {
    TCLAP::CmdLine cmd("Generate md_hom kernel.");

    TCLAP::ValueArg<bool> dynamic_arg("d", "dynamic", "Generate kernel with dynamic input sizes.", false, false, "bool");
    cmd.add(dynamic_arg);

    TCLAP::ValueArg<unsigned int> parallelize_arg("p", "parallelize", "Number of dimensions for which the generation is done in parallel.", false, 0, "unsigned int");
    cmd.add(parallelize_arg);

    TCLAP::MultiArg<std::string> skip_postprocessing_arg("s", "skip-postprocessing", "Skip post processing in this dimension.", false, "string");
    cmd.add(skip_postprocessing_arg);

    // add custom arguments
    add_arguments(cmd);

    // parse arguments
    try {
        cmd.parse(argc, argv);
    } catch (TCLAP::ArgException &e) {
        std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
        exit(EXIT_FAILURE);
    }
    std::vector<md_hom::dimension_t> skip_postprocessing;
    for (const auto &str : skip_postprocessing_arg.getValue()) {
        const std::regex dim_regex("^([lr])([1-9][0-9]*)$");
        std::smatch match;
        if (std::regex_search(str, match, dim_regex)) {
            skip_postprocessing.push_back(match[1].str() == "l" ? md_hom::L(std::atoi(match[2].str().c_str())) : md_hom::R(std::atoi(match[2].str().c_str())));
        } else {
            std::cerr << "error: argument skip-postprocessing (-s) expects values of the form [lr][1-9][0-9]*" << std::endl;
            exit(EXIT_FAILURE);
        }
    }
    skip_postprocessing = md_hom::generator::sort<4, 1>(skip_postprocessing);

    std::string application = "md_hom";
    std::string version = "strided_v3";
    std::string routine = "batch_matmul";
    std::vector<std::string> path = {"kernel", application, version, routine, dynamic_arg.getValue() ? "dynamic" : "static"};
    generation_data_path(path);
    std::string data_dir = "../" + data_directory(path);
    prepare_data_directory(data_dir, true);
    std::string file_1 = data_dir + "/" + routine, file_2 = data_dir + "/" + routine;
    if (!skip_postprocessing.empty()) {
        file_1 += "_skip_pp";
        file_2 += "_skip_pp";
    }
    for (const auto &dim : skip_postprocessing) {
        file_1 += std::string("_") + (dim.type == md_hom::DIM_TYPE::L ? "l" : "r") + std::to_string(dim.nr);
        file_2 += std::string("_") + (dim.type == md_hom::DIM_TYPE::L ? "l" : "r") + std::to_string(dim.nr);
    }
    std::string kernel_name;
    if (dynamic_arg.getValue() == true) {
        file_1 += "_dynamic_1";
        file_2 += "_dynamic_2";
        kernel_name = routine + "_dynamic";
    } else {
        file_1 += "_static_1";
        file_2 += "_static_2";
        kernel_name = routine + "_static";
    }
    file_1 += ".cuda";
    file_2 += ".cuda";

    auto a = md_hom::input_buffer("float", "a", std::vector<std::string>({"l1", "l2", "l3", "r1"}));
    auto b = md_hom::input_buffer("float", "b", std::vector<std::string>({"l1", "l2", "r1", "l4"}));
    auto c = md_hom::result_buffer("float", md_hom::PLUS, "c", std::vector<std::string>({"l1", "l2", "l3", "l4"}));
    auto md_hom = md_hom::md_hom<4, 1>(kernel_name,
                                       md_hom::inputs(a, b),
                                       md_hom::scalar_function([] (bool reduce) {
                                           return std::string("c ") + (reduce ? "+=" : "=") + " a * b;";
                                       }),
                                       md_hom::outputs(c)
    );
    auto generator = md_hom::generator::cuda_generator(md_hom, dynamic_arg.getValue(), skip_postprocessing, parallelize_arg.getValue());
    std::ofstream kernel_file;
    kernel_file.open(file_1, std::fstream::out | std::fstream::trunc);
    kernel_file << generator.kernel_1();
    kernel_file.close();
    kernel_file.open(file_2, std::fstream::out | std::fstream::trunc);
    kernel_file << generator.kernel_2();
    kernel_file.close();
}