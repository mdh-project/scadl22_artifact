//
//  ocl_kernel.hpp
//  ocal_ocl
//
//  Created by Ari Rasch on 20.09.17.
//  Copyright © 2017 Ari Rasch. All rights reserved.
//

#ifndef ocl_kernel_hpp
#define ocl_kernel_hpp

#include "../common/json.hpp"


namespace ocal
{

namespace core
{


// forward declaration
namespace cuda
{
//  class path;
//  class source;
  class kernel_representation;
}


namespace ocl
{

// forward declaration
class source;


class kernel : public abstract::abstract_kernel< kernel, cl::Kernel >
{
  // friend class parent
  friend class abstract::abstract_kernel< kernel, cl::Kernel >;

  // friend classes
  friend class device;


  public:
    kernel()                 = delete;  // Note: kernel should always take arguments (e.g., kernel's source code)
    kernel( const kernel& )  = delete;
    kernel(       kernel&& ) = default;


    template< typename... Ts >
    kernel( const Ts&... args )
      : abstract_kernel( args... )
    {}


    template< typename T, typename... Ts, typename = std::enable_if_t< std::is_base_of< cuda::kernel_representation, typename ::ocal::common::remove_cv_and_references<T>::type >::value > >
    kernel( const T& cuda_source, const Ts&... args )
      : abstract_kernel( ocl::source( common::cuda2opencl( cuda_source.get_source() ) ) , args... )
    {}


  private:
  
    std::vector<std::string> get_kernel_names_in_source( const std::string& kernel_code )
    {
      // defines a regular expression for pattern "__kernel void NAME ("
      std::regex rgx( "__kernel(?:.|\\s)*?(\\w+)\\s*\\(" );
      
      return abstract::abstract_kernel< kernel, cl::Kernel >::get_kernel_names_in_source( rgx, kernel_code );
    }
    

    // ----------------------------------------------------------
    //   static polymorphism
    // ----------------------------------------------------------
  
    void build_kernel_for_device( int unique_device_id )
    {
      // if kernel already has been generated then do nothing
      if( _unique_device_id_to_kernel.find( unique_device_id ) != _unique_device_id_to_kernel.end() )
        return;

      // extract kernel name -- if no kernel name is set by the user then we automatically use the first found kernel in kernel's source file
      auto kernel_name = ( !_kernel_name.empty() ) ? _kernel_name : get_kernel_names_in_source( _kernel_as_string )[ 0 ];

      // prepare kernel flags
      std::string kernel_flags_accumulated;
      for( const auto& flag : _kernel_flags)
        kernel_flags_accumulated += flag + " ";

      // get OpenCL context and device for "unique_device_id"
      auto context = unique_device_id_to_opencl_context( unique_device_id );
      auto device  = unique_device_id_to_opencl_device.at( unique_device_id );
      
      cl::Program program;

      // check if auto-tuned kernel exists or can be generated
      auto vendor_name_as_hash = std::to_string( ocal::common::hash{}( vendor_name_of( unique_device_id ) ) );
      auto device_name_as_hash = std::to_string( ocal::common::hash{}( device_name_of( unique_device_id ) ) );
      auto kernel_hash         = std::to_string( ocal::common::hash{}( _kernel_as_string                  ) );
      auto sp_string           = _sps.get_sps_as_string( "_" );
      auto flags_hash          = std::to_string( ocal::common::hash{}( kernel_flags_accumulated           ) );

      auto path_to_auto_tuned_kernel               = PATH_TO_OCAL_KERNEL_DB           + std::string( "/OCL/" ) + vendor_name_as_hash + std::string( "/" ) + device_name_as_hash + std::string( "/" );
      auto path_to_tuning_script                   = PATH_TO_OCAL_AUTO_TUNING_SCRIPTS + std::string( "/OCL/" );// + kernel_name;// + std::string( "__" ) + kernel_hash;
      auto kernel_name_with_hash                   = kernel_name           + std::string( "__" ) + kernel_hash;
      auto kernel_name_with_hash_and_sps_and_flags = kernel_name_with_hash + std::string( "__" ) + sp_string + std::string( "__" ) + flags_hash;
      
      // if no auto-tuned kernel is in databe but an according tuning script is provided by the user then generate an auto-tuned kernel
      if( !common::file_exists( path_to_auto_tuned_kernel + kernel_name_with_hash_and_sps_and_flags ) && common::file_exists( path_to_tuning_script + kernel_name_with_hash ) )
      {
        auto tuning_script_cmd_line_parameters = path_to_auto_tuned_kernel                                                       + std::string( " " ) +
                                                 kernel_name                                                                     + std::string( " " ) +  // Note: kernel hash and static parameter values have to be appended automaticaly by the auto-tuning script for correctness issues
                                                 std::to_string( unique_device_id_to_opencl_platform_id.at( unique_device_id ) ) + std::string( " " ) +  // TODO: use platform and device -> enables better portability and avoids retuning for the same devices (with same name but different ids)
                                                 std::to_string( unique_device_id_to_opencl_device_id.at( unique_device_id )   ) + std::string( " " ) +
                                                 _sps.get_sps_as_string( " " );
        
        // execute tuning script
        auto cmd = std::string( path_to_tuning_script+kernel_name_with_hash + std::string( " " ) + tuning_script_cmd_line_parameters ).c_str();
        std::system( cmd );
      }
      
      // load binary kernel for sps if kernel exists
      if( common::file_exists( path_to_auto_tuned_kernel + kernel_name_with_hash_and_sps_and_flags ) )
      {
        // load kernel binary form HD
        std::ifstream kernel_file( path_to_auto_tuned_kernel + kernel_name_with_hash_and_sps_and_flags, std::ios_base::in | std::ios_base::binary | std::ios_base::ate );

        // get the size of the file
        auto size = kernel_file.tellg();
        
        // allocate memory
        std::unique_ptr< char[] > binary( new char[ size ] );
        
        // set position in file to the beginning
        kernel_file.seekg( 0, std::ios::beg );
        
        // read the hole file
        kernel_file.read( binary.get(), size );
        
        // close it
        kernel_file.close();
        
        // create program from binary
        program = cl::Program( context,
                               {1, device},
                               cl::Program::Binaries( 1, std::make_pair(binary.get(), size) ) // Note: generates std::vector with 1 element that is initialized with std::make_pair(...)
                             );
                
        // load thread configuration from JSON if exists
        auto path_to_json = path_to_auto_tuned_kernel + kernel_name_with_hash_and_sps_and_flags + std::string( ".json" );
        if( common::file_exists( path_to_json ) )
        {
          // open file
          std::ifstream input_file( path_to_json );
          
          // load json
          json json_file;
          input_file >> json_file;
          
          // set thread configuration
          auto& thread_configurations = _unique_device_id_to_thread_configurations[ unique_device_id ];
          auto& global_size           = thread_configurations[ 0 ];
          auto& local_size            = thread_configurations[ 1 ];
          
          global_size[ 0 ] = json_file[ "global_size" ][ "0" ];
          global_size[ 1 ] = json_file[ "global_size" ][ "1" ];
          global_size[ 2 ] = json_file[ "global_size" ][ "2" ];

          local_size[ 0 ] = json_file[ "local_size" ][ "0" ];
          local_size[ 1 ] = json_file[ "local_size" ][ "1" ];
          local_size[ 2 ] = json_file[ "local_size" ][ "2" ];
        }
      }
      
      // generate un-tuned kernel
      else if ( _sps.empty() )
      {
        #ifdef CACHE_KERNELS
          auto path_to_kernel = PATH_TO_OCAL_KERNEL_CACHE + std::string( "/OCL/" ) + vendor_name_as_hash + std::string( "/" ) + device_name_as_hash + std::string( "/" ) + kernel_hash + std::string("__") + flags_hash;
        
          if( common::file_exists( path_to_kernel ) )
          {
            // load kernel binary form HD
            std::ifstream kernel_file( path_to_kernel, std::ios_base::in | std::ios_base::binary | std::ios_base::ate );

            // get the size of the file
            auto size = kernel_file.tellg();
            
            // allocate memory
            std::unique_ptr< char[] > binary( new char[ size ] );
            
            // set position in file to the beginning
            kernel_file.seekg( 0, std::ios::beg );
            
            // read the whole file
            kernel_file.read( binary.get(), size );
            
            // close it
            kernel_file.close();
            
            // create program from binary
            program = cl::Program( context,
                                   {1, device},                                                   // Note: generates std::vector with 1 element that is initialized with std::make_pair(...)
                                   cl::Program::Binaries( 1, std::make_pair(binary.get(), size) )
                                 );
          }
          else
          {
            // create program from source
            program = cl::Program( context, cl::Program::Sources( 1, std::make_pair(_kernel_as_string.c_str(), _kernel_as_string.length() ) ) );

            // build program
            try
            {
              program.build( std::vector<cl::Device>( 1, device ), kernel_flags_accumulated.c_str() );
            }
            catch( cl::Error& err )
            {
              if( err.err() == CL_BUILD_PROGRAM_FAILURE )
              {
                auto buildLog = program.getBuildInfo<CL_PROGRAM_BUILD_LOG>( device );
                std::cout << std::endl << "Build failed! Log:" << std::endl << buildLog << std::endl;
              }

              throw;
            }

            // get binary sizes
            auto binary_sizes = program.getInfo< CL_PROGRAM_BINARY_SIZES >();

            // get device id
            int device_id = 0;
            for( int i = 0 ; i < binary_sizes.size() ; ++i )
              if( binary_sizes[ i ] != 0 )
               device_id = i;
            
            // get binaries
            auto binaries = program.getInfo< CL_PROGRAM_BINARIES >();

            // save binary
            std::system( std::string( "mkdir -p " + PATH_TO_OCAL_KERNEL_CACHE + "/OCL/" + vendor_name_as_hash + std::string( "/" ) + device_name_as_hash + std::string( "/" ) ).c_str() );
            std::ofstream out_file( path_to_kernel, std::ios_base::out | std::ios_base::trunc | std::ios_base::binary );
            out_file.write( binaries[ device_id ], binary_sizes[ device_id ] );
          }
          
        #else
          
          // create program from source
          program = cl::Program( context, cl::Program::Sources( 1, std::make_pair(_kernel_as_string.c_str(), _kernel_as_string.length() ) ) );
          
        #endif
      }
      
      // error case
      else
      {
        std::wcerr << "Neither auto-tuned kernel nor tuning script exists for desired program \"" << kernel_name.c_str() << "\"" << std::endl;
        
        assert( false );
        exit( EXIT_FAILURE );
      }
            
      // build program
      try
      {
        program.build( std::vector<cl::Device>( 1, device ), kernel_flags_accumulated.c_str() );  // TODO: was passiert mit flags, wenn binary geladen wurde?
      }
      catch( cl::Error& err )
      {
        if( err.err() == CL_BUILD_PROGRAM_FAILURE )
        {
          auto buildLog = program.getBuildInfo<CL_PROGRAM_BUILD_LOG>( device );
          std::cout << std::endl << "Build failed! Log:" << std::endl << buildLog << std::endl;
        }

        throw;
      }
      
      // extract kernel
      try
      {
        auto kernel = cl::Kernel( program, kernel_name.c_str(), &error );
        
        _unique_device_id_to_kernel[ unique_device_id ] = kernel;
      }
      catch( cl::Error& err )
      {
        if( err.err() == CL_BUILD_PROGRAM_FAILURE )
        {
          auto buildLog = program.getBuildInfo<CL_PROGRAM_BUILD_LOG>( device );
          std::cout << std::endl << "Build failed! Log:" << std::endl << buildLog << std::endl;
        }

        throw;
      }
    }
};


class kernel_representation
{};


// helper classes for differentiation between passing kernel 1) as path to source and 2) kernel's source as string
class path : public abstract::path, public kernel_representation
{
  public:
    // generic ctor
    template< typename... Ts >
    path( Ts... args )
      : abstract::path( args... ) //, kernel( *this )
    {}
};


class source : public abstract::source, public kernel_representation
{
  public:
    // generic ctor
    template< typename... Ts >
    source( Ts... args )
      : abstract::source( args... ) //, kernel( *this )
    {}
};


} // namespace "ocl"


#ifndef OCAL_CUDA
namespace cuda
{

// used for abstracting path and source
class kernel_representation
{};


// helper classes for differentiation between passing kernel 1) as path to source and 2) kernel's source as string
class path : public abstract::path, public kernel_representation
{
  public:
    // generic ctor
    template< typename... Ts >
    path( Ts... args )
      : abstract::path( args... )
    {}  
};


class source : public abstract::source, public kernel_representation
{
  public:
    // generic ctor
    template< typename... Ts >
    source( Ts... args )
      : abstract::source( args... )
    {}
};

} // namespace "cuda"
#endif


} // namespace "core"

} // namespace "ocal"


#endif /* ocl_kernel_hpp */
