//
// Created by Richard Schulze on 15.06.2019.
//

#ifndef OCAL_WRAPPER_HPP
#define OCAL_WRAPPER_HPP

#include <cstddef>
#include <tuple>
#include <chrono>

// TODO event list grows for each evaluation
#if defined(OCAL_OCL_WRAPPER) && defined(OCAL_CUDA_WRAPPER)
#include "ocal.hpp"
#elif defined(OCAL_OCL_WRAPPER)
#include "ocal_ocl.hpp"
#elif defined(OCAL_CUDA_WRAPPER)
#include "ocal_cuda.hpp"
#endif

#include "helper.hpp"
#include "tp_value.hpp"

#include <fstream>
#include <iostream>
#include <memory>
#include <utility>

//enables lazy evaluation
namespace
{
template< typename T >
class wrapper
{
    // save l-values as references and r-values by value
    using T_ref_free  = typename std::remove_reference<T>::type;
    using T_save_type = typename std::conditional_t< std::is_rvalue_reference<T>::value, T_ref_free, T_ref_free& >;

public:
    wrapper( T value )
            : _value( value )
    {}


    T_ref_free get_value()
    {
        return _value;
    }


private:
    T_save_type _value;
};
}

namespace atf {
namespace cf {

using nd_range_t              = std::array<size_t,3>;
using thread_configurations_t = std::map< ::atf::configuration, std::array<nd_range_t,2> >;

template< typename T_0, typename T_1 = size_t, typename T_2 = size_t >
auto GS( T_0&& gs_0, T_1&& gs_1 = 1, T_2&& gs_2 = 1 )
{
    return std::make_tuple( wrapper<T_0&&>( std::forward<T_0>(gs_0) ), wrapper<T_1&&>( std::forward<T_1>(gs_1) ), wrapper<T_2&&>( std::forward<T_2>(gs_2) ) );
}


template< typename T_0, typename T_1 = size_t, typename T_2 = size_t >
auto LS( T_0&& ls_0, T_1&& ls_1 = 1, T_2&& ls_2 = 1 )
{
    return std::make_tuple( wrapper<T_0&&>( std::forward<T_0>(ls_0) ), wrapper<T_1&&>( std::forward<T_1>(ls_1) ), wrapper<T_2&&>( std::forward<T_2>(ls_2) ) );
}


template <typename OCAL_TYPE>
class kernel_info
{
public:
    kernel_info(const OCAL_TYPE &ocal_source, const std::string &kernel_name = "", const std::vector<std::string> &flags = {})
    : _ocal_source(ocal_source), _kernel_name(kernel_name), _flags(flags) {}

    OCAL_TYPE &ocal_source() {
        return _ocal_source;
    }

    const OCAL_TYPE &ocal_source() const {
        return _ocal_source;
    }

    const std::string &kernel_name() const {
        return _kernel_name;
    }

    const std::vector<std::string> &flags() const {
        return _flags;
    }
private:
    OCAL_TYPE _ocal_source;
    std::string _kernel_name;
    std::vector<std::string> _flags;
};

template<typename OCAL_TYPE>
class device_info
{
public:
    device_info( const int& platform_number, const int& device_number )
            : _platform_id(platform_number), _device_id(device_number)
    {
    }

    device_info( )
            : _platform_id(0), _device_id(0)
    {
    }

    // copy constructor
    device_info(const device_info<OCAL_TYPE> &other) {
        _platform_id = other._platform_id;
        _device_id = other._device_id;
        if (other._initialized)
            initialize();
    }

    size_t platform_id() const {
        return _platform_id;
    }


    size_t device_id() const {
        return _device_id;
    }

    OCAL_TYPE &device() {
        return *_device;
    }

    void initialize() {
        if (_initialized) return;
        initialize_helper();
        _initialized = true;
    }


private:
    bool _initialized = false;
    std::unique_ptr<OCAL_TYPE> _device;

    size_t _platform_id;
    size_t _device_id;

    void initialize_helper();
};
#ifdef OCAL_OCL_WRAPPER
template <>
void device_info<OCL>::initialize_helper() {
    _device = std::make_unique<OCL>(_platform_id, _device_id);
}
#endif
#ifdef OCAL_CUDA_WRAPPER
template <>
void device_info<CUDA>::initialize_helper() {
    _device = std::make_unique<CUDA>(_device_id);
}
#endif

template <typename T>
void wait_for_event(const T &event, unsigned long long timeout = 0) {
    if (timeout > 0) {
        auto timeout_lambda = [event, timeout]() {
            ns_sleep(timeout);

            if (event.status() == T::RUNNING) {
                // kernel still running
                std::cerr << "kernel took more than " << timeout << " ns, killing process" << std::endl;
                exit(1);
            }
        };
        std::thread timeout_thread(timeout_lambda);

        event.wait();
        timeout_thread.detach();
    } else {
        event.wait();
    }
}

enum TIMEOUT_TYPE {
    FACTOR, ABSOLUTE
};
typedef union {
    float factor;
    unsigned long long absolute;
} timeout_value;
typedef struct {
    TIMEOUT_TYPE type;
    timeout_value value;
} timeout;
enum CHECK_INTERVAL {
    CHECK_NONE, CHECK_FINAL, CHECK_ALL
};

template<typename OCAL_DEVICE_TYPE, typename OCAL_KERNEL_TYPE,
        typename GS_0, typename GS_1, typename GS_2,
        typename LS_0, typename LS_1, typename LS_2,
        typename... INPUT_Ts>
class ocal_wrapper_class {
    unsigned long long best_runtime;
public:
    ocal_wrapper_class(const device_info<OCAL_DEVICE_TYPE> &device,

                       const kernel_info<OCAL_KERNEL_TYPE> &kernel,
                       const std::tuple<GS_0, GS_1, GS_2> &global_size,
                       const std::tuple<LS_0, LS_1, LS_2> &local_size,
                       const std::tuple<INPUT_Ts...> &kernel_inputs,

                       const size_t warm_ups,
                       const size_t num_evaluations,

                       const timeout warm_up_timeout,
                       const timeout evaluation_timeout,

                       const bool silent) :
            _device_info(device), _kernel_inputs(kernel_inputs), _kernel(kernel),
            _global_size_pattern(global_size), _local_size_pattern(local_size), _num_evaluations(num_evaluations),
            _warm_ups(warm_ups), _silent(silent), _gold_ptr(), _delete_gold_ptr(false),
            _warm_up_timeout(warm_up_timeout), _evaluation_timeout(evaluation_timeout) {
        best_runtime = std::numeric_limits<unsigned long long>::max() / _num_evaluations;
    }

    ~ocal_wrapper_class() {
        if (_delete_gold_ptr && !_gold_ptr.empty()) {
            free_gold_pointers(_kernel_inputs);
        }
    }

    void warm_ups(size_t warm_ups) {
        _warm_ups = warm_ups;
    }

    void evaluations(size_t evaluations) {
        _num_evaluations = evaluations;
    }

    unsigned long long operator()(configuration &configuration,
                                  unsigned long long *compile_time = nullptr, int *error_code = nullptr) {
        // update tp values
        for (auto &tp : configuration) {
            auto tp_value = tp.second;
            tp_value.update_tp();
//            if (!_silent && tp.first.find("NOT_USED") == std::string::npos)
//                std::cout << tp.first << "\t" << tp.second.value().size_t_val()<< std::endl;
        }

        // calculate timeouts
        unsigned long warm_up_timeout = 0;
        if (_warm_up_timeout.type == ABSOLUTE) {
            warm_up_timeout = _warm_up_timeout.value.absolute;
        } else if (best_runtime != std::numeric_limits<unsigned long long>::max() / _num_evaluations
                   && best_runtime <= std::numeric_limits<unsigned long long>::max() / _warm_up_timeout.value.factor) {
            warm_up_timeout = static_cast<unsigned long>(_warm_up_timeout.value.factor * best_runtime);
        }
        unsigned long evaluation_timeout = 0;
        if (_evaluation_timeout.type == ABSOLUTE) {
            evaluation_timeout = _evaluation_timeout.value.absolute;
        } else if (best_runtime != std::numeric_limits<unsigned long long>::max() / _num_evaluations
                   && best_runtime <= std::numeric_limits<unsigned long long>::max() / _evaluation_timeout.value.factor) {
            evaluation_timeout = static_cast<unsigned long>(_evaluation_timeout.value.factor * best_runtime);
        }

        // get global and local size
        size_t gs_0 = std::get<0>(_global_size_pattern).get_value();
        size_t gs_1 = std::get<1>(_global_size_pattern).get_value();
        size_t gs_2 = std::get<2>(_global_size_pattern).get_value();

        size_t ls_0 = std::get<0>(_local_size_pattern).get_value();
        size_t ls_1 = std::get<1>(_local_size_pattern).get_value();
        size_t ls_2 = std::get<2>(_local_size_pattern).get_value();

//        std::cout << "gs: " << gs_0 << ", " << gs_1 << ", " << gs_2 << std::endl;
//        std::cout << "ls: " << ls_0 << ", " << ls_1 << ", " << ls_2 << std::endl;

        // create flags
        std::vector<std::string> flags = _kernel.flags();
        for (const auto &tp : configuration) {
            flags.emplace_back(" -D ");
            flags.back().append(tp.second.name());
            flags.back().append("=");
            flags.back().append(tp.second.value());
        }

        unsigned long long runtime = 0;

        // compile kernel
        ocal::kernel kernel(_kernel.ocal_source(), _kernel.kernel_name(), flags);
        try {
            auto start = std::chrono::system_clock::now();
            _device_info.device()(kernel);
            auto end = std::chrono::system_clock::now();

            auto runtime_in_sec = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
            std::cout << "compilation time for kernel: " << runtime_in_sec << "ms" << std::endl;
            if (compile_time != nullptr) *compile_time += runtime_in_sec;
        }
#ifdef OCAL_OCL_WRAPPER
        catch (const cl::Error &err) {
            for (const auto &flag : flags) {
                std::cout << flag << std::endl;
            }
            std::cout << "OpenCL error code: " << err.err() << std::endl;
            if (error_code != nullptr) *error_code = err.err();
            throw;
        }
#endif
#ifdef OCAL_CUDA_WRAPPER
        catch (const std::runtime_error &err) {
            for (const auto &flag : flags) {
                std::cout << flag << std::endl;
            }
            std::cout << "CUDA error code: " << err.what() << std::endl;
            if (error_code != nullptr) *error_code = std::atoi(err.what());
            throw;
        }
#endif

        // set global and local size
        _device_info.device()(
                ocl::nd_range(gs_0, gs_1, gs_2),
                ocl::nd_range(ls_0, ls_1, ls_2)
        );

        // warm ups
        if (_warm_ups > 0) {
            // run first warm up with timeout
            std::apply(_device_info.device(), _kernel_inputs);
            auto timeout_event = _device_info.device().last_event();
            wait_for_event(timeout_event, warm_up_timeout);
        }
        for (size_t i = 1; i < _warm_ups; ++i) {
            std::apply(_device_info.device(), _kernel_inputs);
            _device_info.device().last_event().wait();
        }

        // kernel launch with profiling
        unsigned long long kernel_runtime_in_ns = 0;
        bool skip = false;
        if (_num_evaluations > 0) {
            // run first evaluation with timeout
            std::apply(_device_info.device(), _kernel_inputs);
            auto timeout_event = _device_info.device().last_event();
            wait_for_event(timeout_event, evaluation_timeout);

            auto tmp_runtime = _device_info.device().last_runtime();
            kernel_runtime_in_ns += tmp_runtime;

            if (_num_evaluations > 1 && kernel_runtime_in_ns > best_runtime * _num_evaluations) {
                if (!_gold_ptr.empty() && _check_interval == CHECK_ALL) check_results_helper(1, configuration);
                runtime = kernel_runtime_in_ns;
                skip = true;
            }
        }
        if (!skip) {
            for (size_t i = 1; i < _num_evaluations; ++i) {
                std::apply(_device_info.device(), _kernel_inputs);

                auto tmp_runtime = _device_info.device().last_runtime();
                kernel_runtime_in_ns += tmp_runtime;

                if (_num_evaluations > 1 && kernel_runtime_in_ns > best_runtime * _num_evaluations) {
                    if (!_gold_ptr.empty() && _check_interval == CHECK_ALL) check_results_helper(1, configuration);
                    runtime = kernel_runtime_in_ns / (i + 1);
                    skip = true;
                    break;
                }
            }
        }
        if (!skip) kernel_runtime_in_ns /= _num_evaluations;
        if (!skip && !_gold_ptr.empty() && (_check_interval == CHECK_ALL || _check_interval == CHECK_FINAL)) check_results_helper(1, configuration);
        runtime = kernel_runtime_in_ns;

        if (runtime < best_runtime) {
            best_runtime = runtime;
        }
        return runtime;
    }

    template<typename... Ps>
    void check_results(CHECK_INTERVAL check_interval, const Ps*... data) {
        _gold_ptr = std::vector<void*>({((void*)data)...});
        _delete_gold_ptr = false;
        _check_interval = check_interval;
    }

    void check_results(CHECK_INTERVAL check_interval, const std::vector<std::string> &gold_files) {
        check_for_gold_file(gold_files, _kernel_inputs);
        _delete_gold_ptr = true;
        _check_interval = check_interval;
    }

private:
    device_info<OCAL_DEVICE_TYPE> _device_info;

    // Kernel
    kernel_info<OCAL_KERNEL_TYPE> _kernel;
    std::tuple<GS_0, GS_1, GS_2>  _global_size_pattern;
    std::tuple<LS_0, LS_1, LS_2>  _local_size_pattern;
    std::tuple<INPUT_Ts...>       _kernel_inputs;

    size_t                        _num_evaluations;
    size_t                        _warm_ups;

    const bool                    _silent;
    const timeout                 _warm_up_timeout;
    const timeout                 _evaluation_timeout;

    std::vector<void *>           _gold_ptr;
    bool                          _delete_gold_ptr;
    CHECK_INTERVAL                _check_interval;

    // helper for parsing gold files
    template <typename... Ts>
    void check_for_gold_file(std::vector<std::string> gold_files, const std::tuple<Ts...> &inputs) {
        check_for_gold_file(gold_files, inputs, std::make_index_sequence<sizeof...(Ts)>());
    }
    template <typename... Ts, size_t... Is>
    void check_for_gold_file(std::vector<std::string> &gold_files, const std::tuple<Ts...> &inputs, std::index_sequence<Is...>) {
        check_for_gold_file(gold_files, std::get<Is>(inputs)...);
    }
    template <typename T, typename... Ts>
    void check_for_gold_file(std::vector<std::string> &gold_files, const T &input, const Ts&... inputs) {
        check_for_gold_file(gold_files, inputs...);
    }
    template <typename T, typename... Ts>
    void check_for_gold_file(std::vector<std::string> &gold_files, const ocal::common::read_class<ocal::buffer<T>> &buffer, const Ts&... inputs) {
        if (!gold_files.front().empty()) {
            std::ifstream is(gold_files.front());
            std::istream_iterator<T> start(is), end;
            std::vector<T> expected_result(start, end);
            if (expected_result.size() != buffer.get().size()) {
                throw std::runtime_error("gold file \"" + gold_files.front() + "\" contains insufficient number of results");
            }
            T *gold_data = new T[expected_result.size()];
            for (int i = 0; i < expected_result.size(); ++i) {
                gold_data[i] = expected_result[i];
            }
            _gold_ptr.push_back(gold_data);
        } else {
            _gold_ptr.push_back(nullptr);
        }
        gold_files.erase(gold_files.begin());
        if (!gold_files.empty())
            check_for_gold_file(gold_files, inputs...);
    }
    template <typename T, typename... Ts>
    void check_for_gold_file(std::vector<std::string> &gold_files, const ocal::common::write_class<ocal::buffer<T>> &buffer, const Ts&... inputs) {
        if (!gold_files.front().empty()) {
            std::ifstream is(gold_files.front());
            std::istream_iterator<T> start(is), end;
            std::vector<T> expected_result(start, end);
            if (expected_result.size() != buffer.get().size()) {
                throw std::runtime_error("gold file \"" + gold_files.front() + "\" contains insufficient number of results");
            }
            T *gold_data = new T[expected_result.size()];
            for (int i = 0; i < expected_result.size(); ++i) {
                gold_data[i] = expected_result[i];
            }
            _gold_ptr.push_back(gold_data);
        } else {
            _gold_ptr.push_back(nullptr);
        }
        gold_files.erase(gold_files.begin());
        if (!gold_files.empty())
            check_for_gold_file(gold_files, inputs...);
    }
    template <typename T, typename... Ts>
    void check_for_gold_file(std::vector<std::string> &gold_files, const ocal::common::read_write_class<ocal::buffer<T>> &buffer, const Ts&... inputs) {
        if (!gold_files.front().empty()) {
            std::ifstream is(gold_files.front());
            std::istream_iterator<T> start(is), end;
            std::vector<T> expected_result(start, end);
            if (expected_result.size() != buffer.get().size()) {
                throw std::runtime_error("gold file \"" + gold_files.front() + "\" contains insufficient number of results");
            }
            T *gold_data = new T[expected_result.size()];
            for (int i = 0; i < expected_result.size(); ++i) {
                gold_data[i] = expected_result[i];
            }
            _gold_ptr.push_back(gold_data);
        } else {
            _gold_ptr.push_back(nullptr);
        }
        gold_files.erase(gold_files.begin());
        if (!gold_files.empty())
            check_for_gold_file(gold_files, inputs...);
    }
    void check_for_gold_file(std::vector<std::string> &gold_files) {
    }

    // helper for freeing gold pointers
    template <typename... Ts>
    void free_gold_pointers(const std::tuple<Ts...> &inputs) {
        free_gold_pointers(inputs, std::make_index_sequence<sizeof...(Ts)>());
    }
    template <typename... Ts, size_t... Is>
    void free_gold_pointers(const std::tuple<Ts...> &inputs, std::index_sequence<Is...>) {
        free_gold_pointers(0, std::get<Is>(inputs)...);
    }
    template <typename T, typename... Ts>
    void free_gold_pointers(int nr, const T &input, const Ts&... inputs) {
        free_gold_pointers(nr, inputs...);
    }
    template <typename T, typename... Ts>
    void free_gold_pointers(int nr, const ocal::common::read_class<ocal::buffer<T>> &buffer, const Ts&... inputs) {
        delete[] (T*) _gold_ptr[nr];
        if (nr < _gold_ptr.size() - 1)
            free_gold_pointers(nr + 1, inputs...);
    }
    template <typename T, typename... Ts>
    void free_gold_pointers(int nr, const ocal::common::write_class<ocal::buffer<T>> &buffer, const Ts&... inputs) {
        delete[] (T*) _gold_ptr[nr];
        if (nr < _gold_ptr.size() - 1)
            free_gold_pointers(nr + 1, inputs...);
    }
    template <typename T, typename... Ts>
    void free_gold_pointers(int nr, const ocal::common::read_write_class<ocal::buffer<T>> &buffer, const Ts&... inputs) {
        delete[] (T*) _gold_ptr[nr];
        if (nr < _gold_ptr.size() - 1)
            free_gold_pointers(nr + 1, inputs...);
    }
    void free_gold_pointers(int nr) {
    }

    // helper for checking result buffers against gold data
    template <typename... Ts>
    std::tuple<bool, size_t, size_t> check_buffers(std::tuple<Ts...> &inputs) {
        return check_buffers(inputs, std::make_index_sequence<sizeof...(Ts)>());
    }
    template <typename... Ts, size_t... Is>
    std::tuple<bool, size_t, size_t> check_buffers(std::tuple<Ts...> &inputs, std::index_sequence<Is...>) {
        return check_buffers(0, std::get<Is>(inputs)...);
    }
    template <typename T, typename... Ts>
    std::tuple<bool, size_t, size_t> check_buffers(int nr, T &input, Ts&... inputs) {
        return check_buffers(nr, inputs...);
    }
    template <typename T, typename... Ts>
    std::tuple<bool, size_t, size_t> check_buffers(int nr, ocal::common::read_class<ocal::buffer<T>> &buffer, Ts&... inputs) {
        T *gold_data = (T*) _gold_ptr[nr];
        if (gold_data != nullptr) {
            for (int i = 0; i < buffer.get().size(); ++i) {
                if (buffer.get()[i] != gold_data[i]) {
                    return std::make_tuple(true, nr, i);
                }
            }
        }

        if (nr < _gold_ptr.size() - 1)
            return check_buffers(nr + 1, inputs...);
        else
            return std::make_tuple(false, 0, 0);
    }
    template <typename T, typename... Ts>
    std::tuple<bool, size_t, size_t> check_buffers(int nr, ocal::common::write_class<ocal::buffer<T>> &buffer, Ts&... inputs) {
        T *gold_data = (T*) _gold_ptr[nr];
        if (gold_data != nullptr) {
            for (int i = 0; i < buffer.get().size(); ++i) {
                if (buffer.get()[i] != gold_data[i]) {
                    return std::make_tuple(true, nr, i);
                }
            }
        }

        if (nr < _gold_ptr.size() - 1)
            return check_buffers(nr + 1, inputs...);
        else
            return std::make_tuple(false, 0, 0);
    }
    template <typename T, typename... Ts>
    std::tuple<bool, size_t, size_t> check_buffers(int nr, ocal::common::read_write_class<ocal::buffer<T>> &buffer, Ts&... inputs) {
        T *gold_data = (T*) _gold_ptr[nr];
        if (gold_data != nullptr) {
            for (int i = 0; i < buffer.get().size(); ++i) {
                if (buffer.get()[i] != gold_data[i]) {
                    return std::make_tuple(true, nr, i);
                }
            }
        }

        if (nr < _gold_ptr.size() - 1)
            return check_buffers(nr + 1, inputs...);
        else
            return std::make_tuple(false, 0, 0);
    }
    std::tuple<bool, size_t, size_t> check_buffers(int nr) {
        return std::make_tuple(false, 0, 0);
    }

    // helper for writing erroneous buffer to file
    template <typename... Ts>
    void write_buffer_to_file(int buffer_nr, std::ofstream &stream, std::tuple<Ts...> &inputs) {
        write_buffer_to_file(buffer_nr, stream, inputs, std::make_index_sequence<sizeof...(Ts)>());
    }
    template <typename... Ts, size_t... Is>
    void write_buffer_to_file(int buffer_nr, std::ofstream &stream, std::tuple<Ts...> &inputs, std::index_sequence<Is...>) {
        write_buffer_to_file(buffer_nr, stream, 0, std::get<Is>(inputs)...);
    }
    template <typename T, typename... Ts>
    void write_buffer_to_file(int buffer_nr, std::ofstream &stream, int nr, T &input, Ts&... inputs) {
        write_buffer_to_file(buffer_nr, stream, nr, inputs...);
    }
    template <typename T, typename... Ts>
    void write_buffer_to_file(int buffer_nr, std::ofstream &stream, int nr, ocal::common::read_class<ocal::buffer<T>> &buffer, Ts&... inputs) {
        if (buffer_nr == nr) {
            stream << "result buffer: " << std::endl;
            for (int i = 0; i < buffer.get().size(); ++i) {
                stream << buffer.get()[i] << "\t";
            }
            stream << std::endl << "gold buffer: " << std::endl;
            T *gold_data = (T*) _gold_ptr[nr];
            for (int i = 0; i < buffer.get().size(); ++i) {
                stream << gold_data[i] << "\t";
            }
            stream << std::endl;
        } else {
            write_buffer_to_file(buffer_nr, stream, nr + 1, inputs...);
        }
    }
    template <typename T, typename... Ts>
    void write_buffer_to_file(int buffer_nr, std::ofstream &stream, int nr, ocal::common::write_class<ocal::buffer<T>> &buffer, Ts&... inputs) {
        if (buffer_nr == nr) {
            stream << "result buffer: " << std::endl;
            for (int i = 0; i < buffer.get().size(); ++i) {
                stream << buffer.get()[i] << "\t";
            }
            stream << std::endl << "gold buffer: " << std::endl;
            T *gold_data = (T*) _gold_ptr[nr];
            for (int i = 0; i < buffer.get().size(); ++i) {
                stream << gold_data[i] << "\t";
            }
            stream << std::endl;
        } else {
            write_buffer_to_file(buffer_nr, stream, nr + 1, inputs...);
        }
    }
    template <typename T, typename... Ts>
    void write_buffer_to_file(int buffer_nr, std::ofstream &stream, int nr, ocal::common::read_write_class<ocal::buffer<T>> &buffer, Ts&... inputs) {
        if (buffer_nr == nr) {
            stream << "result buffer: " << std::endl;
            for (int i = 0; i < buffer.get().size(); ++i) {
                stream << buffer.get()[i] << "\t";
            }
            stream << std::endl << "gold buffer: " << std::endl;
            T *gold_data = (T*) _gold_ptr[nr];
            for (int i = 0; i < buffer.get().size(); ++i) {
                stream << gold_data[i] << "\t";
            }
            stream << std::endl;
        } else {
            write_buffer_to_file(buffer_nr, stream, nr + 1, inputs...);
        }
    }
    void write_buffer_to_file(int buffer_nr, std::ofstream &stream, int nr) {
    }

    void check_results_helper(int kernel, configuration &config) {
        auto check_results = check_buffers(_kernel_inputs);

        if (std::get<0>(check_results)) {
            // std::cout << "result is incorrect" << std::endl;
            std::ofstream log_file;
            log_file.open("errors.txt", std::ofstream::out | std::ofstream::trunc);
            log_file << "error in buffer " << std::get<1>(check_results) << " at index " << std::get<2>(check_results) << std::endl;
            log_file << "configuration: " << std::endl;
            for (const auto &val : config) {
                log_file << "#define " << val.first << " " << val.second.value() << std::endl;
            }
            write_buffer_to_file(std::get<1>(check_results), log_file, _kernel_inputs);
            log_file << std::endl;
            log_file.close();
            exit(EXIT_FAILURE);
        } else {
            // std::cout << "result is correct" << std::endl;
        }
    }
};


template<typename OCAL_DEVICE_TYPE, typename OCAL_KERNEL_TYPE,
        typename GS_0, typename GS_1, typename GS_2,
        typename LS_0, typename LS_1, typename LS_2,
        typename... INPUT_Ts>
auto ocal(const device_info<OCAL_DEVICE_TYPE>& device,

          const kernel_info<OCAL_KERNEL_TYPE>& kernel,
          const std::tuple<GS_0, GS_1, GS_2>&  global_size,
          const std::tuple<LS_0, LS_1, LS_2>&  local_size,
          const std::tuple<INPUT_Ts...>&       kernel_inputs,

          const size_t                         warm_ups,
          const size_t                         num_evaluations,

          const timeout                        warm_up_timeout = {ABSOLUTE, {.absolute = 0}},
          const timeout                        evaluation_timeout = {ABSOLUTE, {.absolute = 0}},

          const bool                           silent = false) {
    return ocal_wrapper_class<OCAL_DEVICE_TYPE, OCAL_KERNEL_TYPE, GS_0, GS_1, GS_2, LS_0, LS_1, LS_2, INPUT_Ts...>(
            device, kernel, global_size, local_size, kernel_inputs, warm_ups, num_evaluations,
            warm_up_timeout, evaluation_timeout, silent
    );
}

}
}

#endif //OCAL_WRAPPER_HPP
