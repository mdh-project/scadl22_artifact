//
//  ocal_buffer.hpp
//  ocal_ocal
//
//  Created by Ari Rasch on 20.09.17.
//  Copyright © 2017 Ari Rasch. All rights reserved.
//

#ifndef ocal_buffer_hpp
#define ocal_buffer_hpp


namespace ocal
{


// buffer
template< typename T >
using buffer = core::ocl::buffer< T >;
//template< typename T>
//class buffer : public core::ocl::buffer< T >
//{
//  public:
//    template< typename... Ts >
//    buffer( const Ts&... args )
//      : core::ocl::buffer< T >( args... )
//    {}
//
//    // delete functions that serve implementation purposes
//    buffer( size_t size, T* content               )                          = delete;
////    void set_host_memory_ptr( std::shared_ptr<T> new_content ) = delete;
////    T*   get_host_memory_ptr()                                 = delete;
//
//};



// host buffer
template< typename T >
using host_buffer = core::ocl::host_buffer< T >;
//template< typename T >
//class host_buffer : public core::ocl::host_buffer< T >
//{
//  public:
//    template< typename... Ts >
//    host_buffer( const Ts&... args )
//      : core::ocl::host_buffer< T >( args... )
//    {}
//
//    // delete functions that serve implementation purposes
////    void set_host_memory_ptr( std::shared_ptr<T> new_content ) = delete;
////    T*   get_host_memory_ptr()                                 = delete;
//};


// unified buffer
template< typename T >
using unified_buffer = core::ocl::unified_buffer< T >;

//template< typename T >
//class unified_buffer : public core::ocl::unified_buffer< T >
//{
//  public:
//    template< typename... Ts >
//    unified_buffer( const Ts&... args )
//      : core::ocl::unified_buffer< T >( args... )
//    {}
//
//    // delete functions that serve implementation purposes
////    void set_host_memory_ptr( std::shared_ptr<T> new_content ) = delete;
////    T*   get_host_memory_ptr()                                 = delete;
//};


// local buffer
template< typename T >
using local_buffer = core::ocl::local_buffer< T >;


} // namespace "ocal"


#endif /* ocal_buffer_hpp */
