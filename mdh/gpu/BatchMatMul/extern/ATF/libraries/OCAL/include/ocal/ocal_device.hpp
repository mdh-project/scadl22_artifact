//
//  ocal_device.hpp
//  ocal_ocal
//
//  Created by Ari Rasch on 20.09.17.
//  Copyright © 2017 Ari Rasch. All rights reserved.
//

#ifndef ocal_device_hpp
#define ocal_device_hpp


namespace ocal
{


// possible DEV_TYPEs
using OCL  = core::ocl::device;
using CUDA = core::cuda::device;

// TODO: reicht einfach erben von DEV_TYPE aus anstatt Implementierung unten?
template< typename DEV_TYPE > // Note: DEV_TYPE is either ocl::device or cuda::device
class device
{
  public:
  
    // ----------------------------------------------------------
    //   ctor: device initialization
    // ----------------------------------------------------------
  
  
    template< typename... Ts >
    device( const Ts&... args )
      : _device( args... )
    {}
  
  
  
    // ----------------------------------------------------------
    //   kernel
    // ----------------------------------------------------------
  
//    template< typename T, typename... Ts, typename = std::enable_if_t< std::is_same< core::ocl::kernel,                    typename ::ocal::common::remove_cv_and_references< T >::type >::value ||
//                                                                       std::is_same< core::cuda::kernel,                   typename ::ocal::common::remove_cv_and_references< T >::type >::value ||
//                                                                       std::is_base_of< core::ocl::kernel_representation,  typename ::ocal::common::remove_cv_and_references< T >::type >::value ||
//                                                                       std::is_base_of< core::cuda::kernel_representation, typename ::ocal::common::remove_cv_and_references< T >::type >::value
//                                                                     > >
//    auto& operator()( T&& kernel, Ts&&... args )
//    {
//      _device( std::forward< T >( kernel ), std::forward< Ts >( args )... );
//
//      return *this;
//    }

    // initialization by "ocal::kernel"
//    template< typename T, typename DEV_TYPE_hlpr = DEV_TYPE, typename std::enable_if_t< std::is_same< DEV_TYPE_hlpr, OCL >::value >* = nullptr >
//    auto& operator()( T&& kernel )
//    {
//      _device( kernel.get_ocl_kernel() );
//
//      return *this;
//    }
//
//    template< typename T, typename DEV_TYPE_hlpr = DEV_TYPE, typename std::enable_if_t< std::is_same< DEV_TYPE_hlpr, CUDA >::value >* = nullptr >
//    auto& operator()( T&& kernel )
//    {
//      _device( kernel.get_cuda_kernel() );
//
//      return *this;
//    }



    // non-"ocal::kernel" initialization (uses "ocl::source"/"cuda::source")
    template< typename T, typename... Ts, typename = std::enable_if_t< std::is_base_of< core::ocl::kernel_representation,  typename ::ocal::common::remove_cv_and_references< T >::type >::value ||
                                                                       std::is_base_of< core::cuda::kernel_representation, typename ::ocal::common::remove_cv_and_references< T >::type >::value
                                                                     > >
    auto& operator()( T&& kernel, Ts&&... args )
    {
      _device( std::forward< T >( kernel ), std::forward< Ts >( args )... );

      return *this;
    }




    // ----------------------------------------------------------
    //   thread configuration
    // ----------------------------------------------------------
  
    auto& operator()( core::ocl::nd_range gs, core::ocl::nd_range ls  )
    {
      _device( gs, ls );
    
      return *this;
    }

  
    auto& operator()( core::cuda::dim3 gs, core::cuda::dim3 bs  )
    {
      _device( gs, bs );

      return *this;
    }



    // ----------------------------------------------------------
    //   kernel inputs
    // ----------------------------------------------------------

    template< typename... Ts, size_t NUM_ARGS = sizeof...( Ts ), typename indices = std::make_index_sequence< NUM_ARGS > >
    auto& operator()( const Ts&... args )
    {
      auto prepared_args = this->prepare_kernel_arguments< NUM_ARGS >( args... );
      pass_to_device( prepared_args, indices{} );
      
      return *this;
    }
  
  
    template< typename T, size_t... I>
    auto pass_to_device( T& args_as_tuple, std::index_sequence<I...> )
    {
      _device( std::get<I>( args_as_tuple )... );
    }
  
  
    template< size_t NUM_ARGS, typename T, typename... Ts, typename = std::enable_if_t< std::is_fundamental<T>::value >, typename = std::enable_if_t<( NUM_ARGS > 0 )> >
    auto prepare_kernel_arguments( T val, const Ts&... args )
    {
      return prepare_kernel_arguments< NUM_ARGS - 1 >( args..., val );
    }
  

    // OCL
    template< size_t NUM_ARGS, typename buffer_t, typename... Ts, typename DEV_TYPE_hlpr = DEV_TYPE, typename std::enable_if_t< std::is_same< DEV_TYPE_hlpr, OCL >::value >* = nullptr, typename = std::enable_if_t<( NUM_ARGS > 0 )> >
    auto prepare_kernel_arguments( common::read_class< buffer_t > wrapped_buffer, const Ts&... args ) // Note: "buffer_t" is either "buffer", "host_buffer" or "unified_buffer"
    {
      // if this device is an ocl::device then cast "buffer" to ocl::buffer and otherwise to "cuda::buffer"
      return prepare_kernel_arguments< NUM_ARGS -1 >( args..., common::read( wrapped_buffer.get().get_ocl_ocal_buffer_with_read_access() ) );
    }


    template< size_t NUM_ARGS, typename buffer_t, typename... Ts, typename DEV_TYPE_hlpr = DEV_TYPE, typename std::enable_if_t< std::is_same< DEV_TYPE_hlpr, OCL >::value >* = nullptr, typename = std::enable_if_t<( NUM_ARGS > 0 )> >
    auto prepare_kernel_arguments( common::write_class< buffer_t > wrapped_buffer, const Ts&... args ) // Note: "buffer_t" is either "buffer", "host_buffer" or "unified_buffer"
    {
      // if this device is an ocl::device then cast "buffer" to ocl::buffer and otherwise to "cuda::buffer"
      return prepare_kernel_arguments< NUM_ARGS -1 >( args..., common::write( wrapped_buffer.get().get_ocl_ocal_buffer_with_write_access() ) );
    }


    template< size_t NUM_ARGS, typename buffer_t, typename... Ts, typename DEV_TYPE_hlpr = DEV_TYPE, typename std::enable_if_t< std::is_same< DEV_TYPE_hlpr, OCL >::value >* = nullptr, typename = std::enable_if_t<( NUM_ARGS > 0 )> >
    auto prepare_kernel_arguments( common::read_write_class< buffer_t > wrapped_buffer, const Ts&... args ) // Note: "buffer_t" is either "buffer", "host_buffer" or "unified_buffer"
    {
      // if this device is an ocl::device then cast "buffer" to ocl::buffer and otherwise to "cuda::buffer"
      return prepare_kernel_arguments< NUM_ARGS -1 >( args..., common::read_write( wrapped_buffer.get().get_ocl_ocal_buffer_with_read_write_access() ) );
    }


    // CUDA
    template< size_t NUM_ARGS, typename buffer_t, typename... Ts, typename DEV_TYPE_hlpr = DEV_TYPE, typename std::enable_if_t< std::is_same< DEV_TYPE_hlpr, CUDA >::value >* = nullptr, typename = std::enable_if_t<( NUM_ARGS > 0 )> >
    auto prepare_kernel_arguments( common::read_class< buffer_t > wrapped_buffer, const Ts&... args ) // Note: "buffer_t" is either "buffer", "host_buffer" or "unified_buffer"
    {
      // if this device is an ocl::device then cast "buffer" to ocl::buffer and otherwise to "cuda::buffer"
      return prepare_kernel_arguments< NUM_ARGS -1 >( args..., common::read( wrapped_buffer.get().get_cuda_ocal_buffer_with_read_access() ) );
    }


    template< size_t NUM_ARGS, typename buffer_t, typename... Ts, typename DEV_TYPE_hlpr = DEV_TYPE, typename std::enable_if_t< std::is_same< DEV_TYPE_hlpr, CUDA >::value >* = nullptr, typename = std::enable_if_t<( NUM_ARGS > 0 )> >
    auto prepare_kernel_arguments( common::write_class< buffer_t > wrapped_buffer, const Ts&... args ) // Note: "buffer_t" is either "buffer", "host_buffer" or "unified_buffer"
    {
      // if this device is an ocl::device then cast "buffer" to ocl::buffer and otherwise to "cuda::buffer"
      return prepare_kernel_arguments< NUM_ARGS -1 >( args..., common::write( wrapped_buffer.get().get_cuda_ocal_buffer_with_write_access() ) );
    }


    template< size_t NUM_ARGS, typename buffer_t, typename... Ts, typename DEV_TYPE_hlpr = DEV_TYPE, typename std::enable_if_t< std::is_same< DEV_TYPE_hlpr, CUDA >::value >* = nullptr, typename = std::enable_if_t<( NUM_ARGS > 0 )> >
    auto prepare_kernel_arguments( common::read_write_class< buffer_t > wrapped_buffer, const Ts&... args ) // Note: "buffer_t" is either "buffer", "host_buffer" or "unified_buffer"
    {
      // if this device is an ocl::device then cast "buffer" to ocl::buffer and otherwise to "cuda::buffer"
      return prepare_kernel_arguments< NUM_ARGS -1 >( args..., common::read_write( wrapped_buffer.get().get_cuda_ocal_buffer_with_read_write_access() ) );
    }

  
    template< size_t NUM_ARGS, typename... Ts, typename = std::enable_if_t< NUM_ARGS == 0 > >
    auto prepare_kernel_arguments( const Ts&... args )
    {
      return std::make_tuple( args... );
    }


    // local buffer - case: OpenCL device
    template< typename T, typename... Ts, typename T_DEV = DEV_TYPE, std::enable_if_t< std::is_same< T_DEV, OCL>::value >* = nullptr >
    auto& operator()( core::ocl::local_buffer<T> local_buffer, const Ts&... args )
    {
      _device( local_buffer, args... );
      
      return *this;
    }


    // case: CUDA device
    template< typename T, typename... Ts, typename T_DEV = DEV_TYPE, std::enable_if_t< std::is_same< T_DEV, CUDA>::value >* = nullptr >
    auto& operator()( core::cuda::shared_buffer<T> shared_buffer, const Ts&... args )
    {
      _device( shared_buffer, args... );
      
      return *this;
    }
  
  
    // ----------------------------------------------------------
    //   device information
    // ----------------------------------------------------------

    template< typename... Ts > //, typename T_DEV = DEV_TYPE, std::enable_if_t< std::is_same< T_DEV, OCL>::value >* = nullptr >
    void information( Ts&&... args )
    {
      _device.information( std::forward<Ts>( args )... );
    }

    void synchronize()
    {
      _device.synchronize();
    }

    auto &last_event() const
    {
      return _device.last_event();
    }


    unsigned long long last_runtime()
    {
      return _device.last_runtime();
    }


    std::string name()
    {
      return _device.name();
    }


    size_t max_threads_per_group()
    {
      return _device.max_threads_per_group();
    }


    std::vector<size_t> max_group_size()
    {
      return _device.max_group_size();
    }


    size_t max_local_or_shared_memory()
    {
      return _device.max_local_or_shared_memory();
    }

  
  private:
    DEV_TYPE _device;
};


} // namespace "ocal"


#endif /* ocal_device_hpp */
