//
//  cuda_kernel.hpp
//  ocal_cuda
//
//  Created by Ari Rasch on 20.09.17.
//  Copyright © 2017 Ari Rasch. All rights reserved.
//

#ifndef cuda_kernel_hpp
#define cuda_kernel_hpp

#include "../common/json.hpp"


namespace ocal
{

namespace core
{

// forward declaration
namespace ocl
{
//  class path;
//  class source;
  class kernel_representation;
}


namespace cuda
{


// forward declaration
class source;


class kernel : public abstract::abstract_kernel< kernel, CUfunction_CUmodule_wrapper >
{
  // friend class parent
  friend class abstract::abstract_kernel< kernel, CUfunction_CUmodule_wrapper >;

  // friend classes
  friend class device;
  
  
  public:
    kernel()                 = delete;  // Note: kernel should always take arguments (e.g., kernel's source code)
    kernel( const kernel&  ) = delete;
    kernel(       kernel&& ) = default;


    template< typename... Ts >
    kernel( const Ts&... args )
      : abstract_kernel( args... )
    {
      //initialize_cuda();
    }
  

    template< typename T, typename... Ts, typename = std::enable_if_t< std::is_base_of< ocl::kernel_representation, typename ::ocal::common::remove_cv_and_references<T>::type >::value > >
    kernel( const T& ocl_source, const Ts&... args )
      : abstract_kernel( cuda::source( common::opencl2cuda( ocl_source.get_source() ) ) , args... )
    {
      //initialize_cuda();
    }
  
  
  private:
 
    std::vector<std::string> get_kernel_names_in_source( const std::string& kernel_code )
    {
      // defines a regular expression for pattern "__global__ void NAME ("
      std::regex rgx( "__global__(?:.|\\s)*?void\\s*(\\w+)\\s*\\(" );
      
      return abstract::abstract_kernel< kernel, CUfunction_CUmodule_wrapper >::get_kernel_names_in_source( rgx, kernel_code );
    }
 
  
  
    // ----------------------------------------------------------
    //   static polymorphism
    // ----------------------------------------------------------
  
    void build_kernel_for_device( int unique_device_id )
    {
      // if kernel already built then do nothing
      if( _unique_device_id_to_kernel.find( unique_device_id ) != _unique_device_id_to_kernel.end() )
        return;
      
      // switch to actual cuda context
      set_cuda_context( unique_device_id );

      // extract kernel name -- if no kernel name is set by the user then, OCAL automatically uses the first found kernel in kernel's source file
      auto kernel_name = ( !_kernel_name.empty() ) ? _kernel_name : get_kernel_names_in_source( _kernel_as_string )[ 0 ];

      // prepare kernel flags
      std::string kernel_flags_accumulated;
      for( const auto& flag : _kernel_flags)
        kernel_flags_accumulated += flag + " ";

      char* ptx = nullptr;
      
      // check if auto-tuned kernel exists or can be generated
      auto device_name_as_hash = std::to_string( std::hash<std::string>{}( device_name_of( unique_device_id ) ) );
      auto kernel_hash = std::to_string( std::hash<std::string>{}( _kernel_as_string        ) );
      auto sps_string  = _sps.get_sps_as_string( "_" );
      auto flags_hash  = std::to_string( std::hash<std::string>{}( kernel_flags_accumulated ) );
      
      auto path_to_auto_tuned_kernel               = PATH_TO_OCAL_KERNEL_DB           + std::string( "/CUDA/" ) + device_name_as_hash + std::string( "/" );
      auto path_to_tuning_script                   = PATH_TO_OCAL_AUTO_TUNING_SCRIPTS + std::string( "/CUDA/" );
      auto kernel_name_with_hash                   = kernel_name           + std::string( "__" ) + kernel_hash;
      auto kernel_name_with_hash_and_sps_and_flags = kernel_name_with_hash + std::string( "__" ) + sps_string + std::string( "__" ) + flags_hash;
      
      // if no auto-tuned kernel is in database but an according tuning script is provided by the user, then generate an auto-tuned kernel
      if( !common::file_exists( path_to_auto_tuned_kernel + kernel_name_with_hash_and_sps_and_flags ) && common::file_exists( path_to_tuning_script + kernel_name_with_hash ) )
      {
        auto tuning_script_cmd_line_parameters = path_to_auto_tuned_kernel + std::string( " " ) +
                                                 kernel_name               + std::string( " " ) + // Note: kernel hash and static parameter values have to be appended automaticaly by the auto-tuning script for correctness issues
                                                 _sps.get_sps_as_string( " " ); // Note: no device_id required in CUDA since PTX is portable
        
        // execute tuning script
        auto cmd = std::string( path_to_tuning_script+kernel_name_with_hash + std::string( " " ) + tuning_script_cmd_line_parameters ).c_str();
        std::system( cmd );
      }
      
      // load binary kernel for sps if kernel exists
      if( common::file_exists( path_to_auto_tuned_kernel + kernel_name_with_hash_and_sps_and_flags ) )
      {
        // load kernel binary form HD
        std::ifstream kernel_file( path_to_auto_tuned_kernel + kernel_name_with_hash_and_sps_and_flags, std::ios_base::in | std::ios_base::binary | std::ios_base::ate );

        // get the size of the file
        auto ptx_size = kernel_file.tellg();
        
        // allocate memory
        ptx = new char[ ptx_size ];
        
        // set position in file to the beginning
        kernel_file.seekg( 0, std::ios::beg );
        
        // read the hole file
        kernel_file.read( ptx, ptx_size );
        
        // close it
        kernel_file.close();        
      }
      
      // load thread configuration from JSON if exists
      auto path_to_json = path_to_auto_tuned_kernel + kernel_name_with_hash_and_sps_and_flags + std::string( ".json" );
      if( common::file_exists( path_to_json ) )
      {
        // open file
        std::ifstream input_file( path_to_json );
        
        // load json
        json json_file;
        input_file >> json_file;
        
        // set thread configuration
        auto& thread_configurations = _unique_device_id_to_thread_configurations[ unique_device_id ];
        auto& grid_size             = thread_configurations[ 0 ];
        auto& block_size            = thread_configurations[ 1 ];
        
        grid_size[ 0 ] = json_file[ "grid_size" ][ "0" ];
        grid_size[ 1 ] = json_file[ "grid_size" ][ "1" ];
        grid_size[ 2 ] = json_file[ "grid_size" ][ "2" ];

        block_size[ 0 ] = json_file[ "block_size" ][ "0" ];
        block_size[ 1 ] = json_file[ "block_size" ][ "1" ];
        block_size[ 2 ] = json_file[ "block_size" ][ "2" ];
      }
      
      // generate un-tuned kernel
      else if( _sps.empty() )
      {
        #ifdef CACHE_KERNELS
          auto path_to_kernel = PATH_TO_OCAL_KERNEL_CACHE + std::string( "/CUDA/" ) + device_name_as_hash + std::string( "/" ) + kernel_hash + std::string("__") + flags_hash;
        
          if( common::file_exists( path_to_kernel ) )
          {
            // load kernel binary form HD
            std::ifstream kernel_file( path_to_kernel, std::ios_base::in | std::ios_base::binary | std::ios_base::ate );

            // get the size of the file
            auto ptx_size = kernel_file.tellg();
            
            // allocate memory
            ptx = new char[ ptx_size ];
            
            // set position in file to the beginning
            kernel_file.seekg( 0, std::ios::beg );
            
            // read the hole file
            kernel_file.read( ptx, ptx_size );
            
            // close it
            kernel_file.close();
            
          }
          else
          {
            // create program
            nvrtcProgram program;
            NVRTC_SAFE_CALL
            (
              nvrtcCreateProgram( &program,                   // prog
                                  _kernel_as_string.c_str(),  // buffer
                                  "cuda_program",             // name
                                  0,                          // numHeaders
                                  nullptr,                    // headers
                                  nullptr                     // includeNames
                                );
            );
            
            // prepare kernel flags
            std::vector< const char* > _kernel_flags_as_c_string;
            std::transform( _kernel_flags.begin(), _kernel_flags.end(), std::back_inserter( _kernel_flags_as_c_string ), []( std::string& s ){ return s.c_str(); } );
            
            auto num_options = static_cast<int>( _kernel_flags.size() );
            
            // build program
            nvrtcResult compileResult = nvrtcCompileProgram( program,                         // prog
                                                             num_options,                     // numOptions
                                                             _kernel_flags_as_c_string.data() // options
                                                           );
            
            if( compileResult != NVRTC_SUCCESS ) {
              // Obtain compilation log from the program.
              size_t log_size;
              NVRTC_SAFE_CALL( nvrtcGetProgramLogSize( program, &log_size ) );

              char* log = new char[ log_size ];
              NVRTC_SAFE_CALL( nvrtcGetProgramLog( program, log ) );

              std::cout << log << std::endl;

              delete[] log;

              // Destroy the program.
              NVRTC_SAFE_CALL( nvrtcDestroyProgram( &program ) );

              throw std::runtime_error(std::to_string(compileResult));
            }
            
            // extract kernel -- if no kernel name is set by the user then we automatically use the first found kernel in kernel's source file
            size_t ptx_size;
            NVRTC_SAFE_CALL( nvrtcGetPTXSize( program, &ptx_size ) );
            
            ptx = new char[ ptx_size ];
            NVRTC_SAFE_CALL( nvrtcGetPTX( program, ptx ) );
            
            #if( EMIT_PTX == 1 )
              std::cout << ptx << std::endl;
            #endif
            
            // save ptx on disk
            std::system( std::string( "mkdir -p " + PATH_TO_OCAL_KERNEL_CACHE + "/CUDA/" + device_name_as_hash + std::string( "/" ) ).c_str() );
            std::ofstream out_file( path_to_kernel, std::ios_base::out | std::ios_base::trunc | std::ios_base::binary );
            out_file.write( ptx, ptx_size );
            
            // Destroy the program.
            NVRTC_SAFE_CALL( nvrtcDestroyProgram( &program ) );
          }
        
        #else
          // create program
          nvrtcProgram program;
          NVRTC_SAFE_CALL
          (
            nvrtcCreateProgram( &program,                   // prog
                                _kernel_as_string.c_str(),  // buffer
                                "cuda_program",             // name
                                0,                          // numHeaders
                                nullptr,                    // headers
                                nullptr                     // includeNames
                              );
          );
          
          // prepare kernel flags
          std::vector< const char* > _kernel_flags_as_c_string;
          std::transform( _kernel_flags.begin(), _kernel_flags.end(), std::back_inserter( _kernel_flags_as_c_string ), []( std::string& s ){ return s.c_str(); } );

          auto num_options = static_cast<int>( _kernel_flags.size() );
          
          // build program
          nvrtcResult compileResult = nvrtcCompileProgram( program,                         // prog
                                                           num_options,                     // numOptions
                                                           _kernel_flags_as_c_string.data() // options
                                                         );
          
          if( compileResult != NVRTC_SUCCESS ) {
            // Obtain compilation log from the program.
            size_t log_size;
            NVRTC_SAFE_CALL( nvrtcGetProgramLogSize( program, &log_size ) );

            char* log = new char[ log_size ];
            NVRTC_SAFE_CALL( nvrtcGetProgramLog( program, log ) );

            std::cout << log << std::endl;

            delete[] log;

            // Destroy the program.
            NVRTC_SAFE_CALL( nvrtcDestroyProgram( &program ) );

            throw std::runtime_error(std::to_string(compileResult));
          }
          
          // extract kernel -- if no kernel name is set by the user then we automatically use the first found kernel in kernel's source file
          size_t ptx_size;
          NVRTC_SAFE_CALL( nvrtcGetPTXSize( program, &ptx_size ) );
          
          ptx = new char[ ptx_size ];
          NVRTC_SAFE_CALL( nvrtcGetPTX( program, ptx ) );
          
          #if( EMIT_PTX == 1 )
            std::cout << ptx << std::endl;
          #endif
          
          // Destroy the program.
          NVRTC_SAFE_CALL( nvrtcDestroyProgram( &program ) );
        #endif
      }
      
      // error case
      else
      {
        std::wcerr << "Neither auto-tuned kernel nor tuning script exists for desired program \"" << kernel_name.c_str() << "\"" << std::endl;
        
        assert( false );
        exit( EXIT_FAILURE );
      }
      
      // Load the generated PTX and get a handle to the SAXPY kernel.
      CUfunction kernel;
      
      assert( _unique_device_id_to_kernel.find( unique_device_id ) == _unique_device_id_to_kernel.end() ); // Note: if kernel is already compiled for device then this function ("build_kernel_for_device") should not be called again due to performance issues
      CUmodule& module = _unique_device_id_to_kernel[ unique_device_id ].module(); //_unique_device_id_to_cuda_module_wrapper[ unique_device_id ].get(); // Note: creates CUfunction/module wrapper
      
      CUDA_SAFE_CALL( cuModuleLoadDataEx ( &module, ptx, 0, 0, 0                ) );
      CUDA_SAFE_CALL( cuModuleGetFunction( &kernel, module, kernel_name.c_str() ) );
      
      // save the kernel
      _unique_device_id_to_kernel.at( unique_device_id ) = kernel;
      
      delete[] ptx;
    }
};


// used for abstracting path and source
class kernel_representation
{};


// helper classes for differentiation between passing kernel 1) as path to source and 2) kernel's source as string
class path : public abstract::path, public kernel_representation
{
  public:
    // generic ctor
    template< typename... Ts >
    path( Ts... args )
      : abstract::path( args... ) //, kernel( *this )
    {}
};


class source : public abstract::source, public kernel_representation
{
  public:
    // generic ctor
    template< typename... Ts >
    source( Ts... args )
      : abstract::source( args... ) //, kernel( *this )
    {}
};


} // namespace "cuda"


#ifndef OCAL_OCL
namespace ocl
{

// used for abstracting path and source
class kernel_representation
{};

// helper classes for differentiation between passing kernel 1) as path to source and 2) kernel's source as string
class path : public abstract::path, public kernel_representation
{
  public:
    // generic ctor
    template< typename... Ts >
    path( Ts... args )
      : abstract::path( args... )
    {}  
};


class source : public abstract::source, public kernel_representation
{
  public:
    // generic ctor
    template< typename... Ts >
    source( Ts... args )
      : abstract::source( args... )
    {}
};

} // namespace "ocl"
#endif


} // namespace "core"

} // namespace "ocal"


#endif /* kernel_hpp */
