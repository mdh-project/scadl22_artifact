//
//  cuda.hpp
//  
//
//  Created by Ari Rasch on 17/10/16.
//
//

#ifndef cuda_hpp
#define cuda_hpp

#include <vector>
#include <array>
#include <string>
#include <type_traits>
#include <unordered_map>
#include <iostream>
#include <regex>
#include <sstream>
#include <fstream>
#include <cmath>
#include <cstdlib>
#include <memory> 
#include <thread>

#include "assert.h"

#if 0//defined(__APPLE__) || defined(__linux__)
  #include "/usr/local/cuda/include/nvrtc.h"
#else
  #include <nvrtc.h>
#endif

#if 0
  #include "/Developer/NVIDIA/CUDA-8.0/lib/stubs/CUDA.framework/Versions/A/Headers/cuda.h"
#else
  #include <cuda.h>
#endif

// number of CUDA queues for overlapping computations with communications
#ifndef NUM_CUDA_STREAMS_PER_DEVICE
  #define NUM_CUDA_STREAMS_PER_DEVICE  3
#endif


// maximal length of devices' names
#ifndef LENGTH_DEVICE_NAME
  #define LENGTH_DEVICE_NAME  256
#endif


// global definitions
namespace ocal
{

namespace core
{

namespace cuda
{


// used for CUDA initialization
std::unordered_map<std::thread::id, int>   current_context_to_unique_device_id;

using command_queue_map = std::unordered_map<int, CUstream>;

std::unordered_map<int, CUcontext>         unique_device_id_to_cuda_context;          // 1:1 correspondence
std::unordered_map<int, CUdevice>          unique_device_id_to_cuda_device;           // 1:1 correspondence

std::unordered_map<int, command_queue_map> unique_device_id_to_cuda_streams;          // 1:1 correspondence
std::unordered_map<int, int>               unique_device_id_to_actual_cuda_stream_id; // 1:1 correspondence


} // namespace "cuda"

} // namespace "core"

} // namespace "ocal"


// common classes
#include "../common/helper.hpp"
#include "../common/event_manager.hpp"
#include "../common/source_to_source_translation.hpp"

#include "../../common/helper.hpp"
#include "../../common/kernel_argument_specifiers.hpp"
#include "../../common/return_proxy.hpp"
#include "../../common/static_parameters.hpp"


// ocal::core::common helper
#include "../common/helper.hpp"

// ocal::cuda helper
#include "cuda_helper.hpp"


// global definitions
namespace ocal
{

namespace core
{

namespace cuda
{

// init OCL
int init_dummy = []()
{
  // initialize CUDA
  CUDA_SAFE_CALL( cuInit(0) );
  
  return 0;
}();

using cuda_event_manager = ocal::core::common::event_manager< CUevent_wrapper >;

} // namespace "cuda"

} // namespace "core"

} // namespace "ocal"


// abstract classes
#include "../abstract/abstract_buffer.hpp"
#include "../abstract/abstract_host_buffer.hpp"
#include "../abstract/abstract_unified_buffer.hpp"
#include "../abstract/abstract_local_or_shared_buffer.hpp"
#include "../abstract/abstract_thread_configuration.hpp"
#include "../abstract/abstract_kernel.hpp"
#include "../abstract/abstract_device.hpp"


// ocal::cuda header
#include "cuda_buffer.hpp"
#include "cuda_host_buffer.hpp"
#include "cuda_unified_buffer.hpp"
#include "cuda_local_buffer.hpp"
#include "cuda_dim3.hpp"
#include "cuda_kernel.hpp"
#include "cuda_device.hpp"


// global definitions
namespace ocal
{

namespace core
{

namespace cuda
{

class cuda_init_dummy_class
{
  public:
    // CUDA "init"
    cuda_init_dummy_class()
    {
      ocal::core::cuda::device{ 0 }; // set initial device (required if a cuda::host_buffer/cuda::unified_buffer is decleread before a cuda::device)
    }
  
  
    // CUDA "free"
    ~cuda_init_dummy_class()
    {
      for( auto& elem : unique_device_id_to_cuda_context )
      {
        auto& context = elem.second;
        CUDA_SAFE_CALL( cuCtxDestroy( context ) );
      }
    }
};

// init/free CUDA
cuda_init_dummy_class cuda_init_dummy = cuda_init_dummy_class{};

//// init OCL
//int cuda_init_dummy = []()
//{
//  // set initial device (required if a cuda::host_buffer/cuda::unified_buffer is decleread before a cuda::device)
//  ocal::core::cuda::device{ 0 };
//
//  return 0;
//}();


} // namespace "cuda"

} // namespace "core"

} // namespace "ocal"


#endif /* cuda_hpp */
