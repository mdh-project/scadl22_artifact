import argparse
import pickle
import os.path
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

def main():
    parser = argparse.ArgumentParser(description='Authorize login to Google Sheets API.')
    parser.add_argument('credentials', type=str, help='The credentials to use.')
    parser.add_argument('pickle', type=str, help='Where to store the pickle file.')
    args = parser.parse_args()

    # If modifying these scopes, delete the file token.pickle.
    SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists(args.pickle):
        with open(args.pickle, 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                args.credentials, SCOPES)
            creds = flow.run_console()
        # Save the credentials for the next run
        with open(args.pickle, 'wb') as token:
            pickle.dump(creds, token)


if __name__ == '__main__':
    main()