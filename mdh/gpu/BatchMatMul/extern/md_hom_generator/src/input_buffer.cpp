//
// Created by Richard Schulze on 30.10.2017.
//
#include "input_buffer.hpp"

#include <iostream>
#include "helper.hpp"

namespace md_hom {

input_buffer_class::input_buffer_class(const std::string &data_type,
                                       const std::string &name,
                                       const std::vector<std::vector<std::string>> &index_functions,
                                       const std::vector<std::vector<std::string>> &global_index_functions,
                                       const std::vector<std::string> &global_dimension_sizes)
        : _data_type(data_type), _name(name), _index_functions(index_functions), _global_index_functions(global_index_functions.empty() ? index_functions : global_index_functions), _global_dimension_sizes(global_dimension_sizes) {
    if (index_functions.size() < 3) {
        throw std::runtime_error("at least 3 index functions have to be passed");
    }
    const std::regex dim_regex("([lr])([1-9][0-9]*)");
    std::smatch match;
    for (auto index_function : _index_functions[0]) {
        _used_dimensions.emplace_back(0);
        while (std::regex_search(index_function, match, dim_regex)) {
            _used_dimensions.back().push_back(match[1].str() == "l" ? L(std::atoi(match[2].str().c_str())) : R(std::atoi(match[2].str().c_str())));
            index_function = match.suffix();
        }
    }
}

const std::string &input_buffer_class::name() const {
    return _name;
}

const std::vector<std::vector<std::string>> &input_buffer_class::index_functions() const {
    return _index_functions;
}

const std::vector<std::vector<std::string>> &input_buffer_class::global_index_functions() const {
    return _global_index_functions;
}

const std::vector<std::string> &input_buffer_class::global_dimension_sizes() const {
    return _global_dimension_sizes;
}

const std::vector<std::vector<dimension_t>> &input_buffer_class::used_dimensions() const {
    return _used_dimensions;
}

const std::string &input_buffer_class::data_type() const {
    return _data_type;
}

input_buffer_class input_buffer(const std::string &data_type,
                                const std::string &name,
                                const std::vector<std::vector<std::string>> &index_functions,
                                const std::vector<std::vector<std::string>> &global_index_functions,
                                const std::vector<std::string> &global_dimension_sizes) {
    return input_buffer_class(data_type, name, index_functions, global_index_functions, global_dimension_sizes);
}

input_buffer_class input_buffer(const std::string &data_type,
                                const std::string &name,
                                const std::vector<std::string> &index_functions,
                                const std::vector<std::vector<std::string>> &global_index_functions,
                                const std::vector<std::string> &global_dimension_sizes) {
    return input_buffer_class(data_type, name, {index_functions, index_functions, index_functions}, global_index_functions, global_dimension_sizes);
}

input_buffer_class input_buffer(const std::string &data_type,
                                const std::string &name,
                                const std::string &index_function,
                                const std::vector<std::vector<std::string>> &global_index_functions,
                                const std::vector<std::string> &global_dimension_sizes) {
    return input_buffer_class(data_type, name, {{index_function}, {index_function}, {index_function}}, global_index_functions, global_dimension_sizes);
}

input_buffer_class input_buffer(const std::string &data_type,
                                const std::string &name,
                                const std::vector<dimension_t> &used_dimensions) {
    std::vector<std::vector<std::string>> index_functions(1);
    for (int i = 0; i < used_dimensions.size(); ++i)
        index_functions[0].push_back(std::string(used_dimensions[i].type == DIM_TYPE::L ? "l" : "r") + std::to_string(used_dimensions[i].nr));
    return input_buffer_class(data_type, name, index_functions, {}, {});
}

}