//
// Created by Richard Schulze on 06.11.17.
//
#include <types.hpp>
#include <regex>
#include "result_buffer.hpp"

namespace md_hom {

std::pair<std::function<std::string(const std::string&, const std::string&)>, std::function<std::string(const std::string&, const std::string&)>> PLUS(
        [](const std::string &lhs, const std::string &rhs) { return lhs + " + " + rhs; },
        [](const std::string &lhs, const std::string &rhs) { return lhs + " += " + rhs; }
);

result_buffer_class::result_buffer_class(const std::string &data_type,
                                         const std::pair<std::function<std::string(const std::string&, const std::string&)>, std::function<std::string(const std::string&, const std::string&)>> &op,
                                         const std::string &name,
                                         const std::vector<std::string> &index_functions,
                                         const std::vector<std::string> &dimension_sizes)
        : _name(name), _index_functions(index_functions), _op(op), _data_type(data_type), _dimension_sizes(dimension_sizes) {
    if (index_functions.empty()) {
        throw std::runtime_error("index functions can not be empty");
    }
    const std::regex dim_regex("l([1-9][0-9]*)");
    std::smatch match;
    for (auto index_function : _index_functions) {
        while (std::regex_search(index_function, match, dim_regex)) {
            _dimension_order.push_back({DIM_TYPE::L, std::atoi(match[1].str().c_str())});
            index_function = match.suffix();
        }
    }
}

result_buffer_class result_buffer(const std::string &data_type,
                                  const std::pair<std::function<std::string(const std::string&, const std::string&)>, std::function<std::string(const std::string&, const std::string&)>> &op,
                                  const std::string &name,
                                  const std::vector<std::string> &index_functions,
                                  const std::vector<std::string> &dimension_sizes) {
    return result_buffer_class(data_type, op, name, index_functions, dimension_sizes);
}

result_buffer_class result_buffer(const std::string &data_type,
                                  const std::pair<std::function<std::string(const std::string&, const std::string&)>, std::function<std::string(const std::string&, const std::string&)>> &op,
                                  const std::string &name,
                                  const std::string &index_function,
                                  const std::vector<std::string> &dimension_sizes) {
    return result_buffer_class(data_type, op, name, {index_function}, dimension_sizes);
}

}