//
//  abstract_buffer.hpp
//  ocal_abstract
//
//  Created by Ari Rasch on 20.09.17.
//  Copyright © 2017 Ari Rasch. All rights reserved.
//

#ifndef abstract_buffer_hpp
#define abstract_buffer_hpp


namespace ocal
{

namespace core
{

namespace abstract
{


template< typename child_t, typename host_memory_t, typename buffer_t, typename event_t, typename host_memory_ptr_t, typename T >
class abstract_buffer : public common::event_manager< event_t >
{
  friend class ::ocal::common::return_proxy< abstract_buffer< child_t, host_memory_t, buffer_t, event_t, host_memory_ptr_t, T >, T >;

  public:
    template< typename... Ts >
    abstract_buffer( size_t size, const Ts&... args )
      : _size( size ), _host_memory( args... ), _ptr_to_host_memory( size, _host_memory ), _unique_device_id_to_device_buffer(), _host_is_dirty( false ), _unique_device_id_to_dirty_flag(), _unique_device_id_to_command_queue_or_stream_id(), _id_of_last_active_device( -1 ) // TODO: refac dirty_flag->dirty_bit?
    {}
  
  
//    // standard ctor: only used for late assignment
//    abstract_buffer( size_t size = 0 )
//      : _size( size ), _host_memory(), _ptr_to_host_memory( size, _host_memory ), _unique_device_id_to_device_buffer(), _host_is_dirty( false ), _unique_device_id_to_dirty_flag(), _unique_device_id_to_command_queue_or_stream_id(), _id_of_last_active_device( -1 ) // TODO: refac dirty_flag->dirty_bit?
//    {}

    abstract_buffer()                          = delete;  // Note: buffer are initialized with input size
    abstract_buffer( const abstract_buffer&  ) = default; // Note: required to put buffers in std::vector
    abstract_buffer(       abstract_buffer&& ) = default;

    abstract_buffer& operator=( const abstract_buffer&  ) = default;
    abstract_buffer& operator=(       abstract_buffer&& ) = default;

  
    // returns a proxy which enables differentiating betweeen read and write accesses
    auto operator[]( size_t i )
    {
      assert( i < _size );
           
      return ::ocal::common::return_proxy< abstract_buffer< child_t, host_memory_t, buffer_t, event_t, host_memory_ptr_t, T >, T >( *this, i );
    }
  
    virtual ~abstract_buffer() = default;
  
  
    // ----------------------------------------------------------
    //   set/get host memory
    // ----------------------------------------------------------

    void set_content( T* new_content )
    {
      // pointer on me as child
      auto me_as_child = dynamic_cast< child_t* >( this );
    
      // run child class' function
      me_as_child->set( new_content );
    }


    std::vector<T> get_content()
    {
      // pointer on me as child
      auto me_as_child = dynamic_cast< child_t* >( this );

      // run child class' function
      return me_as_child->get();
    }

    size_t size() const
    {
        return _size;
    }
    
  
    // ----------------------------------------------------------
    //   buffer access functions
    // ----------------------------------------------------------
 
    // returns the device buffer
    buffer_t& get_device_buffer_with_read_access( int unique_device_id, int command_queue_id )
    {
      // set id of last active device
      this->set_id_of_last_active_device( unique_device_id );
      
      // create device buffer if it does not already exists
      this->create_device_buffer_if_not_existent( unique_device_id, command_queue_id );

      // Auskommentiert, damit lesende Zugriffe auf den Buffer mit Hostzugriffen überlagert
      // werden können, z.B.:
      // Stream 1:  --- HtoD - Kernel 1 (read_write buffer) - DtoH (z.B. für Host read) ----
      // Stream 2:  ----------------------------------------- Kernel 2 (read buffer) -------
      // refresh command queue id
//      _unique_device_id_to_command_queue_or_stream_id.at( unique_device_id ) = command_queue_id;

      // refresh data in buffer
      this->update_device_buffer( unique_device_id );
      
      // return device buffer
      return this->_unique_device_id_to_device_buffer.at( unique_device_id );
    }

  
    // returns the device buffer and sets dirty flags
    buffer_t& get_device_buffer_with_write_access( int unique_device_id, int command_queue_id )
    {
      // set id of last active device
      this->set_id_of_last_active_device( unique_device_id );

      // create device buffer if it does not already exists
      this->create_device_buffer_if_not_existent( unique_device_id, command_queue_id );

      // refresh command queue id
      _unique_device_id_to_command_queue_or_stream_id.at( unique_device_id ) = command_queue_id;

      // set dirty flags
      this->set_host_memory_as_dirty();
      this->set_all_device_buffers_as_dirty_except( unique_device_id );
      
      // return device buffer
      return this->_unique_device_id_to_device_buffer.at( unique_device_id );
    }
  
  
    // returns the device buffer and sets dirty flags
    buffer_t& get_device_buffer_with_read_write_access( int unique_device_id, int command_queue_id )
    {
      // set id of last active device
      this->set_id_of_last_active_device( unique_device_id );

      // create device buffer if it does not already exists
      this->create_device_buffer_if_not_existent( unique_device_id, command_queue_id );

      // refresh command queue id
      _unique_device_id_to_command_queue_or_stream_id.at( unique_device_id ) = command_queue_id;
      
      // refresh data in buffer
      this->update_device_buffer( unique_device_id );
    
      // set dirty flags
      this->set_host_memory_as_dirty();
      this->set_all_device_buffers_as_dirty_except( unique_device_id );
      
      // return device buffer
      return this->_unique_device_id_to_device_buffer.at( unique_device_id );
    }
  

  protected:
  
    // -------------------------------------------------------------------
    //   native Buffer management: creation and H2D/D2H data transfers
    // -------------------------------------------------------------------

    // creates an abstract device if the device does not already exists
    void create_device_buffer_if_not_existent( int unique_device_id, int command_queue_id )
    {
      // pointer on me as child
      auto me_as_child = dynamic_cast< child_t* >( this );
      
      // run child class' function
      me_as_child->create_device_buffer_if_not_existent( unique_device_id, command_queue_id );
    }

  
    void copy_data_from_device_buffer_to_device_buffer( int device_id_of_source, int device_id_of_destination )
    {
      // flush host memory
      this->flush_host_memory();
      
      // pointer on me as child
      auto me_as_child = dynamic_cast< child_t* >( this );
      
      // run child class' function
      me_as_child->copy_data_from_device_buffer_to_device_buffer( device_id_of_source, device_id_of_destination );
      
      // flush host memory
      this->flush_host_memory();
    }


    void copy_data_from_host_memory_to_device_buffer( int unique_device_id )
    {
      // flush host memory
      this->flush_host_memory();

      // pointer on me as child
      auto me_as_child = dynamic_cast< child_t* >( this );
      
      // run child class' function
      me_as_child->copy_data_from_host_memory_to_device_buffer( unique_device_id );
      
      // flush host memory
      this->flush_host_memory();
    }


    void copy_data_from_device_buffer_to_host_memory( int unique_device_id )
    {
      // flush host memory
      this->flush_host_memory();

      // pointer on me as child
      auto me_as_child = dynamic_cast< child_t* >( this );
      
      // run child class' function
      me_as_child->copy_data_from_device_buffer_to_host_memory( unique_device_id );
      
      // flush host memory
      this->flush_host_memory();
    }



    // ----------------------------------------------------------
    //   flag management
    // ----------------------------------------------------------

    // setter
    void set_all_device_buffers_as_dirty_except( int unique_device_id )
    {
      for( auto& flags : _unique_device_id_to_dirty_flag )
        if( flags.first == unique_device_id )
          flags.second = false;
        else
          flags.second = true;
    }


    void set_all_device_buffers_as_dirty()
    {
      for( auto& flags : _unique_device_id_to_dirty_flag )
        flags.second = true;
    }

  
    void set_device_buffer_as_up_to_date( int unique_device_id )
    {
      _unique_device_id_to_dirty_flag.at( unique_device_id ) = false;
    }
  
  
    void set_host_memory_as_dirty()
    {
      _host_is_dirty = true;
    }


    void set_host_memory_as_up_to_date()
    {
      _host_is_dirty = false;
    }
  
  
    // getter
    bool is_device_buffer_up_to_date( int unique_device_id ) const
    {
      return _unique_device_id_to_dirty_flag.find( unique_device_id ) != _unique_device_id_to_dirty_flag.end() && // device exists?
             _unique_device_id_to_dirty_flag.at( unique_device_id ) == false;                                     // device up to date?
    }
  
  
    bool is_host_memory_up_to_date() const
    {
      return _host_is_dirty == false;
    }


    // ----------------------------------------------------------
    //   buffer refreshing functions (manage dirty flags)
    // ----------------------------------------------------------

    void update_host_memory()
    {
      // if host is already up to date
      if( this->is_host_memory_up_to_date() )
      {
        // wait for critical events to finish
        this->sync_write_events_on_host_memory();

        return;
      }
      
      // find clean buffer
      int source_unique_device_id = this->get_unique_device_id_of_clean_device_buffer();
      
      // copy data from device to host
      this->sync_write_events_on_device_buffer( source_unique_device_id );
      this->sync_read_events_on_host_memory();
      this->sync_write_events_on_host_memory();
      
      this->copy_data_from_device_buffer_to_host_memory( source_unique_device_id );
      
      this->sync_write_events_on_host_memory();
      
      // manage dirty flags
      this->set_host_memory_as_up_to_date();
    }
  
  
    void update_device_buffer( int unique_device_id )
    {
      // if device buffer is already up to date
      if( this->is_device_buffer_up_to_date( unique_device_id ) )
      {
        // wait for critical events to finish
        this->sync_write_events_on_device_buffer( unique_device_id );

        return;
      }
      
      // if host memory is up to date then copy data from host memory to device buffer, otherwise: copy data from D2H and then from H2D
      if( this->is_host_memory_up_to_date() )
      {
        this->sync_write_events_on_host_memory();
        this->sync_write_events_on_device_buffer( unique_device_id );
        
        this->copy_data_from_host_memory_to_device_buffer( unique_device_id );
      }
      else
      {
        // find clean buffer
        int source_unique_device_id = get_unique_device_id_of_clean_device_buffer();
        
        // copy data from device to device over host
        this->sync_write_events_on_device_buffer( source_unique_device_id );
        this->sync_read_events_on_device_buffer( unique_device_id );
        this->sync_write_events_on_device_buffer( unique_device_id );
        
        this->copy_data_from_device_buffer_to_device_buffer( source_unique_device_id, unique_device_id );
        
        this->sync_write_events_on_device_buffer( unique_device_id );
      }
      
      // manage dirty flags
      this->set_device_buffer_as_up_to_date( unique_device_id );
      this->set_host_memory_as_up_to_date();
    }
  
  
    // ----------------------------------------------------------
    //   host memory access functions (manage dirty flags)
    // ----------------------------------------------------------

    // scalar read access of host memory
    void read_from_host_memory( size_t i, T& val )
    {
      assert( i < _size );
           
      this->update_host_memory();
      
      val = _ptr_to_host_memory[ i ];
    }

  
    // scalar write access of host memory
    void write_in_host_memory( size_t i, const T& val )
    {
      assert( i < _size );
      
      this->update_host_memory();

      _ptr_to_host_memory[ i ] = val;

      // set dirty flags
      this->set_all_device_buffers_as_dirty();
    }


    // ----------------------------------------------------------
    //   helper
    // ----------------------------------------------------------
  
    // find up-to-date device buffer and return device's unique_device_id
    auto get_unique_device_id_of_clean_device_buffer() const
    {
      // iterate over flags
      for( const auto& flag : _unique_device_id_to_dirty_flag )
        // if flag is clean
        if( flag.second == false )
          // return device id
          return flag.first;
      
      assert( false && "should never be reached" );
      return -1;
    }
  

    // required to minimize map/unmap operations of OpenCL host_buffers
    void flush_host_memory()
    {
      //_ptr_to_host_memory.flush();
      _ptr_to_host_memory.flush(); // = host_memory_ptr_t( _host_memory );  // Note: assigning the same object causes flushing the buffer in case of OpenCL host/unified buffer and nothing otherwise
    }
  
  
    void set_id_of_last_active_device( int unique_device_id )
    {
      _id_of_last_active_device = unique_device_id;
    }

  
  protected:
    size_t                            _size;
    host_memory_t                     _host_memory;
    host_memory_ptr_t                 _ptr_to_host_memory;
    std::unordered_map<int, buffer_t> _unique_device_id_to_device_buffer;
    bool                              _host_is_dirty;
    std::unordered_map<int, bool>     _unique_device_id_to_dirty_flag;
    std::unordered_map<int, int>      _unique_device_id_to_command_queue_or_stream_id;
    int                               _id_of_last_active_device;
};


} // namespace "abstract"

} // namespace "core"

} // namespace "ocal"


#endif /* abstract_buffer_hpp */
