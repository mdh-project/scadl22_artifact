//
//  abstract_host_buffer.hpp
//  ocal
//
//  Created by Ari Rasch on 15.10.17.
//  Copyright © 2017 Ari Rasch. All rights reserved.
//

#ifndef abstract_host_buffer_h
#define abstract_host_buffer_h


namespace ocal
{

namespace core
{

namespace abstract
{


template< typename child_t, typename host_memory_t, typename buffer_t, typename event_t, typename host_memory_ptr_t, typename T >
class abstract_host_buffer : public abstract_buffer< abstract_host_buffer< child_t, host_memory_t, buffer_t, event_t, host_memory_ptr_t, T >, // child_t
                                                     host_memory_t, buffer_t, event_t, host_memory_ptr_t, T
                                                   >
{
  // friend class parent
  friend class abstract_buffer< abstract_host_buffer< child_t, host_memory_t, buffer_t, event_t, host_memory_ptr_t, T >, // child_t
                                host_memory_t, buffer_t, event_t, host_memory_ptr_t, T
                              >;
  
  public:
    // generic ctor
    template< typename... Ts >
    abstract_host_buffer( size_t size, const Ts&... args )
      : abstract_buffer< abstract_host_buffer< child_t, host_memory_t, buffer_t, event_t, host_memory_ptr_t, T >, host_memory_t, buffer_t, event_t, host_memory_ptr_t, T >( size, args... )
    {}


//    // standard ctor: only used for late assignment
//    abstract_host_buffer( size_t size = 0 )
//      : abstract_buffer< abstract_host_buffer< child_t, host_memory_t, buffer_t, event_t, host_memory_ptr_t, T >, host_memory_t, buffer_t, event_t, host_memory_ptr_t, T >( size )
//    {}
  
  
    abstract_host_buffer()                               = delete;  // Note: buffer are initialized with input size
    abstract_host_buffer( const abstract_host_buffer&  ) = default; // Note: required to put buffers in std::vector
    abstract_host_buffer(       abstract_host_buffer&& ) = default;

    abstract_host_buffer& operator=( const abstract_host_buffer&  ) = default;
    abstract_host_buffer& operator=(       abstract_host_buffer&& ) = default;

  
    virtual ~abstract_host_buffer() = default;
  
    
  protected:
  
    // ---------------------------------------------------------------------------------------
    //   native Buffer management: creation and H2D/D2H data transfers (static polymorphism)
    // ---------------------------------------------------------------------------------------

    // creates an abstract device if the device does not already exists
    void create_device_buffer_if_not_existent( int unique_device_id, int command_queue_id ) 
    {
      // pointer on me as child
      auto me_as_child = dynamic_cast< child_t* >( this );
      
      // run child class' function
      return me_as_child->create_device_buffer_if_not_existent( unique_device_id, command_queue_id );
    }
  
  
    void copy_data_from_device_buffer_to_device_buffer( int device_id_of_source, int device_id_of_destination )
    {
      // pointer on me as child
      auto me_as_child = dynamic_cast< child_t* >( this );
      
      // run child class' function
      return me_as_child->copy_data_from_device_buffer_to_device_buffer( device_id_of_source, device_id_of_destination );
    }

  
    void copy_data_from_host_memory_to_device_buffer( int unique_device_id )
    {
      // pointer on me as child
      auto me_as_child = dynamic_cast< child_t* >( this );
      
      // run child class' function
      return me_as_child->copy_data_from_host_memory_to_device_buffer( unique_device_id );
    }
  
  
    void copy_data_from_device_buffer_to_host_memory( int unique_device_id )
    {
      // pointer on me as child
      auto me_as_child = dynamic_cast< child_t* >( this );
      
      // run child class' function
      return me_as_child->copy_data_from_device_buffer_to_host_memory( unique_device_id );
    }
};


} // namespace "abstract"

} // namespace "core"

} // namespace "ocal"


#endif /* abstract_host_buffer_h */
