//
//  cuda_helper.hpp
//  ocal_cuda
//
//  Created by Ari Rasch on 20.09.17.
//  Copyright © 2017 Ari Rasch. All rights reserved.
//

#ifndef cuda_helper_hpp
#define cuda_helper_hpp


namespace ocal
{

namespace core
{

namespace cuda
{


// checking NVRTC errors
#define NVRTC_SAFE_CALL(x)                                        \
  do {                                                            \
    nvrtcResult result = x;                                       \
    if (result != NVRTC_SUCCESS) {                                \
      std::cerr << "\nerror: " #x " failed with error "           \
                << nvrtcGetErrorString(result) << '\n';           \
      assert(false);exit(1);                                      \
    }                                                             \
  } while(0)
  
  
// checking CUDA errors  
#define CUDA_SAFE_CALL(x)                                         \
  do {                                                            \
    CUresult result = x;                                          \
    if (result != CUDA_SUCCESS) {                                 \
      const char *msg;                                            \
      cuGetErrorName(result, &msg);                               \
      std::cerr << "\nerror: " #x " failed with error "           \
                << msg << '\n';                                   \
      assert(false);exit(1);                                      \
    }                                                             \
  } while(0)

  
void set_cuda_context( int unique_device_id )
{
  // if device's context is current then do nothing
  if( current_context_to_unique_device_id[std::this_thread::get_id()] == unique_device_id )
   return;

  #if 0
    // pop current context
    CUDA_SAFE_CALL( cuCtxPopCurrent( nullptr ) );
  
    // push device's context
    auto context = unique_device_id_to_cuda_context.at( unique_device_id );
    CUDA_SAFE_CALL( cuCtxPushCurrent( context ) );
  
  #else // Note: this should be the better way ( TODO -> test)
    auto context = unique_device_id_to_cuda_context.at( unique_device_id );
    CUDA_SAFE_CALL( cuCtxSetCurrent( context ) );
  
  #endif
  
  current_context_to_unique_device_id[std::this_thread::get_id()] = unique_device_id;
}


std::string device_name_of( int unique_device_id )
{
    auto cuda_device = unique_device_id_to_cuda_device.at( unique_device_id );

    char* device_name_as_c_str = new char[ LENGTH_DEVICE_NAME ];
    cuDeviceGetName( device_name_as_c_str, LENGTH_DEVICE_NAME, cuda_device );
    std::string device_name = device_name_as_c_str;
    delete[] device_name_as_c_str;

    return device_name;
}


// helper for managing the lifetime of cuda modules
class CUfunction_CUmodule_wrapper
{
  public:
    CUfunction_CUmodule_wrapper() = default;
  
    ~CUfunction_CUmodule_wrapper()
    {
      // CUDA_SAFE_CALL( cuModuleUnload( _module ) ); // TODO: counter einfügen der im copy-ctor inkrementiert (für Anzahl CUfunction_CUmodule_wrapper-Objekte) und module erst bei counter==0 freigeben
    }
  
  
    auto& kernel()
    {
      return _kernel;
    }
  
  
    auto& module()
    {
      return _module;
    }
  
  
    auto& operator=( const CUfunction& kernel )
    {
      _kernel = kernel;
      
      return *this;
    }
  
    operator CUfunction&()
    {
      return _kernel;
    }
  
  private:
    CUfunction _kernel;
    CUmodule   _module;
};


// manages destruction of CUevent objects (CUevent_RAII_wrapper) and provides for them a convenient interface (CUevent_wrapper)
class CUevent_wrapper : public common::event_wrapper
{
  private:
    class CUevent_RAII_wrapper
    {
      public:
        ~CUevent_RAII_wrapper()
        {
          CUDA_SAFE_CALL( cuEventDestroy( _event ) );
        }
      
        auto& get()
        {
          return _event;
        }
      
      private:
        CUevent _event;
    };
  
  
  public:
    CUevent_wrapper() = default;

    CUevent_wrapper( int device_id, int stream_id, bool disable_timing = true, bool record_event = true )
      : _event( std::make_shared<CUevent_RAII_wrapper>() )
    {
      // switch to actual cuda context
      set_cuda_context( device_id );
      
      auto cuda_stream = unique_device_id_to_cuda_streams.at( device_id ).at( stream_id );
      
      CUDA_SAFE_CALL( cuEventCreate( &( _event->get() ), disable_timing ? CU_EVENT_DISABLE_TIMING : 0 ) );
      if (record_event)
        CUDA_SAFE_CALL( cuEventRecord( _event->get(), cuda_stream )                  );
    }
  
  
    auto& get()
    {
      return (*_event).get();
    }
  
  
    void wait() const
    {
      CUDA_SAFE_CALL( ::cuEventSynchronize( _event->get() ) );
    }


    STATUS status() const
    {
      auto status = ::cuEventQuery( _event->get() );
      if (status == CUDA_SUCCESS)
        return COMPLETE;
      else if (status == CUDA_ERROR_NOT_READY)
        return RUNNING;
      else
        CUDA_SAFE_CALL(status);
    }
  
  
  private:
    std::shared_ptr< CUevent_RAII_wrapper > _event;
};


size_t NUM_DEVICES()
{
  int count;
  CUDA_SAFE_CALL( cuDeviceGetCount( &count ) );

  return count;
}

void cuda_synchronize()
{
  for( auto& elem : unique_device_id_to_cuda_context )
  {
    auto& dev = elem.first;
    set_cuda_context( dev );
    
    CUDA_SAFE_CALL( cuCtxSynchronize() );
  }
}

  
} // namespace "cuda"

} // namespace "core"

} // namespace "ocal"


#endif /* cuda_helper_hpp */
