//
//  ocl_host_buffer.hpp
//  ocal
//
//  Created by Ari Rasch on 15.10.17.
//  Copyright © 2017 Ari Rasch. All rights reserved.
//

#ifndef ocl_host_buffer_h
#define ocl_host_buffer_h


namespace ocal
{

namespace core
{

namespace ocl
{


template< typename T >
class host_buffer : public abstract::abstract_host_buffer< host_buffer<T>,                 // (required for static polymorphism)
                                                           cl::Buffer,                     // host_memory_t
                                                           cl::Buffer,                     // buffer_t
                                                           clEvent_wrapper,                // even_t
                                                           opencl_host_buffer_wrapper<T>,  // host_memory_ptr_t
                                                           T
                                                         >
{
  // friend class parent
  friend class abstract::abstract_host_buffer< host_buffer<T>,                 // (required for static polymorphism)
                                               cl::Buffer,                     // host_memory_t
                                               cl::Buffer,                     // buffer_t
                                               clEvent_wrapper,                // even_t
                                               opencl_host_buffer_wrapper<T>,  // host_memory_ptr_t
                                               T
                                             >;
  
  private:
    // enabling easier access to parent's members
    using abstract::abstract_host_buffer< host_buffer<T>, cl::Buffer, cl::Buffer, clEvent_wrapper, opencl_host_buffer_wrapper<T>, T >::_size;
    using abstract::abstract_host_buffer< host_buffer<T>, cl::Buffer, cl::Buffer, clEvent_wrapper, opencl_host_buffer_wrapper<T>, T >::_host_memory;
    using abstract::abstract_host_buffer< host_buffer<T>, cl::Buffer, cl::Buffer, clEvent_wrapper, opencl_host_buffer_wrapper<T>, T >::_ptr_to_host_memory;
    using abstract::abstract_host_buffer< host_buffer<T>, cl::Buffer, cl::Buffer, clEvent_wrapper, opencl_host_buffer_wrapper<T>, T >::_unique_device_id_to_device_buffer;
    using abstract::abstract_host_buffer< host_buffer<T>, cl::Buffer, cl::Buffer, clEvent_wrapper, opencl_host_buffer_wrapper<T>, T >::_unique_device_id_to_dirty_flag;
    using abstract::abstract_host_buffer< host_buffer<T>, cl::Buffer, cl::Buffer, clEvent_wrapper, opencl_host_buffer_wrapper<T>, T >::_host_is_dirty;
    using abstract::abstract_host_buffer< host_buffer<T>, cl::Buffer, cl::Buffer, clEvent_wrapper, opencl_host_buffer_wrapper<T>, T >::_unique_device_id_to_command_queue_or_stream_id;
    using abstract::abstract_host_buffer< host_buffer<T>, cl::Buffer, cl::Buffer, clEvent_wrapper, opencl_host_buffer_wrapper<T>, T >::_id_of_last_active_device;
  
  
  public:
    host_buffer( size_t size = 0 )
      : abstract::abstract_host_buffer< host_buffer<T>, cl::Buffer, cl::Buffer, clEvent_wrapper, opencl_host_buffer_wrapper<T>, T >( size,
                                                                                                                                     opencl_platform_id_to_context.at( PLATFORM_ID_OF_HOST_DEVICE ),
                                                                                                                                     CL_MEM_READ_WRITE | CL_MEM_ALLOC_HOST_PTR,  // Note: "CL_MEM_ALLOC_HOST_PTR" causes memory to be allocated in host memory
                                                                                                                                     size * sizeof( T )
                                                                                                                                   )
    {}
  
  
    host_buffer( size_t size, T init_value )
      : abstract::abstract_host_buffer< host_buffer<T>, cl::Buffer, cl::Buffer, clEvent_wrapper, opencl_host_buffer_wrapper<T>, T >( size,
                                                                                                                                     opencl_platform_id_to_context.at( PLATFORM_ID_OF_HOST_DEVICE ),
                                                                                                                                     CL_MEM_READ_WRITE | CL_MEM_ALLOC_HOST_PTR,  // Note: "CL_MEM_ALLOC_HOST_PTR" causes memory to be allocated in host memory
                                                                                                                                     size * sizeof( T )
                                                                                                                                   )
    {
      if( init_value == 0 )
        std::memset( _ptr_to_host_memory.get(), 0, _size * sizeof(T) );

      else
        for( int i = 0 ; i < _size ; ++i )
          this->operator[]( i ) = init_value;      
    }
  

    host_buffer( const std::vector<T>& vec )
      : abstract::abstract_host_buffer< host_buffer<T>, cl::Buffer, cl::Buffer, clEvent_wrapper, opencl_host_buffer_wrapper<T>, T >( vec.size(),
                                                                                                                                     opencl_platform_id_to_context.at( PLATFORM_ID_OF_HOST_DEVICE ),
                                                                                                                                     CL_MEM_READ_WRITE | CL_MEM_ALLOC_HOST_PTR,  // Note: "CL_MEM_ALLOC_HOST_PTR" causes memory to be allocated in host memory
                                                                                                                                     vec.size() * sizeof( T )
                                                                                                                                   )
    {
      this->set_content( vec.data() ); 
    }



    // copy ctor instantiates shallow copy
    //host_buffer()                         = delete;  // Note: buffer are initialized with input size
    host_buffer( const host_buffer<T>&  ) = default; // Note: required to put buffers in std::vector
    host_buffer(       host_buffer<T>&& ) = default;

    host_buffer& operator=( const host_buffer<T>&  ) = default;
    host_buffer& operator=(       host_buffer<T>&& ) = default;


    // ----------------------------------------------------------
    //   get host memory pointer
    // ----------------------------------------------------------

    T* get_host_memory_ptr()
    {
      this->update_host_memory();
    
      // set only host buffer as up to date
      this->set_all_device_buffers_as_dirty();
      this->set_host_memory_as_up_to_date();
      
      return _ptr_to_host_memory;
    }


    // ----------------------------------------------------------
    //   set/get host memory (static polymorphism)
    // ----------------------------------------------------------

    void set_content( T* new_content )
    {
      // wait for critical events to finish
      this->sync_read_events_on_host_memory();
      this->sync_write_events_on_host_memory();

      // copy data
      std::memcpy( _ptr_to_host_memory, new_content, _size * sizeof( T ) );

      // set only host buffer as up to date
      this->set_all_device_buffers_as_dirty();
      this->set_host_memory_as_up_to_date();
    }
    
  
    std::vector<T> get_content()
    {
      // update host memory
      this->update_host_memory();
      
      // declare result vector
      std::vector<T> content;
      content.reserve( _size );
      
      // copy data
      content.insert( content.end(), &_ptr_to_host_memory[ 0 ], &_ptr_to_host_memory[ _size ] );
      
      return content;
    }

  
    // ------------------------------------------------------------------------------
    //   implicit cast to cl::Buffer (enables compatibility to OpenCL-Libraries)
    // ------------------------------------------------------------------------------

    operator cl::Buffer()
    {
      assert( _unique_device_id_to_device_buffer.find( _id_of_last_active_device ) != _unique_device_id_to_device_buffer.end() );
      
      auto stream_id = _unique_device_id_to_command_queue_or_stream_id.at( _id_of_last_active_device );
      
      return this->get_device_buffer_with_read_write_access( _id_of_last_active_device, stream_id );
    }
  
  private:
  
    // ----------------------------------------------------------------------------------------
    //   native Buffer management: creation and H2D/D2H data transfers (static polymorphism)
    // ----------------------------------------------------------------------------------------

    // creates an abstract device if the device does not already exists
    virtual void create_device_buffer_if_not_existent( int unique_device_id, int command_queue_id ) 
    {
      // create device buffer if it does not already exist
      if( _unique_device_id_to_device_buffer.find( unique_device_id ) == _unique_device_id_to_device_buffer.end() )
      {
        _unique_device_id_to_device_buffer[ unique_device_id ] = cl::Buffer( unique_device_id_to_opencl_context( unique_device_id ),
                                                                             CL_MEM_READ_WRITE,
                                                                             _size * sizeof( T )
                                                                           );
        _unique_device_id_to_dirty_flag[ unique_device_id ] = true;

        // set command queue id
        _unique_device_id_to_command_queue_or_stream_id[ unique_device_id ] = command_queue_id;
      }
    }
  
  
    virtual void copy_data_from_device_buffer_to_device_buffer( int device_id_of_source, int device_id_of_destination ) 
    {
      assert( (device_id_of_source != device_id_of_destination) && "makes no sense" );
      
      // copy data from device to device over host
      this->copy_data_from_device_buffer_to_host_memory( device_id_of_source      );
      
      this->sync_read_events_on_host_memory();
      this->sync_write_events_on_host_memory();
      
      this->copy_data_from_host_memory_to_device_buffer( device_id_of_destination );
    }

  
    virtual void copy_data_from_host_memory_to_device_buffer( int unique_device_id ) 
    {
      // get actual command queue of device "unique_device_id"
      auto  command_queue_id = _unique_device_id_to_command_queue_or_stream_id.at( unique_device_id );
      auto& command_queue    = unique_device_id_to_opencl_command_queues.at( unique_device_id ).at( command_queue_id );
  
      // get pointer to host memory
      auto random_unique_device_id_of_host_platform = get_random_unique_device_id_for_platform_id( PLATFORM_ID_OF_HOST_DEVICE );
      auto command_queue_of_host_device             = unique_device_id_to_actual_opencl_command_queue( random_unique_device_id_of_host_platform );

      auto ptr_to_host_memory = (T*) command_queue_of_host_device.enqueueMapBuffer( _host_memory,
                                                                                    CL_TRUE,
                                                                                    CL_MAP_READ,
                                                                                    0,               // offset
                                                                                    _size * sizeof( T )
                                                                                 );
      
      // copy data from host to device
      try
      {
        clEvent_wrapper event;
        command_queue.enqueueWriteBuffer( _unique_device_id_to_device_buffer.at( unique_device_id ),
                                          CL_FALSE,           // blocking write
                                          0,                  // offset
                                          _size * sizeof(T),  // data size
                                          ptr_to_host_memory, // data content,
                                          nullptr,            // event list
                                          &( event.get() )    // event
                                        );
        
        // unmap pointer
        command_queue_of_host_device.enqueueUnmapMemObject( _host_memory,
                                                            ptr_to_host_memory
                                                          );

        // set critical events
        this->set_event_device_buffer_of_device_is_written( event, unique_device_id );
        this->set_event_host_memory_is_read( event );
      }
      catch( cl::Error& err )
      {
        check_opencl_exception( err );
        throw;
      }
    }
  
  
    virtual void copy_data_from_device_buffer_to_host_memory( int unique_device_id ) 
    {
      assert( _unique_device_id_to_device_buffer.find( unique_device_id ) != _unique_device_id_to_device_buffer.end() );  // assert device buffer exists
      
      // get default command queue of device "unique_device_id"
      auto command_queue_id = _unique_device_id_to_command_queue_or_stream_id.at( unique_device_id );
      auto command_queue    = unique_device_id_to_opencl_command_queues.at( unique_device_id ).at( command_queue_id );
      
      // get pointer to host memory
      auto random_unique_device_id_of_host_platform = get_random_unique_device_id_for_platform_id( PLATFORM_ID_OF_HOST_DEVICE );
      auto command_queue_of_host_device             = unique_device_id_to_actual_opencl_command_queue( random_unique_device_id_of_host_platform );

      auto ptr_to_host_memory = (T*) command_queue_of_host_device.enqueueMapBuffer( _host_memory,
                                                                                    CL_TRUE,
                                                                                    CL_MAP_READ,
                                                                                    0,               // offset
                                                                                    _size * sizeof( T )
                                                                                 );
      
      // copy data from device to host
      try
      {
        clEvent_wrapper event;
        command_queue.enqueueReadBuffer( _unique_device_id_to_device_buffer.at( unique_device_id ),
                                         CL_FALSE,           // blocking write
                                         0,                  // offset
                                         _size * sizeof(T),  // data size
                                         ptr_to_host_memory, // data content
                                         nullptr,            // event list
                                         &( event.get() )    // event
                                        );
        
        // unmap pointer
        command_queue_of_host_device.enqueueUnmapMemObject( _host_memory,
                                                            ptr_to_host_memory
                                                          );
        
        this->set_event_device_buffer_of_device_is_read( event, unique_device_id );
        this->set_event_host_memory_is_written( event );
      }
      catch( cl::Error& err )
      {
        check_opencl_exception( err );
        throw;
      }
    }
};


} // namespace "ocl"

} // namespace "core"

} // namespace "ocal"

#endif /* ocl_host_buffer_h */
