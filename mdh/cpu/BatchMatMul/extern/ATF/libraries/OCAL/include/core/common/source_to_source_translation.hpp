//
//  source_to_source_translation.hpp
//  ocal
//
//  Created by Ari Rasch on 09.11.17.
//  Copyright © 2017 Ari Rasch. All rights reserved.
//

#ifndef source_to_source_translation_hpp
#define source_to_source_translation_hpp


namespace ocal
{

namespace core
{

namespace common
{


std::string opencl2cuda( std::string ocl_source ) // Note: requires helper functions to be tagged with the "inline" keyword
{
  // ocl source code to generate
  std::string cuda_source;

  // remove c-style comments out of source code
  cuda_source = common::remove_c_style_comments( ocl_source );

  // keyword replacements
  cuda_source = std::regex_replace( cuda_source, std::regex( "__kernel"                ), "__global__"      );
  cuda_source = std::regex_replace( cuda_source, std::regex( "restrict"                ), "__restrict__"    );
  cuda_source = std::regex_replace( cuda_source, std::regex( "barrier\\s*\\([^)]*\\s*" ), "__syncthreads( " );

  cuda_source = std::regex_replace( cuda_source, std::regex( "get_global_id\\s*\\(\\s*0\\s*\\)" ), "( blockIdx.x * blockDim.x + threadIdx.x )" );
  cuda_source = std::regex_replace( cuda_source, std::regex( "get_global_id\\s*\\(\\s*1\\s*\\)" ), "( blockIdx.y * blockDim.y + threadIdx.y )" );
  cuda_source = std::regex_replace( cuda_source, std::regex( "get_global_id\\s*\\(\\s*2\\s*\\)" ), "( blockIdx.z * blockDim.z + threadIdx.z )" );

  cuda_source = std::regex_replace( cuda_source, std::regex( "get_local_id\\s*\\(\\s*0\\s*\\)" ), "threadIdx.x" );
  cuda_source = std::regex_replace( cuda_source, std::regex( "get_local_id\\s*\\(\\s*1\\s*\\)" ), "threadIdx.y" );
  cuda_source = std::regex_replace( cuda_source, std::regex( "get_local_id\\s*\\(\\s*2\\s*\\)" ), "threadIdx.z" );

  cuda_source = std::regex_replace( cuda_source, std::regex( "get_group_id\\s*\\(\\s*0\\s*\\)" ), "blockIdx.x" );
  cuda_source = std::regex_replace( cuda_source, std::regex( "get_group_id\\s*\\(\\s*1\\s*\\)" ), "blockIdx.y" );
  cuda_source = std::regex_replace( cuda_source, std::regex( "get_group_id\\s*\\(\\s*2\\s*\\)" ), "blockIdx.z" );
  
  cuda_source = std::regex_replace( cuda_source, std::regex( "get_global_size\\s*\\(\\s*0\\s*\\)" ), "gridDim.x * blockDim.x" );
  cuda_source = std::regex_replace( cuda_source, std::regex( "get_global_size\\s*\\(\\s*1\\s*\\)" ), "gridDim.y * blockDim.y" );
  cuda_source = std::regex_replace( cuda_source, std::regex( "get_global_size\\s*\\(\\s*2\\s*\\)" ), "gridDim.z * blockDim.z" );

  cuda_source = std::regex_replace( cuda_source, std::regex( "get_local_size\\s*\\(\\s*0\\s*\\)" ), "blockDim.x" );
  cuda_source = std::regex_replace( cuda_source, std::regex( "get_local_size\\s*\\(\\s*1\\s*\\)" ), "blockDim.y" );
  cuda_source = std::regex_replace( cuda_source, std::regex( "get_local_size\\s*\\(\\s*2\\s*\\)" ), "blockDim.z" );

  cuda_source = std::regex_replace( cuda_source, std::regex( "get_num_groups\\s*\\(\\s*0\\s*\\)" ), "gridDim.x" );
  cuda_source = std::regex_replace( cuda_source, std::regex( "get_num_groups\\s*\\(\\s*1\\s*\\)" ), "gridDim.y" );
  cuda_source = std::regex_replace( cuda_source, std::regex( "get_num_groups\\s*\\(\\s*2\\s*\\)" ), "gridDim.z" );

  // add "extern "C""
  cuda_source = std::regex_replace( cuda_source, std::regex( "__global__" ), "extern \"C\" \n __global__" );
  
  // add "__device__" tag
  cuda_source = std::regex_replace( cuda_source, std::regex( "inline" ), "__device__ inline" );
  
  // remove address space qualifier
  cuda_source = std::regex_replace( cuda_source, std::regex( "__global\\s+" ), "" );
  cuda_source = std::regex_replace( cuda_source, std::regex( "__private"    ), "" );
  
  // handle shared memory - case: local array
  cuda_source = std::regex_replace( cuda_source, std::regex( "__local(.*\\;)" ), "__shared__ $1" ); // Note: replace "__local" by "__shared__" for local/shared arrays
  
  // handle shared memory - case: dynamic shared memory
  auto rgx                = std::regex( "__global__\\s+\\w+\\s+\\w+\\((?:.)*?__local\\s*(\\w*)\\s*\\*\\s*(\\w+)\\s*\\)" );
  auto rgx_iterator_begin = std::sregex_iterator( cuda_source.begin(), cuda_source.end(), rgx );
  auto rgx_iterator_end   = std::sregex_iterator();
  
  if( rgx_iterator_begin != rgx_iterator_end )
  {
    auto shared_var_type = ( *std::sregex_iterator( cuda_source.begin(), cuda_source.end(), rgx ) ).str( 1 );
    auto shared_var_name = ( *std::sregex_iterator( cuda_source.begin(), cuda_source.end(), rgx ) ).str( 2 );
    
    cuda_source = std::regex_replace( cuda_source, std::regex( "(__global__\\s*void\\s*\\w+\\s*\\([^)]*)\\s*\\,\\s*__local" ),
                                                               "$1" + std::string( " )" )
                                                             ); // Note: remove __shared__ parameter in global function, for this: match entire kernel signature except argument list's closing brace (correctness ensured only for one kernel per file)
    cuda_source = std::regex_replace( cuda_source, std::regex( "__global__[^\\{]*\\{"                                         ),
                                                               "$1" + std::string( "\n extern __shared__ " ) + shared_var_type + std::string( " " ) + shared_var_name + std::string( "[];" )
                                                             ); // Note: add line "extern __shared__ ..."
  }
  
  // handle shared memory - case: otherwise, e.g., in helper/device functions
  cuda_source = std::regex_replace( cuda_source, std::regex( "__local" ), "" ); // Note: remove all remaining __local tags, e.g., in helper/device functions

  
  // translate OpenCL built-in functions to CUDA
  // TODO

  return cuda_source;
}


std::string cuda2opencl( std::string cuda_source ) // Note: does currently not allow __device__ functions in "cuda_source" since, in OpenCL, function parameters require address space qualifier
{
  // ocl source code to generate
  std::string ocl_source;

  // remove c-style comments out of source code
  ocl_source = common::remove_c_style_comments( cuda_source );

  // remove "extern "C""
  ocl_source = std::regex_replace( ocl_source, std::regex( "extern \"C\"" ), "" );

  // keyword replacements
  ocl_source = std::regex_replace( ocl_source, std::regex( "__global__(?:\\s*static)*" ), "__kernel"                                              );
  ocl_source = std::regex_replace( ocl_source, std::regex( "__shared__"                ), "__local"                                               );
  ocl_source = std::regex_replace( ocl_source, std::regex( "__restrict__"              ), "restrict"                                              );
  ocl_source = std::regex_replace( ocl_source, std::regex( "__syncthreads()"           ), "barrier( CLK_GLOBAL_MEM_FENCE | CLK_LOCAL_MEM_FENCE )" );

  ocl_source = std::regex_replace( ocl_source, std::regex( "threadIdx.x" ), "get_local_id( 0 )" );
  ocl_source = std::regex_replace( ocl_source, std::regex( "threadIdx.y" ), "get_local_id( 1 )" );
  ocl_source = std::regex_replace( ocl_source, std::regex( "threadIdx.z" ), "get_local_id( 2 )" );

  ocl_source = std::regex_replace( ocl_source, std::regex( "blockIdx.x" ), "get_group_id( 0 )" );
  ocl_source = std::regex_replace( ocl_source, std::regex( "blockIdx.y" ), "get_group_id( 1 )" );
  ocl_source = std::regex_replace( ocl_source, std::regex( "blockIdx.z" ), "get_group_id( 2 )" );

  ocl_source = std::regex_replace( ocl_source, std::regex( "blockDim.x" ), "get_local_size( 0 )" );
  ocl_source = std::regex_replace( ocl_source, std::regex( "blockDim.y" ), "get_local_size( 1 )" );
  ocl_source = std::regex_replace( ocl_source, std::regex( "blockDim.z" ), "get_local_size( 2 )" );

  ocl_source = std::regex_replace( ocl_source, std::regex( "gridDim.x" ), "get_num_groups( 0 )" );
  ocl_source = std::regex_replace( ocl_source, std::regex( "gridDim.y" ), "get_num_groups( 1 )" );
  ocl_source = std::regex_replace( ocl_source, std::regex( "gridDim.z" ), "get_num_groups( 2 )" );

  // remove "__device__" tag (Note: currently needless since __device__ are not supported in OCAL)
  ocl_source = std::regex_replace( ocl_source, std::regex( "__device__" ), "" );

  // add address space qualifier "__global" to kernel arguments that have a pointer type
  ocl_source = std::regex_replace( ocl_source, std::regex( "(int\\s*?\\*|float\\s*?\\*|double\\s*?\\*)" ), "__global $1" );

  // handle dynamic shared memory
  auto rgx                 = std::regex( "extern __local\\s+(.*?)\\s+([^\\[]+)" );  // Note: at this point, "__shared__" has be replaced by "__local"
  auto rgx_iterator_begin  = std::sregex_iterator( ocl_source.begin(), ocl_source.end(), rgx );
  auto rgx_iterator_end    = std::sregex_iterator();
  
  if( rgx_iterator_begin != rgx_iterator_end )
  {
    auto shared_var_type = ( *std::sregex_iterator( ocl_source.begin(), ocl_source.end(), rgx ) ).str( 1 );
    auto shared_var_name = ( *std::sregex_iterator( ocl_source.begin(), ocl_source.end(), rgx ) ).str( 2 );
    
    ocl_source = std::regex_replace( ocl_source, std::regex( "(__kernel\\s*void\\s*\\w+\\s*\\([^)]*)\\s*" ),
                                                             "$1" + std::string( ", __local " ) + shared_var_type + std::string( "* " ) + shared_var_name + std::string(" ")
                                                           ); // Note: match entire kernel signature except argument list's closing brace (correctness ensured only for one kernel per file)
    ocl_source = std::regex_replace( ocl_source, std::regex( "extern __local.*\\n"                    ),
                                                             ""
                                                           ); // Note: remove line "extern __shared__ ..."
  }
  
  // translate CUDA built-in functions to OpenCL
  // TODO
  
  return ocl_source;
}


} // namespace "common"

} // namespace "core"

} // namespace "ocal"


#endif /* source_to_source_translation_hpp */
