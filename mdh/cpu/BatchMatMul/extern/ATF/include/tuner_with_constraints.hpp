//
//  tuner_with_constraints.hpp
//  new_atf_lib
//
//  Created by Ari Rasch on 21/03/2017.
//  Copyright © 2017 Ari Rasch. All rights reserved.
//

#ifndef tuner_with_constraints_h
#define tuner_with_constraints_h

#include <tuple>
#include <thread>

#include "tuner.hpp"

#include "helper.hpp"
#include "tp.hpp"

#include "abort_conditions.hpp"
#include "search_space_tree.hpp"

#include <netinet/in.h>
#include <sys/un.h>

#define INFINT_USE_EXCEPTIONS
#include "InfInt.h"
#undef  INFINT_USE_EXCEPTIONS

namespace atf
{

// helper
template< typename... TPs >
class G_class
{
  public:
    G_class( TPs&... tps );
  
    G_class()                 = delete;
    G_class( const G_class& ) = default;
    G_class(       G_class& ) = default;
  
    ~G_class() = default;
  
    auto tps() const;

  private:
    std::tuple<TPs&...> _tps;
  
};

template< typename... TPs >
auto G( TPs&... tps )
{
  return G_class<TPs...>( tps... );
}

// process wrapper helper
enum PROCESS_WRAPPER_TYPE {
    NONE, LOCAL, REMOTE
};
typedef union {
    struct sockaddr_in addr_in;
    struct sockaddr_un addr_un;
} addr;
  
class tuner_with_constraints : public tuner
{
  public:
    typedef std::tuple< size_t, size_t, std::chrono::high_resolution_clock::time_point, configuration, unsigned long long > history_entry; // entry: evaluations, valid evaluations, actual tuning runtime, configuration, configuration's cost
    typedef std::function<void()> progress_callback_t;
    typedef std::function<bool(atf::configuration&)> is_valid_type;

  public:
    template< typename abort_condition_t >
    tuner_with_constraints( const abort_condition_t& abort_condition, const std::string &logging_file = "", PROCESS_WRAPPER_TYPE wrapper_type = NONE, const std::string &wrapper_command = "", const is_valid_type &is_valid = {}, const bool& abort_on_error = false, const progress_callback_t &progress_callback = {}, unsigned long long callback_timeout = 600 );

    tuner_with_constraints( const bool& abort_on_error );
    tuner_with_constraints();


    tuner_with_constraints( const tuner_with_constraints& other ) // = default;
      :
      _logging_file(other._logging_file),
      _wrapper_type(other._wrapper_type),
      _wrapper_command(other._wrapper_command),
      _abort_condition( other._abort_condition->copy() ),
      _abort_on_error( other._abort_on_error ),
      _progress_callback( other._progress_callback ),
      _callback_timeout( other._callback_timeout ),
      _last_callback( other._last_callback ),
      _number_of_evaluated_configs( other._number_of_evaluated_configs ),
      _number_of_invalid_configs( other._number_of_invalid_configs ),
      _evaluations_required_to_find_best_found_result( other._evaluations_required_to_find_best_found_result  ),
      _valid_evaluations_required_to_find_best_found_result( other._valid_evaluations_required_to_find_best_found_result ),
      _history( other._history ),
      _threads()
    {}

    tuner_with_constraints( tuner_with_constraints&& other ) //= default;
      :
      _logging_file(other._logging_file),
      _wrapper_type(other._wrapper_type),
      _wrapper_command(other._wrapper_command),
      _abort_condition( other._abort_condition->copy() ),
      _abort_on_error( other._abort_on_error ),
      _progress_callback( other._progress_callback ),
      _callback_timeout( other._callback_timeout ),
      _last_callback( other._last_callback ),
      _number_of_evaluated_configs( other._number_of_evaluated_configs ),
      _number_of_invalid_configs( other._number_of_invalid_configs ),
      _evaluations_required_to_find_best_found_result( other._evaluations_required_to_find_best_found_result  ),
      _valid_evaluations_required_to_find_best_found_result( other._valid_evaluations_required_to_find_best_found_result ),
      _history( other._history ),
      _threads()
    {}
  
    virtual ~tuner_with_constraints();

    virtual std::string display_string() const = 0;

    // set tuning parameters
    template< typename... Ts, typename... range_ts, typename... callables >
    tuner_with_constraints& operator()( tp_t<Ts,range_ts,callables>&... tps );
  
    // first application of operator(): IS (required due to ambiguity)
    template< typename T, typename range_t, typename callable >
    tuner_with_constraints& operator()( tp_t<T,range_t,callable>& tp );
  

    // set tuning parameters
    template< typename... Ts, typename... G_CLASSES >
    tuner_with_constraints& operator()( G_class<Ts...> G_class, G_CLASSES... G_classes );

    template< typename callable >
//          typename T, typename range_t, typename tp_callable, std::enable_if_t<( !std::is_same<callable, tp_t<T,range_t,tp_callable> >::value )>* = nullptr >
    configuration operator()( const callable& program ); // program must take config_t and return a size_t
    template< typename callable >
//          typename T, typename range_t, typename tp_callable, std::enable_if_t<( !std::is_same<callable, tp_t<T,range_t,tp_callable> >::value )>* = nullptr >
    configuration operator()( callable& program ); // program must take config_t and return a size_t

    std::chrono::high_resolution_clock::time_point tuning_start_time() const;

    size_t constrained_search_space_size() const;
    InfInt unconstrained_search_space_size() const;
    size_t number_of_evaluated_configs()       const;
    size_t number_of_valid_evaluated_configs() const;
    size_t generation_time() const;

    unsigned long long best_measured_result()      const;
  
    configuration best_configuration() const;

    std::string display_string_with_abort_condition() const;

    void set_is_valid(const is_valid_type &is_valid);

    void set_progress_info_callback(const progress_callback_t &progress_callback);

    void set_progress_callback_timeout(unsigned long long callback_timeout);

    bool enable_process_wrapper(int argc, const char **argv);

    static bool is_process_wrapper(int argc, const char **argv) {
      bool is_wrapper = false;
      for (int i = 0; i < argc; ++i) {
        if (strcmp(argv[i], "process_wrapper") == 0) {
          is_wrapper = true;
          break;
        }
      }
      return is_wrapper;
    }

  private:
    template< typename... Ts, typename... rest_tp_tuples >
    void insert_tp_names_in_search_space( G_class<Ts...> tp_tuple, rest_tp_tuples... tuples );

    void insert_tp_names_in_search_space();

    template< typename... Ts, size_t... Is >
    void insert_tp_names_of_one_tree_in_search_space( G_class<Ts...> tp_tuple, std::index_sequence<Is...> );

    template< typename T, typename range_t, typename callable, typename... Ts >
    void insert_tp_names_of_one_tree_in_search_space( tp_t<T,range_t,callable>& tp, Ts&... tps );
  
    void insert_tp_names_of_one_tree_in_search_space();
  
    template< size_t TREE_ID, typename... Ts, typename... rest_tp_tuples>
    tuner_with_constraints& generate_config_trees( G_class<Ts...> tp_tuple, rest_tp_tuples... tuples );

    template< size_t TREE_ID, typename... Ts, size_t... Is, typename... rest_tp_tuples >
    tuner_with_constraints& generate_config_trees( G_class<Ts...> tp_tuple, std::index_sequence<Is...>, rest_tp_tuples... tuples );

    template< size_t TREE_ID >
    tuner_with_constraints& generate_config_trees();

    template< size_t TREE_ID, size_t TREE_DEPTH, typename T, typename range_t, typename callable, typename... Ts, std::enable_if_t<( TREE_DEPTH>0 )>* = nullptr >
    void generate_single_config_tree( tp_t<T,range_t,callable>& tp, Ts&... tps );
  
    template< size_t TREE_ID, size_t TREE_DEPTH, typename... Ts, std::enable_if_t<( TREE_DEPTH==0 )>* = nullptr >
    void generate_single_config_tree( const Ts&... values );

    // TODO: loeschen
    template< typename T, typename... Ts >
    void print_path(T val, Ts... tps);

    // TODO: loeschen
    void print_path();

  public:
    virtual void          initialize( const search_space& search_space) = 0;
    virtual void          finalize()                                    = 0;
    virtual configuration get_next_config()                             = 0;
    virtual void          report_result( const unsigned long long& result )         = 0;
  
  private:
    const std::string                  _logging_file;
    const PROCESS_WRAPPER_TYPE         _wrapper_type;
    std::string                        _wrapper_command;
    bool                               _is_wrapper;
    int                                _socket;
    std::string                        _server;
    std::map<std::string, std::string> _wrapper_configuration;
    unsigned short int                 _port;
    is_valid_type                      _is_valid;
    std::unique_ptr<cond::abort>       _abort_condition;
    const bool                         _abort_on_error;
    progress_callback_t                _progress_callback;
    unsigned long long                 _callback_timeout;
    std::chrono::high_resolution_clock::time_point _last_callback;

    size_t                             _number_of_evaluated_configs;
    size_t                             _number_of_invalid_configs;
    size_t                             _evaluations_required_to_find_best_found_result;
    size_t                             _valid_evaluations_required_to_find_best_found_result;
    size_t                             _generation_time;
    InfInt                             _unconstrained_search_space_size = "1";

    std::vector<history_entry>         _history; // history of best results
    
    std::vector<std::thread>           _threads;

  public:
    const std::vector<history_entry>& history() const;

  protected:
    search_space_tree                  _search_space;
    unsigned long long execute_wrapper(configuration &configuration,
                                       unsigned long long *compile_time = nullptr, int *error_code = nullptr);
};


} // namespace "atf"

#include "detail/tuner_with_constraints_def.hpp"

#endif /* tuner_with_constraints_h */
