//
// Created by Richard Schulze on 02.11.17.
//

#ifndef MD_BLAS_LOOP_GENERATOR_HPP
#define MD_BLAS_LOOP_GENERATOR_HPP

#include <string>
#include <tuple>
#include <functional>
#include <utility>
#include <iostream>

#include "macros.hpp"
#include "types.hpp"
#include "helper.hpp"
#include <chrono>
#include <future>

namespace md_hom {
namespace generator {

enum POSITIONING {
    OUTER_PREPEND,
    INNER_PREPEND,
    INNER_APPEND,
    OUTER_APPEND
};
enum FILTER_VALUE {
    FIRST,
    TOTAL_FIRST,
    LAST,
    TOTAL_LAST,
    ANY
};
template<unsigned int L_DIMS, unsigned int R_DIMS>
class loop_generator {
public:
    typedef std::function<std::string(LEVEL, const dimension_t&, char*, bool*)> generator;
    typedef std::function<bool(LEVEL, const dimension_t&, const std::vector<LEVEL>&, const std::vector<dimension_t>&)> filter;

    loop_generator(const unsigned int kernel, const macros<L_DIMS, R_DIMS> &macros, bool runtime_inputs,
                   const std::vector<LEVEL> &level,
                   const std::vector<dimension_t> &dimensions,
                   bool print_progress = false,
                   const std::vector<dimension_t> *l_dims_after_first_r_dim = nullptr,
#ifndef REDUCTION_COPY_COMPLETE
                   bool reduction_loops = false,
#endif
                   const std::vector<dimension_t> &skip_post_processing = {},
                   unsigned int thread_nesting = 0
    )
            : _macros(macros), _runtime_inputs(runtime_inputs), _level(level), _dimensions(dimensions), _print_progress(print_progress), _l_dims_after_first_r_dim(l_dims_after_first_r_dim),
#ifndef REDUCTION_COPY_COMPLETE
              _reduction_loops(reduction_loops),
#endif
              _skip_post_processing(skip_post_processing),
              _thread_nesting(thread_nesting)
    {
        if (_l_dims_after_first_r_dim != nullptr) {
            for (const auto dim : _dimensions)
                if (dim.type == DIM_TYPE::R) {
                    _first_r_dim_nr = dim.nr;
                    break;
                }
        }

        // add index variables initialization (all levels, all dimensions)
        _additional_code_outer_prepend.emplace_back([](LEVEL level, const dimension_t &dimension, const std::vector<LEVEL> &levels,
                                                       const std::vector<dimension_t> &dimensions) {
                                                        return true;
                                                    },
                                                    [&,kernel](auto level, const auto &dimension, char *phase, bool *first_iteration) -> std::string {
                                                        std::string code;
                                                        code.append(stringf("#if defined(CACHE_CB_OFFSET) && CACHE_CB_OFFSET != 0\n%c_cb_offset_%c_%d = %s * %s;\n#endif",
                                                                            lower_case(level), lower_case(dimension.type), dimension.nr,
                                                                            _macros.fu_id(kernel, parent_level(level), dimension),
                                                                            _macros.num_fu(kernel, level, dimension)
                                                        ));
                                                        return code;
                                                    });
        // add index variables update (all levels, all dimensions)
        _additional_code_inner_append.emplace_back([](LEVEL level, const dimension_t &dimension, const std::vector<LEVEL> &levels,
                                                      const std::vector<dimension_t> &dimensions) {
                                                       return true;
                                                   },
                                                   [&,kernel](auto level, const auto &dimension, char *phase, bool *first_iteration) -> std::string {
                                                       for (const auto p_level : parent_levels(level, true)) {
                                                           if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(p_level, dimension)] == 3) {
                                                               return "";
                                                           }
                                                       }
                                                       std::string code;
                                                       code.append(stringf("#if defined(CACHE_CB_OFFSET) && CACHE_CB_OFFSET != 0\n%c_cb_offset_%c_%d += GET_%s_SIZE_%c_%d * %s;\n#endif",
                                                                           lower_case(level), lower_case(dimension.type), dimension.nr,
                                                                           upper_case(long_level(parent_level(level))), dimension.type, dimension.nr,
                                                                           _macros.num_cached_iterations(kernel, level, dimension, phase)
                                                       ));
                                                       return code;
                                                   });
    }

    void add_code(const filter &filter_func, POSITIONING positioning, const generator &code_gen) {
        switch (positioning) {
            case OUTER_PREPEND:
                _additional_code_outer_prepend.emplace_back(filter_func, code_gen);
                break;
            case INNER_PREPEND:
                _additional_code_inner_prepend.emplace_back(filter_func, code_gen);
                break;
            case INNER_APPEND:
                // emplace as second to last because of index variable code_gen
                _additional_code_inner_append.insert(_additional_code_inner_append.end() - 1, std::pair<filter, generator>(filter_func, code_gen));
                break;
            case OUTER_APPEND:
                _additional_code_outer_append.emplace_back(filter_func, code_gen);
                break;
        }
    }

    std::string from(unsigned int kernel, LEVEL start_level, const dimension_t &start_dimension,
                     const generator &body, bool parentheses = true,
                     char *phase = nullptr, bool *first_iteration = nullptr) {
        char p[3 * (L_DIMS + R_DIMS)];
        for (int i = 0; i < 3 * (L_DIMS + R_DIMS); ++i) {
            p[i] = phase != nullptr ? phase[i] : 1;
        }
        bool fi[3 * (L_DIMS + R_DIMS)];
        if (first_iteration == nullptr) {
            memset(fi, 0, 3 * (L_DIMS + R_DIMS) * sizeof(bool));
            for (const auto r_dim : dim_range(0, R_DIMS)) {
                fi[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(LEVEL::GLOBAL, r_dim)] = true; // GLOBAL is always in first iteration
            }
        } else {
            memcpy(fi, first_iteration, 3 * (L_DIMS + R_DIMS) * sizeof(bool));
        }

        if (_level.empty() || _dimensions.empty()) {
            return body(start_level, start_dimension, phase, fi);
        }

        unsigned int level_nr = 0;
        unsigned int dimension_nr = 0;
        for (unsigned int i = 0; i < _level.size(); ++i) {
            if (start_level == _level[i]) {
                level_nr = i;
            }
        }
        for (unsigned int i = 0; i < _dimensions.size(); ++i) {
            if (start_dimension == _dimensions[i]) {
                dimension_nr = i;
            }
        }

        if (_print_progress) {
            std::cout << "kernel " << kernel << " [calculating work load ...";
            for (int i = 0; i < 45; ++i) std::cout << " ";
            std::cout << "] 0 %\r";
            std::cout.flush();
            from_impl(kernel, level_nr, dimension_nr, body, parentheses, p, fi, true);
        }
        _start = std::chrono::high_resolution_clock::now();
        auto tmp = from_impl(kernel, level_nr, dimension_nr, body, parentheses, p, fi, false);
        if (_print_progress) std::cout << std::endl;
        return tmp;
    }

    static std::string loop_variable(LEVEL level, const dimension_t &dimension) {
        return stringf("%c_step_%c_%d", lower_case(level), lower_case(dimension.type), dimension.nr);
    }

    static std::vector<std::string> loop_variable(const std::vector<LEVEL> &level, const std::vector<dimension_t> &dimension) {
        return multi_stringf("%c_step_%c_%d",
                             lower_case(level),
                             lower_case(split_dim_range(dimension).first),
                             split_dim_range(dimension).second);
    }

private:
    typedef std::vector<std::pair<filter, generator>> generator_vector;

    const macros<L_DIMS, R_DIMS>  &_macros;
    const bool                     _runtime_inputs;
    const std::vector<LEVEL>       _level;
    const std::vector<dimension_t> _dimensions;
    const std::vector<dimension_t> *_l_dims_after_first_r_dim;
#ifndef REDUCTION_COPY_COMPLETE
    const bool                     _reduction_loops;
#endif
    const std::vector<dimension_t> _skip_post_processing;
    const unsigned int             _thread_nesting;
    unsigned int                   _first_r_dim_nr;
    generator_vector               _additional_code_outer_prepend;
    generator_vector               _additional_code_inner_prepend;
    generator_vector               _additional_code_inner_append;
    generator_vector               _additional_code_outer_append;

    std::chrono::time_point<std::chrono::high_resolution_clock, std::chrono::nanoseconds> _start;
    const bool _print_progress;
    size_t _max_progress = 0;
    size_t _progress = 0;

    // helper for matching a filter struct with actual levels and dimensions
    bool matches(const filter &filter, LEVEL level, const dimension_t & dimension) {
        return filter(level, dimension, _level, _dimensions);
    }

    std::string from_impl(unsigned int kernel, unsigned int level_nr, unsigned int dimension_nr,
                          const generator &body, bool parentheses,
                          char *phase, bool *first_iteration, bool calc_max_progress) {
        std::string result;
        const auto& level = _level[level_nr];
        const auto& dimension = _dimensions[dimension_nr];

        bool first_iteration_adapted[3 * (L_DIMS + R_DIMS)];
        memcpy(first_iteration_adapted, first_iteration, 3 * (L_DIMS + R_DIMS) * sizeof(bool));
        first_iteration_adapted[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(level, dimension)] = true;

        bool my_dim_in_phase3 = false;
        LEVEL top_level_in_phase3;
        for (LEVEL p_level : parent_levels(level, true)) {
            if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(p_level, dimension)] == 3) {
                my_dim_in_phase3 = true;
                top_level_in_phase3 = p_level;
                break;
            }
        }

        unsigned int sub_level_nr = level_nr;
        unsigned int sub_dimension_nr = dimension_nr;
        if (level_nr != _level.size() - 1 || dimension_nr != _dimensions.size() - 1) {
            if (dimension_nr == _dimensions.size() - 1) {
                sub_dimension_nr = 0;
                ++sub_level_nr;
            } else {
                ++sub_dimension_nr;
            }
        }

        std::string moved_ifs_condition;
        if (_l_dims_after_first_r_dim != nullptr && level == LEVEL::PRIVATE && dimension.type == DIM_TYPE::R && dimension.nr == _first_r_dim_nr) {
            for (const auto dim : _dimensions) {
                if (dim.type == DIM_TYPE::R && dim.nr == _first_r_dim_nr) break;

                bool dim_in_phase3 = false;
                LEVEL dim_top_level_in_phase3;
                for (LEVEL p_level : parent_levels(level, true)) {
                    if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(p_level, dim)] == 3) {
                        dim_in_phase3 = true;
                        dim_top_level_in_phase3 = p_level;
                        break;
                    }
                }
                if (dim_in_phase3) {
                    if (!moved_ifs_condition.empty()) moved_ifs_condition += " && ";
                    if (dim_top_level_in_phase3 == LEVEL::PRIVATE) {
                        moved_ifs_condition.append(stringf("(%s < (%s + %s - 1) / %s)",
                                                           _macros.fu_id(kernel, parent_level(level), dim),
                                                           _macros.num_extra_elements(kernel, level, dim, phase),
                                                           _macros.num_fu(kernel, level, dim),
                                                           _macros.num_fu(kernel, level, dim)));
                    } else {
                        moved_ifs_condition.append(stringf("(%s < %s - %s * %s)",
                                                           _macros.fu_id(kernel, parent_level(level), dim),
                                                           _macros.num_extra_elements(kernel, dim_top_level_in_phase3, dim),
                                                           _macros.fu_id(kernel, parent_level(dim_top_level_in_phase3), dim),
                                                           _macros.num_fu(kernel, dim_top_level_in_phase3, dim)));
                    }
                }

            }
        }

        char phase_1[3 * (L_DIMS + R_DIMS)];
        for (int i = 0; i < 3 * (L_DIMS + R_DIMS); ++i) {
            phase_1[i] = phase != nullptr ? phase[i] : 1;
        }
        phase_1[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(level, dimension)] = 1;
        char phase_2[3 * (L_DIMS + R_DIMS)];
        for (int i = 0; i < 3 * (L_DIMS + R_DIMS); ++i) {
            phase_2[i] = phase != nullptr ? phase[i] : 1;
        }
        phase_2[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(level, dimension)] = 2;
        char phase_3[3 * (L_DIMS + R_DIMS)];
        for (int i = 0; i < 3 * (L_DIMS + R_DIMS); ++i) {
            phase_3[i] = phase != nullptr ? phase[i] : 1;
        }
        phase_3[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(level, dimension)] = 3;
        std::vector<std::future<std::string>> threads;

        auto phase_1_generator = [&]() {
            std::string result;
            std::string body_first;
            std::string body_rest;
            for (const auto &code_gen : _additional_code_inner_prepend) {
                if (matches(code_gen.first, level, dimension)) {
                    if (dimension.type == DIM_TYPE::R) {
                        if (calc_max_progress) {
                            ++_max_progress;
                        } else {
                            if (_print_progress) inc_progress(kernel);
                            body_first.append(code_gen.second(level, dimension, phase_1, first_iteration_adapted));
                            body_first.append("\n\n");
                        }
                    }
                    if (dimension.type == DIM_TYPE::L || !my_dim_in_phase3) {
                        if (calc_max_progress) {
                            ++_max_progress;
                        } else {
                            if (_print_progress) inc_progress(kernel);
                            body_rest.append(code_gen.second(level, dimension, phase_1, first_iteration));
                            body_rest.append("\n\n");
                        }
                    }
                }
            }
            if (level_nr == _level.size() - 1 && dimension_nr == _dimensions.size() - 1) {
                // reached end of hierarchy
#ifdef ADD_CB_INFO_COMMENTS
                for (const auto& dim : _dimensions) {
                for (const auto& l : _level) {
                    loop_body.append(stringf("// %c_cb_%c_%d: %s\n",
                                             l, dim.type, dim.nr,
                                             phase_1[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(l, dim)] != 1 ? "incomplete" : "complete"));
                }
            }
#endif
                if (dimension.type == DIM_TYPE::R) {
                    if (calc_max_progress) {
                        ++_max_progress;
                    } else {
                        if (_print_progress) inc_progress(kernel);
                        body_first.append(body(level, dimension, phase_1, first_iteration_adapted));
                    }
                }
                if (dimension.type == DIM_TYPE::L || !my_dim_in_phase3) {
                    if (calc_max_progress) {
                        ++_max_progress;
                    } else {
                        if (_print_progress) inc_progress(kernel);
                        body_rest.append(body(level, dimension, phase_1, first_iteration));
                    }
                }
            } else {
                if (dimension.type == DIM_TYPE::R) {
                    if (sub_dimension_nr == 0) {
                        body_first.append("\n");
                    }
                    body_first.append(from_impl(kernel, sub_level_nr, sub_dimension_nr, body, parentheses, phase_1, first_iteration_adapted, calc_max_progress));
                    if (sub_dimension_nr == 0) {
                        body_first.append("\n");
                    }
                }
                if (dimension.type == DIM_TYPE::L || !my_dim_in_phase3) {
                    if (sub_dimension_nr == 0) {
                        body_rest.append("\n");
                    }
                    body_rest.append(from_impl(kernel, sub_level_nr, sub_dimension_nr, body, parentheses, phase_1, first_iteration, calc_max_progress));
                    if (sub_dimension_nr == 0) {
                        body_rest.append("\n");
                    }
                }
            }
            for (const auto &code_gen : _additional_code_inner_append) {
                if (matches(code_gen.first, level, dimension)) {
                    if (dimension.type == DIM_TYPE::R) {
                        if (calc_max_progress) {
                            ++_max_progress;
                        } else {
                            if (_print_progress) inc_progress(kernel);
                            body_first.append("\n\n");
                            body_first.append(code_gen.second(level, dimension, phase_1, first_iteration_adapted));
                        }
                    }
                    if (dimension.type == DIM_TYPE::L || !my_dim_in_phase3) {
                        if (calc_max_progress) {
                            ++_max_progress;
                        } else {
                            if (_print_progress) inc_progress(kernel);
                            body_rest.append("\n\n");
                            body_rest.append(code_gen.second(level, dimension, phase_1, first_iteration));
                        }
                    }
                }
            }

            // process regular cache blocks
            if (my_dim_in_phase3) {
                if (!calc_max_progress) {
                    result.append(
                            stringf("// parent level in phase 3: process extra %s elements in dimension %c_%d with a subset of all FUs\n",
                                    lower_case(long_level(level)), dimension.type, dimension.nr));
                }
                for (const auto &code_gen : _additional_code_outer_prepend) {
                    if (matches(code_gen.first, level, dimension)) {
                        if (calc_max_progress) {
                            ++_max_progress;
                        } else {
                            if (_print_progress) inc_progress(kernel);
                            result.append(code_gen.second(level, dimension, phase_1, first_iteration));
                            result.append("\n\n");
                        }
                    }
                }

                if (!calc_max_progress && !moved_ifs_condition.empty()) {
                    // add moved ifs
                    result.append(stringf("#if L_REDUCTION == PRIVATE && (%s)\n", concat(multi_stringf("%s > 1", _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS))), " || ")));
                    result.append(stringf("if (%s) // moved ifs\n", moved_ifs_condition));
                    result.append("#endif\n");
                    result.append("{\n");
                }

                if (!calc_max_progress) {
                    if (R_DIMS > 0 && dimension.type == DIM_TYPE::L && _l_dims_after_first_r_dim != nullptr && level == LEVEL::PRIVATE && !contains(*_l_dims_after_first_r_dim, dimension)
                        #ifndef REDUCTION_COPY_COMPLETE
                        && !_reduction_loops
#endif
                            ) {
                        // move if to make barriers work
                        result.append(indent(stringf("#if L_REDUCTION > PRIVATE || (%s)\n", concat(multi_stringf("%s == 1", _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS))), " && ")), !moved_ifs_condition.empty()));
                    }
#ifndef REDUCTION_COPY_COMPLETE
                    if (!_reduction_loops || R_DIMS == 0 || dimension.type == DIM_TYPE::R || level != LEVEL::PRIVATE)
#endif
                    {
                        result.append(indent(stringf("if (%s < %s - %s * %s)\n",
                                                     _macros.fu_id(kernel, parent_level(level), dimension),
                                                     _macros.num_extra_elements(kernel, top_level_in_phase3, dimension),
                                                     _macros.fu_id(kernel, parent_level(top_level_in_phase3), dimension),
                                                     _macros.num_fu(kernel, top_level_in_phase3, dimension)), !moved_ifs_condition.empty()));
                    }
                    if (R_DIMS > 0 && dimension.type == DIM_TYPE::L && _l_dims_after_first_r_dim != nullptr && level == LEVEL::PRIVATE && !contains(*_l_dims_after_first_r_dim, dimension)
                        #ifndef REDUCTION_COPY_COMPLETE
                        && !_reduction_loops
#endif
                            ) {
                        // move if to make barriers work
                        result.append(indent("#endif\n", !moved_ifs_condition.empty()));
                    }
                    result.append(indent("{\n", !moved_ifs_condition.empty()));
                    if (dimension.type == DIM_TYPE::R) {
                        result.append(indent(stringf("size_t %s = 0;\n", loop_variable(level, dimension)), 1 + !moved_ifs_condition.empty()));
                        result.append(indent(body_first, 1 + !moved_ifs_condition.empty()));
                    } else {
                        result.append(indent(stringf("size_t %s = 0;\n", loop_variable(level, dimension)), 1 + !moved_ifs_condition.empty()));
                        result.append(indent(body_rest, 1 + !moved_ifs_condition.empty()));
                    }
                }

                if (!calc_max_progress) {
                    result.append(indent("\n}\n", !moved_ifs_condition.empty()));
                }

                if (!calc_max_progress && !moved_ifs_condition.empty()) {
                    result.append("}// moved ifs\n");
                }

                for (const auto &code_gen : _additional_code_outer_append) {
                    if (matches(code_gen.first, level, dimension)) {
                        if (calc_max_progress) {
                            ++_max_progress;
                        } else {
                            if (_print_progress) inc_progress(kernel);
                            result.append("\n\n");
                            result.append(code_gen.second(level, dimension, phase_1, first_iteration));
                        }
                    }
                }
                return result;
            }

            for (const auto &code_gen : _additional_code_outer_prepend) {
                if (matches(code_gen.first, level, dimension)) {
                    if (calc_max_progress) {
                        ++_max_progress;
                    } else {
                        if (_print_progress) inc_progress(kernel);
                        result.append(code_gen.second(level, dimension, phase_1, first_iteration));
                        result.append("\n\n");
                    }
                }
            }

            if (!calc_max_progress && !moved_ifs_condition.empty()) {
                // add moved ifs
                result.append(stringf("#if L_REDUCTION == PRIVATE && (%s)\n", concat(multi_stringf("%s > 1", _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS))), " || ")));
                result.append(stringf("if (%s) // moved ifs\n", moved_ifs_condition));
                result.append("#endif\n");
                result.append("{\n");
            }

            if (!calc_max_progress) {
                result.append(indent(stringf("// phase 1: process %s cached iterations in dimension %c_%d with all FUs\n",
                                             lower_case(long_level(level)), dimension.type, dimension.nr), !moved_ifs_condition.empty()));
                if (_runtime_inputs && (level == LEVEL::LOCAL || phase_1[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dimension)] == 2)) {
                    if (dimension.type == DIM_TYPE::R)
                        result.append(indent(stringf("if (%s > 0) {\n", _macros.num_steps(kernel, level, dimension, phase_1)), !moved_ifs_condition.empty()));
                } else {
                    result.append(indent(stringf("#if %s > 0\n", _macros.num_steps(kernel, level, dimension, phase_1)), !moved_ifs_condition.empty()));
                }
//            bool first_iteration_in_parent_level = true;
                if (dimension.type == DIM_TYPE::R) {
//                bool relevant = true;
//                for (const auto dim : _dimensions) {
//                    if (!relevant || dim.type != DIM_TYPE::R) continue;
//
//                    first_iteration_in_parent_level = first_iteration_in_parent_level && first_iteration[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dim)];
//                    if (dim.type == dimension.type && dim.nr == dimension.nr)
//                        relevant = false;
//                }
//            }
//            if (dimension.type == DIM_TYPE::R && first_iteration_in_parent_level) {
//                auto first_level_not_in_first_iter = get_first_parent_level_not_in_first_iteration<L_DIMS, R_DIMS>(parent_level(level), first_iteration);
//                if (first_level_not_in_first_iter != parent_level(level)) {
//                    // there is a parent level that is not in its first iteration
//                    // check if that level saves its results in memory different from this level
//                    result.append(stringf("#if %c_CB_RES_DEST_LEVEL > %c_CB_RES_DEST_LEVEL\n", first_level_not_in_first_iter, parent_level(level)));
//                }
                    result.append(indent(stringf("size_t %s = 0;\n", loop_variable(level, dimension)), !moved_ifs_condition.empty()));
                    result.append(indent(body_first, !moved_ifs_condition.empty()));
                    if (!_runtime_inputs || (level != LEVEL::LOCAL && phase_1[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dimension)] == 1)) {
                        result.append(indent(stringf("\n#if %s > 1", _macros.num_steps(kernel, level, dimension, phase_1)), !moved_ifs_condition.empty()));
                    }
                    result.append(indent(stringf("\nfor (%s = 1; %s < %s; ++%s)%s%s",
                                                 loop_variable(level, dimension),
                                                 loop_variable(level, dimension),
                                                 _macros.num_steps(kernel, level, dimension, phase_1),
                                                 loop_variable(level, dimension),
                                                 parentheses ? " {" : "",
                                                 body_rest.empty() && level_nr * _dimensions.size() + dimension_nr ==
                                                                      (_level.size() * _dimensions.size() - 1) ? "" : "\n"), !moved_ifs_condition.empty()));
                    result.append(indent(body_rest, 1 + !moved_ifs_condition.empty()));
                    if (parentheses) {
                        result.append(indent(stringf("\n} // end of \"%s\"-loop", loop_variable(level, dimension)), !moved_ifs_condition.empty()));
                    }
                    if (!_runtime_inputs || (level != LEVEL::LOCAL && phase_1[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dimension)] == 1)) {
                        result.append(indent("\n#endif", !moved_ifs_condition.empty()));
                    }
//                if (first_level_not_in_first_iter != parent_level(level)) {
//                    result.append("\n#else\n");
//                    result.append(stringf("for (size_t %s = 0; %s < %s; ++%s)%s%s",
//                                          loop_variable(level, dimension),
//                                          loop_variable(level, dimension),
//                                          _macros.num_steps(kernel, level, dimension, phase_1),
//                                          loop_variable(level, dimension),
//                                          parentheses ? " {" : "",
//                                          body_rest.empty() && level_nr * _dimensions.size() + dimension_nr ==
//                                                               (_level.size() * _dimensions.size() - 1) ? "" : "\n"));
//                    result.append(indent(body_rest, 1));
//                    if (parentheses) {
//                        result.append(stringf("\n} // end of \"%s\"-loop",
//                                              loop_variable(level, dimension)));
//                    }
//                    result.append("\n#endif");
//                }
                } else {
                    result.append(indent(stringf("for (size_t %s = 0; %s < %s; ++%s)%s%s",
                                                 loop_variable(level, dimension),
                                                 loop_variable(level, dimension),
                                                 _macros.num_steps(kernel, level, dimension, phase_1),
                                                 loop_variable(level, dimension),
                                                 parentheses ? " {" : "",
                                                 body_rest.empty() && level_nr * _dimensions.size() + dimension_nr ==
                                                                      (_level.size() * _dimensions.size() - 1) ? "" : "\n"), !moved_ifs_condition.empty()));
                    result.append(indent(body_rest, 1 + !moved_ifs_condition.empty()));
                    if (parentheses) {
                        result.append(indent(stringf("\n} // end of \"%s\"-loop",
                                                     loop_variable(level, dimension)), !moved_ifs_condition.empty()));
                    }
                }
                if (_runtime_inputs && (level == LEVEL::LOCAL || phase_1[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dimension)] == 2)) {
                    if (dimension.type == DIM_TYPE::R)
                        result.append(indent("\n}", !moved_ifs_condition.empty()));
                } else {
                    result.append(indent("\n#endif", !moved_ifs_condition.empty()));
                }
            }
            return result;
        };
        if (my_dim_in_phase3) {
            return phase_1_generator();
        }
        if (!calc_max_progress && level == LEVEL::LOCAL && dimension_nr < _thread_nesting) {
            threads.emplace_back(std::async(phase_1_generator));
        } else {
            result.append(phase_1_generator());
        }


        // phase 2: process extra iterations where all FUs participate
#ifdef PHASE_2
        auto phase_2_generator = [&] () {
            std::string result;
            std::string body_first;
            std::string body_rest;
            for (const auto &code_gen : _additional_code_inner_prepend) {
                if (matches(code_gen.first, level, dimension)) {
                    if (dimension.type == DIM_TYPE::R) {
                        if (calc_max_progress) {
                            ++_max_progress;
                        } else {
                            if (_print_progress) inc_progress(kernel);
                            body_first.append(code_gen.second(level, dimension, phase_2, first_iteration_adapted));
                            body_first.append("\n\n");
                        }
                    }
                    if (calc_max_progress) {
                        ++_max_progress;
                    } else {
                        if (_print_progress) inc_progress(kernel);
                        body_rest.append(code_gen.second(level, dimension, phase_2, first_iteration));
                        body_rest.append("\n\n");
                    }
                }
            }
            if (level_nr == _level.size() - 1 && dimension_nr == _dimensions.size() - 1) {
                // reached end of hierarchy
                if (dimension.type == DIM_TYPE::R) {
                    if (calc_max_progress) {
                        ++_max_progress;
                    } else {
                        if (_print_progress) inc_progress(kernel);
                        body_first.append(body(level, dimension, phase_2, first_iteration_adapted));
                    }
                }
                if (calc_max_progress) {
                    ++_max_progress;
                } else {
                    if (_print_progress) inc_progress(kernel);
                    body_rest.append(body(level, dimension, phase_2, first_iteration));
                }
            } else {
                if (dimension.type == DIM_TYPE::R) {
                    body_first.append(from_impl(kernel, sub_level_nr, sub_dimension_nr, body, parentheses, phase_2,
                                                first_iteration_adapted, calc_max_progress));
                }
                body_rest.append(
                        from_impl(kernel, sub_level_nr, sub_dimension_nr, body, parentheses, phase_2, first_iteration, calc_max_progress));
            }
            for (const auto &code_gen : _additional_code_inner_append) {
                if (matches(code_gen.first, level, dimension)) {
                    if (dimension.type == DIM_TYPE::R) {
                        if (calc_max_progress) {
                            ++_max_progress;
                        } else {
                            if (_print_progress) inc_progress(kernel);
                            body_first.append("\n\n");
                            body_first.append(code_gen.second(level, dimension, phase_2, first_iteration_adapted));
                        }
                    }
                    if (calc_max_progress) {
                        ++_max_progress;
                    } else {
                        if (_print_progress) inc_progress(kernel);
                        body_rest.append("\n\n");
                        body_rest.append(code_gen.second(level, dimension, phase_2, first_iteration));
                    }
                }
            }

            if (!calc_max_progress) {
                result.append(indent(stringf("\n// phase 2: process extra %s iterations in dimension %c_%d with all FUs",
                                             lower_case(long_level(level)), dimension.type, dimension.nr), !moved_ifs_condition.empty()));
                if (_runtime_inputs && (level == LEVEL::LOCAL || phase_1[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dimension)] == 2)) {
                    result.append(indent(stringf("\nif (%s > 0) {", _macros.num_extra_cached_iterations(kernel, level, dimension, phase_1)), !moved_ifs_condition.empty()));
                } else {
                    result.append(indent(stringf("\n#if %s > 0", _macros.num_extra_cached_iterations(kernel, level, dimension, phase_1)), !moved_ifs_condition.empty()));
                }

                result.append(indent("\n{", !moved_ifs_condition.empty()));
                result.append(indent(stringf("\nconst size_t %s = %s;",
                                             loop_generator<L_DIMS, R_DIMS>::loop_variable(level, dimension),
                                             _macros.num_steps(kernel, level, dimension, phase_1)), 1 + !moved_ifs_condition.empty()));
                if (dimension.type == DIM_TYPE::R) {
                    if (_runtime_inputs && (level == LEVEL::LOCAL || phase_1[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dimension)] == 2)) {
                        result.append(indent(stringf("\nif (%s == 0) {", _macros.num_steps(kernel, level, dimension, phase_1)), 1 + !moved_ifs_condition.empty()));
                    } else {
                        result.append(indent(stringf("\n#if %s == 0", _macros.num_steps(kernel, level, dimension, phase_1)), 1 + !moved_ifs_condition.empty())); // TODO can be improved for multiple R-Dims (check previous R-DIMS as well?)
                    }
                    result.append("\n").append(indent(body_first, 1 + !moved_ifs_condition.empty()));
                    if (_runtime_inputs && (level == LEVEL::LOCAL || phase_1[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dimension)] == 2)) {
                        result.append(indent("\n} else {", 1 + !moved_ifs_condition.empty()));
                    } else {
                        result.append(indent("\n#else", 1 + !moved_ifs_condition.empty()));
                    }
                }
                result.append("\n").append(indent(body_rest, 1 + !moved_ifs_condition.empty()));
                if (dimension.type == DIM_TYPE::R) {
                    if (_runtime_inputs && (level == LEVEL::LOCAL || phase_1[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dimension)] == 2)) {
                        result.append(indent("\n}", 1 + !moved_ifs_condition.empty()));
                    } else {
                        result.append(indent("\n#endif", 1 + !moved_ifs_condition.empty()));
                    }
                }
                result.append(indent("\n}", !moved_ifs_condition.empty()));
                if (_runtime_inputs && (level == LEVEL::LOCAL || phase_1[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dimension)] == 2)) {
                    result.append(indent("\n}", !moved_ifs_condition.empty()));
                } else {
                    result.append(indent("\n#endif", !moved_ifs_condition.empty()));
                }
            }
            return result;
        };
        if (!contains(_skip_post_processing, dimension)) {
            if (!calc_max_progress && level == LEVEL::LOCAL && dimension_nr < _thread_nesting) {
                threads.emplace_back(std::async(phase_2_generator));
            } else {
                result.append(phase_2_generator());
            }
        }
#endif

#ifdef PHASE_3
        if (level != LEVEL::PRIVATE) {
            auto phase_3_generator = [&] () {
                std::string result;
                std::string body_first;
                std::string body_rest;
                for (const auto &code_gen : _additional_code_inner_prepend) {
                    if (matches(code_gen.first, level, dimension)) {
                        if (dimension.type == DIM_TYPE::R) {
                            if (calc_max_progress) {
                                ++_max_progress;
                            } else {
                                if (_print_progress) inc_progress(kernel);
                                body_first.append(code_gen.second(level, dimension, phase_3, first_iteration_adapted));
                                body_first.append("\n\n");
                            }
                        }
                        if (calc_max_progress) {
                            ++_max_progress;
                        } else {
                            if (_print_progress) inc_progress(kernel);
                            body_rest.append(code_gen.second(level, dimension, phase_3, first_iteration));
                            body_rest.append("\n\n");
                        }
                    }
                }
                if (level_nr == _level.size() - 1 && dimension_nr == _dimensions.size() - 1) {
                    // reached end of hierarchy
                    if (dimension.type == DIM_TYPE::R) {
                        if (calc_max_progress) {
                            ++_max_progress;
                        } else {
                            if (_print_progress) inc_progress(kernel);
                            body_first.append(body(level, dimension, phase_3, first_iteration_adapted));
                        }
                    }
                    if (calc_max_progress) {
                        ++_max_progress;
                    } else {
                        if (_print_progress) inc_progress(kernel);
                        body_rest.append(body(level, dimension, phase_3, first_iteration));
                    }
                } else {
                    if (dimension.type == DIM_TYPE::R) {
                        body_first.append(from_impl(kernel, sub_level_nr, sub_dimension_nr, body, parentheses, phase_3, first_iteration_adapted, calc_max_progress));
                    }
                    body_rest.append(from_impl(kernel, sub_level_nr, sub_dimension_nr, body, parentheses, phase_3, first_iteration, calc_max_progress));
                }
                for (const auto &code_gen : _additional_code_inner_append) {
                    if (matches(code_gen.first, level, dimension)) {
                        if (dimension.type == DIM_TYPE::R) {
                            if (calc_max_progress) {
                                ++_max_progress;
                            } else {
                                if (_print_progress) inc_progress(kernel);
                                body_first.append("\n\n");
                                body_first.append(code_gen.second(level, dimension, phase_3, first_iteration_adapted));
                            }
                        }
                        if (calc_max_progress) {
                            ++_max_progress;
                        } else {
                            if (_print_progress) inc_progress(kernel);
                            body_rest.append("\n\n");
                            body_rest.append(code_gen.second(level, dimension, phase_3, first_iteration));
                        }
                    }
                }

                if (!calc_max_progress) {
                    result.append(indent(stringf("\n// phase 3: process extra %s elements in dimension %c_%d with a subset of all FUs",
                                                 lower_case(long_level(level)), dimension.type, dimension.nr), !moved_ifs_condition.empty()));
                    if (_runtime_inputs && (level == LEVEL::LOCAL || phase_1[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dimension)] == 2)) {
                        result.append(indent(stringf("\nif (%s > 0) {", _macros.num_extra_elements(kernel, level, dimension, phase_1)), !moved_ifs_condition.empty()));
                    } else {
                        result.append(indent(stringf("\n#if %s > 0", _macros.num_extra_elements(kernel, level, dimension, phase_1)), !moved_ifs_condition.empty()));
                    }

                    if (R_DIMS > 0 && dimension.type == DIM_TYPE::L && _l_dims_after_first_r_dim != nullptr && level == LEVEL::PRIVATE && !contains(*_l_dims_after_first_r_dim, dimension)
#ifndef REDUCTION_COPY_COMPLETE
                        && !_reduction_loops
#endif
                            ) {
                        // move if to make barriers work
                        result.append(stringf("\n#if L_REDUCTION > PRIVATE || (%s)", concat(multi_stringf("%s == 1", _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS))), " && ")));
                    }
#ifndef REDUCTION_COPY_COMPLETE
                    if (!_reduction_loops || R_DIMS == 0 || dimension.type == DIM_TYPE::R || level != LEVEL::PRIVATE)
#endif
                    {
                        result.append(indent(stringf("\nif (%s < (%s + %s - 1) / %s)",
                                                     _macros.fu_id(kernel, parent_level(level), dimension),
                                                     _macros.num_extra_elements(kernel, level, dimension, phase_1),
                                                     _macros.num_fu(kernel, level, dimension),
                                                     _macros.num_fu(kernel, level, dimension)), !moved_ifs_condition.empty()));
                    }
                    if (R_DIMS > 0 && dimension.type == DIM_TYPE::L && _l_dims_after_first_r_dim != nullptr && level == LEVEL::PRIVATE && !contains(*_l_dims_after_first_r_dim, dimension)
#ifndef REDUCTION_COPY_COMPLETE
                        && !_reduction_loops
#endif
                            ) {
                        // move if to make barriers work
                        result.append("\n#endif");
                    }
                    result.append("\n{");
                    result.append(
                            indent(stringf("\n// set step variable for access to res buffer\nconst size_t %s = %s + (%s > 0);",
                                           loop_generator<L_DIMS, R_DIMS>::loop_variable(level, dimension),
                                           _macros.num_steps(kernel, level, dimension, phase_1),
                                           _macros.num_extra_cached_iterations(kernel, level, dimension, phase_1)
                            ), 1));
                    if (dimension.type == DIM_TYPE::R) {
                        if (_runtime_inputs && (level == LEVEL::LOCAL || phase_1[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dimension)] == 2)) {
                            result.append(indent(stringf("\nif (%s == 0 && %s == 0) {",
                                                         _macros.num_steps(kernel, level, dimension, phase_1),
                                                         _macros.num_extra_cached_iterations(kernel, level, dimension, phase_1)), 1));
                        } else {
                            result.append(indent(stringf("\n#if %s == 0 && %s == 0",
                                                         _macros.num_steps(kernel, level, dimension, phase_1),
                                                         _macros.num_extra_cached_iterations(kernel, level, dimension, phase_1)), 1)); // TODO can be improved for multiple R-Dims (check previous R-DIMS as well?)
                        }
                        result.append("\n").append(indent(body_first, 1));
                        if (_runtime_inputs && (level == LEVEL::LOCAL || phase_1[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dimension)] == 2)) {
                            result.append(indent("\n} else {", 1));
                        } else {
                            result.append(indent("\n#else", 1));
                        }
                    }
                    result.append("\n").append(indent(body_rest, 1));
                    if (dimension.type == DIM_TYPE::R) {
                        if (_runtime_inputs && (level == LEVEL::LOCAL || phase_1[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dimension)] == 2)) {
                            result.append(indent("\n}", 1));
                        } else {
                            result.append(indent("\n#endif", 1));
                        }
                    }
                    result.append(indent("\n}", !moved_ifs_condition.empty()));
                    if (_runtime_inputs && (level == LEVEL::LOCAL || phase_1[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dimension)] == 2)) {
                        result.append(indent("\n}", !moved_ifs_condition.empty()));
                    } else {
                        result.append(indent("\n#endif", !moved_ifs_condition.empty()));
                    }
                }
                return result;
            };
            if (!contains(_skip_post_processing, dimension)) {
                if (!calc_max_progress && level == LEVEL::LOCAL && dimension_nr < _thread_nesting) {
                    threads.emplace_back(std::async(phase_3_generator));
                } else {
                    result.append(phase_3_generator());
                }
            }
        }
#endif

        for (auto &f : threads) {
            result.append(f.get());
        }

        if (!calc_max_progress && !moved_ifs_condition.empty()) {
            result.append("\n}// moved ifs");
        }

        for (const auto &code_gen : _additional_code_outer_append) {
            if (matches(code_gen.first, level, dimension)) {
                if (calc_max_progress) {
                    ++_max_progress;
                } else {
                    if (_print_progress) inc_progress(kernel);
                    result.append("\n\n");
                    result.append(code_gen.second(level, dimension, phase, first_iteration));
                }
            }
        }
        return result;
    }

    void inc_progress(int kernel) {
        ++_progress;
        float progress = float(_progress) / _max_progress;
        auto eta = std::chrono::seconds(int((1 / progress - 1) * std::chrono::duration_cast<std::chrono::seconds>(std::chrono::high_resolution_clock::now() - _start).count()));
        int barWidth = 70;
        std::cout << "kernel " << kernel << " [";
        int pos = barWidth * progress;
        for (int i = 0; i < barWidth; ++i) {
            if (i < pos) std::cout << "=";
            else if (i == pos) std::cout << ">";
            else std::cout << " ";
        }
        std::cout << "] " << int(progress * 100.0) << " % (~ " << stringf("%.1f", eta.count() / 60.0f) << " min left)\r";
        std::cout.flush();
    }
};

}
}

#endif //MD_BLAS_LOOP_GENERATOR_HPP
