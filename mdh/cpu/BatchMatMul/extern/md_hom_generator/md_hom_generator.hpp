//
// Created by Richard Schulze on 31.10.2017.
//

#define PHASE_2
#define PHASE_3

//#define PRINT_ITERATION_STEP
//#define PRINT_CACHING_DEBUG_SWAP_PTRS
//#define PRINT_CACHING_DEBUG_LOAD_CB
//#define PRINT_CACHING_DEBUG_INDIVIDUAL_ELEMENTS
//#define PRINT_CACHING_DEBUG_SCALAR_FUNCTION_CB
//#define PRINT_CACHING_DEBUG_SCALAR_FUNCTION_ELEMENTS
//#define PRINT_BARRIER

//#define ADD_CB_INFO_COMMENTS
//#define REDUCTION_COPY_COMPLETE

#ifndef MD_BLAS_MD_HOM_HPP
#define MD_BLAS_MD_HOM_HPP

#include "include/helper.hpp"
#include "include/input_buffer.hpp"
#include "include/input_buffer_wrapper.hpp"
#include "include/input_scalar.hpp"
#include "include/input_scalar_wrapper.hpp"
#include "include/input_wrapper.hpp"
#include "include/loop_generator.hpp"
#include "include/macros.hpp"
#include "include/md_hom.hpp"
#include "include/ocl_generator.hpp"
#include "include/result_buffer.hpp"
#include "include/scalar_function.hpp"
#include "include/types.hpp"
#include "include/configuration_generator.hpp"

#endif //MD_BLAS_MD_HOM_HPP
