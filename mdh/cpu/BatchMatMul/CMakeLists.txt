project(md_hom_strided_v3)
cmake_minimum_required(VERSION 3.12)

set(CMAKE_CXX_STANDARD 17)

set(CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/cmake/Modules")

# add extern folder to include paths
include_directories(extern)

# add data_util library
add_subdirectory(extern/data_util)

# OpenCL
find_package(OpenCL REQUIRED)

# add argument parser library
include_directories(extern/argument_parser)

# add md_hom_generator library
add_subdirectory(extern/md_hom_generator)

# add ATF library
add_subdirectory(extern/ATF)
include_directories(${ATF_INCLUDE_DIRS})

# add google sheets helper
include_directories(extern/google_sheets)

# add executable
add_executable(generate_md_hom_strided_v3_batch_matmul src/generate.cpp)
target_link_libraries(generate_md_hom_strided_v3_batch_matmul md_hom_generator data_util)
add_executable(gold_md_hom_strided_v3_batch_matmul src/gold.cpp)
target_link_libraries(gold_md_hom_strided_v3_batch_matmul data_util ${OpenCL_LIBRARY})
add_executable(tune_md_hom_strided_v3_batch_matmul src/tune.cpp)
target_link_libraries(tune_md_hom_strided_v3_batch_matmul data_util ${ATF_LIBRARIES})
add_executable(bench_md_hom_strided_v3_batch_matmul src/bench.cpp)
target_link_libraries(bench_md_hom_strided_v3_batch_matmul data_util ${OpenCL_LIBRARY})
