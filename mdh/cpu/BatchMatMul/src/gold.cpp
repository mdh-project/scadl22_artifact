#include "data_util/include/data_util.hpp"
#include "tclap/CmdLine.h"
#include "OCAL/ocal_ocl.hpp"

#include "meta_info.cpp"
#include "data_info.cpp"
#include "gold_info.cpp"

#include <fstream>
#include <signal.h>

void termination_handler(int sig, siginfo_t *info, void *ucontext) {
    if (sig == SIGFPE) {
        std::cerr << "caught SIGFPE with error code ";
        switch(info->si_code) {
            case FPE_INTDIV:
                std::cerr << "FPE_INTDIV (integer divide by zero)";
                break;
            case FPE_INTOVF:
                std::cerr << "FPE_INTOVF (integer overflow)";
                break;
            case FPE_FLTDIV:
                std::cerr << "FPE_FLTDIV (floating-point divide by zero)";
                break;
            case FPE_FLTOVF:
                std::cerr << "FPE_FLTOVF (floating-point overflow)";
                break;
            case FPE_FLTUND:
                std::cerr << "FPE_FLTUND (floating-point underflow)";
                break;
            case FPE_FLTRES:
                std::cerr << "FPE_FLTRES (floating-point inexact result)";
                break;
            case FPE_FLTINV:
                std::cerr << "FPE_FLTINV (floating-point invalid operation)";
                break;
            case FPE_FLTSUB:
                std::cerr << "FPE_FLTSUB (subscript out of range)";
                break;
            default:
                std::cerr << "unknown";
                break;
        }
        std::cerr << std::endl;
        exit(sig);
    }
}

int main(int argc, const char **argv) {
    // define command line arguments
    TCLAP::CmdLine cmd("Calculate gold results.");

    TCLAP::ValueArg<bool> catch_sigfpe_arg("", "catch-sigfpe", "Whether to catch the SIGFPE signal. Can be useful for checking whether the gold file contains inaccuracies.", false, false, "bool");
    cmd.add(catch_sigfpe_arg);

    TCLAP::ValueArg<int> input_size_l_1_arg("", "input-size-l-1", "Input size in dimension L_1.", true, 0, "int");
    cmd.add(input_size_l_1_arg);

    TCLAP::ValueArg<int> input_size_l_2_arg("", "input-size-l-2", "Input size in dimension L_2.", true, 0, "int");
    cmd.add(input_size_l_2_arg);

    TCLAP::ValueArg<int> input_size_l_3_arg("", "input-size-l-3", "Input size in dimension L_3.", true, 0, "int");
    cmd.add(input_size_l_3_arg);

    TCLAP::ValueArg<int> input_size_l_4_arg("", "input-size-l-4", "Input size in dimension L_4.", true, 0, "int");
    cmd.add(input_size_l_4_arg);

    TCLAP::ValueArg<int> input_size_r_1_arg("", "input-size-r-1", "Input size in dimension R_1.", true, 0, "int");
    cmd.add(input_size_r_1_arg);

    // add custom arguments
    add_arguments(cmd);

    // parse arguments
    try {
        cmd.parse(argc, argv);
    } catch (TCLAP::ArgException &e) {
        std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
    }

    // register signal handler for SIGFPE to catch calculation errors
    if (catch_sigfpe_arg.getValue()) {
        struct sigaction new_action, old_action;
        new_action.sa_sigaction = termination_handler;
        sigemptyset(&new_action.sa_mask);
        new_action.sa_flags = SA_SIGINFO;
        sigaction(SIGFPE, NULL, &old_action);
        if (old_action.sa_handler != SIG_IGN)
            sigaction(SIGFPE, &new_action, NULL);
    }

    // prepare directory for gold results
    std::string application = "md_hom";
    std::string version = "strided_v3";
    std::string routine = "batch_matmul";
    int input_size_l_1 = input_size_l_1_arg.getValue();
    int input_size_l_2 = input_size_l_2_arg.getValue();
    int input_size_l_3 = input_size_l_3_arg.getValue();
    int input_size_l_4 = input_size_l_4_arg.getValue();
    int input_size_r_1 = input_size_r_1_arg.getValue();
    std::string input_size = std::to_string(input_size_l_1) + "x" + std::to_string(input_size_l_2) + "x" + std::to_string(input_size_l_3) + "x" + std::to_string(input_size_l_4) + "x" + std::to_string(input_size_r_1);
    std::vector<std::string> path = {"gold", application, version, routine, input_size};
    gold_data_path(path);
    std::string data_dir = "../" + data_directory(path);
    prepare_data_directory(data_dir);

    init_data(std::vector<int>({input_size_l_1_arg.getValue()}), std::vector<int>({input_size_l_2_arg.getValue()}), std::vector<int>({input_size_l_3_arg.getValue()}), std::vector<int>({input_size_l_4_arg.getValue()}), std::vector<int>({input_size_r_1_arg.getValue()}));
    calculate_gold(data_dir, input_size_l_1_arg.getValue(), input_size_l_2_arg.getValue(), input_size_l_3_arg.getValue(), input_size_l_4_arg.getValue(), input_size_r_1_arg.getValue());
}