#!/usr/bin/env bash

csv_file=$1
config_nr=$2
config_json=$3

# get csv config data
csv_header=`cat $csv_file | head -n 1 | cut -d";" -f7-`
csv_data=`cat $csv_file | head -n $((config_nr)) | tail -n -1 | cut -d";" -f7-`

# convert csv data to json
IFS=';' read -ra split_header <<< "$csv_header"
IFS=';' read -ra split_data <<< "$csv_data"
echo "{" > $config_json
for (( i=0; i<${#split_header[@]}; i++ )); do
  [ $i -gt 0 ] && echo "," >> $config_json
  printf "    \"${split_header[i]}\": ${split_data[i]}" >> $config_json
done
printf "\n}" >> $config_json