//
// Created by Richard Schulze on 01.11.2017.
//

#ifndef MD_BLAS_OCL_GENERATOR_HPP
#define MD_BLAS_OCL_GENERATOR_HPP

#include <memory>

#include "helper.hpp"
#include "md_hom.hpp"
#include "input_buffer.hpp"
#include "input_buffer_wrapper.hpp"
#include "input_wrapper.hpp"
#include "loop_generator.hpp"
#include "macros.hpp"
#include "result_buffer.hpp"
#include "types.hpp"
#include "result_buffer_wrapper.hpp"
#include "input_scalar_wrapper.hpp"

namespace md_hom {
namespace generator {


template <unsigned int L_DIMS, unsigned int R_DIMS, typename TOs, typename... TIs>
class ocl_generator_class {
public:
    ocl_generator_class(const md_hom_class<L_DIMS, R_DIMS, TOs, TIs...> &md_hom, bool runtime_inputs, const std::vector<dimension_t> &skip_post_processing, unsigned int thread_nesting, bool cuda)
            : _md_hom(md_hom), _runtime_inputs(runtime_inputs), _cuda(cuda), _macros(_runtime_inputs), _input_wrappers_1(), _input_names_1(),
              _result_wrapper(md_hom.outputs(), _macros, _md_hom.dimension_order(), runtime_inputs, skip_post_processing, _cuda) {
        wrap_inputs_1(md_hom.inputs(), std::make_index_sequence<sizeof...(TIs)>());
        get_input_names_and_definitions_1(md_hom.inputs(), std::make_index_sequence<sizeof...(TIs)>());
        get_output_names_and_definitions_1(md_hom.outputs(), std::make_index_sequence<std::tuple_size<TOs>::value>());
        wrap_inputs_2(md_hom.outputs(), std::make_index_sequence<std::tuple_size<TOs>::value>());
        get_input_names_and_definitions_2(md_hom.outputs(), std::make_index_sequence<std::tuple_size<TOs>::value>());
        get_output_names_and_definitions_2(md_hom.outputs(), std::make_index_sequence<std::tuple_size<TOs>::value>());

        bool r_dim_found = false;
        for (const auto &dim : _md_hom.dimension_order()) {
            if (dim.type == DIM_TYPE::R) {
                r_dim_found = true;
            } else if (r_dim_found) {
                _l_dims_after_first_r_dim.push_back(dim);
            }
        }

        for (unsigned int kernel = 1; kernel <= 1 + (R_DIMS > 0); ++kernel) {
            std::cout << "kernel " << kernel << " [generating macro definitions ...";
            for (int i = 0; i < 38; ++i) std::cout << " ";
            std::cout << "] 0 %\r";
            std::cout.flush();
            auto& kernel_source = kernel == 1 ? _kernel_1 : _kernel_2;

            std::stringstream ocl_function_macros;
            if (_cuda) {
                for (unsigned int ocl_dim = 2; ocl_dim < L_DIMS + R_DIMS; ++ocl_dim) {
                    bool first = true;
                    for (const auto &dim : dim_range(L_DIMS, R_DIMS)) {
                        ocl_function_macros << std::endl << stringf(
                                "%s OCL_DIM_%c_%d == %d\n#define NUM_WG_%d %s\n#define NUM_WI_%d NUM_WI_%c_%d",
                                first ? "#if  " : "#elif",
                                dim.type, dim.nr,
                                ocl_dim,
                                ocl_dim,
                                kernel == 1 || dim.type == DIM_TYPE::L ? stringf("NUM_WG_%c_%d", dim.type, dim.nr) : "1",
                                ocl_dim,
                                dim.type, dim.nr
                        );
                        first = false;
                    }
                    ocl_function_macros << std::endl << "#endif";
                }
                for (const auto &dim : dim_range(L_DIMS, R_DIMS)) {
                    for (int ocl_dim = 0; ocl_dim < 2; ++ocl_dim) {
                        ocl_function_macros << stringf(R"(
%s OCL_DIM_%c_%d == %d
#define GET_GLOBAL_ID_%c_%d   (blockIdx.%c * blockDim.%c + threadIdx.%c)
#define GET_LOCAL_ID_%c_%d    threadIdx.%c
#define GET_GROUP_ID_%c_%d    blockIdx.%c
#define GET_GLOBAL_SIZE_%c_%d %s
#define GET_LOCAL_SIZE_%c_%d  NUM_WI_%c_%d)",
                                                       ocl_dim == 0 ? "#if" : "#elif",
                                                       dim.type, dim.nr,
                                                       ocl_dim,

                                                       dim.type, dim.nr, ocl_dim == 0 ? 'x' : (ocl_dim == 1 ? 'y' : 'z'), ocl_dim == 0 ? 'x' : (ocl_dim == 1 ? 'y' : 'z'), ocl_dim == 0 ? 'x' : (ocl_dim == 1 ? 'y' : 'z'),
                                                       dim.type, dim.nr, ocl_dim == 0 ? 'x' : (ocl_dim == 1 ? 'y' : 'z'),
                                                       dim.type, dim.nr, ocl_dim == 0 ? 'x' : (ocl_dim == 1 ? 'y' : 'z'),
                                                       dim.type, dim.nr,
                                                       kernel == 1 || dim.type == DIM_TYPE::L
                                                       ? stringf(
                                                               "(NUM_WG_%c_%d * NUM_WI_%c_%d)",
                                                               dim.type, dim.nr,
                                                               dim.type, dim.nr
                                                       )
                                                       : stringf(
                                                               "NUM_WI_%c_%d",
                                                               dim.type, dim.nr
                                                       ),
                                                       dim.type, dim.nr,
                                                       dim.type, dim.nr
                        );
                    }
                    if (L_DIMS + R_DIMS > 2) {
                        ocl_function_macros << "\n#else";
                        if (kernel == 1 || dim.type == DIM_TYPE::L) {
                            ocl_function_macros << stringf("\n#define GET_GLOBAL_SIZE_%c_%d (NUM_WG_%c_%d * NUM_WI_%c_%d)",
                                                           dim.type, dim.nr,
                                                           dim.type, dim.nr,
                                                           dim.type, dim.nr
                            );
                        } else {
                            ocl_function_macros << stringf("\n#define GET_GLOBAL_SIZE_%c_%d NUM_WI_%c_%d",
                                                           dim.type, dim.nr,
                                                           dim.type, dim.nr
                            );
                        }
                        ocl_function_macros << stringf("\n#define GET_LOCAL_SIZE_%c_%d  NUM_WI_%c_%d",
                                                       dim.type, dim.nr,
                                                       dim.type, dim.nr
                        );
                        bool first = true;
                        for (unsigned int ocl_dim = 2; ocl_dim < L_DIMS + R_DIMS; ++ocl_dim) {
                            ocl_function_macros << std::endl << stringf(
                                    "%s OCL_DIM_%c_%d == %d",
                                    first ? "#if  " : "#elif",
                                    dim.type, dim.nr,
                                    ocl_dim
                            );
                            ocl_function_macros << stringf("\n#define GET_GLOBAL_ID_%c_%d    ((blockIdx.z * blockDim.z + threadIdx.z)%s%s)",
                                                           dim.type, dim.nr,
                                                           ocl_dim > 2 ? stringf(" %% (%s)", concat(multi_stringf("NUM_WI_%d * NUM_WG_%d", uint_range(ocl_dim, L_DIMS + R_DIMS - 1), uint_range(ocl_dim, L_DIMS + R_DIMS - 1)), " * ")) : "",
                                                           ocl_dim < L_DIMS + R_DIMS - 1 ? stringf(" / (%s)", concat(multi_stringf("NUM_WI_%d * NUM_WG_%d", uint_range(ocl_dim + 1, L_DIMS + R_DIMS - 1), uint_range(ocl_dim + 1, L_DIMS + R_DIMS - 1)), " * ")) : ""
                            );
                            ocl_function_macros << stringf("\n#define GET_LOCAL_ID_%c_%d    (threadIdx.z%s%s)",
                                                           dim.type, dim.nr,
                                                           ocl_dim > 2 ? stringf(" %% (%s)", concat(multi_stringf("NUM_WI_%d", uint_range(ocl_dim, L_DIMS + R_DIMS - 1)), " * ")) : "",
                                                           ocl_dim < L_DIMS + R_DIMS - 1 ? stringf(" / (%s)", concat(multi_stringf("NUM_WI_%d", uint_range(ocl_dim + 1, L_DIMS + R_DIMS - 1)), " * ")) : ""
                            );
                            ocl_function_macros << stringf("\n#define GET_GROUP_ID_%c_%d    (blockIdx.z%s%s)",
                                                           dim.type, dim.nr,
                                                           ocl_dim > 2 ? stringf(" %% (%s)", concat(multi_stringf("NUM_WG_%d", uint_range(ocl_dim, L_DIMS + R_DIMS - 1)), " * ")) : "",
                                                           ocl_dim < L_DIMS + R_DIMS - 1 ? stringf(" / (%s)", concat(multi_stringf("NUM_WG_%d", uint_range(ocl_dim + 1, L_DIMS + R_DIMS - 1)), " * ")) : ""
                            );
                            first = false;
                        }
                        ocl_function_macros << "\n#endif";
                    }
                    ocl_function_macros << "\n#endif";
                }
            } else {
                if (L_DIMS + R_DIMS <= 3) {
                    ocl_function_macros << concat(
                            multi_stringf(R"(
#define GET_GLOBAL_ID_L_%d   get_global_id(OCL_DIM_L_%d)
#define GET_LOCAL_ID_L_%d    get_local_id(OCL_DIM_L_%d)
#define GET_GROUP_ID_L_%d    get_group_id(OCL_DIM_L_%d)
#define GET_GLOBAL_SIZE_L_%d (NUM_WG_L_%d * NUM_WI_L_%d)
#define GET_LOCAL_SIZE_L_%d  NUM_WI_L_%d)",
                                          dim_range_nrs(L_DIMS, 0),
                                          dim_range_nrs(L_DIMS, 0),
                                          dim_range_nrs(L_DIMS, 0),
                                          dim_range_nrs(L_DIMS, 0),
                                          dim_range_nrs(L_DIMS, 0),
                                          dim_range_nrs(L_DIMS, 0),
                                          dim_range_nrs(L_DIMS, 0),
                                          dim_range_nrs(L_DIMS, 0),
                                          dim_range_nrs(L_DIMS, 0),
                                          dim_range_nrs(L_DIMS, 0),
                                          dim_range_nrs(L_DIMS, 0)
                            )
                    );
                    ocl_function_macros << concat(
                            multi_stringf(R"(
#define GET_GLOBAL_ID_R_%d   get_global_id(OCL_DIM_R_%d)
#define GET_LOCAL_ID_R_%d    get_local_id(OCL_DIM_R_%d)
#define GET_GROUP_ID_R_%d    get_group_id(OCL_DIM_R_%d)
#define GET_GLOBAL_SIZE_R_%d %s
#define GET_LOCAL_SIZE_R_%d  NUM_WI_R_%d)",
                                          dim_range_nrs(0, R_DIMS),
                                          dim_range_nrs(0, R_DIMS),
                                          dim_range_nrs(0, R_DIMS),
                                          dim_range_nrs(0, R_DIMS),
                                          dim_range_nrs(0, R_DIMS),
                                          dim_range_nrs(0, R_DIMS),
                                          dim_range_nrs(0, R_DIMS),
                                          kernel == 1
                                          ? multi_stringf("(NUM_WG_R_%d * NUM_WI_R_%d)",
                                                          dim_range_nrs(0, R_DIMS),
                                                          dim_range_nrs(0, R_DIMS))
                                          : multi_stringf("NUM_WI_R_%d",
                                                          dim_range_nrs(0, R_DIMS)),
                                          dim_range_nrs(0, R_DIMS),
                                          dim_range_nrs(0, R_DIMS)
                            )
                    );
                } else {
                    for (unsigned int ocl_dim = 2; ocl_dim < L_DIMS + R_DIMS; ++ocl_dim) {
                        bool first = true;
                        for (const auto &dim : dim_range(L_DIMS, R_DIMS)) {
                            ocl_function_macros << std::endl << stringf(
                                    "%s OCL_DIM_%c_%d == %d\n#define NUM_WG_%d %s\n#define NUM_WI_%d NUM_WI_%c_%d",
                                    first ? "#if  " : "#elif",
                                    dim.type, dim.nr,
                                    ocl_dim,
                                    ocl_dim,
                                    kernel == 1 || dim.type == DIM_TYPE::L ? stringf("NUM_WG_%c_%d", dim.type, dim.nr) : "1",
                                    ocl_dim,
                                    dim.type, dim.nr
                            );
                            first = false;
                        }
                        ocl_function_macros << std::endl << "#endif";
                    }
                    for (const auto &dim : dim_range(L_DIMS, R_DIMS)) {
                        ocl_function_macros << stringf(R"(
#if OCL_DIM_%c_%d < 2
#define GET_GLOBAL_ID_%c_%d   get_global_id(OCL_DIM_%c_%d)
#define GET_LOCAL_ID_%c_%d    get_local_id(OCL_DIM_%c_%d)
#define GET_GROUP_ID_%c_%d    get_group_id(OCL_DIM_%c_%d)
#define GET_GLOBAL_SIZE_%c_%d %s
#define GET_LOCAL_SIZE_%c_%d  NUM_WI_%c_%d
#else)",
                                                       dim.type, dim.nr,

                                                       dim.type, dim.nr,
                                                       dim.type, dim.nr,
                                                       dim.type, dim.nr,
                                                       dim.type, dim.nr,
                                                       dim.type, dim.nr,
                                                       dim.type, dim.nr,
                                                       dim.type, dim.nr,
                                                       kernel == 1 || dim.type == DIM_TYPE::L
                                                       ? stringf(
                                                               "(NUM_WG_%c_%d * NUM_WI_%c_%d)",
                                                               dim.type, dim.nr,
                                                               dim.type, dim.nr
                                                       )
                                                       : stringf(
                                                               "NUM_WI_%c_%d",
                                                               dim.type, dim.nr
                                                       ),
                                                       dim.type, dim.nr,
                                                       dim.type, dim.nr
                        );
                        if (kernel == 1 || dim.type == DIM_TYPE::L) {
                            ocl_function_macros << stringf("\n#define GET_GLOBAL_SIZE_%c_%d (NUM_WG_%c_%d * NUM_WI_%c_%d)",
                                                           dim.type, dim.nr,
                                                           dim.type, dim.nr,
                                                           dim.type, dim.nr
                            );
                        } else {
                            ocl_function_macros << stringf("\n#define GET_GLOBAL_SIZE_%c_%d NUM_WI_%c_%d",
                                                           dim.type, dim.nr,
                                                           dim.type, dim.nr
                            );
                        }
                        ocl_function_macros << stringf("\n#define GET_LOCAL_SIZE_%c_%d  NUM_WI_%c_%d",
                                                       dim.type, dim.nr,
                                                       dim.type, dim.nr
                        );
                        bool first = true;
                        for (unsigned int ocl_dim = 2; ocl_dim < L_DIMS + R_DIMS; ++ocl_dim) {
                            ocl_function_macros << std::endl << stringf(
                                    "%s OCL_DIM_%c_%d == %d",
                                    first ? "#if  " : "#elif",
                                    dim.type, dim.nr,
                                    ocl_dim
                            );
                            ocl_function_macros << stringf("\n#define GET_GLOBAL_ID_%c_%d    (get_global_id(2)%s%s)",
                                                           dim.type, dim.nr,
                                                           ocl_dim > 2 ? stringf(" %% (%s)", concat(multi_stringf("NUM_WI_%d * NUM_WG_%d", uint_range(ocl_dim, L_DIMS + R_DIMS - 1), uint_range(ocl_dim, L_DIMS + R_DIMS - 1)), " * ")) : "",
                                                           ocl_dim < L_DIMS + R_DIMS - 1 ? stringf(" / (%s)", concat(multi_stringf("NUM_WI_%d * NUM_WG_%d", uint_range(ocl_dim + 1, L_DIMS + R_DIMS - 1), uint_range(ocl_dim + 1, L_DIMS + R_DIMS - 1)), " * ")) : ""
                            );
                            ocl_function_macros << stringf("\n#define GET_LOCAL_ID_%c_%d    (get_local_id(2)%s%s)",
                                                           dim.type, dim.nr,
                                                           ocl_dim > 2 ? stringf(" %% (%s)", concat(multi_stringf("NUM_WI_%d", uint_range(ocl_dim, L_DIMS + R_DIMS - 1)), " * ")) : "",
                                                           ocl_dim < L_DIMS + R_DIMS - 1 ? stringf(" / (%s)", concat(multi_stringf("NUM_WI_%d", uint_range(ocl_dim + 1, L_DIMS + R_DIMS - 1)), " * ")) : ""
                            );
                            ocl_function_macros << stringf("\n#define GET_GROUP_ID_%c_%d    (get_group_id(2)%s%s)",
                                                           dim.type, dim.nr,
                                                           ocl_dim > 2 ? stringf(" %% (%s)", concat(multi_stringf("NUM_WG_%d", uint_range(ocl_dim, L_DIMS + R_DIMS - 1)), " * ")) : "",
                                                           ocl_dim < L_DIMS + R_DIMS - 1 ? stringf(" / (%s)", concat(multi_stringf("NUM_WG_%d", uint_range(ocl_dim + 1, L_DIMS + R_DIMS - 1)), " * ")) : ""
                            );
                            first = false;
                        }
                        ocl_function_macros << "\n#endif";
                        ocl_function_macros << "\n#endif";
                    }
                }
            }

            kernel_source = stringf(
                    R"(#define CACHE_CB_OFFSET 1%s

%s// helper macros
%s
%s

%s
%s

%s
%s
%s
%s

#define PRIVATE 0
#define LOCAL   1
#define GLOBAL  2)",
                    _cuda ? "\n#define USE_SHUFFLE_REDUCTIONS 1" : "",
                    _runtime_inputs ? concat(multi_stringf("#undef INPUT_SIZE_%c_%d\n", dim_range_types(L_DIMS, R_DIMS), dim_range_nrs(L_DIMS, R_DIMS))) : "",
                    permutation_cases<dimension_t, unsigned int>(
                            dim_range(L_DIMS, R_DIMS),
                            [&](const dimension_t& variable) -> std::string {
                                return stringf("OCL_DIM_%c_%d", variable.type, variable.nr);
                            },
                            uint_range(0, L_DIMS + R_DIMS - 1),
                            [&](const unsigned int &option) -> std::string {
                                return std::to_string(option);
                            },
                            [&](unsigned int nesting_level, const dimension_t &variable, const unsigned int &option, bool shortcut_macro, const std::vector<std::pair<dimension_t, unsigned int>> &path) -> std::string {
                                return stringf(
                                        "#define CONCAT_IN_DESCENDING_OCL_ORDER_%d(%s) %s",
                                        option,
                                        make_standard_params(L_DIMS + R_DIMS),
                                        make_standard_indices(V(variable), L_DIMS, R_DIMS).front());
                            }
                    ),
                    stringf("#define CONCAT_IN_DESCENDING_OCL_ORDER(%s) %s",
                            make_standard_params(L_DIMS + R_DIMS),
                            concat(multi_stringf("CONCAT_IN_DESCENDING_OCL_ORDER_%d(%s)",
                                                 uint_range(L_DIMS + R_DIMS - 1, 0),
                                                 V(make_standard_params(L_DIMS + R_DIMS))
                            ))),
                    permutation_cases<unsigned int, dimension_t>(
                            uint_range(0, L_DIMS + R_DIMS - 1),
                            [&](const unsigned int &variable) -> std::string {
                                return std::to_string(L_DIMS + R_DIMS - 1 - variable);
                            },
                            dim_range(L_DIMS, R_DIMS),
                            [&](const dimension_t& option) -> std::string {
                                return stringf("OCL_DIM_%c_%d", option.type, option.nr);
                            },
                            [&](unsigned int nesting_level, const unsigned int &variable, const dimension_t &option, bool shortcut_macro, const std::vector<std::pair<unsigned int, dimension_t>> &path) -> std::string {
                                return stringf("#define FLAT_INDEX_IN_DESCENDING_OCL_ORDER_%d(%s, %s) %s",
                                               L_DIMS + R_DIMS - 1 - variable,
                                               make_standard_params(L_DIMS + R_DIMS, "%%val%%_id"),
                                               make_standard_params(L_DIMS + R_DIMS, "%%val%%_size"),
                                               L_DIMS + R_DIMS - 1 - variable < L_DIMS + R_DIMS - 1
                                               ? stringf("(FLAT_INDEX_IN_DESCENDING_OCL_ORDER_%d(%s, %s)) * (%s) + (%s)",
                                                         L_DIMS + R_DIMS - 1 - variable + 1,
                                                         make_standard_params(L_DIMS + R_DIMS, "%%val%%_id"),
                                                         make_standard_params(L_DIMS + R_DIMS, "%%val%%_size"),
                                                         make_standard_indices(V(option), L_DIMS, R_DIMS, "%%val%%_size").front(),
                                                         make_standard_indices(V(option), L_DIMS, R_DIMS, "%%val%%_id").front())
                                               : make_standard_indices(V(option), L_DIMS, R_DIMS, "%%val%%_id").front()
                                );
                            }
                    ),
                    stringf("#define FLAT_INDEX_IN_DESCENDING_OCL_ORDER(%s, %s) FLAT_INDEX_IN_DESCENDING_OCL_ORDER_0(%s, %s)",
                            make_standard_params(L_DIMS + R_DIMS, "%%val%%_id"),
                            make_standard_params(L_DIMS + R_DIMS, "%%val%%_size"),
                            make_standard_params(L_DIMS + R_DIMS, "%%val%%_id"),
                            make_standard_params(L_DIMS + R_DIMS, "%%val%%_size")),
                    L_DIMS == 0 ? "" : permutation_cases<dimension_t, unsigned int>(
                            dim_range(L_DIMS, 0),
                            [&](const dimension_t& variable) -> std::string {
                                return stringf("OCL_DIM_%c_%d", variable.type, variable.nr);
                            },
                            uint_range(0, L_DIMS + R_DIMS - 1),
                            [&](const unsigned int &option) -> std::string {
                                return std::to_string(option);
                            },
                            [&](unsigned int nesting_level, const dimension_t &variable, const unsigned int &option, bool shortcut_macro, const std::vector<std::pair<dimension_t, unsigned int>> &path) -> std::string {
                                if (shortcut_macro) {
                                    return stringf(
                                            "#define DESCENDING_L_DIMS_%d(%s) %s",
                                            option,
                                            make_standard_params(L_DIMS),
                                            make_standard_indices(V(variable), L_DIMS, 0).front());
                                } else {
                                    return stringf(
                                            "#define DESCENDING_L_DIMS_%d(%s) %s",
                                            L_DIMS - 1 - path.size(),
                                            make_standard_params(L_DIMS),
                                            make_standard_indices(V(variable), L_DIMS, 0).front());
                                }
                            }
                    ),
                    permutation_cases<dimension_t, unsigned int>(
                            dim_range(L_DIMS, R_DIMS),
                            [&](const dimension_t& variable) -> std::string {
                                return stringf("OCL_DIM_%c_%d", variable.type, variable.nr);
                            },
                            uint_range(0, L_DIMS + R_DIMS - 1),
                            [&](const unsigned int &option) -> std::string {
                                return std::to_string(option);
                            },
                            [&](unsigned int nesting_level, const dimension_t &variable, const unsigned int &option, bool shortcut_macro, const std::vector<std::pair<dimension_t, unsigned int>> &path) -> std::string {
                                return stringf(
                                        "#define DESCENDING_DIMS_%d(%s) %s",
                                        option,
                                        make_standard_params(L_DIMS + R_DIMS),
                                        make_standard_indices(V(variable), L_DIMS, R_DIMS).front());
                            }
                    ),
                    kernel == 1 ? "" : "\n\n#define CEIL(x,y) (((x) + (y) - 1) / (y))\n",
                    ocl_function_macros.str()
            );

            if (!_md_hom.prepend_code().empty()) {
                kernel_source.append("\n\n");
                kernel_source.append(_md_hom.prepend_code());
            }

            kernel_source.append(stringf(R"(

// =============== macro definitions per dimension ============================
%s
// =============== end of macro definitions per dimension =====================)",
                                         _macros.definitions(kernel)
            ));
            kernel_source.append("\n\n// =============== macro definitions per buffer ===============================\n");
            for (const auto &wrapper : kernel == 1 ? _input_wrappers_1: _input_wrappers_2) {
                if (wrapper->has_definitions(kernel)) {
                    kernel_source.append(wrapper->definitions(kernel));
                    kernel_source.append("\n\n\n");
                }
            }
            kernel_source.append(_result_wrapper.definitions(kernel));
            kernel_source.append("\n// =============== end of macro definitions per buffer ========================");
            kernel_source.append(stringf("\n\n// =============== kernel %d ===================================================\n", kernel));
            kernel_source.append(stringf(
                    "%svoid %s_%d(%s%s%s, %s%s) {\n",
                    _cuda ? "extern \"C\" __global__\n" : "__kernel ",
                    md_hom.routine_name(), kernel,
                    _runtime_inputs ? (kernel == 1 ? concat(multi_stringf("const int INPUT_SIZE_%c_%d, ", dim_range_types(L_DIMS, R_DIMS), dim_range_nrs(L_DIMS, R_DIMS))) : "")
                                    : "",
                    concat((kernel == 1 ? _input_parameter_definitions_1 : _input_parameter_definitions_2), ", "),
                    concat(multi_stringf(", %s%s * const %s res_g_%s_raw", _cuda ? V<std::string>("") : V<std::string>("__global "), _output_types, _cuda ? V<std::string>("__restrict__") : V<std::string>("restrict"), _output_names_2)),
                    concat((kernel == 1 ? _output_parameter_definitions_1 : _output_parameter_definitions_2), ", "),
                    _runtime_inputs ? (kernel == 1 ? "" : concat(multi_stringf(", const int INPUT_SIZE_%c_%d", dim_range_types(L_DIMS, R_DIMS), dim_range_nrs(L_DIMS, R_DIMS))))
                                    : ""
            ));
            if (_runtime_inputs && kernel == 2) {
                // determine global cache block size in R dimensions
                for (const auto r_dim : dim_range(0, R_DIMS)) {
                    kernel_source.append(indent(stringf("size_t %s = NUM_WG_R_%d;\n", _macros.cb_size(kernel, LEVEL::GLOBAL, r_dim), r_dim.nr), 1));
                    kernel_source.append(indent(stringf("if (NUM_WG_R_%d * NUM_WI_R_%d > INPUT_SIZE_R_%d) {\n", r_dim.nr, r_dim.nr, r_dim.nr), 1));
                    kernel_source.append(indent(stringf("%s = (INPUT_SIZE_R_%d + NUM_WI_R_%d - 1) / NUM_WI_R_%d;\n", _macros.cb_size(kernel, LEVEL::GLOBAL, r_dim), r_dim.nr, r_dim.nr, r_dim.nr), 2));
                    kernel_source.append(indent("}\n\n", 1));
                }
            }

            // define variables
            kernel_source.append(indent("// map md_hom dimensions to OpenCL dimensions\n", 1));
            for (const auto &dim : dim_range(L_DIMS, R_DIMS)) {
                if (kernel == 2 && dim.type == DIM_TYPE::R) {
                    kernel_source.append(indent(stringf("const size_t i_wg_%c_%d = 0;\n", lower_case(dim.type), dim.nr), 1));
                } else {
                    kernel_source.append(indent(stringf("#if NUM_WG_%c_%d == 1\n", dim.type, dim.nr), 1));
                    kernel_source.append(indent(stringf("const size_t i_wg_%c_%d = 0;\n", lower_case(dim.type), dim.nr), 1));
                    kernel_source.append(indent("#else\n", 1));
                    kernel_source.append(indent(stringf("const size_t i_wg_%c_%d = GET_GROUP_ID_%c_%d;\n", lower_case(dim.type), dim.nr, dim.type, dim.nr), 1));
                    kernel_source.append(indent("#endif\n", 1));
                }
                kernel_source.append(indent(stringf("#if NUM_WI_%c_%d == 1\n", dim.type, dim.nr), 1));
                kernel_source.append(indent(stringf("const size_t i_wi_%c_%d = 0;\n", lower_case(dim.type), dim.nr), 1));
                kernel_source.append(indent("#else\n", 1));
                kernel_source.append(indent(stringf("const size_t i_wi_%c_%d = GET_LOCAL_ID_%c_%d;\n", lower_case(dim.type), dim.nr, dim.type, dim.nr), 1));
                kernel_source.append(indent("#endif\n", 1));
                kernel_source.append(indent("\n", 1));
            }

            // define index variables
            kernel_source.append("\n");
            kernel_source.append("#if defined(CACHE_CB_OFFSET) && CACHE_CB_OFFSET != 0\n");
            for (const auto level : {LEVEL::LOCAL, LEVEL::PRIVATE}) {
                for (const auto dimension : dim_range(L_DIMS, R_DIMS)) {
                    kernel_source.append(stringf("  size_t %c_cb_offset_%c_%d;\n",
                                                 lower_case(level), lower_case(dimension.type), dimension.nr
                    ));
                }
            }
            kernel_source.append("#endif\n");
            kernel_source.append("\n");
            kernel_source.append("\n");

            kernel_source.append(indent("// declare variables for input buffers\n", 1));
            for (auto level : {LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE}) {
                for (const auto &input : kernel == 1 ? _input_wrappers_1 : _input_wrappers_2) {
                    if (input->supports_caching()) {
                        if (level != LEVEL::GLOBAL) {
                            kernel_source.append(indent(stringf("#if (defined(%s_CACHE_%c_CB) && %s_CACHE_%c_CB != 0) || (!defined(%s_CACHE_%c_CB) && CACHE_%c_CB != 0)\n",
                                    upper_case(input->input_name()), level, upper_case(input->input_name()), level,
                                    upper_case(input->input_name()), level, level
                            ), 1));
                        }
                        kernel_source.append(indent(input->caching_variable_declaration(kernel, level), 1));
                        if (level != LEVEL::GLOBAL) {
                            kernel_source.append(indent("#endif\n", 1));
                        }
                    }
                }
            }
            kernel_source.append("\n");
            kernel_source.append(indent(_result_wrapper.variable_declarations(kernel), 1));
            kernel_source.append("\n");
            kernel_source.append("\n");
            if (R_DIMS > 0) {
                // reset memory destination for reduction if necessary
                kernel_source.append(indent(_result_wrapper.reset_reduction_memory_space(kernel), 1));
                kernel_source.append("\n");
                kernel_source.append("\n");
            }
#if 0
            kernel_source.append(indent("if (get_global_id(0) + get_global_id(1) + get_global_id(2) == 0) {\n", 1));
            for (const auto dim : dim_range(L_DIMS, R_DIMS)) {
                for (const auto level : {LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE}) {
                    kernel_source.append(indent(stringf(
                            "DEBUG_PRINT(\"%-55s%s\\n\", %s);\n",
                            _macros.cb_size(kernel, level, dim),
                            "%4d",
                            _macros.cb_size(kernel, level, dim)
                    ), 2));
                }
                for (const auto level : {LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE}) {
                    kernel_source.append(indent(stringf(
                            "DEBUG_PRINT(\"%-55s%s\\n\", %s);\n",
                            _macros.num_steps(kernel, level, dim),
                            "%4d",
                            _macros.num_steps(kernel, level, dim)
                    ), 2));
                }
                for (const auto level : {LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE}) {
                    kernel_source.append(indent(stringf(
                            "DEBUG_PRINT(\"%-55s%s\\n\", %s);\n",
                            _macros.num_cached_iterations(kernel, level, dim),
                            "%4d",
                            _macros.num_cached_iterations(kernel, level, dim)
                    ), 2));
                }
                for (const auto level : {LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE}) {
                    kernel_source.append(indent(stringf(
                            "DEBUG_PRINT(\"%-55s%s\\n\", %s);\n",
                            _macros.num_extra_cached_iterations(kernel, level, dim),
                            "%4d",
                            _macros.num_extra_cached_iterations(kernel, level, dim)
                    ), 2));
                }
                for (const auto level : {LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE}) {
                    kernel_source.append(indent(stringf(
                            "DEBUG_PRINT(\"%-55s%s\\n\", %s);\n",
                            _macros.num_extra_elements(kernel, level, dim),
                            "%4d",
                            _macros.num_extra_elements(kernel, level, dim)
                    ), 2));
                }
                for (const auto level : {LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE}) {
                    kernel_source.append(indent(stringf(
                            "DEBUG_PRINT(\"%-55s%s\\n\", %s);\n",
                            _macros.num_processed_elements(kernel, level, dim),
                            "%4d",
                            _macros.num_processed_elements(kernel, level, dim)
                    ), 2));
                }
                for (LEVEL level : {LEVEL::PRIVATE, LEVEL::LOCAL}) {
                    for (LEVEL first_complete_level : parent_levels(level, false, ORDER::ASCENDING)) {
                        kernel_source.append(indent(stringf(
                                "DEBUG_PRINT(\"%-55s%s\\n\", %s);\n",
                                _macros.incomplete_cb_size(kernel, level, dim, first_complete_level),
                                "%4d",
                                _macros.incomplete_cb_size(kernel, level, dim, first_complete_level)
                        ), 2));
                    }
                }
                for (LEVEL level : {LEVEL::PRIVATE, LEVEL::LOCAL}) {
                    for (LEVEL first_complete_level : parent_levels(parent_level(level), false, ORDER::ASCENDING)) {
                        kernel_source.append(indent(stringf(
                                "DEBUG_PRINT(\"%-55s%s\\n\", %s);\n",
                                _macros.num_steps_in_incomplete_cb(kernel, level, dim, first_complete_level),
                                "%4d",
                                _macros.num_steps_in_incomplete_cb(kernel, level, dim, first_complete_level)
                        ), 2));
                    }
                }
                for (LEVEL level : {LEVEL::PRIVATE, LEVEL::LOCAL}) {
                    for (LEVEL first_complete_level : parent_levels(level, false, ORDER::ASCENDING)) {
                        kernel_source.append(indent(stringf(
                                "DEBUG_PRINT(\"%-55s%s\\n\", %s);\n",
                                _macros.num_cached_iterations_in_incomplete_cb(kernel, level, dim, first_complete_level),
                                "%4d",
                                _macros.num_cached_iterations_in_incomplete_cb(kernel, level, dim, first_complete_level)
                        ), 2));
                    }
                }
                for (LEVEL level : {LEVEL::PRIVATE, LEVEL::LOCAL}) {
                    for (LEVEL first_complete_level : parent_levels(parent_level(level), false, ORDER::ASCENDING)) {
                        kernel_source.append(indent(stringf(
                                "DEBUG_PRINT(\"%-55s%s\\n\", %s);\n",
                                _macros.num_extra_cached_iterations_in_incomplete_cb(kernel, level, dim, first_complete_level),
                                "%4d",
                                _macros.num_extra_cached_iterations_in_incomplete_cb(kernel, level, dim, first_complete_level)
                        ), 2));
                    }
                }
                for (LEVEL level : {LEVEL::PRIVATE, LEVEL::LOCAL}) {
                    for (LEVEL first_complete_level : parent_levels(parent_level(level), false, ORDER::ASCENDING)) {
                        kernel_source.append(indent(stringf(
                                "DEBUG_PRINT(\"%-55s%s\\n\", %s);\n",
                                _macros.num_extra_elements_in_incomplete_cb(kernel, level, dim, first_complete_level),
                                "%4d",
                                _macros.num_extra_elements_in_incomplete_cb(kernel, level, dim, first_complete_level)
                        ), 2));
                    }
                }
                for (LEVEL level : {LEVEL::PRIVATE, LEVEL::LOCAL}) {
                    for (LEVEL first_complete_level : parent_levels(level, false, ORDER::ASCENDING)) {
                        kernel_source.append(indent(stringf(
                                "DEBUG_PRINT(\"%-55s%s\\n\", %s);\n",
                                _macros.num_processed_elements_in_incomplete_cb(kernel, level, dim, first_complete_level),
                                "%4d",
                                _macros.num_processed_elements_in_incomplete_cb(kernel, level, dim, first_complete_level)
                        ), 2));
                    }
                }
            }
            kernel_source.append(indent("}\n\n", 1));
//            kernel_source.append(indent("return;\n", 1));
#endif

            // generate loops
            auto loops = loop_generator<L_DIMS, R_DIMS>(kernel, _macros, _runtime_inputs, V(LEVEL::LOCAL, LEVEL::PRIVATE),
                                                        _md_hom.dimension_order(), true, &_l_dims_after_first_r_dim,
#ifndef REDUCTION_COPY_COMPLETE
                                                        false,
#endif
                                                        skip_post_processing,
                                                        thread_nesting
            );
#ifdef PRINT_ITERATION_STEP
            // debug print for current step
            loops.add_code([](LEVEL level, const dimension_t &dimension, const std::vector<LEVEL> &levels,
                              const std::vector<dimension_t> &dimensions) {
                               return true;
                           }, INNER_PREPEND,
                           [&](auto level, const auto &dimension, char *phase, bool *first_iteration) -> std::string {
                               return stringf("DEBUG_PRINT(\"kernel %d %s = %%2lu\\n\", %s);", kernel, loops.loop_variable(level, dimension), loops.loop_variable(level, dimension));
                           });
#endif
            // callback for generating caching code
            auto caching_code = [&, kernel] (auto cb_level, auto &input, auto level, const auto &dimension, char *phase, bool *first_iteration, bool dbl_buffering) {
                std::string caching_code;

                // determine present and skipped dimensions
                std::vector<dimension_t> present_dimensions;
                std::vector<dimension_t> skipped_dimensions;
                std::vector<dimension_t> parent_present_dimensions;
                std::vector<dimension_t> parent_skipped_dimensions;
                auto check_skipped_present = [&] (auto loop_level, auto level, auto &present_dimensions, auto &skipped_dimensions) {
                    if (level > loop_level) {
                        // if generation level comes after loop level, all dimensions are present on loop level
                        present_dimensions = sort<L_DIMS, R_DIMS>(input->distinct_used_dimensions());
                    } else if (level < loop_level) {
                        // if generation level comes before loop level, all dimensions have been skipped on loop level
                        skipped_dimensions = sort<L_DIMS, R_DIMS>(input->distinct_used_dimensions());
                    } else {
                        // if generation level is same as loop level, assume all dimensions are present on loop level
                        // and filter out skipped dimensions
                        present_dimensions = sort<L_DIMS, R_DIMS>(input->distinct_used_dimensions());
                        // iterate through dimension order backwards
                        for (int i = _md_hom.dimension_order().size() - 1; i >= 0; --i) {
                            // if generation dimension was reached, all present dimensions have been found
                            if (_md_hom.dimension_order()[i] == dimension) {
                                break;
                            }
                            // if current dimension was assumed to be present, move it over to skipped dimensions
                            if (contains(present_dimensions, _md_hom.dimension_order()[i])) {
                                skipped_dimensions.push_back(_md_hom.dimension_order()[i]);
                                present_dimensions.erase(std::find(present_dimensions.begin(), present_dimensions.end(), _md_hom.dimension_order()[i]));
                            }
                        }
                        skipped_dimensions = reverse(skipped_dimensions);
                    }
                };
                // determine which dimensions are present and which have been skipped on cb_level
                check_skipped_present(cb_level, level, present_dimensions, skipped_dimensions);
                // determine which dimensions are present and which have been skipped on parent level of cb_level,
                // if that level exists and is not global level
                if (parent_level(cb_level) > LEVEL::GLOBAL)
                    check_skipped_present(parent_level(cb_level), level, parent_present_dimensions, parent_skipped_dimensions);

                // determine whether this code loads a post processing cache block
                bool post_processing = false;
                for (const auto dim : present_dimensions) {
                    post_processing = phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(level, dim)] != 1;
                    if (post_processing)
                        break;
                }

                // generate condition that checks whether or not to generate caching code at this position
                bool static_use_code_at_this_position_condition = true;
                std::string use_code_at_this_position_condition;
                // loop on last present or parent last present dimension has to have more than 1 iteration
                if (!present_dimensions.empty()) {
                    if (!use_code_at_this_position_condition.empty()) use_code_at_this_position_condition += " && ";
                    use_code_at_this_position_condition += stringf("(%s + (%s > 0) + (%s > 0) > 1)",
                                                                   _macros.num_steps(kernel, cb_level, present_dimensions.back(), phase),
                                                                   _macros.num_extra_cached_iterations(kernel, cb_level, present_dimensions.back(), phase),
                                                                   _macros.num_extra_elements(kernel, cb_level, present_dimensions.back(), phase));
                    if (parent_level(cb_level) > LEVEL::GLOBAL)
                        // if parent is lower than global, check phase of parent level to determine if condition is statically known
                        static_use_code_at_this_position_condition = static_use_code_at_this_position_condition && (!_runtime_inputs || phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(cb_level), present_dimensions.back())] == 2);
                    else
                        static_use_code_at_this_position_condition = static_use_code_at_this_position_condition && !_runtime_inputs;
                }
                if (present_dimensions.empty() && !parent_present_dimensions.empty()) {
                    if (!use_code_at_this_position_condition.empty()) use_code_at_this_position_condition += " && ";
                    use_code_at_this_position_condition += stringf("(%s + (%s > 0) + (%s > 0) > 1)",
                                                                   _macros.num_steps(kernel, parent_level(cb_level), parent_present_dimensions.back(), phase),
                                                                   _macros.num_extra_cached_iterations(kernel, parent_level(cb_level), parent_present_dimensions.back(), phase),
                                                                   _macros.num_extra_elements(kernel, parent_level(cb_level), parent_present_dimensions.back(), phase));
                    static_use_code_at_this_position_condition = static_use_code_at_this_position_condition && !_runtime_inputs;
                }
                // if all cb_level dimension have been skipped, this code will load cache block 0
                bool loading_cb_0 = true;
                for (const auto &dim : input->distinct_used_dimensions()) {
                    if (!contains(skipped_dimensions, dim)) {
                        loading_cb_0 = false;
                        break;
                    }
                }
                // all parent skipped dimension can only have 1 iteration
                for (const auto &dim : parent_skipped_dimensions) {
                    if (!use_code_at_this_position_condition.empty()) use_code_at_this_position_condition += " && ";
                    use_code_at_this_position_condition += stringf("(%s + (%s > 0) + (%s > 0) == 1)",
                                                                   _macros.num_steps(kernel, parent_level(cb_level), dim, phase),
                                                                   _macros.num_extra_cached_iterations(kernel, parent_level(cb_level), dim),
                                                                   _macros.num_extra_elements(kernel, parent_level(cb_level), dim, phase)
                    );
                    static_use_code_at_this_position_condition = static_use_code_at_this_position_condition && !_runtime_inputs;
                }
                std::string use_code_at_this_position_skipped_dims_condition;
                for (const auto &dim : skipped_dimensions) {
//                    if (contains(parent_skipped_dimensions, dim)) continue; // skip this dimension if it was already processed on parent level
                    if (!use_code_at_this_position_skipped_dims_condition.empty()) use_code_at_this_position_skipped_dims_condition += " && ";
                    if (contains(parent_skipped_dimensions, dim)) {
                        use_code_at_this_position_skipped_dims_condition += stringf("((%s * (%s + (%s > 0) + (%s > 0))) + ((%s > 0) * (%s + (%s > 0) + (%s > 0))) + (%s > 0) == 1)",
                                                                                    _macros.num_steps(kernel, parent_level(cb_level), dim),
                                                                                    _macros.num_steps(kernel, cb_level, dim),
                                                                                    _macros.num_extra_cached_iterations(kernel, cb_level, dim, phase),
                                                                                    _macros.num_extra_elements(kernel, cb_level, dim, phase),

                                                                                    _macros.num_extra_cached_iterations(kernel, parent_level(cb_level), dim),
                                                                                    _macros.num_steps_in_incomplete_cb(kernel, cb_level, dim, LEVEL::GLOBAL),
                                                                                    _macros.num_extra_cached_iterations_in_incomplete_cb(kernel, cb_level, dim, LEVEL::GLOBAL),
                                                                                    _macros.num_extra_elements_in_incomplete_cb(kernel, cb_level, dim, LEVEL::GLOBAL),

                                                                                    _macros.num_extra_elements(kernel, parent_level(cb_level), dim, phase)
                        );
                    } else {
                        use_code_at_this_position_skipped_dims_condition += stringf("(%s + (%s > 0) + (%s > 0) == 1)",
                                                                                    _macros.num_steps(kernel, cb_level, dim, phase),
                                                                                    _macros.num_extra_cached_iterations(kernel, cb_level, dim, phase),
                                                                                    _macros.num_extra_elements(kernel, cb_level, dim, phase));
                    }
                    if (parent_level(cb_level) > LEVEL::GLOBAL)
                        // if parent is lower than global, check phase of parent level to determine if condition is statically known
                        static_use_code_at_this_position_condition = static_use_code_at_this_position_condition && (!_runtime_inputs || phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(cb_level), dim)] == 2);
                    else
                        static_use_code_at_this_position_condition = static_use_code_at_this_position_condition && !_runtime_inputs;
                }
                // generate special condition for cache block 0 to account for double buffering
                if (loading_cb_0 && dbl_buffering) {
                    // TODO does not work for dynamic input sizes
                    use_code_at_this_position_condition = stringf("(%s) && ((defined(%s_CACHE_%c_CB) && %s_CACHE_%c_CB == 2) || (!defined(%s_CACHE_%c_CB) && CACHE_%c_CB == 2))",
                                                                  use_code_at_this_position_condition.empty() ? "1" : use_code_at_this_position_condition,
                                                                  upper_case(input->input_name()), cb_level, upper_case(input->input_name()), cb_level,
                                                                  upper_case(input->input_name()), cb_level, cb_level
                    );
                } else {
                    if (!use_code_at_this_position_condition.empty() && !use_code_at_this_position_skipped_dims_condition.empty()) use_code_at_this_position_condition += " && ";
                    if (!use_code_at_this_position_skipped_dims_condition.empty()) use_code_at_this_position_condition.append(use_code_at_this_position_skipped_dims_condition);
                }

                // add conditions whether to use caching code at this position or not
                caching_code.append(stringf("#if (defined(%s_CACHE_%c_CB) && %s_CACHE_%c_CB == %d) || (!defined(%s_CACHE_%c_CB) && CACHE_%c_CB == %d)\n",
                                            upper_case(input->input_name()), cb_level, upper_case(input->input_name()), cb_level, dbl_buffering ? 2 : 1,
                                            upper_case(input->input_name()), cb_level, cb_level, dbl_buffering ? 2 : 1
                ));
                if (!use_code_at_this_position_condition.empty()) {
                    caching_code.append(stringf("%sif (%s)\n",
                                                static_use_code_at_this_position_condition ? "#" : "",
                                                use_code_at_this_position_condition
                    ));
                }

                // if a skipped dimension is solely in phase 3 or a parent level of a skipped dimension is in phase 3,
                // exclude FUs from caching that do not process cache blocks
                for (const auto dim : parent_skipped_dimensions) {
                    caching_code.append(stringf("#if %s == 0 && %s == 0\n",
                                                _macros.num_steps(kernel, parent_level(cb_level), dim, phase),
                                                _macros.num_extra_cached_iterations(kernel, parent_level(cb_level), dim, phase)
                    ));
                    caching_code.append(stringf("if (%s < (%s + %s - 1) / %s)\n",
                                                _macros.fu_id(kernel, parent_level(parent_level(cb_level)), dim),
                                                _macros.num_extra_elements(kernel, parent_level(cb_level), dim, phase),
                                                _macros.num_fu(kernel, parent_level(cb_level), dim),
                                                _macros.num_fu(kernel, parent_level(cb_level), dim)
                    ));
                    caching_code.append("#endif\n");
                }
                for (const auto dim : skipped_dimensions) {
                    if (contains(parent_skipped_dimensions, dim)) {
                        caching_code.append(stringf("#if %s == 0 && %s == 0\n",
                                                    _macros.num_steps(kernel, parent_level(cb_level), dim, phase),
                                                    _macros.num_extra_cached_iterations(kernel, parent_level(cb_level), dim, phase)
                        ));
                        caching_code.append(stringf("if (%s < %s - %s * %s)\n",
                                                    _macros.fu_id(kernel, parent_level(cb_level), dim),
                                                    _macros.num_extra_elements(kernel, parent_level(cb_level), dim, phase),
                                                    _macros.fu_id(kernel, parent_level(parent_level(cb_level)), dim),
                                                    _macros.num_fu(kernel, parent_level(cb_level), dim)
                        ));
                        caching_code.append("#endif\n");
                    } else {
                        if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(cb_level), dim)] == 3) {
                            caching_code.append(stringf("if (%s < %s - %s * %s)\n",
                                                        _macros.fu_id(kernel, parent_level(cb_level), dim),
                                                        _macros.num_extra_elements(kernel, parent_level(cb_level), dim, phase),
                                                        _macros.fu_id(kernel, parent_level(parent_level(cb_level)), dim),
                                                        _macros.num_fu(kernel, parent_level(cb_level), dim)
                            ));
                        }
                    }
                }

                caching_code.append("{\n");

                // swap double buffering pointers
                auto swap_pointer_code = [&] () {
                    std::string swap_code;
                    swap_code.append(stringf("cb_%c_%s_read = cb_%c_%s_read == cb_%c_%s_0 ? cb_%c_%s_1 : cb_%c_%s_0;\n",
                                             lower_case(cb_level), lower_case(input->input_name()),
                                             lower_case(cb_level), lower_case(input->input_name()),
                                             lower_case(cb_level), lower_case(input->input_name()),
                                             lower_case(cb_level), lower_case(input->input_name()),
                                             lower_case(cb_level), lower_case(input->input_name())));
                    swap_code.append(stringf("cb_%c_%s_write = cb_%c_%s_write == cb_%c_%s_0 ? cb_%c_%s_1 : cb_%c_%s_0;\n",
                                             lower_case(cb_level), lower_case(input->input_name()),
                                             lower_case(cb_level), lower_case(input->input_name()),
                                             lower_case(cb_level), lower_case(input->input_name()),
                                             lower_case(cb_level), lower_case(input->input_name()),
                                             lower_case(cb_level), lower_case(input->input_name())));
#ifdef PRINT_CACHING_DEBUG_SWAP_PTRS
                    swap_code.append(stringf("DEBUG_PRINT(\"%7s ptrs swapped: cb_%c_%s_write: %%16p cb_%c_%s_read: %%16p\\n\", cb_%c_%s_write, cb_%c_%s_read);\n",
                                             lower_case(long_level(cb_level)),
                                             lower_case(cb_level), lower_case(input->input_name()),
                                             lower_case(cb_level), lower_case(input->input_name()),
                                             lower_case(cb_level), lower_case(input->input_name()),
                                             lower_case(cb_level), lower_case(input->input_name())
                    ));
#endif
                    return swap_code;
                };
                if (!loading_cb_0 && dbl_buffering) {
                    caching_code.append(indent(swap_pointer_code(), 1));
                }

                // define step variables for skipped dimensions
                std::map<dimension_t, std::vector<LEVEL>> skipped_loops;
                if (cb_level == LEVEL::LOCAL) {
                    caching_code.append(indent(concat(multi_stringf("size_t %s = 0;", loop_generator<L_DIMS, R_DIMS>::loop_variable(V(LEVEL::LOCAL), skipped_dimensions)), "\n"), 1));
                    caching_code.append("\n");
                    for (const auto &dim : skipped_dimensions) {
                        skipped_loops[dim] = {LEVEL::LOCAL};
                    }
                } else {
                    caching_code.append(indent(concat(multi_stringf("size_t %s = 0;", join(loop_generator<L_DIMS, R_DIMS>::loop_variable(V(LEVEL::LOCAL), parent_skipped_dimensions),
                                                                                           loop_generator<L_DIMS, R_DIMS>::loop_variable(V(LEVEL::PRIVATE), skipped_dimensions))), "\n"), 1));
                    caching_code.append("\n");
                    for (const auto &dim : parent_skipped_dimensions) {
                        skipped_loops[dim] = {LEVEL::LOCAL};
                    }
                    for (const auto &dim : skipped_dimensions) {
                        if (skipped_loops.find(dim) == skipped_loops.end()) {
                            skipped_loops[dim] = {LEVEL::PRIVATE};
                        } else {
                            skipped_loops[dim].push_back(LEVEL::PRIVATE);
                        }
                    }
                }

                // define variables for present dimensions when double buffering and not post processing
                if (dbl_buffering && !present_dimensions.empty() && !post_processing) {
                    caching_code.append(indent(concat(multi_stringf("size_t tmp_%s = %s;\n#define %s tmp_%s",
                                                                    loop_generator<L_DIMS, R_DIMS>::loop_variable(V(cb_level), present_dimensions),
                                                                    loop_generator<L_DIMS, R_DIMS>::loop_variable(V(cb_level), present_dimensions),
                                                                    loop_generator<L_DIMS, R_DIMS>::loop_variable(V(cb_level), present_dimensions),
                                                                    loop_generator<L_DIMS, R_DIMS>::loop_variable(V(cb_level), present_dimensions)), "\n"), 1));
                    caching_code.append("\n");
                    caching_code.append("  bool last_cache_block = false;\n");
                    std::vector<dimension_t> prev_input_dimensions;
                    std::vector<dimension_t> next_iter_dimensions;
                    for (const auto dim : _md_hom.dimension_order()) {
                        next_iter_dimensions.push_back(dim);
                        if (dim == dimension)
                            break;
                    }
                    next_iter_dimensions = reverse(next_iter_dimensions);
                    for (const auto &dim : next_iter_dimensions) {
                        if (dim == next_iter_dimensions.front()) {
                            caching_code.append("  if ");
                        } else {
                            caching_code.append("  } else if ");
                        }
                        if (contains(input->distinct_used_dimensions(), dim)) {
                            // if dim is relevant for buffer, only load next cache block if it will be a phase 1 cache block
                            caching_code.append(stringf("(%s < %s - 1) {\n",
                                                        loop_generator<L_DIMS, R_DIMS>::loop_variable(cb_level, dim),
                                                        _macros.num_steps(kernel, cb_level, dim, phase)));
                            for (const auto &prev_dim : next_iter_dimensions) {
                                if (prev_dim == dim) break;
                                if (contains(input->distinct_used_dimensions(), prev_dim))
                                    caching_code.append(stringf("    %s = 0;\n", loop_generator<L_DIMS, R_DIMS>::loop_variable(cb_level, prev_dim)));
                            }
                            if (contains(input->distinct_used_dimensions(), dim)) {
                                caching_code.append(stringf("    ++%s;\n", loop_generator<L_DIMS, R_DIMS>::loop_variable(cb_level, dim)));
                            }

                            prev_input_dimensions.push_back(dim);
                        } else {
                            // if dim is not relevant for this buffer, load first cache block
                            caching_code.append(stringf("(%s < (%s + (%s > 0) + (%s < (%s + %s - 1) / %s)) - 1) {\n",
                                                        loop_generator<L_DIMS, R_DIMS>::loop_variable(cb_level, dim),
                                                        _macros.num_steps(kernel, cb_level, dim, phase),
                                                        _macros.num_extra_cached_iterations(kernel, cb_level, dim, phase),
                                                        _macros.fu_id(kernel, parent_level(cb_level), dim),
                                                        _macros.num_extra_elements(kernel, cb_level, dim, phase),
                                                        _macros.num_fu(kernel, cb_level, dim),
                                                        _macros.num_fu(kernel, cb_level, dim)));
                            caching_code.append(concat(multi_stringf("    %s = 0;\n", loop_generator<L_DIMS, R_DIMS>::loop_variable(V(cb_level), prev_input_dimensions))));
                        }
                    }
                    if (parent_level(cb_level) > LEVEL::GLOBAL) {
                        for (const auto &dim : reverse(_md_hom.dimension_order())) {
                            caching_code.append(stringf("  } else if (%s < (%s + (%s > 0) + (%s < (%s + %s - 1) / %s)) - 1) {\n",
                                                        loop_generator<L_DIMS, R_DIMS>::loop_variable(parent_level(cb_level), dim),
                                                        _macros.num_steps(kernel, parent_level(cb_level), dim, phase),
                                                        _macros.num_extra_cached_iterations(kernel, parent_level(cb_level), dim, phase),
                                                        _macros.fu_id(kernel, parent_level(parent_level(cb_level)), dim),
                                                        _macros.num_extra_elements(kernel, parent_level(cb_level), dim, phase),
                                                        _macros.num_fu(kernel, parent_level(cb_level), dim),
                                                        _macros.num_fu(kernel, parent_level(cb_level), dim)));
                            if (contains(input->distinct_used_dimensions(), dim)) {
                                caching_code.append("    last_cache_block = true;\n");
                            } else {
                                caching_code.append(concat(multi_stringf("    %s = 0;\n", loop_generator<L_DIMS, R_DIMS>::loop_variable(V(cb_level), input->distinct_used_dimensions()))));
                            }
                        }
                    }
                    caching_code.append("  } else {\n");
                    caching_code.append("    last_cache_block = true;\n");
                    caching_code.append("  }\n");
                    caching_code.append("  if (!last_cache_block)\n");
                }

                caching_code.append("  {\n");

                // generate caching code: ignore double buffering, when loading first double buffered cache block or caching code has been pulled in front of used dimensions on cb_level (i.e. only 1 cb exists)
                caching_code.append(indent(input->caching_copy_code(kernel, cb_level, phase, skipped_loops, dbl_buffering), 2));

                // reset step variables
                if (dbl_buffering && !loading_cb_0 && !post_processing) {
                    // reset step variables
                    caching_code.append(indent(concat(multi_stringf("  #undef %s", loop_generator<L_DIMS, R_DIMS>::loop_variable(V(cb_level), present_dimensions)), "\n"), 1));
                    caching_code.append("\n");
                }

                // swap pointers after loading first cache block, when that will be the only cache block that gets loaded
                // or when double buffering and loading a post processing cache block
                if (dbl_buffering && (loading_cb_0 || post_processing)) {
                    if (!use_code_at_this_position_skipped_dims_condition.empty())
                        caching_code.append(stringf("    #if %s\n", use_code_at_this_position_skipped_dims_condition));
                    caching_code.append(indent(swap_pointer_code(), 2));
                    if (!use_code_at_this_position_skipped_dims_condition.empty())
                        caching_code.append("    #endif\n\n");
                }

                // wait for cache block to be loaded if double buffering and post processing (this is done here and not
                // combined for all inputs, because loading a private cache block for this input may occur before all
                // post processing cache blocks for other inputs have been loaded)
                if (dbl_buffering && post_processing && cb_level == LEVEL::LOCAL)
                    caching_code.append(stringf("    // wait for this cache block to be loaded, because it is a post processing cache block that can not be double buffered\n    %s\n", barrier(cb_level, _cuda)));

                // close ifs for checking whether to use caching code at this position
                caching_code.append("  }\n");
                caching_code.append("}\n");
                if (!use_code_at_this_position_condition.empty())
                    caching_code.append("#endif\n");
                caching_code.append("#endif\n");

                return caching_code;
            };
            // add caching code (barriers generation only works for local level, because private level does not need barriers)
            bool caching_buffers_exist = false;
            std::vector<dimension_t> all_caching_buffers_distinct_used_dimensions;
            for (const auto &input : kernel == 1 ? _input_wrappers_1 : _input_wrappers_2) {
                if (!(input->supports_caching())) continue;

                caching_buffers_exist = true;

                for (const auto dim : input->distinct_used_dimensions()) {
                    if (!contains(all_caching_buffers_distinct_used_dimensions, dim))
                        all_caching_buffers_distinct_used_dimensions.push_back(dim);
                }
            }
            if (caching_buffers_exist) {
                for (auto cb_level : {LEVEL::LOCAL, LEVEL::PRIVATE}) {
                    for (int dbl_buf = 0; dbl_buf < 2; ++dbl_buf) {
                        // add caching code for loading cache block 0
                        for (const auto &input : kernel == 1 ? _input_wrappers_1 : _input_wrappers_2) {
                            // local level, first dimension
                            loops.add_code([](LEVEL level, const dimension_t &dimension, const std::vector<LEVEL> &levels,
                                              const std::vector<dimension_t> &dimensions) {
                                               return level == LEVEL::LOCAL && dimension.type == dimensions.front().type && dimension.nr == dimensions.front().nr;
                                           }, OUTER_PREPEND,
                                           [&, cb_level, dbl_buf](auto level, const auto &dimension, char *phase, bool *first_iteration) -> std::string {
                                               return caching_code(cb_level, input, LEVEL::GLOBAL, _md_hom.dimension_order().back(), phase, first_iteration, dbl_buf);
                                           });
                        }

                        // add barrier for waiting after loading cache block 0 when double buffering
                        if (dbl_buf && cb_level == LEVEL::LOCAL) {
                            // local level, first dimension
                            loops.add_code([](LEVEL level, const dimension_t &dimension, const std::vector<LEVEL> &levels,
                                              const std::vector<dimension_t> &dimensions) {
                                               return level == LEVEL::LOCAL && dimension.type == dimensions.front().type && dimension.nr == dimensions.front().nr;
                                           }, OUTER_PREPEND,
                                           [&, cb_level, kernel](auto level, const auto &dimension, char *phase, bool *first_iteration) -> std::string {
                                               std::stringstream code;
                                               code << "#if ";
                                               bool first = true;
                                               for (const auto &input : kernel == 1 ? _input_wrappers_1 : _input_wrappers_2) {
                                                   if (!input->supports_caching()) continue;

                                                   if (!first)
                                                       code << " || \\\n    ";

                                                   code << stringf("(defined(%s_CACHE_%c_CB) && %s_CACHE_%c_CB == 2) || (!defined(%s_CACHE_%c_CB) && CACHE_%c_CB == 2)",
                                                                   upper_case(input->input_name()), cb_level, upper_case(input->input_name()), cb_level,
                                                                   upper_case(input->input_name()), cb_level, cb_level
                                                   );

                                                   first = false;
                                               }
                                               code << std::endl << "// wait for first double buffered cache block to be loaded";
                                               code << std::endl << barrier(cb_level, _cuda);
                                               code << std::endl << "#endif";
                                               return code.str();
                                           });
                        }

                        // add caching code for loading next cache blocks
                        for (const auto &input : kernel == 1 ? _input_wrappers_1 : _input_wrappers_2) {
                            if (!(input->supports_caching())) continue;
                            if (!input->distinct_used_dimensions().empty()) {
                                // up to cb_level, any dimension relevant for this buffer
                                loops.add_code([&, cb_level](LEVEL level, const dimension_t &dimension, const std::vector<LEVEL> &levels,
                                                   const std::vector<dimension_t> &dimensions) {
                                                   return level <= cb_level && contains(input->distinct_used_dimensions(), dimension);
                                               }, INNER_PREPEND,
                                               [&, cb_level, dbl_buf](auto level, const auto &dimension, char *phase, bool *first_iteration) -> std::string {
                                                   return caching_code(cb_level, input, level, dimension, phase, first_iteration, dbl_buf);
                                               });
                            }
                        }

                        // add barrier after innermost normal caching to wait for next cache block to be loaded
                        if (!dbl_buf && cb_level == LEVEL::LOCAL) {
                            // up to cb_level, any dimension relevant for any buffer
                            loops.add_code([&, cb_level](LEVEL level, const dimension_t &dimension, const std::vector<LEVEL> &levels,
                                                                        const std::vector<dimension_t> &dimensions) {
                                               return level == LEVEL::LOCAL && dimension.type == dimensions.front().type && dimension.nr == dimensions.front().nr;
                                           }, OUTER_PREPEND,
                                           [&, cb_level, kernel](auto level, const auto &dimension, char *phase, bool *first_iteration) -> std::string {
                                               std::stringstream code;
                                               // generate barrier here, when:
                                               //     1. at least one buffer has normal caching enabled
                                               bool first = true;
                                               for (const auto &input : kernel == 1 ? _input_wrappers_1 : _input_wrappers_2) {
                                                   if (!input->supports_caching()) continue;

                                                   if (first)
                                                       code << "#if (";
                                                   else
                                                       code << " || \\\n     ";

                                                   code << stringf("((defined(%s_CACHE_%c_CB) && %s_CACHE_%c_CB == 1) || (!defined(%s_CACHE_%c_CB) && CACHE_%c_CB == 1))",
                                                                   upper_case(input->input_name()), cb_level, upper_case(input->input_name()), cb_level,
                                                                   upper_case(input->input_name()), cb_level, cb_level
                                                   );

                                                   first = false;
                                               }
                                               if (!first)
                                                   code << ")";
                                               //     2. a buffer does double buffering on the next lower level and all subsequent dimensions that are used by that buffer only have one cache block (otherwise this is not the innermost normal caching code for this buffer)
                                               //        OR all subsequent dimensions that are used by buffers with normal caching enabled only have one cache block (otherwise this is not the innermost normal caching code)
                                               std::vector<dimension_t> subsequent_dimensions;
                                               for (const auto dim : reverse(_md_hom.dimension_order())) {
                                                   if (dim == dimension) break;
                                                   if (contains(all_caching_buffers_distinct_used_dimensions, dim)) subsequent_dimensions.push_back(dim);
                                               }
                                               subsequent_dimensions = reverse(subsequent_dimensions);
                                               if (!subsequent_dimensions.empty())
                                                   code << " && (\\\n        ";
                                               first = true;
                                               //        a buffer does double buffering on the next lower level and all subsequent dimensions that are used by that buffer only have one cache block (otherwise this is not the innermost normal caching code for this buffer)
                                               for (const auto &input : kernel == 1 ? _input_wrappers_1 : _input_wrappers_2) {
                                                   bool buffer_has_subsequent_dimensions = false;
                                                   for (const auto dim : input->distinct_used_dimensions()) {
                                                       if (contains(subsequent_dimensions, dim)) {
                                                           buffer_has_subsequent_dimensions = true;
                                                           break;
                                                       }
                                                   }
                                                   if (!input->supports_caching() || !buffer_has_subsequent_dimensions) continue;

                                                   if (!first)
                                                       code << " || \\\n        ";

                                                   code << stringf("(((defined(%s_CACHE_%c_CB) && %s_CACHE_%c_CB == 2) || (!defined(%s_CACHE_%c_CB) && CACHE_%c_CB == 2))",
                                                                   upper_case(input->input_name()), sub_level(cb_level), upper_case(input->input_name()), sub_level(cb_level),
                                                                   upper_case(input->input_name()), sub_level(cb_level), sub_level(cb_level)
                                                   );
                                                   for (const auto dim : subsequent_dimensions) {
                                                       if (!contains(input->distinct_used_dimensions(), dim)) continue;
                                                       code << stringf(" && ((%s + (%s > 0) + (%s > 0)) == 1)",
                                                                       _macros.num_steps(kernel, level, dim, phase),
                                                                       _macros.num_extra_cached_iterations(kernel, level, dim, phase),
                                                                       _macros.num_extra_elements(kernel, level, dim, phase)
                                                       );
                                                   }
                                                   code << ")";

                                                   first = false;
                                               }
                                               if (!subsequent_dimensions.empty())
                                                   code << "\\\n    ) || (\\\n        ";
                                               //        OR all subsequent dimensions that are used by buffers with normal caching enabled only have one cache block (otherwise this is not the innermost normal caching code)
                                               for (const auto dim : subsequent_dimensions) {
                                                   if (dim != subsequent_dimensions.front())
                                                       code << " && \\\n        ";

                                                   code << stringf("(((%s + (%s > 0) + (%s > 0)) == 1) || (\\\n            ",
                                                                   _macros.num_steps(kernel, level, dim, phase),
                                                                   _macros.num_extra_cached_iterations(kernel, level, dim, phase),
                                                                   _macros.num_extra_elements(kernel, level, dim, phase)
                                                   );

                                                   first = true;
                                                   for (const auto &input : kernel == 1 ? _input_wrappers_1 : _input_wrappers_2) {
                                                       if (!input->supports_caching() || !contains(input->distinct_used_dimensions(), dim)) continue;

                                                       if (!first)
                                                           code << " && \\\n            ";

                                                       code << stringf("((defined(%s_CACHE_%c_CB) && %s_CACHE_%c_CB != 1) || (!defined(%s_CACHE_%c_CB) && CACHE_%c_CB != 1))",
                                                                       upper_case(input->input_name()), cb_level, upper_case(input->input_name()), cb_level,
                                                                       upper_case(input->input_name()), cb_level, cb_level
                                                       );

                                                       first = false;
                                                   }

                                                   code << "\\\n        ))";
                                               }
                                               if (!subsequent_dimensions.empty())
                                                   code << "\\\n    )";

                                               code << std::endl << "// wait for next normal buffered cache block to be loaded";
                                               code << std::endl << barrier(cb_level, _cuda);
                                               code << std::endl << "#endif";
                                               return code.str();
                                           });
                            // up to cb_level, any dimension relevant for any buffer
                            loops.add_code([&, cb_level](LEVEL level, const dimension_t &dimension, const std::vector<LEVEL> &levels,
                                                                        const std::vector<dimension_t> &dimensions) {
                                               return level <= cb_level && contains(all_caching_buffers_distinct_used_dimensions, dimension);
                                           }, INNER_PREPEND,
                                           [&, cb_level, kernel](auto level, const auto &dimension, char *phase, bool *first_iteration) -> std::string {
                                               std::stringstream code;
                                               // generate barrier here, when:
                                               //     1. there are multiple cache blocks in this dimension (otherwise no buffer would cache here)
                                               code << stringf("#if ((%s + (%s > 0) + (%s > 0)) > 1)",
                                                               _macros.num_steps(kernel, level, dimension, phase),
                                                               _macros.num_extra_cached_iterations(kernel, level, dimension, phase),
                                                               _macros.num_extra_elements(kernel, level, dimension, phase)
                                               );
                                               //     2. at least one buffer that uses this dimension has normal caching enabled (otherwise no buffer would cache here, even if multiple cache blocks exist in this dimension)
                                               bool first = true;
                                               for (const auto &input : kernel == 1 ? _input_wrappers_1 : _input_wrappers_2) {
                                                   if (!input->supports_caching() || !contains(input->distinct_used_dimensions(), dimension)) continue;

                                                   if (first)
                                                       code << " && (\\\n        ";
                                                   else
                                                       code << " || \\\n        ";

                                                   code << stringf("((defined(%s_CACHE_%c_CB) && %s_CACHE_%c_CB == 1) || (!defined(%s_CACHE_%c_CB) && CACHE_%c_CB == 1))",
                                                                   upper_case(input->input_name()), cb_level, upper_case(input->input_name()), cb_level,
                                                                   upper_case(input->input_name()), cb_level, cb_level
                                                   );

                                                   first = false;
                                               }
                                               if (!first)
                                                   code << "\\\n    )";
                                               //     3. a buffer does double buffering on the next lower level and all subsequent dimensions that are used by that buffer only have one cache block (otherwise this is not the innermost normal caching code for this buffer)
                                               //        OR all subsequent dimensions that are used by buffers with normal caching enabled only have one cache block (otherwise this is not the innermost normal caching code)
                                               std::vector<dimension_t> subsequent_dimensions;
                                               for (const auto dim : reverse(_md_hom.dimension_order())) {
                                                   if (dim == dimension) break;
                                                   if (contains(all_caching_buffers_distinct_used_dimensions, dim)) subsequent_dimensions.push_back(dim);
                                               }
                                               subsequent_dimensions = reverse(subsequent_dimensions);
                                               if (!subsequent_dimensions.empty())
                                                   code << " && (\\\n        ";
                                               first = true;
                                               //        a buffer does double buffering on the next lower level and all subsequent dimensions that are used by that buffer only have one cache block (otherwise this is not the innermost normal caching code for this buffer)
                                               for (const auto &input : kernel == 1 ? _input_wrappers_1 : _input_wrappers_2) {
                                                   bool buffer_has_subsequent_dimensions = false;
                                                   for (const auto dim : input->distinct_used_dimensions()) {
                                                       if (contains(subsequent_dimensions, dim)) {
                                                           buffer_has_subsequent_dimensions = true;
                                                           break;
                                                       }
                                                   }
                                                   if (!input->supports_caching() || !buffer_has_subsequent_dimensions) continue;

                                                   if (!first)
                                                       code << " || \\\n        ";

                                                   code << stringf("(((defined(%s_CACHE_%c_CB) && %s_CACHE_%c_CB == 2) || (!defined(%s_CACHE_%c_CB) && CACHE_%c_CB == 2))",
                                                                   upper_case(input->input_name()), sub_level(cb_level), upper_case(input->input_name()), sub_level(cb_level),
                                                                   upper_case(input->input_name()), sub_level(cb_level), sub_level(cb_level)
                                                   );
                                                   for (const auto dim : subsequent_dimensions) {
                                                       if (!contains(input->distinct_used_dimensions(), dim)) continue;
                                                       code << stringf(" && ((%s + (%s > 0) + (%s > 0)) == 1)",
                                                                       _macros.num_steps(kernel, level, dim, phase),
                                                                       _macros.num_extra_cached_iterations(kernel, level, dim, phase),
                                                                       _macros.num_extra_elements(kernel, level, dim, phase)
                                                       );
                                                   }
                                                   code << ")";

                                                   first = false;
                                               }
                                               if (!subsequent_dimensions.empty())
                                                   code << "\\\n    ) || (\\\n        ";
                                               //        OR all subsequent dimensions that are used by buffers with normal caching enabled only have one cache block (otherwise this is not the innermost normal caching code)
                                               for (const auto dim : subsequent_dimensions) {
                                                   if (dim != subsequent_dimensions.front())
                                                       code << " && \\\n        ";

                                                   code << stringf("(((%s + (%s > 0) + (%s > 0)) == 1) || (\\\n        ",
                                                                   _macros.num_steps(kernel, level, dim, phase),
                                                                   _macros.num_extra_cached_iterations(kernel, level, dim, phase),
                                                                   _macros.num_extra_elements(kernel, level, dim, phase)
                                                   );

                                                   first = true;
                                                   for (const auto &input : kernel == 1 ? _input_wrappers_1 : _input_wrappers_2) {
                                                       if (!input->supports_caching() || !contains(input->distinct_used_dimensions(), dim)) continue;

                                                       if (!first)
                                                           code << " && \\\n            ";

                                                       code << stringf("((defined(%s_CACHE_%c_CB) && %s_CACHE_%c_CB != 1) || (!defined(%s_CACHE_%c_CB) && CACHE_%c_CB != 1))",
                                                                       upper_case(input->input_name()), cb_level, upper_case(input->input_name()), cb_level,
                                                                       upper_case(input->input_name()), cb_level, cb_level
                                                       );

                                                       first = false;
                                                   }

                                                   code << "\\\n        ))";
                                               }
                                               if (!subsequent_dimensions.empty())
                                                   code << "\\\n    )";

                                               code << std::endl << "// wait for next normal buffered cache block to be loaded";
                                               code << std::endl << barrier(cb_level, _cuda);
                                               code << std::endl << "#endif";
                                               return code.str();
                                           });
                        }
                    }
                }
            }

            int first_r_dim = 0;
            if (R_DIMS > 0) {
                for (const auto d : md_hom.dimension_order()) {
                    if (d.type == DIM_TYPE::R) {
                        first_r_dim = d.nr;
                        break;
                    }
                }
                // any level, first r dimension
                loops.add_code([first_r_dim](LEVEL level, const dimension_t &dimension, const std::vector<LEVEL> &levels,
                                  const std::vector<dimension_t> &dimensions) {
                                   return dimension.type == DIM_TYPE::R && dimension.nr == first_r_dim;
                               }, OUTER_APPEND,
                               [&](auto level, const auto &dimension, char *phase, bool *first_iteration) -> std::string {
                                   std::string code;
                                   code.append(stringf("#if %c_REDUCTION == %s && (%s)\n",
                                                       LEVEL::LOCAL, upper_case(long_level(level)),
                                                       concat(multi_stringf("%s > 1", _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS))), " || ")));
                                   code.append(_result_wrapper.reduction(kernel, LEVEL::LOCAL, level, phase, first_iteration, kernel == 1 ? _input_names_1 : _input_names_2));
                                   code.append("\n#endif");
                                   return code;

                               });
            }
            // any level, between l and r dimensions
            loops.add_code([first_r_dim](LEVEL level, const dimension_t &dimension, const std::vector<LEVEL> &levels,
                                         const std::vector<dimension_t> &dimensions) {
                               return first_r_dim == 0
                                      ? (dimension.type == DIM_TYPE::L && dimension.nr == dimensions.back().nr)
                                      : (dimension.type == DIM_TYPE::R && dimension.nr == first_r_dim);
                           },
                           first_r_dim == 0 ? INNER_APPEND : OUTER_APPEND,
                           [&](auto level, const auto &dimension, char *phase, bool *first_iteration) -> std::string {
                               std::string code = _result_wrapper.write_back_code(kernel, level, phase, first_iteration);
                               return code;
                           });
            if (R_DIMS > 0) {
                // local level, first dimension (end of kernel)
                loops.add_code([](LEVEL level, const dimension_t &dimension, const std::vector<LEVEL> &levels,
                                  const std::vector<dimension_t> &dimensions) {
                                   return level == LEVEL::LOCAL && dimension.type == dimensions.front().type && dimension.nr == dimensions.front().nr;
                               }, OUTER_APPEND,
                               [&](auto level, const auto &dimension, char *phase, bool *first_iteration) -> std::string {
                                   std::string code;
                                   code.append(stringf("#if %c_REDUCTION == %s && (%s)\n",
                                                       LEVEL::LOCAL, upper_case(long_level(LEVEL::GLOBAL)),
                                                       concat(multi_stringf("%s > 1", _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS))), " || ")));
                                   code.append(_result_wrapper.reduction(kernel, LEVEL::LOCAL, LEVEL::GLOBAL, phase, first_iteration, kernel == 1 ? _input_names_1 : _input_names_2));
                                   code.append("\n\n");
                                   code.append(_result_wrapper.write_back_code(kernel, LEVEL::GLOBAL, phase, first_iteration));
                                   code.append("\n#endif");
                                   return code;
                               });
            }
            // add barrier after processing cache blocks in case input caching is enabled
            if (caching_buffers_exist) {
                for (const auto cb_level : {LEVEL::LOCAL}) {
                    // up to cb_level, any dimension relevant for any buffer
                    loops.add_code([&, cb_level](LEVEL level, const dimension_t &dimension, const std::vector<LEVEL> &levels,
                                                 const std::vector<dimension_t> &dimensions) {
                                       return level <= cb_level && contains(all_caching_buffers_distinct_used_dimensions, dimension);
                                   }, INNER_APPEND,
                                   [&, cb_level, kernel](auto level, const auto &dimension, char *phase, bool *first_iteration) -> std::string {
                                       std::stringstream code;
                                       // generate barrier here, when:
                                       //     1. there are multiple cache blocks in this dimension (otherwise no buffer would cache here)
                                       code << stringf("#if ((%s + (%s > 0) + (%s > 0)) > 1)",
                                                       _macros.num_steps(kernel, level, dimension, phase),
                                                       _macros.num_extra_cached_iterations(kernel, level, dimension, phase),
                                                       _macros.num_extra_elements(kernel, level, dimension, phase)
                                       );
                                       //     2. at least one buffer that uses this dimension has normal or double caching enabled (otherwise no buffer would cache here, even if multiple cache blocks exist in this dimension)
                                       bool first = true;
                                       for (const auto &input : kernel == 1 ? _input_wrappers_1 : _input_wrappers_2) {
                                           if (!input->supports_caching() || !contains(input->distinct_used_dimensions(), dimension)) continue;

                                           if (first)
                                               code << " && (\\\n        ";
                                           else
                                               code << " || \\\n        ";

                                           code << stringf("((defined(%s_CACHE_%c_CB) && %s_CACHE_%c_CB != 0) || (!defined(%s_CACHE_%c_CB) && CACHE_%c_CB != 0))",
                                                           upper_case(input->input_name()), cb_level, upper_case(input->input_name()), cb_level,
                                                           upper_case(input->input_name()), cb_level, cb_level
                                           );

                                           first = false;
                                       }
                                       if (!first)
                                           code << "\\\n    )";
                                       //     3. all subsequent dimensions that are used by buffers with normal or double caching enabled only have one cache block (otherwise this is not the innermost caching code)
                                       std::vector<dimension_t> subsequent_dimensions;
                                       for (const auto dim : reverse(_md_hom.dimension_order())) {
                                           if (dim == dimension) break;
                                           if (contains(all_caching_buffers_distinct_used_dimensions, dim)) subsequent_dimensions.push_back(dim);
                                       }
                                       subsequent_dimensions = reverse(subsequent_dimensions);
                                       if (!subsequent_dimensions.empty())
                                           code << " && (\\\n        ";
                                       for (const auto dim : subsequent_dimensions) {
                                           if (dim != subsequent_dimensions.front())
                                               code << " && \\\n        ";

                                           code << stringf("((%s + (%s > 0) + (%s > 0)) == 1) || (\\\n            ",
                                                           _macros.num_steps(kernel, level, dim, phase),
                                                           _macros.num_extra_cached_iterations(kernel, level, dim, phase),
                                                           _macros.num_extra_elements(kernel, level, dim, phase)
                                           );

                                           first = true;
                                           for (const auto &input : kernel == 1 ? _input_wrappers_1 : _input_wrappers_2) {
                                               if (!input->supports_caching() || !contains(input->distinct_used_dimensions(), dim)) continue;

                                               if (!first)
                                                   code << " && \\\n            ";

                                               code << stringf("((defined(%s_CACHE_%c_CB) && %s_CACHE_%c_CB == 0) || (!defined(%s_CACHE_%c_CB) && CACHE_%c_CB == 0))",
                                                               upper_case(input->input_name()), cb_level, upper_case(input->input_name()), cb_level,
                                                               upper_case(input->input_name()), cb_level, cb_level
                                               );

                                               first = false;
                                           }

                                           code << "\\\n        )";
                                       }
                                       if (!subsequent_dimensions.empty())
                                           code << "\\\n    )";

                                       code << std::endl << "// wait for current cache block to be processed before loading next cache block";
                                       code << std::endl << barrier(cb_level, _cuda);
                                       code << std::endl << "#endif";
                                       return code.str();
                                   });
                }
            }
            auto tmp = loops.from(
                    kernel, LEVEL::LOCAL, _md_hom.dimension_order().front(),
                    [&,this](auto level, const auto &dimension, char *phase, bool *first_iteration) -> std::string {
                        auto loop_body = [&](bool reduce) {
                            std::string loop_body = "// process one mda element\n";
                            if (kernel == 1) {
                                for (const auto &dim : dim_range(L_DIMS, R_DIMS)) {
                                    loop_body.append(stringf("#define %c%d %s\n",
                                                             lower_case(dim.type), dim.nr,
                                                             _macros.fu_index_conversion(kernel, LEVEL::LOCAL, dim, _macros.fu_index_conversion(kernel, LEVEL::PRIVATE, dim, _macros.iteration_to_index_conversion(kernel, LEVEL::PRIVATE, dim, iterations_loop_variable(LEVEL::PRIVATE, dim))))
                                    ));
                                }
                                for (const auto &input : _input_wrappers_1) {
                                    if (input->supports_caching()) {
                                        for (int value_id = 1; value_id < input->index_functions().size() - 1; ++value_id) {
                                            loop_body.append(stringf("#define %s%s %s(%s)\n",
                                                                     input->input_name(), input->index_functions().size() == 3 ? "" : stringf("_%d", value_id),
                                                                     input->fu_macro_name(kernel, value_id, LEVEL::PRIVATE, phase),
                                                                     concat(_macros.iteration_to_index_conversion(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, R_DIMS), iterations_loop_variable(LEVEL::PRIVATE, dim_range(L_DIMS, R_DIMS))), ", ")
                                            ));
                                        }
                                    }
                                }
                                for (const result_buffer_class &output : _outputs_1) {
                                    loop_body.append(stringf("#define %s %s\n", output.name(), _result_wrapper.call_fu_res_macro(output.name(), kernel, LEVEL::PRIVATE, phase, _macros.iteration_to_index_conversion(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), iterations_loop_variable(LEVEL::PRIVATE, dim_range(L_DIMS, 0))))));
                                }
                                loop_body.append(_md_hom.get_scalar_function().function_body(reduce));
#ifdef PRINT_CACHING_DEBUG_SCALAR_FUNCTION_ELEMENTS
                                loop_body.append("\n// debug printing");
                                loop_body.append("\nDEBUG_PRINT(\"calling f with parameters:\\n\");");
                                for (const auto &input : _input_wrappers_1) {
                                    if (input->supports_caching()) {
                                        for (int value_id = 1; value_id < input->index_functions().size() - 1; ++value_id) {
                                            std::string printf_code = stringf(
                                                    "\nDEBUG_PRINT(\"    %s%s: %%s(=%%%%16p)%s (== %%%%4.0f)\\n\", %%s, %%s, %s%s);",
                                                    input->input_name(), input->index_functions().size() == 3 ? "" : stringf("_%d", value_id),
                                                    concat(wrap(repeat(std::string("%%2lu"), input->index_functions()[0].size()), "[", "]")),
                                                    input->input_name(), input->index_functions().size() == 3 ? "" : stringf("_%d", value_id)
                                            );
                                            std::string debug_code;
                                            std::vector<std::string> converted_indices = _macros.iteration_to_index_conversion(V(kernel), V(level), dim_range(L_DIMS, R_DIMS), iterations_loop_variable(level, dim_range(L_DIMS, R_DIMS)));
                                            for (const auto p_level : level_range(level, LEVEL::LOCAL)) {
                                                debug_code.append(stringf("\n%s (defined(%s_CACHE_%c_CB) && %s_CACHE_%c_CB == 2) || (!defined(%s_CACHE_%c_CB) && CACHE_%c_CB == 2)",
                                                                          debug_code.empty() ? "#if" : "#elif",
                                                                          upper_case(input->input_name()), p_level, upper_case(input->input_name()), p_level,
                                                                          upper_case(input->input_name()), p_level, p_level
                                                ));
                                                debug_code.append(stringf(printf_code,
                                                                          stringf("cb_%c_%s_read", lower_case(p_level), lower_case(input->input_name())),
                                                                          stringf("cb_%c_%s_read", lower_case(p_level), lower_case(input->input_name())),
                                                                          concat(multi_stringf("BUFFER_%s_INDEX_%d(%s)", V(upper_case(input->input_name())), uint_range(input->index_functions()[0].size() - 1, 0), V(concat(input->replace_vars_in_index_function(V<unsigned int>(value_id), uint_range(0, input->index_functions()[value_id].size() - 1), V(converted_indices)), ", "))), ", ")));
                                                debug_code.append(stringf("\n%s (defined(%s_CACHE_%c_CB) && %s_CACHE_%c_CB == 1) || (!defined(%s_CACHE_%c_CB) && CACHE_%c_CB == 1)",
                                                                          debug_code.empty() ? "#if" : "#elif",
                                                                          upper_case(input->input_name()), p_level, upper_case(input->input_name()), p_level,
                                                                          upper_case(input->input_name()), p_level, p_level
                                                ));
                                                debug_code.append(stringf(printf_code,
                                                                          stringf("cb_%c_%s", lower_case(p_level), lower_case(input->input_name())),
                                                                          stringf("cb_%c_%s", lower_case(p_level), lower_case(input->input_name())),
                                                                          concat(multi_stringf("BUFFER_%s_INDEX_%d(%s)", V(upper_case(input->input_name())), uint_range(input->index_functions()[0].size() - 1, 0), V(concat(input->replace_vars_in_index_function(V<unsigned int>(value_id), uint_range(0, input->index_functions()[value_id].size() - 1), V(converted_indices)), ", "))), ", ")));
                                                converted_indices = _macros.index_conversion(V(kernel), V(level), dim_range(L_DIMS, R_DIMS), converted_indices, V(phase));
                                            }
                                            debug_code.append("\n#else");
                                            debug_code.append(stringf(printf_code,
                                                                      stringf("      %s_buf", lower_case(input->input_name())),
                                                                      stringf("%s_buf", lower_case(input->input_name())),
                                                                      concat(input->replace_vars_in_index_function(V<unsigned int>(value_id), uint_range(0, input->index_functions()[value_id].size() - 1), V(converted_indices)), ", ")));
                                            debug_code.append("\n#endif");
                                            loop_body.append(debug_code);
                                        }
                                    }
                                }
                                loop_body.append("\n");
#endif
                                loop_body.append("\n");
                                for (const result_buffer_class &output : _outputs_1) {
                                    loop_body.append(stringf("#undef %s\n", output.name()));
                                }
                                for (const auto &input : _input_wrappers_1) {
                                    if (input->supports_caching()) {
                                        for (int value_id = 1; value_id < input->index_functions().size() - 1; ++value_id) {
                                            loop_body.append(stringf("#undef %s%s\n", input->input_name(), input->index_functions().size() == 3 ? "" : stringf("_%d", value_id)));
                                        }
                                    }
                                }
                                for (const auto &dim : dim_range(L_DIMS, R_DIMS)) {
                                    loop_body.append(stringf("#undef %c%d\n", lower_case(dim.type), dim.nr));
                                }
                            } else {
                                for (int i = 0; i < _inputs_2.size(); ++i) {
                                    loop_body.append(stringf("%s;\n",
                                                             !reduce
                                                             ? stringf("%s = %s", _result_wrapper.call_fu_res_macro(_outputs_1[i].get().name(), kernel, LEVEL::PRIVATE, phase, _macros.iteration_to_index_conversion(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), iterations_loop_variable(LEVEL::PRIVATE, dim_range(L_DIMS, 0)))), _input_wrappers_2[i]->call_fu_macro(kernel, 1, LEVEL::PRIVATE, phase, _macros.iteration_to_index_conversion(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, R_DIMS), iterations_loop_variable(LEVEL::PRIVATE, dim_range(L_DIMS, R_DIMS)))))
                                                             : _outputs_1[i].get().reduce_and_store(_result_wrapper.call_fu_res_macro(_outputs_1[i].get().name(), kernel, LEVEL::PRIVATE, phase, _macros.iteration_to_index_conversion(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, 0), iterations_loop_variable(LEVEL::PRIVATE, dim_range(L_DIMS, 0)))), _input_wrappers_2[i]->call_fu_macro(kernel, 1, LEVEL::PRIVATE, phase, _macros.iteration_to_index_conversion(V(kernel), V(LEVEL::PRIVATE), dim_range(L_DIMS, R_DIMS), iterations_loop_variable(LEVEL::PRIVATE, dim_range(L_DIMS, R_DIMS)))))));
                                }
                            }
                            return loop_body;
                        };
                        std::string code;
                        if (R_DIMS == 0) {
                            code.append(wrap_in_iterations_loop<L_DIMS, R_DIMS>(
                                    kernel,
                                    LEVEL::PRIVATE,
                                    _md_hom.dimension_order(),
                                    loop_body(false),
                                    true,
                                    phase,
                                    true));
                        } else {
                            bool first_iteration_in_level = true;
                            for (const auto dim : dim_range(0, R_DIMS)) {
                                first_iteration_in_level = first_iteration_in_level && first_iteration[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(level, dim)];
                            }
                            if (first_iteration_in_level) {
                                auto first_level_not_in_first_iter = get_first_parent_level_not_in_first_iteration<L_DIMS, R_DIMS>(level, first_iteration);
                                if (first_level_not_in_first_iter != level) {
                                    // there is a parent level that is not in its first iteration
                                    // check if that level saves its results in memory different from this level
                                    code.append(stringf(
#ifdef REDUCTION_COPY_COMPLETE
                                    "#if %c_CB_RES_DEST_LEVEL > %c_CB_RES_DEST_LEVEL%s\n",
                                            first_level_not_in_first_iter, level,
                                            R_DIMS > 0 ? stringf(" || (L_REDUCTION == %s && (%s) && (%c_CB_RES_DEST_LEVEL < LOCAL || %c_CB_RES_DEST_LEVEL == GLOBAL))", upper_case(long_level(level)), concat(multi_stringf("%s > 1", _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS))), " || "), level, level) : ""
#else
                                            "#if (%c_CB_RES_DEST_LEVEL > %c_CB_RES_DEST_LEVEL%s)%s\n", first_level_not_in_first_iter, level,
                                            R_DIMS > 0 ? stringf(" || (L_REDUCTION == %s && (%s) && %c_CB_RES_DEST_LEVEL == GLOBAL)", upper_case(long_level(level)), concat(multi_stringf("%s > 1", _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS))), " || "), level) : "",
                                            R_DIMS > 0 ? stringf(" && (L_REDUCTION != %s || (%s) || %c_CB_RES_DEST_LEVEL >= LOCAL)", upper_case(long_level(level)), concat(multi_stringf("%s == 1", _macros.num_fu(V(kernel), V(LEVEL::LOCAL), dim_range(0, R_DIMS))), " && "), level) : ""
#endif
                                    ));
                                }

                                std::function<std::string(std::vector<dimension_t>,bool)> wrap_in_loops = [&](std::vector<dimension_t> dimensions, bool first) -> std::string {
                                    if (dimensions.empty()) {
                                        return loop_body(!first);
                                    }
                                    if (dimensions.front().type == DIM_TYPE::L || !first) {
                                        return wrap_in_iterations_loop<L_DIMS, R_DIMS>(
                                                kernel,
                                                LEVEL::PRIVATE,
                                                V(dimensions.front()),
                                                wrap_in_loops(std::vector<dimension_t>(dimensions.begin() + 1, dimensions.end()), first),
                                                true,
                                                phase,
                                                true
                                        );
                                    } else {
                                        bool dim_in_phase3 = false;
                                        for (LEVEL p_level : parent_levels(level, true)) {
                                            if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(p_level, dimensions.front())] == 3) {
                                                dim_in_phase3 = true;
                                                break;
                                            }
                                        }
                                        return stringf("size_t %s = 0;\n%s\n%s",
                                                       iterations_loop_variable(LEVEL::PRIVATE, dimensions.front()),
                                                       wrap_in_loops(std::vector<dimension_t>(dimensions.begin() + 1, dimensions.end()), true),
                                                       dim_in_phase3 ? "" : stringf("\n%s%sfor (%s = 1; %s < %s; ++%s) {\n%s\n}%s\n",
                                                                                    (_runtime_inputs && phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dimensions.front())] == 2)
                                                                                    ? ""
                                                                                    : stringf("#if (%s > 1)\n", _macros.num_cached_iterations(kernel, LEVEL::PRIVATE, dimensions.front(), phase)),
                                                                                    "#pragma unroll\n",
                                                                                    iterations_loop_variable(LEVEL::PRIVATE, dimensions.front()),
                                                                                    iterations_loop_variable(LEVEL::PRIVATE, dimensions.front()),
                                                                                    _macros.num_cached_iterations(kernel, LEVEL::PRIVATE, dimensions.front(), phase),
                                                                                    iterations_loop_variable(LEVEL::PRIVATE, dimensions.front()),
                                                                                    indent(wrap_in_loops(std::vector<dimension_t>(dimensions.begin() + 1, dimensions.end()), false), 1),
                                                                                    (_runtime_inputs && phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dimensions.front())] == 2)
                                                                                    ? ""
                                                                                    : "\n#endif"
                                                       )
                                        );
                                    }
                                };

                                code.append(wrap_in_loops(_md_hom.dimension_order(), true));
                                if (first_level_not_in_first_iter != level) {
                                    code.append("\n#else\n");
                                    code.append(wrap_in_iterations_loop<L_DIMS, R_DIMS>(
                                            kernel,
                                            LEVEL::PRIVATE,
                                            _md_hom.dimension_order(),
                                            loop_body(true),
                                            true,
                                            phase,
                                            true));
                                    code.append("\n#endif");
                                }
                            } else {
                                code.append(wrap_in_iterations_loop<L_DIMS, R_DIMS>(
                                        kernel,
                                        LEVEL::PRIVATE,
                                        _md_hom.dimension_order(),
                                        loop_body(true),
                                        true,
                                        phase,
                                        true));
                            }
                        }

#ifdef PRINT_CACHING_DEBUG_SCALAR_FUNCTION_CB
                        std::string debug_print = "// debug printing";
                        debug_print.append(stringf("\nDEBUG_PRINT(\"processing private cache block %s\\n\", %s);\n",
                                concat(multi_stringf("%c_%d: %%lu", dim_range_types(L_DIMS, R_DIMS), dim_range_nrs(L_DIMS, R_DIMS)), ", "),
                                concat(multi_stringf("%c_step_%c_%d", V(lower_case(level)), lower_case(dim_range_types(L_DIMS, R_DIMS)), dim_range_nrs(L_DIMS, R_DIMS)), ", ")
                        ));
                        debug_print.append(code);
                        code = debug_print;
#endif

                        return code;
                    });
            indent_inplace(tmp, 1);
            kernel_source.append(tmp);

            kernel_source.append("\n}\n");
            kernel_source.append(stringf("// =============== end of kernel %d ============================================", kernel));
        }
    }

    std::string kernel_1() const {
        return _kernel_1;
    }

    std::string kernel_2() const {
        return _kernel_2;
    }

private:
    const md_hom_class<L_DIMS, R_DIMS, TOs, TIs...> &_md_hom;
    const bool                                      _runtime_inputs;
    const bool                                      _cuda;
    std::vector<dimension_t> _l_dims_after_first_r_dim;

    const macros<L_DIMS, R_DIMS>                 _macros;
    std::vector<std::unique_ptr<input_wrapper>>  _input_wrappers_1;
    std::vector<std::string>                     _input_names_1;
    std::vector<std::string>                     _output_names_1;
    std::vector<std::string>                     _input_parameter_definitions_1;
    std::vector<std::string>                     _output_parameter_definitions_1;
    std::vector<std::reference_wrapper<const result_buffer_class>> _outputs_1;
    std::vector<std::string>                     _output_types;
    std::vector<std::unique_ptr<input_buffer_class>> _inputs_2;
    std::vector<std::unique_ptr<input_wrapper>>  _input_wrappers_2;
    std::vector<std::string>                     _input_names_2;
    std::vector<std::string>                     _output_names_2;
    std::vector<std::string>                     _input_parameter_definitions_2;
    std::vector<std::string>                     _output_parameter_definitions_2;
    const result_buffer_wrapper<L_DIMS, R_DIMS, TOs> _result_wrapper;

    std::string _kernel_1;
    std::string _kernel_2;

    // helper for wrapping inputs
    template< size_t... Is >
    void wrap_inputs_1(const std::tuple<TIs...> &inputs, std::index_sequence<Is...>) {
        wrap_inputs_1_impl(std::get<Is>(inputs)...);
    }
    template<typename... ARGS>
    void wrap_inputs_1_impl(const input_buffer_class &buffer, ARGS&... args) {
        _input_wrappers_1.emplace_back(new input_buffer_wrapper<L_DIMS, R_DIMS>(buffer, _macros, _runtime_inputs, _cuda));
        wrap_inputs_1_impl(args...);
    }
    template<typename... ARGS>
    void wrap_inputs_1_impl(const input_scalar_class &scalar, ARGS&... args) {
        _input_wrappers_1.emplace_back(new input_scalar_wrapper<L_DIMS, R_DIMS>(scalar, _macros));
        wrap_inputs_1_impl(args...);
    }
    void wrap_inputs_1_impl() {}
    template< size_t... Is >
    void wrap_inputs_2(const TOs &outputs, std::index_sequence<Is...>) {
        wrap_inputs_2_impl(std::get<Is>(outputs)...);
    }
    template<typename... ARGS>
    void wrap_inputs_2_impl(const result_buffer_class &buffer, ARGS&... args) {
        _outputs_1.emplace_back(std::cref(buffer));
        _output_types.emplace_back(buffer.data_type());
        _inputs_2.emplace_back(new input_buffer_class(buffer.data_type(), std::string("int_res_") + buffer.name(), {multi_stringf("%c%d", lower_case(dim_range_types(L_DIMS, R_DIMS)), dim_range_nrs(L_DIMS, R_DIMS)), multi_stringf("%c%d", lower_case(dim_range_types(L_DIMS, R_DIMS)), dim_range_nrs(L_DIMS, R_DIMS)), multi_stringf("%c%d", lower_case(dim_range_types(L_DIMS, R_DIMS)), dim_range_nrs(L_DIMS, R_DIMS))}, {}, {}));
        _input_wrappers_2.emplace_back(new input_buffer_wrapper<L_DIMS, R_DIMS>(*_inputs_2.back(), _macros, _runtime_inputs, _cuda));
        wrap_inputs_2_impl(args...);
    }
    void wrap_inputs_2_impl() {}

    // helper for getting the input & output names and parameter definitions
    template< size_t... Is >
    void get_input_names_and_definitions_1(const std::tuple<TIs...> &inputs, std::index_sequence<Is...>) {
        get_input_names_and_definitions_1_impl(std::get<Is>(inputs)...);
    }
    template<typename... ARGS>
    void get_input_names_and_definitions_1_impl(const input_buffer_class &buffer, ARGS&... args) {
        _input_names_1.push_back(buffer.name());
        _input_parameter_definitions_1.push_back((_cuda ? "" : "__global ") + buffer.data_type() + " const * const" + (_cuda ? " __restrict__ " : " restrict ") + buffer.name() + "_buf_raw");
        get_input_names_and_definitions_1_impl(args...);
    }
    template<typename... ARGS>
    void get_input_names_and_definitions_1_impl(const input_scalar_class &scalar, ARGS&... args) {
        _input_names_1.push_back(scalar.name());
        _input_parameter_definitions_1.push_back("const " + scalar.data_type() + " " + scalar.name());
        get_input_names_and_definitions_1_impl(args...);
    }
    void get_input_names_and_definitions_1_impl() {}
    template< size_t... Is >
    void get_output_names_and_definitions_1(const TOs &outputs, std::index_sequence<Is...>) {
        get_output_names_and_definitions_1_impl(std::get<Is>(outputs)...);
    }
    template<typename... ARGS>
    void get_output_names_and_definitions_1_impl(const result_buffer_class &buffer, ARGS&... args) {
        _output_names_1.push_back(std::string("int_res_") + buffer.name());
        _output_parameter_definitions_1.push_back((_cuda ? "" : "__global ") + buffer.data_type() + " * const" + (_cuda ? " __restrict__ " : " restrict ") +  "int_res_" + buffer.name() + "_raw");
        get_output_names_and_definitions_1_impl(args...);
    }
    void get_output_names_and_definitions_1_impl() {}
    template< size_t... Is >
    void get_input_names_and_definitions_2(const TOs &outputs, std::index_sequence<Is...>) {
        get_input_names_and_definitions_2_impl(std::get<Is>(outputs)...);
    }
    template<typename... ARGS>
    void get_input_names_and_definitions_2_impl(const result_buffer_class &buffer, ARGS&... args) {
        _input_names_2.push_back(std::string("int_res_") + buffer.name());
        _input_parameter_definitions_2.push_back((_cuda ? "" : "__global ") + buffer.data_type() + " const * const" + (_cuda ? " __restrict__ " : " restrict ") +  "int_res_" + buffer.name() + "_buf_raw");
        get_input_names_and_definitions_2_impl(args...);
    }
    void get_input_names_and_definitions_2_impl() {}
    template< size_t... Is >
    void get_output_names_and_definitions_2(const TOs &outputs, std::index_sequence<Is...>) {
        get_output_names_and_definitions_2_impl(std::get<Is>(outputs)...);
    }
    template<typename... ARGS>
    void get_output_names_and_definitions_2_impl(const result_buffer_class &buffer, ARGS&... args) {
        _output_names_2.push_back(buffer.name());
        _output_parameter_definitions_2.push_back((_cuda ? "" : "__global ") + buffer.data_type() + " * const" + (_cuda ? " __restrict__ " : " restrict ") + buffer.name() + "_raw");
        get_output_names_and_definitions_2_impl(args...);
    }
    void get_output_names_and_definitions_2_impl() {}

};

template <unsigned int L_DIMS, unsigned int R_DIMS, typename TOs, typename... TIs>
auto ocl_generator(const md_hom_class<L_DIMS, R_DIMS, TOs, TIs...> &md_hom, bool runtime_inputs = false, const std::vector<dimension_t> &skip_post_processing = {}, unsigned int thread_nesting = 0) {
    return ocl_generator_class<L_DIMS, R_DIMS, TOs, TIs...>(md_hom, runtime_inputs, skip_post_processing, thread_nesting, false);
}

template <unsigned int L_DIMS, unsigned int R_DIMS, typename TOs, typename... TIs>
auto cuda_generator(const md_hom_class<L_DIMS, R_DIMS, TOs, TIs...> &md_hom, bool runtime_inputs = false, const std::vector<dimension_t> &skip_post_processing = {}, unsigned int thread_nesting = 0) {
    return ocl_generator_class<L_DIMS, R_DIMS, TOs, TIs...>(md_hom, runtime_inputs, skip_post_processing, thread_nesting, true);
}

}
}


#endif //MD_BLAS_OCL_GENERATOR_HPP
