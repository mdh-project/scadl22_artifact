#include "data_util/include/data_util.hpp"
#include "tclap/CmdLine.h"
#include "OCAL/ocal_ocl.hpp"
#include "json/json.hpp"
#include "google_sheets_helper.hpp"
using json = nlohmann::json;

#include <chrono>
#include <algorithm>
#include <tuple>
#include <utility>

#include "meta_info.cpp"
#include "data_info.cpp"

template <typename... Ts>
void resize_buffers(size_t size, std::tuple<Ts...> &buffers);
template <typename... Ts>
bool result_correct(double epsilon, std::vector<std::string> gold_files, std::tuple<Ts...> &inputs);

int main(int argc, const char **argv) {
    TCLAP::CmdLine cmd("Benchmark md_hom kernel.");

    TCLAP::ValueArg<int> platform_id_arg("p", "platform-id", "OpenCL platform id.", false, 0, "int");
    cmd.add(platform_id_arg);

    TCLAP::ValueArg<int> device_id_arg("d", "device-id", "OpenCL device id.", false, 0, "int");
    cmd.add(device_id_arg);

    TCLAP::ValueArg<bool> dynamic_arg("", "dynamic", "Use kernel with dynamic input sizes.", false, false, "bool");
    cmd.add(dynamic_arg);

    TCLAP::ValueArg<int> warm_ups_arg("w", "warm-ups", "Number of warm ups per configuration.", false, 10, "int");
    cmd.add(warm_ups_arg);

    TCLAP::ValueArg<int> evaluations_arg("e", "evaluations", "Number of evaluations per configuration.", false, 200, "int");
    cmd.add(evaluations_arg);

    TCLAP::ValueArg<bool> with_confidence_arg("", "with-confidence", "Continue benchmarking after passed number of evaluations until the x% confidence interval is within y% of median.", false, false, "bool");
    cmd.add(with_confidence_arg);

    TCLAP::ValueArg<float> confidence_arg("", "confidence", "Confidence of the confidence interval (e.g. 0.99 for 99% CI).", false, 0.99, "float");
    cmd.add(confidence_arg);

    TCLAP::ValueArg<float> ci_max_dist_to_median_arg("", "ci-max-dist-to-median", "Maximum allowed distance of confidence interval to median (e.g. 0.05 for a CI within 5% of the median).", false, 0.05, "float");
    cmd.add(ci_max_dist_to_median_arg);

    TCLAP::MultiArg<std::string> gold_file_arg("g", "gold-file", "Gold file for result check.", false, "string");
    cmd.add(gold_file_arg);

    TCLAP::ValueArg<double> epsilon_arg("", "epsilon", "The epsilon to use for result checking.", false, 0.0, "double");
    cmd.add(epsilon_arg);

    TCLAP::ValueArg<std::string> config_file_arg("c", "config-file", "The configuration to benchmark. If no file is specified, the configuration file from the data directory will be used.", false, "", "string");
    cmd.add(config_file_arg);

    TCLAP::ValueArg<std::string> kernel_file_1_arg("", "kernel-file-1", "The kernel file for the first kernel to benchmark. If no file is specified, the default kernel file from the kernel directory will be used.", false, "", "string");
    cmd.add(kernel_file_1_arg);

    TCLAP::ValueArg<std::string> kernel_file_2_arg("", "kernel-file-2", "The kernel file for the second kernel to benchmark. If no file is specified, the default kernel file from the kernel directory will be used.", false, "", "string");
    cmd.add(kernel_file_2_arg);

    TCLAP::ValueArg<std::string> gsheets_python_command_arg("", "google-sheets-python-command", "Python command to call for pushing data to Google Sheets API.", false, "", "string");
    cmd.add(gsheets_python_command_arg);

    TCLAP::ValueArg<std::string> gsheets_pickle_arg("", "google-sheets-pickle", "Pickle file to use for login to Google Sheets API.", false, "", "string");
    cmd.add(gsheets_pickle_arg);

    TCLAP::ValueArg<std::string> gsheets_spreadsheet_id_arg("", "google-sheets-spreadsheet-id", "Google Sheets spreadsheet id to push data to.", false, "", "string");
    cmd.add(gsheets_spreadsheet_id_arg);

    TCLAP::ValueArg<std::string> gsheets_sheet_name_arg("", "google-sheets-sheet-name", "Google Sheets sheet name to push data to.", false, "", "string");
    cmd.add(gsheets_sheet_name_arg);

    TCLAP::ValueArg<std::string> gsheets_entry_id_arg("", "google-sheets-entry-id", "Google Sheets entry id to push data to.", false, "", "string");
    cmd.add(gsheets_entry_id_arg);

    TCLAP::ValueArg<std::string> path_suffix_arg("x", "suffix", "Data path suffix.", false, "", "string");
    cmd.add(path_suffix_arg);

    TCLAP::ValueArg<std::string> full_path_arg("", "path", "Data path.", false, "", "string");
    cmd.add(full_path_arg);

    TCLAP::ValueArg<int> input_size_l_1_arg("", "input-size-l-1", "Input size in dimension L_1.", true, 0, "int");
    cmd.add(input_size_l_1_arg);

    TCLAP::ValueArg<int> input_size_r_1_arg("", "input-size-r-1", "Input size in dimension R_1.", true, 0, "int");
    cmd.add(input_size_r_1_arg);

    // add custom arguments
    add_arguments(cmd);

    // parse arguments
    try {
        cmd.parse(argc, argv);
    } catch (TCLAP::ArgException &e) {
        std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
    }
    bool sheets_data_provided = !gsheets_python_command_arg.getValue().empty() ||
                                !gsheets_pickle_arg.getValue().empty() ||
                                !gsheets_spreadsheet_id_arg.getValue().empty() ||
                                !gsheets_sheet_name_arg.getValue().empty() ||
                                !gsheets_entry_id_arg.getValue().empty();
    bool all_sheets_data_provided = !gsheets_python_command_arg.getValue().empty() &&
                                    !gsheets_pickle_arg.getValue().empty() &&
                                    !gsheets_spreadsheet_id_arg.getValue().empty() &&
                                    !gsheets_sheet_name_arg.getValue().empty() &&
                                    !gsheets_entry_id_arg.getValue().empty();
    if (sheets_data_provided && !all_sheets_data_provided) {
        std::cerr << "sheets data was only provided partially" << std::endl;
        exit(EXIT_FAILURE);
    }

    // get device
    ocal::device<OCL> ocal_device(platform_id_arg.getValue(), device_id_arg.getValue());
    std::string dev_name = ocal_device.name();

    // prepare directory for tuning results
    std::string application = "md_hom";
    std::string version = "strided_v3";
    std::string routine = "bias_add_grad";
    int input_size_l_1 = input_size_l_1_arg.getValue();
    int input_size_r_1 = input_size_r_1_arg.getValue();
    std::string input_size = std::to_string(input_size_l_1) + "x" + std::to_string(input_size_r_1);
    std::vector<std::string> path = {dev_name, application, version, routine, dynamic_arg.getValue() ? "dynamic" : "static", input_size};
    std::string data_dir = full_path_arg.getValue();
    if (data_dir.empty()) {
        // build path for benchmarking results if no path was specified in args
        benchmark_data_path(path);
        if (!path_suffix_arg.getValue().empty()) path.emplace_back(path_suffix_arg.getValue());
        data_dir = "../" + data_directory(path);
    }
    prepare_data_directory(data_dir, true);

    // get path for kernel files and kernel name
    path = {"kernel", application, version, routine, dynamic_arg.getValue() ? "dynamic" : "static"};
    generation_data_path(path);
    std::string kernel_dir = "../" + data_directory(path);
    std::string file_1 = kernel_dir, file_2 = kernel_dir;
    std::string kernel_name;
    if (dynamic_arg.getValue() == true) {
        file_1 += "/" + routine + "_dynamic_1";
        file_2 += "/" + routine + "_dynamic_2";
        kernel_name = routine + "_dynamic";
    } else {
        file_1 += "/" + routine + "_static_1";
        file_2 += "/" + routine + "_static_2";
        kernel_name = routine + "_static";
    }
    file_1 += ".cl";
    file_2 += ".cl";
    if (!kernel_file_1_arg.getValue().empty()) {
        file_1 = kernel_file_1_arg.getValue();
    }
    if (!kernel_file_2_arg.getValue().empty()) {
        file_2 = kernel_file_2_arg.getValue();
    }

    // read kernel source
    std::ifstream stream_1(file_1);
    std::string kernel_1 = std::string((std::istreambuf_iterator<char>(stream_1)),
                                       std::istreambuf_iterator<char>());
    stream_1.close();
    std::ifstream stream_2(file_2);
    std::string kernel_2 = std::string((std::istreambuf_iterator<char>(stream_2)),
                                       std::istreambuf_iterator<char>());
    stream_2.close();

    // read configuration
    std::string config_file = config_file_arg.getValue();
    if (config_file.empty()) config_file = data_dir + "/tuned_configuration";
    std::ifstream config_file_stream(config_file, std::ios::in);
    if (!config_file_stream.good()) {
        std::cerr << "configuration file \"" + config_file + "\" could not be found" << std::endl;
        exit(EXIT_FAILURE);
    }
    std::map<std::string, int> configuration = json::parse(config_file_stream);
    config_file_stream.close();

    // ---- execute kernel ----
    // set flags
    std::vector<std::string> flags;
    for (const auto &val : configuration) {
        flags.push_back(std::string("-D") + val.first + "=" + std::to_string(val.second));
    }

    // determine global and local size
    ocl::nd_range gs_1{1, 1, 1};
    ocl::nd_range ls_1{1, 1, 1};
    gs_1.set(std::min(configuration["OCL_DIM_L_1"], 2), gs_1.get(std::min(configuration["OCL_DIM_L_1"], 2)) * configuration["NUM_WG_L_1"] * configuration["NUM_WI_L_1"]);
    ls_1.set(std::min(configuration["OCL_DIM_L_1"], 2), ls_1.get(std::min(configuration["OCL_DIM_L_1"], 2)) * configuration["NUM_WI_L_1"]);
    gs_1.set(std::min(configuration["OCL_DIM_R_1"], 2), gs_1.get(std::min(configuration["OCL_DIM_R_1"], 2)) * configuration["NUM_WG_R_1"] * configuration["NUM_WI_R_1"]);
    ls_1.set(std::min(configuration["OCL_DIM_R_1"], 2), ls_1.get(std::min(configuration["OCL_DIM_R_1"], 2)) * configuration["NUM_WI_R_1"]);

    // initialize data
    init_data(std::vector<int>({input_size_l_1}), std::vector<int>({input_size_r_1}));
    decltype(outputs(0)) ocal_int_res;
    resize_buffers(input_size_l_1 * configuration["NUM_WG_R_1"], ocal_int_res);
    size_t res_g_1_size = input_size_l_1;
    if (configuration["G_CB_RES_DEST_LEVEL"] == 2) {
        res_g_1_size *= configuration["NUM_WG_R_1"];
    }
    if ((configuration["P_CB_RES_DEST_LEVEL"] == 2 && configuration["L_REDUCTION"] >= 0) ||
        (configuration["L_CB_RES_DEST_LEVEL"] == 2 && configuration["L_REDUCTION"] >= 1) ||
        (configuration["G_CB_RES_DEST_LEVEL"] == 2 && configuration["L_REDUCTION"] >= 2)) {
        res_g_1_size *= configuration["NUM_WI_R_1"];
    }
    decltype(outputs(0)) res_g_1;
    resize_buffers(res_g_1_size, res_g_1);

    // profile kernel
    std::map<std::string, std::pair<std::vector<long long>, std::vector<long long>>> runtimes;
    long long compile_time_1 = 0;
    long long compile_time_2 = 0;
    bool needs_second_kernel = configuration["NUM_WG_R_1"] > 1;
    auto compile_start = std::chrono::high_resolution_clock::now();
    ocal_device(ocl::source(kernel_1), kernel_name + "_1", flags);
    auto compile_end = std::chrono::high_resolution_clock::now();
    compile_time_1 = std::chrono::duration_cast<std::chrono::milliseconds>(compile_end - compile_start).count();
    ocal_device(gs_1 , ls_1);
    if (needs_second_kernel) {
        std::cout << std::endl << "benchmarking kernel 1 (0/" << warm_ups_arg.getValue() + evaluations_arg.getValue() << ")";
        runtimes[kernel_name + "_1"] = std::make_pair(std::vector<long long>(), std::vector<long long>());
        int i;
        for (i = 0; i < warm_ups_arg.getValue() + evaluations_arg.getValue(); ++i) {
            if (dynamic_arg.getValue() == true) {
                std::apply(ocal_device, std::tuple_cat(std::make_tuple(input_size_l_1, input_size_r_1), inputs(0), res_g_1, ocal_int_res));
            } else {
                std::apply(ocal_device, std::tuple_cat(inputs(0), res_g_1, ocal_int_res));
            }
            (i < warm_ups_arg.getValue() ? runtimes[kernel_name + "_1"].first : runtimes[kernel_name + "_1"].second).push_back(ocal_device.last_runtime());

            std::cout << "\rbenchmarking kernel 1 (" << i + 1 << "/" << warm_ups_arg.getValue() + evaluations_arg.getValue() << ")";
            std::cout.flush();
        }
        if (with_confidence_arg.getValue()) {
            std::vector<long long> sorted_runtimes = runtimes[kernel_name + "_1"].second;
            std::sort(sorted_runtimes.begin(), sorted_runtimes.end());
            size_t median_index = get_percentile_index(sorted_runtimes, 50);
            auto ci_bounds = get_confidence_interval_indices(sorted_runtimes, median_index, confidence_arg.getValue());
            float confidence_interval_size = ((float) std::max(sorted_runtimes[median_index] - sorted_runtimes[ci_bounds.first],
                                                               sorted_runtimes[ci_bounds.second] - sorted_runtimes[median_index]))
                                             / sorted_runtimes[median_index];
            std::cout << "\rbenchmarking kernel 1 (" << i + 1 << "/" << warm_ups_arg.getValue() + evaluations_arg.getValue() << ", " << (confidence_arg.getValue() * 100) << "% CI within " << (confidence_interval_size * 100) << "% of median)";
            while (confidence_interval_size > ci_max_dist_to_median_arg.getValue()) {
                if (dynamic_arg.getValue() == true) {
                    std::apply(ocal_device, std::tuple_cat(std::make_tuple(input_size_l_1, input_size_r_1), inputs(0), res_g_1, ocal_int_res));
                } else {
                    std::apply(ocal_device, std::tuple_cat(inputs(0), res_g_1, ocal_int_res));
                }

                runtimes[kernel_name + "_1"].second.push_back(ocal_device.last_runtime());
                sorted_runtimes.insert(std::lower_bound(sorted_runtimes.begin(), sorted_runtimes.end(),
                                                        runtimes[kernel_name + "_1"].second.back()),
                                       runtimes[kernel_name + "_1"].second.back());

                median_index = get_percentile_index(sorted_runtimes, 50);
                ci_bounds = get_confidence_interval_indices(sorted_runtimes, median_index, confidence_arg.getValue());
                confidence_interval_size = ((float) std::max(sorted_runtimes[median_index] - sorted_runtimes[ci_bounds.first],
                                                             sorted_runtimes[ci_bounds.second] - sorted_runtimes[median_index]))
                                           / sorted_runtimes[median_index];

                ++i;
                std::cout << "\rbenchmarking kernel 1 (" << i + 1 << "/" << warm_ups_arg.getValue() + evaluations_arg.getValue() << ", " << (confidence_arg.getValue() * 100) << "% CI within " << (confidence_interval_size * 100) << "% of median)";
                std::cout.flush();
            }
        }

        ocl::nd_range gs_2{1, 1, 1};
        ocl::nd_range ls_2{1, 1, 1};
        gs_2.set(std::min(configuration["OCL_DIM_L_1"], 2), gs_2.get(std::min(configuration["OCL_DIM_L_1"], 2)) * configuration["NUM_WG_L_1"] * configuration["NUM_WI_L_1"]);
        ls_2.set(std::min(configuration["OCL_DIM_L_1"], 2), ls_2.get(std::min(configuration["OCL_DIM_L_1"], 2)) * configuration["NUM_WI_L_1"]);
        gs_2.set(std::min(configuration["OCL_DIM_R_1"], 2), gs_2.get(std::min(configuration["OCL_DIM_R_1"], 2)) * configuration["NUM_WI_R_1"]);
        ls_2.set(std::min(configuration["OCL_DIM_R_1"], 2), ls_2.get(std::min(configuration["OCL_DIM_R_1"], 2)) * configuration["NUM_WI_R_1"]);
        size_t res_g_2_size = input_size_l_1;
        if ((configuration["P_CB_RES_DEST_LEVEL"] == 2 && configuration["L_REDUCTION"] >= 0) ||
            (configuration["L_CB_RES_DEST_LEVEL"] == 2 && configuration["L_REDUCTION"] >= 1) ||
            (configuration["G_CB_RES_DEST_LEVEL"] == 2 && configuration["L_REDUCTION"] >= 2)) {
            res_g_2_size *= configuration["NUM_WI_R_1"];
        }
        decltype(outputs(0)) res_g_2;
        resize_buffers(res_g_2_size, res_g_2);

        compile_start = std::chrono::high_resolution_clock::now();
        ocal_device(ocl::source(kernel_2), kernel_name + "_2", flags);
        compile_end = std::chrono::high_resolution_clock::now();
        compile_time_2 = std::chrono::duration_cast<std::chrono::milliseconds>(compile_end - compile_start).count();
        ocal_device(gs_2 , ls_2);
        std::cout << std::endl << "benchmarking kernel 2 (0/" << warm_ups_arg.getValue() + evaluations_arg.getValue() << ")";
        runtimes[kernel_name + "_2"] = std::make_pair(std::vector<long long>(), std::vector<long long>());
        for (i = 0; i < warm_ups_arg.getValue() + evaluations_arg.getValue(); ++i) {
            if (dynamic_arg.getValue() == true) {
                std::apply(ocal_device, std::tuple_cat(ocal_int_res, res_g_2, outputs(0), std::make_tuple(input_size_l_1, input_size_r_1)));
            } else {
                std::apply(ocal_device, std::tuple_cat(ocal_int_res, res_g_2, outputs(0)));
            }
            (i < warm_ups_arg.getValue() ? runtimes[kernel_name + "_2"].first : runtimes[kernel_name + "_2"].second).push_back(ocal_device.last_runtime());

            std::cout << "\rbenchmarking kernel 2 (" << i + 1 << "/" << warm_ups_arg.getValue() + evaluations_arg.getValue() << ")";
            std::cout.flush();
        }
        if (with_confidence_arg.getValue()) {
            std::vector<long long> sorted_runtimes = runtimes[kernel_name + "_2"].second;
            std::sort(sorted_runtimes.begin(), sorted_runtimes.end());
            size_t median_index = get_percentile_index(sorted_runtimes, 50);
            auto ci_bounds = get_confidence_interval_indices(sorted_runtimes, median_index, confidence_arg.getValue());
            float confidence_interval_size = ((float) std::max(sorted_runtimes[median_index] - sorted_runtimes[ci_bounds.first],
                                                               sorted_runtimes[ci_bounds.second] - sorted_runtimes[median_index]))
                                             / sorted_runtimes[median_index];
            std::cout << "\rbenchmarking kernel 2 (" << i + 1 << "/" << warm_ups_arg.getValue() + evaluations_arg.getValue() << ", " << (confidence_arg.getValue() * 100) << "% CI within " << (confidence_interval_size * 100) << "% of median)";
            while (confidence_interval_size > ci_max_dist_to_median_arg.getValue()) {
                if (dynamic_arg.getValue() == true) {
                    std::apply(ocal_device, std::tuple_cat(ocal_int_res, res_g_2, outputs(0), std::make_tuple(input_size_l_1, input_size_r_1)));
                } else {
                    std::apply(ocal_device, std::tuple_cat(ocal_int_res, res_g_2, outputs(0)));
                }

                runtimes[kernel_name + "_2"].second.push_back(ocal_device.last_runtime());
                sorted_runtimes.insert(std::lower_bound(sorted_runtimes.begin(), sorted_runtimes.end(),
                                                        runtimes[kernel_name + "_2"].second.back()),
                                       runtimes[kernel_name + "_2"].second.back());

                median_index = get_percentile_index(sorted_runtimes, 50);
                ci_bounds = get_confidence_interval_indices(sorted_runtimes, median_index, confidence_arg.getValue());
                confidence_interval_size = ((float) std::max(sorted_runtimes[median_index] - sorted_runtimes[ci_bounds.first],
                                                             sorted_runtimes[ci_bounds.second] - sorted_runtimes[median_index]))
                                           / sorted_runtimes[median_index];

                ++i;
                std::cout << "\rbenchmarking kernel 2 (" << i + 1 << "/" << warm_ups_arg.getValue() + evaluations_arg.getValue() << ", " << (confidence_arg.getValue() * 100) << "% CI within " << (confidence_interval_size * 100) << "% of median)";
                std::cout.flush();
            }
        }
    } else {
        std::cout << std::endl << "benchmarking kernel 1 (0/" << warm_ups_arg.getValue() + evaluations_arg.getValue() << ")";
        runtimes[kernel_name + "_1"] = std::make_pair(std::vector<long long>(), std::vector<long long>());
        int i;
        for (i = 0; i < warm_ups_arg.getValue() + evaluations_arg.getValue(); ++i) {
            if (dynamic_arg.getValue() == true) {
                std::apply(ocal_device, std::tuple_cat(std::make_tuple(input_size_l_1, input_size_r_1), inputs(0), res_g_1, outputs(0)));
            } else {
                std::apply(ocal_device, std::tuple_cat(inputs(0), res_g_1, outputs(0)));
            }
            (i < warm_ups_arg.getValue() ? runtimes[kernel_name + "_1"].first : runtimes[kernel_name + "_1"].second).push_back(ocal_device.last_runtime());

            std::cout << "\rbenchmarking kernel 1 (" << i + 1 << "/" << warm_ups_arg.getValue() + evaluations_arg.getValue() << ")";
            std::cout.flush();
        }
        if (with_confidence_arg.getValue()) {
            std::vector<long long> sorted_runtimes = runtimes[kernel_name + "_1"].second;
            std::sort(sorted_runtimes.begin(), sorted_runtimes.end());
            size_t median_index = get_percentile_index(sorted_runtimes, 50);
            auto ci_bounds = get_confidence_interval_indices(sorted_runtimes, median_index, confidence_arg.getValue());
            float confidence_interval_size = ((float) std::max(sorted_runtimes[median_index] - sorted_runtimes[ci_bounds.first],
                                                               sorted_runtimes[ci_bounds.second] - sorted_runtimes[median_index]))
                                             / sorted_runtimes[median_index];
            std::cout << "\rbenchmarking kernel 1 (" << i + 1 << "/" << warm_ups_arg.getValue() + evaluations_arg.getValue() << ", " << (confidence_arg.getValue() * 100) << "% CI within " << (confidence_interval_size * 100) << "% of median)";
            while (confidence_interval_size > ci_max_dist_to_median_arg.getValue()) {
                if (dynamic_arg.getValue() == true) {
                    std::apply(ocal_device, std::tuple_cat(std::make_tuple(input_size_l_1, input_size_r_1), inputs(0), res_g_1, outputs(0)));
                } else {
                    std::apply(ocal_device, std::tuple_cat(inputs(0), res_g_1, outputs(0)));
                }

                runtimes[kernel_name + "_1"].second.push_back(ocal_device.last_runtime());
                sorted_runtimes.insert(std::lower_bound(sorted_runtimes.begin(), sorted_runtimes.end(),
                                                        runtimes[kernel_name + "_1"].second.back()),
                                       runtimes[kernel_name + "_1"].second.back());

                median_index = get_percentile_index(sorted_runtimes, 50);
                ci_bounds = get_confidence_interval_indices(sorted_runtimes, median_index, confidence_arg.getValue());
                confidence_interval_size = ((float) std::max(sorted_runtimes[median_index] - sorted_runtimes[ci_bounds.first],
                                                             sorted_runtimes[ci_bounds.second] - sorted_runtimes[median_index]))
                                           / sorted_runtimes[median_index];

                ++i;
                std::cout << "\rbenchmarking kernel 1 (" << i + 1 << "/" << warm_ups_arg.getValue() + evaluations_arg.getValue() << ", " << (confidence_arg.getValue() * 100) << "% CI within " << (confidence_interval_size * 100) << "% of median)";
                std::cout.flush();
            }
        }
    }
    std::cout << std::endl;

    std::string error_status = "not checked";
    if (!gold_file_arg.getValue().empty()) {
        // check result
        auto output_buffer = outputs(0);
        if (result_correct(epsilon_arg.getValue(), gold_file_arg.getValue(), output_buffer)) {
            error_status = "no errors found";
        } else {
            error_status = "errors found";
        }
    }

    // write configuration to file
    std::ofstream runtime_config_file(data_dir + "/runtime_configuration", std::ios::out | std::ios::trunc);
    runtime_config_file << std::setw(4) << json(configuration);
    runtime_config_file.close();

    // write profiling data to files
    json runtimes_json;
    long long runtimes_min = 0;
    long long runtimes_median = 0;
    long long runtimes_average = 0;
    long long runtimes_max = 0;
    for (auto &kernel : runtimes) {
        runtimes_json[kernel.first]["warm ups"] = kernel.second.first;
        runtimes_json[kernel.first]["evaluations"] = kernel.second.second;
        if (with_confidence_arg.getValue()) {
            std::vector<long long> sorted_runtimes = kernel.second.second;
            std::sort(sorted_runtimes.begin(), sorted_runtimes.end());
            size_t median_index = get_percentile_index(sorted_runtimes, 50);
            auto ci_bounds = get_confidence_interval_indices(sorted_runtimes, median_index, confidence_arg.getValue());
            float confidence_interval_size = ((float) std::max(sorted_runtimes[median_index] - sorted_runtimes[ci_bounds.first],
                                                               sorted_runtimes[ci_bounds.second] - sorted_runtimes[median_index]))
                                             / sorted_runtimes[median_index];
            runtimes_json[kernel.first][std::to_string(confidence_arg.getValue() * 100) + "% confidence interval within x% of median"] = confidence_interval_size;
        }

        runtimes_min += *std::min_element(kernel.second.second.begin(), kernel.second.second.end());
        std::nth_element(kernel.second.second.begin(),
                         kernel.second.second.begin() + (kernel.second.second.size() / 2),
                         kernel.second.second.end());
        runtimes_median += kernel.second.second[kernel.second.second.size() / 2];
        runtimes_average += std::accumulate(kernel.second.second.begin(), kernel.second.second.end(), 0ll) / kernel.second.second.size();
        runtimes_max += *std::max_element(kernel.second.second.begin(), kernel.second.second.end());
    }
    std::ofstream runtimes_file(data_dir + "/runtimes", std::ios::out | std::ios::trunc);
    runtimes_file << std::setw(4) << json(runtimes_json);
    runtimes_file.close();
    std::ofstream runtimes_min_file(data_dir + "/runtimes_min", std::ios::out | std::ios::trunc);
    runtimes_min_file << runtimes_min;
    runtimes_min_file.close();
    std::ofstream runtimes_median_file(data_dir + "/runtimes_median", std::ios::out | std::ios::trunc);
    runtimes_median_file << runtimes_median;
    runtimes_median_file.close();
    std::ofstream runtimes_average_file(data_dir + "/runtimes_average", std::ios::out | std::ios::trunc);
    runtimes_average_file << runtimes_average;
    runtimes_average_file.close();
    std::ofstream runtimes_max_file(data_dir + "/runtimes_max", std::ios::out | std::ios::trunc);
    runtimes_max_file << runtimes_max;
    runtimes_max_file.close();
    json meta_json;
    meta_json["warm ups"] = std::to_string(warm_ups_arg.getValue());
    meta_json["evaluations"] = std::to_string(evaluations_arg.getValue());
    meta_json["gold file"] = gold_file_arg.getValue();
    meta_json["result check epsilon"] = epsilon_arg.getValue();
    meta_json["error status"] = error_status;
    meta_json["compilation time kernel 1"] = std::to_string(compile_time_1);
    meta_json["compilation time kernel 2"] = std::to_string(compile_time_2);
    meta_json["dynamic input sizes"] = dynamic_arg.getValue();
    meta_json["kernel 1 source file"] = file_1;
    meta_json["kernel 2 source file"] = file_2;
    meta_json["runtimes"] = runtimes_json;
    meta_json["runtimes_min"] = runtimes_min;
    meta_json["runtimes_median"] = runtimes_median;
    meta_json["runtimes_average"] = runtimes_average;
    meta_json["runtimes_max"] = runtimes_max;
    std::ofstream meta_file(data_dir + "/benchmark_meta", std::ios::out | std::ios::trunc);
    meta_file << std::setw(4) << meta_json;
    meta_file.close();
    if (all_sheets_data_provided) {
        std::stringstream json_string;
        json_string << meta_json;
        push_data(gsheets_python_command_arg.getValue(),
                  gsheets_pickle_arg.getValue(),
                  gsheets_spreadsheet_id_arg.getValue(),
                  gsheets_sheet_name_arg.getValue(),
                  gsheets_entry_id_arg.getValue(),
                  json_string.str(),
                  false);
    }
}

template <typename T>
bool equal(const T& v1, const T& v2, double epsilon) {
    if (epsilon == 0) return v1 == v2;
    T diff = v1 - v2;
    if (diff < 0) diff = -diff;
    return diff < epsilon;
}

template <typename... Ts, size_t... Is>
void resize_buffers(size_t size, std::tuple<Ts...> &buffers, std::index_sequence<Is...>);
template <typename T, typename... Ts>
void resize_buffers(size_t size, T &input, Ts&... buffers);
template <typename T, typename... Ts>
void resize_buffers(size_t size, ocal::common::write_class<ocal::buffer<T>> &buffer, Ts&... buffers);
template <typename T, typename... Ts>
void resize_buffers(size_t size, ocal::common::read_write_class<ocal::buffer<T>> &buffer, Ts&... buffers);
void resize_buffers(size_t size);

template <typename... Ts>
void resize_buffers(size_t size, std::tuple<Ts...> &buffers) {
    resize_buffers(size, buffers, std::make_index_sequence<sizeof...(Ts)>());
}
template <typename... Ts, size_t... Is>
void resize_buffers(size_t size, std::tuple<Ts...> &buffers, std::index_sequence<Is...>) {
    resize_buffers(size, std::get<Is>(buffers)...);
}
template <typename T, typename... Ts>
void resize_buffers(size_t size, T &input, Ts&... buffers) {
    resize_buffers(size, buffers...);
}
template <typename T, typename... Ts>
void resize_buffers(size_t size, ocal::common::write_class<ocal::buffer<T>> &buffer, Ts&... buffers) {
    if (size != buffer.get().size())
        buffer.get() = ocal::buffer<T>(size);
    resize_buffers(size, buffers...);
}
template <typename T, typename... Ts>
void resize_buffers(size_t size, ocal::common::read_write_class<ocal::buffer<T>> &buffer, Ts&... buffers) {
    if (size != buffer.get().size())
        buffer.get() = ocal::buffer<T>(size);
    resize_buffers(size, buffers...);
}
void resize_buffers(size_t size) {
}


template <typename... Ts, size_t... Is>
bool result_correct(double epsilon, std::vector<std::string> &gold_files, std::tuple<Ts...> &inputs, std::index_sequence<Is...>);
template <typename T, typename... Ts>
bool result_correct(double epsilon, std::vector<std::string> &gold_files, T &input, Ts&... inputs);
template <typename T, typename... Ts>
bool result_correct(double epsilon, std::vector<std::string> &gold_files, ocal::common::read_class<ocal::buffer<T>> &buffer, Ts&... inputs);
template <typename T, typename... Ts>
bool result_correct(double epsilon, std::vector<std::string> &gold_files, ocal::common::write_class<ocal::buffer<T>> &buffer, Ts&... inputs);
template <typename T, typename... Ts>
bool result_correct(double epsilon, std::vector<std::string> &gold_files, ocal::common::read_write_class<ocal::buffer<T>> &buffer, Ts&... inputs);
bool result_correct(double epsilon, std::vector<std::string> &gold_files);

template <typename... Ts>
bool result_correct(double epsilon, std::vector<std::string> gold_files, std::tuple<Ts...> &inputs) {
    return result_correct(epsilon, gold_files, inputs, std::make_index_sequence<sizeof...(Ts)>());
}
template <typename... Ts, size_t... Is>
bool result_correct(double epsilon, std::vector<std::string> &gold_files, std::tuple<Ts...> &inputs, std::index_sequence<Is...>) {
    return result_correct(epsilon, gold_files, std::get<Is>(inputs)...);
}
template <typename T, typename... Ts>
bool result_correct(double epsilon, std::vector<std::string> &gold_files, T &input, Ts&... inputs) {
    return result_correct(epsilon, gold_files, inputs...);
}
template <typename T, typename... Ts>
bool result_correct(double epsilon, std::vector<std::string> &gold_files, ocal::common::read_class<ocal::buffer<T>> &buffer, Ts&... inputs) {
    std::ifstream is(gold_files.front());
    std::istream_iterator<T> start(is), end;
    std::vector<T> expected_result(start, end);
    if (expected_result.size() != buffer.get().size()) {
        return false;
    }
    bool error = false;
    for (int i = 0; i < expected_result.size(); ++i) {
        if (!equal<T>(buffer.get()[i], expected_result[i], epsilon)) {
            error = true;
            break;
        }
    }
    if (error) {
        std::ofstream error_file("errors.txt", std::ios::out | std::ios::trunc);
        error_file << "result:" << std::endl;
        for (int i = 0; i < buffer.get().size(); ++i) {
            error_file << buffer.get()[i] << "\t";
        }
        error_file << std::endl << "gold:" << std::endl;
        for (int i = 0; i < expected_result.size(); ++i) {
            error_file << expected_result[i] << "\t";
        }
        error_file.close();
        return false;
    }

    gold_files.erase(gold_files.begin());
    if (!gold_files.empty())
        return result_correct(epsilon, gold_files, inputs...);
    else
        return true;
}
template <typename T, typename... Ts>
bool result_correct(double epsilon, std::vector<std::string> &gold_files, ocal::common::write_class<ocal::buffer<T>> &buffer, Ts&... inputs) {
    std::ifstream is(gold_files.front());
    std::istream_iterator<T> start(is), end;
    std::vector<T> expected_result(start, end);
    if (expected_result.size() != buffer.get().size()) {
        return false;
    }
    bool error = false;
    for (int i = 0; i < expected_result.size(); ++i) {
        if (!equal<T>(buffer.get()[i], expected_result[i], epsilon)) {
            error = true;
            break;
        }
    }
    if (error) {
        std::ofstream error_file("errors.txt", std::ios::out | std::ios::trunc);
        error_file << "result:" << std::endl;
        for (int i = 0; i < buffer.get().size(); ++i) {
            error_file << buffer.get()[i] << "\t";
        }
        error_file << std::endl << "gold:" << std::endl;
        for (int i = 0; i < expected_result.size(); ++i) {
            error_file << expected_result[i] << "\t";
        }
        error_file.close();
        return false;
    }

    gold_files.erase(gold_files.begin());
    if (!gold_files.empty())
        return result_correct(epsilon, gold_files, inputs...);
    else
        return true;
}
template <typename T, typename... Ts>
bool result_correct(double epsilon, std::vector<std::string> &gold_files, ocal::common::read_write_class<ocal::buffer<T>> &buffer, Ts&... inputs) {
    std::ifstream is(gold_files.front());
    std::istream_iterator<T> start(is), end;
    std::vector<T> expected_result(start, end);
    if (expected_result.size() != buffer.get().size()) {
        return false;
    }
    bool error = false;
    for (int i = 0; i < expected_result.size(); ++i) {
        if (!equal<T>(buffer.get()[i], expected_result[i], epsilon)) {
            error = true;
            break;
        }
    }
    if (error) {
        std::ofstream error_file("errors.txt", std::ios::out | std::ios::trunc);
        error_file << "result:" << std::endl;
        for (int i = 0; i < buffer.get().size(); ++i) {
            error_file << buffer.get()[i] << "\t";
        }
        error_file << std::endl << "gold:" << std::endl;
        for (int i = 0; i < expected_result.size(); ++i) {
            error_file << expected_result[i] << "\t";
        }
        error_file.close();
        return false;
    }

    gold_files.erase(gold_files.begin());
    if (!gold_files.empty())
        return result_correct(epsilon, gold_files, inputs...);
    else
        return true;
}
bool result_correct(double epsilon, std::vector<std::string> &gold_files) {
    return gold_files.empty();
}