#include "data_util/include/data_util.hpp"
#include "ATF/atf.h"
#include "tclap/CmdLine.h"
#include "json/json.hpp"
#include "google_sheets_helper.hpp"
using json = nlohmann::json;

#include <sys/types.h>
#include <sys/wait.h>

#include "meta_info.cpp"
#include "data_info.cpp"

int main(int argc, const char **argv) {
    // check if process wrapper is called
    bool is_wrapper = atf::tuner_with_constraints::is_process_wrapper(argc, argv);

    // define parameters
    TCLAP::CmdLine cmd("Tune md_hom kernel.");

    TCLAP::ValueArg<int> platform_id_arg("p", "platform-id", "OpenCL platform id.", false, 0, "int");
    cmd.add(platform_id_arg);

    TCLAP::ValueArg<int> device_id_arg("d", "device-id", "OpenCL device id.", false, 0, "int");
    cmd.add(device_id_arg);

    TCLAP::ValueArg<bool> dynamic_arg("", "dynamic", "Use kernel with dynamic input sizes.", false, false, "bool");
    cmd.add(dynamic_arg);

    TCLAP::MultiArg<unsigned long long> weights_arg("", "weights", "Weights for average tuning.", false, "unsigned long long");
    cmd.add(weights_arg);

    std::vector<std::string> weighting_functions({
            "weighted_sum",
            "baseline_weighting"
    });
    TCLAP::ValueArg<std::string> weighting_function_arg("", "weighting-function", "The weighting function to use on the case results.", false, "weighted_sum",
                                                        new TCLAP::ValuesConstraint<std::string>(weighting_functions));
    cmd.add(weighting_function_arg);

    TCLAP::MultiArg<std::string> weighting_function_params_arg("", "weighting-function-params", "Parameters for the weighting function.", false, "string");
    cmd.add(weighting_function_params_arg);

    std::vector<std::string> search_space_restrictions({
            "in-cache-l-0", "in-cache-l-1", "in-cache-l-2", "in-cache-l-no-2", "in-cache-p-0", "in-cache-p-1", "in-cache-p-2", "in-cache-p-no-2",
            "int-res-out-cache-l-0", "int-res-out-cache-l-1", "int-res-out-cache-l-no-2", "int-res-out-cache-p-0", "int-res-out-cache-p-1", "int-res-out-cache-p-no-2",
            "l-cb-res-g", "p-cb-res-g",
            "l-cb-res-l", "p-cb-res-l",
            "l-cb-res-p", "p-cb-res-p",
            "pow2", "md_poly",
            "no-pp-l-1", "no-pp-r-1",
            "one-wg-l-1", "one-wg-r-1",
            "pow2-num-wg-l-1", "pow2-num-wg-r-1",
            "one-wi-l-1", "one-wi-r-1",
            "pow2-num-wi-l-1", "pow2-num-wi-r-1",
            "one-l-cb-size-l-1", "one-l-cb-size-r-1",
            "is-l-cb-size-l-1", "is-l-cb-size-r-1",
            "pow2-l-cb-size-l-1", "pow2-l-cb-size-r-1",
            "one-p-cb-size-l-1", "one-p-cb-size-r-1",
            "is-p-cb-size-l-1", "is-p-cb-size-r-1",
            "pow2-p-cb-size-l-1", "pow2-p-cb-size-r-1",
            "pow2-l-1", "pow2-r-1"
    });
    TCLAP::MultiArg<std::string> search_space_restriction_arg("r", "search-space-restriction", "Search space type.", false,
            new TCLAP::ValuesConstraint<std::string>(search_space_restrictions));
    cmd.add(search_space_restriction_arg);

    std::vector<std::string> tuner_types({"exhaustive", "OpenTuner", "random"});
    TCLAP::ValueArg<std::string> tuner_type_arg("t", "tuner", "Tuner type.", false, "exhaustive",
            new TCLAP::ValuesConstraint<std::string>(tuner_types));
    cmd.add(tuner_type_arg);

    std::vector<std::string> abort_condition_types({"evaluations", "seconds", "minutes", "hours"});
    TCLAP::ValueArg<std::string> abort_condition_type_arg("a", "abort-condition", "Abort condition type.", false, "evaluations",
            new TCLAP::ValuesConstraint<std::string>(abort_condition_types));
    cmd.add(abort_condition_type_arg);

    TCLAP::ValueArg<int> abort_condition_value_arg("v", "abort-condition-value", "Abort condition value.", false, 0, "int");
    cmd.add(abort_condition_value_arg);

    TCLAP::ValueArg<int> warm_ups_arg("w", "warm-ups", "Number of warm ups per configuration.", false, 3, "int");
    cmd.add(warm_ups_arg);

    TCLAP::ValueArg<int> evaluations_arg("e", "evaluations", "Number of evaluations per configuration.", false, 5, "int");
    cmd.add(evaluations_arg);

    TCLAP::ValueArg<std::string> kernel_file_1_arg("", "kernel-file-1", "The kernel file for the first kernel to tune. If no file is specified, the default kernel file from the kernel directory will be used.", false, "", "string");
    cmd.add(kernel_file_1_arg);

    TCLAP::ValueArg<std::string> kernel_file_2_arg("", "kernel-file-2", "The kernel file for the second kernel to tune. If no file is specified, the default kernel file from the kernel directory will be used.", false, "", "string");
    cmd.add(kernel_file_2_arg);

    std::vector<std::string> wrapper_types({"none", "local", "remote"});
    TCLAP::ValueArg<std::string> wrapper_type_arg("", "wrapper-type", "Process wrapper type.", false, "none",
                                                  new TCLAP::ValuesConstraint<std::string>(wrapper_types));
    cmd.add(wrapper_type_arg);

    TCLAP::ValueArg<std::string> wrapper_command_arg("", "wrapper-command", "Process wrapper command.", false, std::string(argv[0]), "string");
    cmd.add(wrapper_command_arg);

    TCLAP::MultiArg<std::string> gold_file_arg("g", "gold-file", "Gold files for result check.", false, "string");
    cmd.add(gold_file_arg);

    std::vector<std::string> check_types({"none", "all", "final"});
    TCLAP::ValueArg<std::string> check_type_arg("", "check-type", "Result check type.", false, "none",
                                                  new TCLAP::ValuesConstraint<std::string>(check_types));
    cmd.add(check_type_arg);

    TCLAP::ValueArg<double> epsilon_arg("", "epsilon", "The epsilon to use for result checking.", false, 0.0, "double");
    cmd.add(epsilon_arg);

    std::vector<std::string> timeout_types({"absolute", "factor"});
    TCLAP::ValueArg<std::string> warm_up_timeout_type_arg("", "warm-up-timeout-type", "Warm-up timeout type.", false, "absolute",
                                                  new TCLAP::ValuesConstraint<std::string>(timeout_types));
    cmd.add(warm_up_timeout_type_arg);

    TCLAP::ValueArg<std::string> warm_up_timeout_value_arg("", "warm-up-timeout-value", "Warm-up timeout value.", false, "0", "unsigned long long/float");
    cmd.add(warm_up_timeout_value_arg);

    TCLAP::ValueArg<std::string> evaluation_timeout_type_arg("", "evaluation-timeout-type", "Evaluation timeout type.", false, "absolute",
                                                  new TCLAP::ValuesConstraint<std::string>(timeout_types));
    cmd.add(evaluation_timeout_type_arg);

    TCLAP::ValueArg<std::string> evaluation_timeout_value_arg("", "evaluation-timeout-value", "Evaluation timeout value.", false, "0", "unsigned long long/float");
    cmd.add(evaluation_timeout_value_arg);

    TCLAP::ValueArg<std::string> gsheets_python_command_arg("", "google-sheets-python-command", "Python command to call in progress callback.", false, "", "string");
    cmd.add(gsheets_python_command_arg);

    TCLAP::ValueArg<std::string> gsheets_pickle_arg("", "google-sheets-pickle", "Pickle file to use for login to Google Sheets API.", false, "", "string");
    cmd.add(gsheets_pickle_arg);

    TCLAP::ValueArg<std::string> gsheets_spreadsheet_id_arg("", "google-sheets-spreadsheet-id", "Google Sheets spreadsheet id to push progress info to.", false, "", "string");
    cmd.add(gsheets_spreadsheet_id_arg);

    TCLAP::ValueArg<std::string> gsheets_sheet_name_arg("", "google-sheets-sheet-name", "Google Sheets sheet name to push progress info to.", false, "", "string");
    cmd.add(gsheets_sheet_name_arg);

    TCLAP::ValueArg<std::string> gsheets_entry_id_arg("", "google-sheets-entry-id", "Google Sheets entry id to push progress info to.", false, "", "string");
    cmd.add(gsheets_entry_id_arg);

    TCLAP::ValueArg<unsigned long long> progress_callback_timeout_arg("", "progress-callback-timeout", "Number of seconds after which the progress info callback is called.", false, 600, "unsigned long long");
    cmd.add(progress_callback_timeout_arg);

    TCLAP::ValueArg<bool> async_sheets_push_arg("", "async-sheets-push", "Push to Google Sheets asynchronously.", false, false, "bool");
    cmd.add(async_sheets_push_arg);

    TCLAP::ValueArg<std::string> path_suffix_arg("x", "suffix", "Data path suffix.", false, "", "string");
    cmd.add(path_suffix_arg);

    TCLAP::ValueArg<std::string> full_path_arg("", "path", "Data path.", false, "", "string");
    cmd.add(full_path_arg);

    TCLAP::MultiArg<int> input_size_l_1_arg("", "input-size-l-1", "Input size in dimension L_1.", true, "int");
    cmd.add(input_size_l_1_arg);

    TCLAP::MultiArg<int> input_size_r_1_arg("", "input-size-r-1", "Input size in dimension R_1.", true, "int");
    cmd.add(input_size_r_1_arg);

    // add custom arguments
    add_arguments(cmd);

    // parse arguments
    try {
        cmd.parse(argc, argv);
    } catch (TCLAP::ArgException &e) {
        std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
        exit(EXIT_FAILURE);
    }
    int num_cases = input_size_l_1_arg.getValue().size();
    ;
    if (input_size_l_1_arg.getValue().size() != num_cases || input_size_r_1_arg.getValue().size() != num_cases) {
        std::cerr << "different numbers of values for input size arguments" << std::endl;
        exit(EXIT_FAILURE);
    }
    if (weights_arg.getValue().size() % num_cases != 0) {
        std::cerr << "number of weights has to be a multiple of number of cases" << std::endl;
        exit(EXIT_FAILURE);
    }
    if (gold_file_arg.getValue().size() % num_cases != 0) {
        std::cerr << "number of gold files has to be a multiple of number of cases" << std::endl;
        exit(EXIT_FAILURE);
    }
    atf::PROCESS_WRAPPER_TYPE wrapper_type = wrapper_type_arg.getValue() == "none" ? atf::NONE : (wrapper_type_arg.getValue() == "local" ? atf::LOCAL : atf::REMOTE);
    atf::cf::CHECK_INTERVAL check_interval = check_type_arg.getValue() == "none" ? atf::cf::CHECK_NONE : (check_type_arg.getValue() == "final" ? atf::cf::CHECK_FINAL : atf::cf::CHECK_ALL);
    std::vector<std::pair<char, int>> skip_postprocessing;
    for (const auto &str : search_space_restriction_arg.getValue()) {
        if (starts_with(str, "no-pp-")) {
            char type = str[6];
            int nr = std::atoi(str.substr(8).c_str());
            int insert_index = 0;
            for (const auto &val : skip_postprocessing) {
                if (type == 'l' && val.first == 'r' || (type == val.first && nr < val.second)) break;
                ++insert_index;
            }
            skip_postprocessing.emplace(skip_postprocessing.begin() + insert_index, type, nr);
        }
    }
    bool sheets_data_provided = !gsheets_python_command_arg.getValue().empty() ||
                                !gsheets_pickle_arg.getValue().empty() ||
                                !gsheets_spreadsheet_id_arg.getValue().empty() ||
                                !gsheets_sheet_name_arg.getValue().empty() ||
                                !gsheets_entry_id_arg.getValue().empty();
    bool all_sheets_data_provided = !gsheets_python_command_arg.getValue().empty() &&
                                    !gsheets_pickle_arg.getValue().empty() &&
                                    !gsheets_spreadsheet_id_arg.getValue().empty() &&
                                    !gsheets_sheet_name_arg.getValue().empty() &&
                                    !gsheets_entry_id_arg.getValue().empty();
    if (sheets_data_provided && !all_sheets_data_provided) {
        std::cerr << "sheets data was only provided partially" << std::endl;
        exit(EXIT_FAILURE);
    }

    // initialize device
    atf::cf::device_info<OCL> device_info(platform_id_arg.getValue(), device_id_arg.getValue());
    device_info.initialize();
    std::string dev_name = device_info.device().name();
    std::vector<size_t> max_wi_size = device_info.device().max_group_size();
    size_t combined_max_wi_size = *std::max_element(max_wi_size.begin(), max_wi_size.end());
    size_t max_wg_size_val = device_info.device().max_threads_per_group();

    // prepare directory for tuning results
    std::string application = "md_hom";
    std::string version = "strided_v3";
    std::string routine = "bias_add_grad";
    std::vector<int> input_size_l_1 = input_size_l_1_arg.getValue();
    int avg_input_size_l_1 = std::accumulate(input_size_l_1.begin(), input_size_l_1.end(), 0) / input_size_l_1.size();
    int max_input_size_l_1 = *std::max_element(input_size_l_1.begin(), input_size_l_1.end());
    std::vector<int> input_size_r_1 = input_size_r_1_arg.getValue();
    int avg_input_size_r_1 = std::accumulate(input_size_r_1.begin(), input_size_r_1.end(), 0) / input_size_r_1.size();
    int max_input_size_r_1 = *std::max_element(input_size_r_1.begin(), input_size_r_1.end());
    std::string input_size = std::to_string(avg_input_size_l_1) + "x" + std::to_string(avg_input_size_r_1);
    std::vector<std::string> path = {dev_name, application, version, routine, dynamic_arg.getValue() ? "dynamic" : "static", input_size};
    std::string data_dir = full_path_arg.getValue();
    if (data_dir.empty()) {
        // build path for tuning results if no path was specified in args
        tuning_data_path(path);
        if (!path_suffix_arg.getValue().empty()) path.emplace_back(path_suffix_arg.getValue());
        data_dir = "../" + data_directory(path);
    }
    if (!is_wrapper)
        prepare_data_directory(data_dir);

    // get path for kernel files and kernel name
    path = {"kernel", application, version, routine, dynamic_arg.getValue() ? "dynamic" : "static"};
    generation_data_path(path);
    std::string kernel_dir = "../" + data_directory(path);
    std::string file_1 = kernel_dir + "/" + routine, file_2 = kernel_dir + "/" + routine;
    if (!skip_postprocessing.empty()) {
        file_1 += "_skip_pp";
        file_2 += "_skip_pp";
    }
    for (const auto &val : skip_postprocessing) {
        file_1 += "_" + std::string(1, val.first) + std::to_string(val.second);
        file_2 += "_" + std::string(1, val.first) + std::to_string(val.second);
    }
    std::string kernel_name;
    if (dynamic_arg.getValue() == true) {
        file_1 += "_dynamic_1";
        file_2 += "_dynamic_2";
        kernel_name = routine + "_dynamic";
    } else {
        file_1 += "_static_1";
        file_2 += "_static_2";
        kernel_name = routine + "_static";
    }
    file_1 += ".cl";
    file_2 += ".cl";
    if (!kernel_file_1_arg.getValue().empty())
        file_1 = kernel_file_1_arg.getValue();
    if (!kernel_file_2_arg.getValue().empty())
        file_2 = kernel_file_1_arg.getValue();

    // initialize data
    init_data(input_size_l_1, input_size_r_1);

    // create tuner
    std::string log_file = data_dir + "/tuning_log.csv";
    std::unique_ptr<atf::tuner_with_constraints> tuner_ptr;
    auto create_tuner = [&] (const auto &abort_condition) {
        if (tuner_type_arg.getValue() == "exhaustive") {
            tuner_ptr = std::make_unique<atf::exhaustive_class<>>(abort_condition, log_file, wrapper_type, wrapper_command_arg.getValue());
        } else if (tuner_type_arg.getValue() == "OpenTuner") {
            std::string database_file = data_dir + "/tuning_log.db";
            tuner_ptr = std::make_unique<atf::open_tuner_class<>>(abort_condition, log_file, wrapper_type, wrapper_command_arg.getValue());
            dynamic_cast<atf::open_tuner_class<>*>(tuner_ptr.get())->set_path_to_database(database_file);
        } else if (tuner_type_arg.getValue() == "random") {
            tuner_ptr = std::make_unique<atf::random_search_class<>>(abort_condition, log_file, wrapper_type, wrapper_command_arg.getValue());
        }
    };
    if (abort_condition_type_arg.getValue() == "evaluations") {
        create_tuner(atf::cond::evaluations(abort_condition_value_arg.getValue()));
    } else if (abort_condition_type_arg.getValue() == "seconds") {
        create_tuner(atf::cond::duration<std::chrono::seconds>(abort_condition_value_arg.getValue()));
    } else if (abort_condition_type_arg.getValue() == "minutes") {
        create_tuner(atf::cond::duration<std::chrono::minutes>(abort_condition_value_arg.getValue()));
    } else if (abort_condition_type_arg.getValue() == "hours") {
        create_tuner(atf::cond::duration<std::chrono::hours>(abort_condition_value_arg.getValue()));
    }
    auto& tuner = *tuner_ptr;
    tuner.enable_process_wrapper(argc, argv);

    // prepare meta callback
    auto create_meta = [&] (const auto &tuner, const std::string &status) -> json {
        // write meta information
        json meta_json;
        meta_json["status"] = status;
        meta_json["search space restrictions"] = search_space_restriction_arg.getValue();
        meta_json["unconstrained search space size"] = tuner.unconstrained_search_space_size().toString();
        meta_json["constrained search space size"] = std::to_string(tuner.constrained_search_space_size());
        meta_json["tuner type"] = tuner_type_arg.getValue();
        meta_json["abort condition type"] = abort_condition_type_arg.getValue();
        meta_json["abort condition value"] = std::to_string(abort_condition_value_arg.getValue());
        meta_json["wrapper type"] = wrapper_type_arg.getValue();
        meta_json["wrapper command"] = wrapper_command_arg.getValue();
        meta_json["gold file"] = gold_file_arg.getValue();
        meta_json["check type"] = check_type_arg.getValue();
        meta_json["result check epsilon"] = epsilon_arg.getValue();
        meta_json["warm ups per configuration"] = std::to_string(warm_ups_arg.getValue());
        meta_json["warm up timeout type"] = warm_up_timeout_type_arg.getValue();
        meta_json["warm up timeout value"] = warm_up_timeout_value_arg.getValue();
        meta_json["evaluations per configuration"] = std::to_string(evaluations_arg.getValue());
        meta_json["evaluation timeout type"] = evaluation_timeout_type_arg.getValue();
        meta_json["evaluation timeout value"] = evaluation_timeout_value_arg.getValue();
        meta_json["evaluations"] = std::to_string(tuner.number_of_evaluated_configs());
        meta_json["valid evaluations"] = std::to_string(tuner.number_of_valid_evaluated_configs());
        meta_json["dynamic input sizes"] = dynamic_arg.getValue();
        meta_json["kernel 1 source file"] = file_1;
        meta_json["kernel 2 source file"] = file_2;
        meta_json["average tuning"]["weighting function"] = weighting_function_arg.getValue();
        if (weighting_function_arg.getValue() == "weighted_sum") {
            // no weighting function parameters
        } else if (weighting_function_arg.getValue() == "baseline_weighting") {
            meta_json["average tuning"]["weighting function parameters"]["exponent"] = weighting_function_params_arg.getValue().size() <= 0 ? "1.0" : weighting_function_params_arg.getValue()[0];
            meta_json["average tuning"]["weighting function parameters"]["precision"] = weighting_function_params_arg.getValue().size() <= 1 ? "4" : weighting_function_params_arg.getValue()[1];
            meta_json["average tuning"]["weighting function parameters"]["print speedups"] = weighting_function_params_arg.getValue().size() <= 1 ? "false" : (weighting_function_params_arg.getValue()[2] != "0" ? "true" : "false");
        }
        meta_json["average tuning"]["cases"] = json::array();
        for (int i = 0; i < num_cases; ++i) {
            meta_json["average tuning"]["cases"].emplace_back();
            meta_json["average tuning"]["cases"].back()["platform id"] = platform_id_arg.getValue();
            meta_json["average tuning"]["cases"].back()["device id"] = device_id_arg.getValue();
            meta_json["average tuning"]["cases"].back()["weights"] =
                    weighting_function_arg.getValue() == "weighted_sum" && weights_arg.getValue().empty()
                    ? atf::weights(1)
                    : std::vector<unsigned long long>(weights_arg.getValue().begin() + i * weights_arg.getValue().size() / num_cases, weights_arg.getValue().begin() + (i + 1) * weights_arg.getValue().size() / num_cases);
            meta_json["average tuning"]["cases"].back()["inputs"] = inputs_str(i, input_size_l_1[i], input_size_r_1[i]);
            meta_json["average tuning"]["cases"].back()["outputs"] = outputs_str(i, input_size_l_1[i], input_size_r_1[i]);
        }
        meta_json["generation_time"] = tuner.generation_time();
        meta_json["history_length"] = tuner.history().size();
        meta_json["history"] = std::vector<json>();
        for (const auto &entry : tuner.history()) {
            json entry_json;
            entry_json["evaluations"] = std::get<0>(entry);
            entry_json["valid_evaluations"] = std::get<1>(entry);
            entry_json["milliseconds_since_start"] = std::chrono::duration_cast<std::chrono::milliseconds>(std::get<2>(entry) - std::get<2>(tuner.history().front())).count();
            for (const auto &val : std::get<3>(entry)) {
                switch (val.second.value().type_id()) {
                    case atf::value_type::int_t:
                        entry_json["configuration"][val.first] = val.second.value().int_val();
                        break;
                    case atf::value_type::size_t_t:
                        entry_json["configuration"][val.first] = val.second.value().size_t_val();
                        break;
                    case atf::value_type::float_t:
                        entry_json["configuration"][val.first] = val.second.value().float_val();
                        break;
                    case atf::value_type::double_t:
                        entry_json["configuration"][val.first] = val.second.value().double_val();
                        break;
                    case atf::value_type::string_t:
                        entry_json["configuration"][val.first] = val.second.value().string_val();
                        break;
                    default:
                        entry_json["configuration"][val.first] = std::string(val.second.value());
                }
            }
            entry_json["cost"] = std::get<4>(entry);
            meta_json["history"].push_back(entry_json);
        }

        return meta_json;
    };

    // write meta data to file
    std::ofstream meta_file;
    if (!is_wrapper) {
        meta_file.open(data_dir + "/tuning_meta", std::ios::out | std::ios::trunc);
        meta_file << std::setw(4) << create_meta(tuner, "generating");
        meta_file.close();
    }

    // add progress callback
    if (!is_wrapper && all_sheets_data_provided) {
        auto meta_callback = [&] () {
            std::stringstream json_string;
            json_string << create_meta(tuner, "tuning");
            push_data(gsheets_python_command_arg.getValue(),
                      gsheets_pickle_arg.getValue(),
                      gsheets_spreadsheet_id_arg.getValue(),
                      gsheets_sheet_name_arg.getValue(),
                      gsheets_entry_id_arg.getValue(),
                      json_string.str(),
                      async_sheets_push_arg.getValue());
        };
        tuner.set_progress_info_callback(meta_callback);
        tuner.set_progress_callback_timeout(progress_callback_timeout_arg.getValue());

        // push tuner data to set "generating" status
        std::stringstream json_string;
        json_string << create_meta(tuner, "generating");
        push_data(gsheets_python_command_arg.getValue(),
                  gsheets_pickle_arg.getValue(),
                  gsheets_spreadsheet_id_arg.getValue(),
                  gsheets_sheet_name_arg.getValue(),
                  gsheets_entry_id_arg.getValue(),
                  json_string.str(),
                  async_sheets_push_arg.getValue());
    }

    // define search space
    auto restrictions_begin = search_space_restriction_arg.getValue().begin();
    auto restrictions_end = search_space_restriction_arg.getValue().end();
    auto cache_cb_range = [&] (const std::string &buffer, const std::string &level) {
        int cache_min = 0;
        int cache_max = 2;
        if (std::find(restrictions_begin, restrictions_end, buffer + "-cache-" + level + "-0") != restrictions_end) {
            cache_min = 0;
            cache_max = 0;
        }
        if (std::find(restrictions_begin, restrictions_end, buffer + "-cache-" + level + "-1") != restrictions_end) {
            cache_min = 1;
            cache_max = 1;
        }
        if (std::find(restrictions_begin, restrictions_end, buffer + "-cache-" + level + "-2") != restrictions_end) {
            cache_min = 2;
            cache_max = 2;
        }
        if (std::find(restrictions_begin, restrictions_end, buffer + "-cache-" + level + "-no-2") != restrictions_end) {
            cache_max = std::min(cache_max, 1);
        }
        return atf::interval<int>(cache_min, cache_max);
    };
    auto res_dest_range = [&] (const std::string &level) {
        int res_dest_min = 0;
        int res_dest_max = 2;
        if (std::find(restrictions_begin, restrictions_end, level + "-cb-res-g") != restrictions_end) {
            res_dest_min = 2;
            res_dest_max = 2;
        }
        if (std::find(restrictions_begin, restrictions_end, level + "-cb-res-l") != restrictions_end) {
            res_dest_min = 1;
            res_dest_max = 1;
        }
        if (std::find(restrictions_begin, restrictions_end, level + "-cb-res-p") != restrictions_end) {
            res_dest_min = 0;
            res_dest_max = 0;
        }
        return atf::interval<int>(res_dest_min, res_dest_max);
    };
    auto num_fu_range = [&] (const std::string &level, const std::string &dim, int max) {
        // restrict range
        max = std::min(max, 2048);

        std::string suffix = level + "-" + dim;
        bool pow2 = false;
        int min = 1;
        if (std::find(restrictions_begin, restrictions_end, "one-" + suffix) != restrictions_end) {
            max = 1;
        }
        if (std::find(restrictions_begin, restrictions_end, "pow2") != restrictions_end ||
            std::find(restrictions_begin, restrictions_end, "pow2-" + dim) != restrictions_end ||
            std::find(restrictions_begin, restrictions_end, "pow2-" + suffix) != restrictions_end) {
            min = std::ceil(std::log2(min));
            max = std::ceil(std::log2(max));
            pow2 = true;
        }
        if (level == "wi") {
            max = std::min(static_cast<size_t>(max), combined_max_wi_size);
        }
        if (dim[0] == 'r' && std::find(restrictions_begin, restrictions_end, "md_poly") != restrictions_end) {
            // no parallelization in R dimensions
            max = 1;
        }
        return atf::interval<int>(min, max, [pow2] (int i) -> int { return pow2 ? std::pow(2, i) : i; });
    };
    auto cb_size_range = [&] (const std::string &level, const std::string &dim, int max) {
        // restrict range
        max = std::min(max, 1024);

        std::string suffix = level + "-" + dim;
        bool pow2 = false;
        int min = 1;
        if (std::find(restrictions_begin, restrictions_end, "one-" + suffix) != restrictions_end) {
            max = 1;
        } else if (std::find(restrictions_begin, restrictions_end, "is-" + suffix) != restrictions_end) {
            min = max;
        }
        if (std::find(restrictions_begin, restrictions_end, "pow2-" + suffix) != restrictions_end) {
            pow2 = true;
        } else if (level != "l") {
            if (std::find(restrictions_begin, restrictions_end, "pow2") != restrictions_end ||
                std::find(restrictions_begin, restrictions_end, "pow2" + dim) != restrictions_end) {
                pow2 = true;
            }
        }
        if (pow2) {
            min = std::ceil(std::log2(min));
            max = std::ceil(std::log2(max));
        }
        return atf::interval<int>(min, max, [pow2] (int i) -> int { return pow2 ? std::pow(2, i) : i; });
    };

    auto INT_RES_OUT_CACHE_L_CB = atf::tp("INT_RES_OUT_CACHE_L_CB", cache_cb_range("int-res-out", "l"));
    auto INT_RES_OUT_CACHE_P_CB = atf::tp("INT_RES_OUT_CACHE_P_CB", cache_cb_range("int-res-out", "p"));

    auto OCL_DIM_L_1         = atf::tp("OCL_DIM_L_1",         {0, 1});
    auto OCL_DIM_R_1         = atf::tp("OCL_DIM_R_1",         {0, 1}, atf::unequal(OCL_DIM_L_1));

    auto IN_CACHE_L_CB = atf::tp("IN_CACHE_L_CB", cache_cb_range("in", "l"));
    auto IN_CACHE_P_CB = atf::tp("IN_CACHE_P_CB", cache_cb_range("in", "p"));
    auto BUFFER_IN_DIM_0_ORDER = atf::tp("BUFFER_IN_DIM_0_ORDER", atf::interval(0, 1), [&] (auto BUFFER_IN_DIM_0_ORDER) { return (OCL_DIM_R_1 < OCL_DIM_L_1 && BUFFER_IN_DIM_0_ORDER == 0) || (OCL_DIM_R_1 > OCL_DIM_L_1 && BUFFER_IN_DIM_0_ORDER == 1); });
    auto BUFFER_IN_DIM_1_ORDER = atf::tp("BUFFER_IN_DIM_1_ORDER", atf::interval(0, 1), [&] (auto BUFFER_IN_DIM_1_ORDER) { return (OCL_DIM_L_1 < OCL_DIM_R_1 && BUFFER_IN_DIM_1_ORDER == 0) || (OCL_DIM_L_1 > OCL_DIM_R_1 && BUFFER_IN_DIM_1_ORDER == 1); } && atf::unequal(BUFFER_IN_DIM_0_ORDER));

    auto G_CB_RES_DEST_LEVEL = atf::tp("G_CB_RES_DEST_LEVEL", {2});
    auto L_CB_RES_DEST_LEVEL = atf::tp("L_CB_RES_DEST_LEVEL", res_dest_range("l"), atf::less_than_or_eq(G_CB_RES_DEST_LEVEL));
    bool md_poly_search_space = std::find(restrictions_begin, restrictions_end, "md_poly") != restrictions_end;
    auto P_CB_RES_DEST_LEVEL = atf::tp("P_CB_RES_DEST_LEVEL", res_dest_range("p"), atf::less_than_or_eq(L_CB_RES_DEST_LEVEL) && [&](auto P_CB_RES_DEST_LEVEL) { return P_CB_RES_DEST_LEVEL == L_CB_RES_DEST_LEVEL || !md_poly_search_space; });
    auto L_REDUCTION         = atf::tp("L_REDUCTION",         {2, 1, 0});

    const bool no_pp_l_1 = std::find(restrictions_begin, restrictions_end, "no-pp-l-1") != restrictions_end;
    auto INPUT_SIZE_L_1      = atf::tp("INPUT_SIZE_L_1",      {max_input_size_l_1});
    auto NUM_WG_L_1          = atf::tp("NUM_WG_L_1",          num_fu_range("wg", "l-1", max_input_size_l_1));
    auto NUM_WI_L_1          = atf::tp("NUM_WI_L_1",          num_fu_range("wi", "l-1", max_input_size_l_1), atf::less_than_or_eq((max_input_size_l_1 + NUM_WG_L_1 - 1) / NUM_WG_L_1));
    auto L_CB_SIZE_L_1       = atf::tp("L_CB_SIZE_L_1",       cb_size_range("l-cb", "l-1", max_input_size_l_1), atf::multiple_of(NUM_WI_L_1) && atf::less_than_or_eq((max_input_size_l_1 + NUM_WG_L_1 - 1) / NUM_WG_L_1));
    auto P_CB_SIZE_L_1       = atf::tp("P_CB_SIZE_L_1",       cb_size_range("p-cb", "l-1", max_input_size_l_1), atf::less_than_or_eq(L_CB_SIZE_L_1 / NUM_WI_L_1) && [&](auto P_CB_SIZE_L_1) { return !no_pp_l_1 || ((INPUT_SIZE_L_1 % (NUM_WG_L_1 * NUM_WI_L_1) == 0) && ((INPUT_SIZE_L_1 / (NUM_WG_L_1 * NUM_WI_L_1)) % (L_CB_SIZE_L_1 / NUM_WI_L_1) == 0) && (L_CB_SIZE_L_1 % NUM_WI_L_1 == 0) && ((L_CB_SIZE_L_1 / NUM_WI_L_1) % P_CB_SIZE_L_1 == 0)); });

    const bool no_pp_r_1 = std::find(restrictions_begin, restrictions_end, "no-pp-r-1") != restrictions_end;
    auto INPUT_SIZE_R_1      = atf::tp("INPUT_SIZE_R_1",      {max_input_size_r_1});
    auto NUM_WG_R_1          = atf::tp("NUM_WG_R_1",          num_fu_range("wg", "r-1", max_input_size_r_1));
    auto NUM_WI_R_1          = atf::tp("NUM_WI_R_1",          num_fu_range("wi", "r-1", max_input_size_r_1), atf::less_than_or_eq((max_input_size_r_1 + NUM_WG_R_1 - 1) / NUM_WG_R_1));
    auto L_CB_SIZE_R_1       = atf::tp("L_CB_SIZE_R_1",       cb_size_range("l-cb", "r-1", max_input_size_r_1), atf::multiple_of(NUM_WI_R_1) && atf::less_than_or_eq((max_input_size_r_1 + NUM_WG_R_1 - 1) / NUM_WG_R_1));
    auto P_CB_SIZE_R_1       = atf::tp("P_CB_SIZE_R_1",       cb_size_range("p-cb", "r-1", max_input_size_r_1), atf::less_than_or_eq(L_CB_SIZE_R_1 / NUM_WI_R_1) && [&](auto P_CB_SIZE_R_1) { return !no_pp_r_1 || ((INPUT_SIZE_R_1 % (NUM_WG_R_1 * NUM_WI_R_1) == 0) && ((INPUT_SIZE_R_1 / (NUM_WG_R_1 * NUM_WI_R_1)) % (L_CB_SIZE_R_1 / NUM_WI_R_1) == 0) && (L_CB_SIZE_R_1 % NUM_WI_R_1 == 0) && ((L_CB_SIZE_R_1 / NUM_WI_R_1) % P_CB_SIZE_R_1 == 0)); } && [&](auto P_CB_SIZE_R_1) { return !no_pp_r_1 || NUM_WG_R_1 == 1 || ((NUM_WG_R_1 % NUM_WI_R_1 == 0) && ((NUM_WG_R_1 / NUM_WI_R_1) % (L_CB_SIZE_R_1 / NUM_WI_R_1) == 0) && (L_CB_SIZE_R_1 % NUM_WI_R_1 == 0) && ((L_CB_SIZE_R_1 / NUM_WI_R_1) % P_CB_SIZE_R_1 == 0)); });

    // add parameters to tuner
    tuner(INT_RES_OUT_CACHE_L_CB);
    tuner(INT_RES_OUT_CACHE_P_CB);
    tuner(OCL_DIM_L_1, OCL_DIM_R_1, BUFFER_IN_DIM_0_ORDER, BUFFER_IN_DIM_1_ORDER);
    tuner(IN_CACHE_L_CB);
    tuner(IN_CACHE_P_CB);
    tuner(G_CB_RES_DEST_LEVEL, L_CB_RES_DEST_LEVEL, P_CB_RES_DEST_LEVEL);
    tuner(L_REDUCTION);
    tuner(INPUT_SIZE_L_1, NUM_WG_L_1, NUM_WI_L_1, L_CB_SIZE_L_1, P_CB_SIZE_L_1);
    tuner(INPUT_SIZE_R_1, NUM_WG_R_1, NUM_WI_R_1, L_CB_SIZE_R_1, P_CB_SIZE_R_1);

    auto is_valid = [&](atf::configuration &config) -> bool {
        bool valid = true;

        // check work item sizes
        size_t num_wi[] = {1, 1, 1};
        if (config[OCL_DIM_L_1.name()].value().int_val() == 0)
          num_wi[0] *= config[NUM_WI_L_1.name()].value().int_val();
        if (config[OCL_DIM_R_1.name()].value().int_val() == 0)
          num_wi[0] *= config[NUM_WI_R_1.name()].value().int_val();
        if (config[OCL_DIM_L_1.name()].value().int_val() == 1)
          num_wi[1] *= config[NUM_WI_L_1.name()].value().int_val();
        if (config[OCL_DIM_R_1.name()].value().int_val() == 1)
          num_wi[1] *= config[NUM_WI_R_1.name()].value().int_val();
        if (config[OCL_DIM_L_1.name()].value().int_val() >= 2)
          num_wi[2] *= config[NUM_WI_L_1.name()].value().int_val();
        if (config[OCL_DIM_R_1.name()].value().int_val() >= 2)
          num_wi[2] *= config[NUM_WI_R_1.name()].value().int_val();
        valid = valid && num_wi[0] <= max_wi_size[0] && num_wi[1] <= max_wi_size[1] && num_wi[2] <= max_wi_size[2] && num_wi[0] * num_wi[1] * num_wi[2] <= max_wg_size_val;

        return valid;
    };
    tuner.set_is_valid(is_valid);

    // create kernel wrapper
    atf::cf::timeout warm_up_timeout;
    if (warm_up_timeout_type_arg.getValue() == "absolute") {
        warm_up_timeout.type = atf::cf::ABSOLUTE;
        std::istringstream ss(warm_up_timeout_value_arg.getValue());
        if (!(ss >> warm_up_timeout.value.absolute)) {
            std::cerr << "could not parse \"" << warm_up_timeout_value_arg.getValue() << "\" as unsigned long long" << std::endl;
            exit(EXIT_FAILURE);
        }
    } else if (warm_up_timeout_type_arg.getValue() == "factor") {
        warm_up_timeout.type = atf::cf::FACTOR;
        std::istringstream ss(warm_up_timeout_value_arg.getValue());
        if (!(ss >> warm_up_timeout.value.factor)) {
            std::cerr << "could not parse \"" << warm_up_timeout_value_arg.getValue() << "\" as float" << std::endl;
            exit(EXIT_FAILURE);
        }
    }
    atf::cf::timeout evaluation_timeout;
    if (evaluation_timeout_type_arg.getValue() == "absolute") {
        evaluation_timeout.type = atf::cf::ABSOLUTE;
        std::istringstream ss(evaluation_timeout_value_arg.getValue());
        if (!(ss >> evaluation_timeout.value.absolute)) {
            std::cerr << "could not parse \"" << evaluation_timeout_value_arg.getValue() << "\" as unsigned long long" << std::endl;
            exit(EXIT_FAILURE);
        }
    } else if (evaluation_timeout_type_arg.getValue() == "factor") {
        evaluation_timeout.type = atf::cf::FACTOR;
        std::istringstream ss(evaluation_timeout_value_arg.getValue());
        if (!(ss >> evaluation_timeout.value.factor)) {
            std::cerr << "could not parse \"" << evaluation_timeout_value_arg.getValue() << "\" as float" << std::endl;
            exit(EXIT_FAILURE);
        }
    }
    auto wrapper = atf::cf::ocal_md_hom<OCL, 2, decltype(inputs(0)), decltype(outputs(0))>(
            atf::cf::kernel_info(ocl::path(file_1), kernel_name + "_1"),
            atf::cf::GS(  (OCL_DIM_L_1 == 0) * (NUM_WG_L_1 * NUM_WI_L_1)
                        + (OCL_DIM_R_1 == 0) * (NUM_WG_R_1 * NUM_WI_R_1),

                          (OCL_DIM_L_1 == 1) * (NUM_WG_L_1 * NUM_WI_L_1)
                        + (OCL_DIM_R_1 == 1) * (NUM_WG_R_1 * NUM_WI_R_1)
            ),
            atf::cf::LS(  (OCL_DIM_L_1 == 0) * (NUM_WI_L_1)
                        + (OCL_DIM_R_1 == 0) * (NUM_WI_R_1),

                          (OCL_DIM_L_1 == 1) * (NUM_WI_L_1)
                        + (OCL_DIM_R_1 == 1) * (NUM_WI_R_1)
            ),

            atf::cf::kernel_info(ocl::path(file_2), kernel_name + "_2"),
            atf::cf::GS(  (OCL_DIM_L_1 == 0) * (NUM_WG_L_1 * NUM_WI_L_1)
                        + (OCL_DIM_R_1 == 0) * (             NUM_WI_R_1),

                          (OCL_DIM_L_1 == 1) * (NUM_WG_L_1 * NUM_WI_L_1)
                        + (OCL_DIM_R_1 == 1) * (             NUM_WI_R_1)
            ),
            atf::cf::LS(  (OCL_DIM_L_1 == 0) * (NUM_WI_L_1)
                        + (OCL_DIM_R_1 == 0) * (NUM_WI_R_1),

                          (OCL_DIM_L_1 == 1) * (NUM_WI_L_1)
                        + (OCL_DIM_R_1 == 1) * (NUM_WI_R_1)
            ),

            atf::tps(NUM_WG_R_1),
            atf::tps(NUM_WI_R_1),
            atf::tps(G_CB_RES_DEST_LEVEL, L_CB_RES_DEST_LEVEL, P_CB_RES_DEST_LEVEL),
            atf::tps(L_REDUCTION),

            dynamic_arg.getValue(),

            warm_ups_arg.getValue(), evaluations_arg.getValue(),
            warm_up_timeout, evaluation_timeout
    );
    // add cases
    for (int i = 0; i < num_cases; ++i) {
        wrapper.add_case(device_info,
                         weighting_function_arg.getValue() == "weighted_sum" && weights_arg.getValue().empty()
                         ? atf::weights(1)
                         : std::vector<unsigned long long>(weights_arg.getValue().begin() + i * weights_arg.getValue().size() / num_cases, weights_arg.getValue().begin() + (i + 1) * weights_arg.getValue().size() / num_cases),
                         atf::input_sizes(input_size_l_1[i], input_size_r_1[i]),
                         inputs(i),
                         outputs(i));
    }
    // set weighting function
    if (weighting_function_arg.getValue() == "weighted_sum") {
        wrapper.set_weighting_function(atf::cf::weighting::weighted_sum);
    } else if (weighting_function_arg.getValue() == "baseline_weighting") {
        wrapper.set_weighting_function(atf::cf::weighting::baseline_weighting(
                weighting_function_params_arg.getValue().size() <= 0 ? 1.0 : std::stod(weighting_function_params_arg.getValue()[0]),
                weighting_function_params_arg.getValue().size() <= 1 ? 4 : std::stoul(weighting_function_params_arg.getValue()[1]),
                weighting_function_params_arg.getValue().size() <= 2 ? false : (std::stoi(weighting_function_params_arg.getValue()[2]) != 0)
        ));
    }

    // check results
    if (!gold_file_arg.getValue().empty()) {
        std::vector<std::vector<std::string>> structured_gold_files(num_cases);
        for (int i = 0; i < num_cases; ++i) {
            structured_gold_files[i] = std::vector<std::string>(
                    gold_file_arg.getValue().begin() + i * gold_file_arg.getValue().size() / num_cases,
                    gold_file_arg.getValue().begin() + (i + 1) * gold_file_arg.getValue().size() / num_cases
            );
        }
        wrapper.check_results(check_interval, epsilon_arg.getValue(), structured_gold_files);
    }

    // start tuning
    auto tuned_configuration = tuner(wrapper);

    // log final data
    if (!is_wrapper) {
        // write meta with number of tested configurations (valid, invalid)
        meta_file.open(data_dir + "/tuning_meta", std::ios::out | std::ios::trunc);
        meta_file << std::setw(4) << create_meta(tuner, "finished");
        meta_file.close();

        if (all_sheets_data_provided) {
            // push tuner data to set "finished" status
            std::stringstream json_string;
            json_string << create_meta(tuner, "finished");
            push_data(gsheets_python_command_arg.getValue(),
                      gsheets_pickle_arg.getValue(),
                      gsheets_spreadsheet_id_arg.getValue(),
                      gsheets_sheet_name_arg.getValue(),
                      gsheets_entry_id_arg.getValue(),
                      json_string.str(),
                      async_sheets_push_arg.getValue());
        }

        if (tuner.number_of_valid_evaluated_configs() > 0) {
            // write best configuration to file
            std::map<std::string, int> json_configuration;
            for (const auto &val : tuned_configuration) {
                json_configuration[val.first] = val.second.value().int_val();
            }
            std::ofstream config_file(data_dir + "/tuned_configuration", std::ios::out | std::ios::trunc);
            config_file << std::setw(4) << json(json_configuration);
            config_file.close();
        }
    }
}