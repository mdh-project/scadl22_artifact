#ifndef GOOGLE_SHEETS_HELPER_HPP
#define GOOGLE_SHEETS_HELPER_HPP

#include <chrono>
#include <stdlib.h>

void push_data(const std::string &command, const std::string &pickle, const std::string &spreadsheet_id, const std::string &sheet_name, const std::string &entry_id, const std::string &data, bool async) {
    auto start = std::chrono::high_resolution_clock::now();
    setenv("GSHEETS_PICKLE", pickle.c_str(), 1);
    setenv("GSHEETS_SPREADSHEETID", spreadsheet_id.c_str(), 1);
    setenv("GSHEETS_SHEETNAME", sheet_name.c_str(), 1);
    setenv("GSHEETS_ENTRYID", entry_id.c_str(), 1);
    setenv("GSHEETS_DATA", data.c_str(), 1);

    // call python script to push progress data
    std::string final_command = command;
    if (async)
        final_command += " &";
    system(final_command.c_str());
    auto end = std::chrono::high_resolution_clock::now();
    std::cout << "time to push to google sheets: " << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << "ms" << std::endl;
}


#endif //GOOGLE_SHEETS_HELPER_HPP
