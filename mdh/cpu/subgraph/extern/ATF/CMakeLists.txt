cmake_minimum_required(VERSION 2.8.11)
project(ATF)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fPIC")

set(CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/cmake/Modules")

# Python
find_package(PythonLibs 2.7 REQUIRED)
include_directories(${PYTHON_INCLUDE_DIR})
link_directories(${PYTHON_LIBRARY})

include_directories(include)
include_directories(libraries/OCAL)

get_filename_component(ATF_INCLUDE_DIR_ABSOLUTE . ABSOLUTE)
set(ATF_INCLUDE_DIRS ${ATF_INCLUDE_DIR_ABSOLUTE} ${PYTHON_INCLUDE_DIR})

file(GLOB_RECURSE HEADER_FILES include/*.hpp)
file(GLOB_RECURSE SOURCE_FILES src/abort_conditions.cpp src/op_wrapper.cpp src/helper.cpp src/tp_value.cpp src/tp_value_node.cpp src/tuner_with_constraints.cpp src/tuner_without_constraints.cpp src/value_type.cpp)

# library
add_library(atf SHARED ${SOURCE_FILES} ${HEADER_FILES})
target_link_libraries(atf
        pthread
        ${PYTHON_LIBRARY}
        )
set(ATF_LIBRARIES atf pthread ${PYTHON_LIBRARY})
target_include_directories(atf PUBLIC libraries/OCAL)
if(OpenCL_FOUND)
    target_compile_definitions(atf PUBLIC OCAL_OCL_WRAPPER)
    target_link_libraries(atf ${OpenCL_LIBRARY})
    target_include_directories(atf PUBLIC ${OpenCL_INCLUDE_DIR})
    set(ATF_INCLUDE_DIRS ${ATF_INCLUDE_DIRS} ${OpenCL_INCLUDE_DIR})
    set(ATF_LIBRARIES ${ATF_LIBRARIES} ${OpenCL_LIBRARY})
endif()
if (CUDA_FOUND AND CUDA_NVRTC_LIBRARY)
    target_compile_definitions(atf PUBLIC OCAL_CUDA_WRAPPER)
    target_link_libraries(atf ${CUDA_CUDA_LIBRARY} ${CUDA_CUDART_LIBRARY} ${CUDA_NVRTC_LIBRARY})
    target_include_directories(atf PUBLIC ${CUDA_INCLUDE_DIR})
    set(ATF_INCLUDE_DIRS ${ATF_INCLUDE_DIRS} ${CUDA_INCLUDE_DIR})
    set(ATF_LIBRARIES ${ATF_LIBRARIES} ${CUDA_CUDA_LIBRARY} ${CUDA_CUDART_LIBRARY} ${CUDA_NVRTC_LIBRARY})
endif()

# set parent variables
set(ATF_INCLUDE_DIRS ${ATF_INCLUDE_DIRS} PARENT_SCOPE)
set(ATF_LIBRARIES ${ATF_LIBRARIES} PARENT_SCOPE)