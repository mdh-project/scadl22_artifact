//
//  tuner_with_constraints_with_constraints.cpp
//  new_atf_lib
//
//  Created by Ari Rasch on 21/03/2017.
//  Copyright © 2017 Ari Rasch. All rights reserved.
//

#include <stdio.h>
#include <cstring>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/un.h>
#include <netdb.h>
#include <unistd.h>
#include <regex>

#include <thread>
#include <mutex>
#include <condition_variable>
#include <future>

#include "../include/tuner_with_constraints.hpp"
#include "../include/tp_value_node.hpp"


namespace atf
{
  
tuner_with_constraints::tuner_with_constraints( const bool& abort_on_error )
  : _logging_file(""), _wrapper_type(NONE), _wrapper_command(""), _socket(0), _port(0), _server(""), _is_wrapper(false), _search_space(), _abort_on_error( abort_on_error ), _progress_callback(), _callback_timeout(600), _number_of_evaluated_configs(), _number_of_invalid_configs(), _evaluations_required_to_find_best_found_result(), _history(), _generation_time(0)
{
  _abort_condition = NULL; // new abort_condition_t( abort_condition );

  _history.emplace_back( 0, 0,
                         std::chrono::high_resolution_clock::now(),
                         configuration{},
                         std::numeric_limits<unsigned long long>::max()
                       );

  if (_progress_callback) {
    _progress_callback();
    _last_callback = std::chrono::high_resolution_clock::now();
  }
}


tuner_with_constraints::tuner_with_constraints()
  : _logging_file(""), _wrapper_type(NONE), _wrapper_command(""), _socket(0), _port(0), _server(""), _is_wrapper(false), _search_space(), _abort_on_error( false ), _progress_callback(), _callback_timeout(600), _number_of_evaluated_configs(), _number_of_invalid_configs(), _evaluations_required_to_find_best_found_result(), _history(), _generation_time(0)
{
  _abort_condition = NULL; // new abort_condition_t( abort_condition );
  
  _history.emplace_back( 0, 0,
                         std::chrono::high_resolution_clock::now(),
                         configuration{},
                         std::numeric_limits<unsigned long long>::max()
                       );

  if (_progress_callback) {
    _progress_callback();
    _last_callback = std::chrono::high_resolution_clock::now();
  }
}
  
  
tuner_with_constraints::~tuner_with_constraints()
{
    // close connection
    if (_socket)
        close(_socket);

//  std::cout << "\nNumber of Tree nodes: " << tp_value_node::number_of_nodes() << std::endl;
}


std::chrono::high_resolution_clock::time_point tuner_with_constraints::tuning_start_time() const
{
  return std::get<2>( _history.front() );
}

size_t tuner_with_constraints::constrained_search_space_size() const
{
  return _search_space.num_configs();
}

InfInt tuner_with_constraints::unconstrained_search_space_size() const
{
  return _unconstrained_search_space_size;
}

size_t tuner_with_constraints::number_of_evaluated_configs() const
{
  return _number_of_evaluated_configs;
}


size_t tuner_with_constraints::number_of_valid_evaluated_configs() const
{
  return _number_of_evaluated_configs - _number_of_invalid_configs;
}


size_t tuner_with_constraints::generation_time() const
{
  return _generation_time;
}


unsigned long long tuner_with_constraints::best_measured_result() const
{
  return std::get<4>( _history.back() );
}

configuration tuner_with_constraints::best_configuration() const
{
  return std::get<3>( _history.back() );
}

const std::vector<tuner_with_constraints::history_entry>& tuner_with_constraints::history() const {
  return _history;
}

void tuner_with_constraints::insert_tp_names_in_search_space()
{}

void tuner_with_constraints::insert_tp_names_of_one_tree_in_search_space()
{}



// loeschen
void tuner_with_constraints::print_path()
{
  std::cout << std::endl;
}

std::string tuner_with_constraints::display_string_with_abort_condition() const {
    return display_string() + " (" + _abort_condition->display_string() + ")";
}

void tuner_with_constraints::set_is_valid(const is_valid_type &is_valid) {
    _is_valid = is_valid;
}

void tuner_with_constraints::set_progress_info_callback(const progress_callback_t &progress_callback) {
    _progress_callback = progress_callback;
}

void tuner_with_constraints::set_progress_callback_timeout(unsigned long long callback_timeout) {
    _callback_timeout = callback_timeout;
}

bool tuner_with_constraints::enable_process_wrapper(int argc, const char **argv) {
    _is_wrapper = is_process_wrapper(argc, argv);
    int wrapper_arg_start = -1;
    for (int i = 0; i < argc; ++i) {
        if (strcmp(argv[i], "process_wrapper") == 0) {
            wrapper_arg_start = i + 1;
            break;
        }
    }

    if (_is_wrapper && argc - wrapper_arg_start >= 2) {
        // read server, port, configuration
        _server = argv[wrapper_arg_start + 0];
        _port = std::atoi(argv[wrapper_arg_start + 1]);
        for (int i = wrapper_arg_start + 2; i < argc; i += 2) {
            _wrapper_configuration[argv[i]] = argv[i + 1];
        }
    } else if (_wrapper_command.empty()) {
        _wrapper_command = argv[0];
        _wrapper_command.append(" [flags]");
    }

    if (!_is_wrapper) {
        std::stringstream flags;
        for (int i = 1; i < argc; ++i) {
            flags << "\"" << argv[i] << "\" ";
        }
        flags << "[flags]";
        _wrapper_command = std::regex_replace(_wrapper_command, std::regex("\\[flags\\]"), flags.str());
        switch (_wrapper_type) {
            case LOCAL: {
                _socket = socket(AF_UNIX, SOCK_STREAM, 0);
                if (_socket < 0) {
                    std::cerr << "error while creating socket: " << strerror(errno) << std::endl;
                    exit(1);
                }
                _server = std::tmpnam(nullptr);
                struct sockaddr_un server_addr{};
                memset(&server_addr, 0, sizeof(struct sockaddr_un));
                server_addr.sun_family = AF_UNIX;
                strncpy(server_addr.sun_path, _server.c_str(), sizeof(server_addr.sun_path) - 1);
                if (bind(_socket, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0) {
                    std::cerr << "error while binding socket: " << strerror(errno) << std::endl;
                    exit(1);
                }
                if (listen(_socket, 1) < 0) {
                    std::cerr << "error while setting up queue: " << strerror(errno) << std::endl;
                    exit(1);
                }
                break;
            }
            case REMOTE: {
                _socket = socket(AF_INET, SOCK_STREAM, 0);
                if (_socket < 0) {
                    std::cerr << "error while creating socket: " << strerror(errno) << std::endl;
                    exit(1);
                }
                struct sockaddr_in server_addr{};
                memset(&server_addr, 0, sizeof(server_addr));
                server_addr.sin_family = AF_INET;
                server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
                server_addr.sin_port = htons(0);
                if (bind(_socket, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0) {
                    std::cerr << "error while binding socket: " << strerror(errno) << std::endl;
                    exit(1);
                }
                struct sockaddr_in assigned_server_addr{};
                socklen_t socket_length = sizeof(server_addr);
                if (getsockname(_socket, (struct sockaddr *) &assigned_server_addr, &socket_length) < 0) {
                    std::cerr << "error while getting port: " << strerror(errno) << std::endl;
                    exit(1);
                }
                _server = "localhost";
                _port = ntohs(assigned_server_addr.sin_port);
                if (listen(_socket, 1) < 0) {
                    std::cerr << "error while setting up queue: " << strerror(errno) << std::endl;
                    exit(1);
                }
                break;
            }
            case NONE:break;
        }
    } else {
        _socket = socket(_wrapper_type == LOCAL ? AF_UNIX : AF_INET, SOCK_STREAM, 0);
        if (_socket < 0) {
            std::cerr << "error while creating socket: " << strerror(errno) << std::endl;
            exit(1);
        }

        addr server_addr{};
        if (_wrapper_type == LOCAL) {
            server_addr.addr_un.sun_family = AF_UNIX;
            strncpy(server_addr.addr_un.sun_path, _server.c_str(), sizeof(server_addr.addr_un.sun_path) - 1);
        } else {
            memset(&server_addr.addr_in, 0, sizeof(server_addr.addr_in));
            server_addr.addr_in.sin_family = AF_INET;
            server_addr.addr_in.sin_port = htons(_port);

            struct hostent *hostinfo;
            hostinfo = gethostbyname(_server.c_str());
            if (hostinfo == nullptr)
            {
                std::cerr << "invalid server: " << _server << std::endl;
                exit(1);
            }
            server_addr.addr_in.sin_addr = *(struct in_addr *) hostinfo->h_addr;
        }

        struct sockaddr* addr_ptr = nullptr;
        if (_wrapper_type == LOCAL) {
            addr_ptr = (struct sockaddr*) &server_addr.addr_un;
        } else {
            addr_ptr = (struct sockaddr*) &server_addr.addr_in;
        }
        int status;
        do {
            status = connect(_socket, addr_ptr, _wrapper_type == LOCAL ? sizeof(struct sockaddr_un) : sizeof(struct sockaddr_in));
            if (status < 0) {
                std::cerr << "error while connecting to server: " << strerror(errno) << std::endl;
            }
        } while (status != 0);
    }

    return _is_wrapper;
}

unsigned long long tuner_with_constraints::execute_wrapper(configuration &configuration,
                                                           unsigned long long int *compile_time, int *error_code) {
    std::stringstream flags;
    flags << " -- process_wrapper \"" << _server << "\" " << _port;
    for (const auto &val : configuration) {
        flags << " \"" << val.first << "\"" << " \"" << val.second.value() << "\"";
    }
    std::string command = std::regex_replace(_wrapper_command, std::regex("\\[flags\\]"), flags.str());
    if (_wrapper_type == REMOTE) {
        size_t ssh_pos = command.find("ssh ");
        while (ssh_pos != std::string::npos) {
            // enable port forwarding in case of remote tuning
            command.insert(ssh_pos + 3, std::string(" -R ").append(_server).append(":").append(std::to_string(_port)).append(":").append(_server).append(":").append(std::to_string(_port)));
            ssh_pos = command.find("ssh ", ssh_pos + 4);
        }
    }

    addr client_addr{};
    int client_socket = -1;
    socklen_t client_addr_length = _wrapper_type == LOCAL ? sizeof(struct sockaddr_un) : sizeof(struct sockaddr_in);
    auto wait_for_connection = [&]() {
        struct sockaddr *addr_ptr = nullptr;
        if (_wrapper_type == LOCAL) {
            addr_ptr = (struct sockaddr *) &client_addr.addr_un;
        } else {
            addr_ptr = (struct sockaddr *) &client_addr.addr_in;
        }
        client_socket = accept(_socket, addr_ptr, &client_addr_length);
        if (client_socket < 0) {
            std::cerr << "error while accepting client connection: " << strerror(errno) << std::endl;
            exit(1);
        }
    };
    std::thread wait_for_connection_thread(wait_for_connection);

    // execute process wrapper
    int ret_val;
    std::atomic_bool cancel(false);
    auto exec_command = [&]() {
        ret_val = system(command.c_str());

        if (client_socket < 0) {
            // process wrapper could not connect before failing
            // cancel waiting for connection by connecting to server socket locally
            cancel = true;
            int cancel_client_socket = socket(_wrapper_type == LOCAL ? AF_UNIX : AF_INET, SOCK_STREAM, 0);
            if (cancel_client_socket < 0) {
                std::cerr << "error while creating socket: " << strerror(errno) << std::endl;
                exit(1);
            }

            addr server_addr{};
            if (_wrapper_type == LOCAL) {
                server_addr.addr_un.sun_family = AF_UNIX;
                strncpy(server_addr.addr_un.sun_path, _server.c_str(), sizeof(server_addr.addr_un.sun_path) - 1);
            } else {
                memset(&server_addr.addr_in, 0, sizeof(server_addr.addr_in));
                server_addr.addr_in.sin_family = AF_INET;
                server_addr.addr_in.sin_port = htons(_port);
                struct hostent *hostinfo;
                hostinfo = gethostbyname(_server.c_str());
                if (hostinfo == nullptr) {
                    std::cerr << "invalid server: " << _server << std::endl;
                    exit(1);
                }
                server_addr.addr_in.sin_addr = *(struct in_addr *) hostinfo->h_addr;
            }

            struct sockaddr *addr_ptr = nullptr;
            if (_wrapper_type == LOCAL) {
                addr_ptr = (struct sockaddr *) &server_addr.addr_un;
            } else {
                addr_ptr = (struct sockaddr *) &server_addr.addr_in;
            }
            if (connect(cancel_client_socket, addr_ptr, _wrapper_type == LOCAL ? sizeof(struct sockaddr_un) : sizeof(struct sockaddr_in)) < 0) {
                std::cerr << "error while connecting to server: " << strerror(errno) << std::endl;
                exit(1);
            }
            if (close(cancel_client_socket) != 0) {
                std::cerr << "error while closing client socket: " << strerror(errno) << "(" << errno << ")"
                          << std::endl;
                exit(1);
            }
        }
        if (ret_val == 1280) {
            // result check failed
            exit(EXIT_FAILURE);
        }
    };
    std::thread exec_command_thread(exec_command);

    // wait for client connection
    wait_for_connection_thread.join();

    // if process wrapper failed, throw exception
    if (cancel) {
        exec_command_thread.join();
        // close client connection
        if (close(client_socket) != 0) {
            std::cerr << "error while closing client socket: " << strerror(errno) << "(" << errno << ")"
                      << std::endl;
            exit(1);
        }
        throw std::exception();
    }

    // determine what values to receive
    uint16_t mask_buf = 0;
    auto mask_data = (char *) (&mask_buf);
    size_t left = sizeof(mask_buf);
    ssize_t rc;
    do {
        rc = recv(client_socket, mask_data, left, 0);
        if (rc == 0 && errno != 0) {
            exec_command_thread.join();
            throw std::exception();
        }
        if (rc < 0) {
            std::cerr << "error while receiving runtime: " << strerror(errno) << "(" << errno << ")" << std::endl;
            exit(1);
        }
        mask_data += rc;
        left -= rc;
    } while (left > 0);
    auto recv_mask = htons(mask_buf);
    auto recv_compile_time = static_cast<bool>(recv_mask & 1);
    auto recv_error_code = static_cast<bool>(recv_mask & 2);
    auto recv_runtime = static_cast<bool>(recv_mask & 4);

    if (recv_compile_time) {
        // read compile time
        __uint64_t buf;
        auto data = (char *) (&buf);
        left = sizeof(buf);
        do {
            rc = recv(client_socket, data, left, 0);
            if (rc == 0 && errno != 0) {
                exec_command_thread.join();
                throw std::exception();
            }
            if (rc < 0) {
                std::cerr << "error while receiving compile time: " << strerror(errno) << "(" << errno << ")"
                          << std::endl;
                exit(1);
            }
            data += rc;
            left -= rc;
        } while (left > 0);
        if (compile_time != nullptr) *compile_time = be64toh(buf);
    }

    if (recv_error_code) {
        // read error code
        uint32_t buf;
        auto data = (char *) (&buf);
        left = sizeof(buf);
        do {
            rc = recv(client_socket, data, left, 0);
            if (rc == 0 && errno != 0) {
                exec_command_thread.join();
                throw std::exception();
            }
            if (rc < 0) {
                std::cerr << "error while receiving error code: " << strerror(errno) << "(" << errno << ")"
                          << std::endl;
                exit(1);
            }
            data += rc;
            left -= rc;
        } while (left > 0);
        if (error_code != nullptr) {
            *error_code = ntohl(buf);
        }
        data = (char *) (&buf);
        left = sizeof(buf);
        do {
            rc = recv(client_socket, data, left, 0);
            if (rc == 0 && errno != 0) {
                exec_command_thread.join();
                throw std::exception();
            }
            if (rc < 0) {
                std::cerr << "error while receiving sign of error code: " << strerror(errno) << "(" << errno << ")"
                          << std::endl;
                exit(1);
            }
            data += rc;
            left -= rc;
        } while (left > 0);
        if (error_code != nullptr) {
            if (ntohl(buf) != 0) {
                *error_code *= -1;
            }
        }
    }

    unsigned long long runtime = 0;
    if (recv_runtime) {
        // read client runtime
        __uint64_t buf;
        auto data = (char *) (&buf);
        left = sizeof(buf);
        do {
            rc = recv(client_socket, data, left, 0);
            if (rc == 0 && errno != 0) {
                exec_command_thread.join();
                throw std::exception();
            }
            if (rc < 0) {
                std::cerr << "error while receiving runtime: " << strerror(errno) << "(" << errno << ")"
                          << std::endl;
                exit(1);
            }
            data += rc;
            left -= rc;
        } while (left > 0);
        runtime = be64toh(buf);
    }

    // close client connection
    if (close(client_socket) != 0) {
        std::cerr << "error while closing client socket: " << strerror(errno) << "(" << errno << ")" << std::endl;
        exit(1);
    }

    // wait for process wrapper to terminate
    exec_command_thread.join();

    if (error_code != nullptr && *error_code != 0)
        throw std::exception();

    if (!recv_runtime) throw std::exception();
    return runtime;
}


} // namespace "atf"

