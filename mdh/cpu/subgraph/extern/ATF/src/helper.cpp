#include "helper.hpp"

#include <iostream>
#include <cstring>

namespace atf {
void ns_sleep(unsigned long long ns) {
    int result = 0;
    {
        struct timespec ts_remaining = {
                ns / 1000000000L,
                ns % 1000000000L
        };

        do {
            struct timespec ts_sleep = ts_remaining;
            result = nanosleep(&ts_sleep, &ts_remaining);
        } while (result != 0 && errno == EINTR);
    }
    if (result != 0) {
        std::cerr << "nanosleep failed: " << strerror(errno) << std::endl;
        exit(1);
    }
}
}