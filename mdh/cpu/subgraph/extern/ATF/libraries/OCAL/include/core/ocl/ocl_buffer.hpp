//
//  ocl_buffer.hpp
//  ocal_ocl
//
//  Created by Ari Rasch on 20.09.17.
//  Copyright © 2017 Ari Rasch. All rights reserved.
//

#ifndef ocl_buffer_hpp
#define ocl_buffer_hpp


namespace ocal
{

namespace core
{

namespace ocl
{


template< typename T >
class buffer : public abstract::abstract_buffer< buffer<T>,                          // (required for static polymorphism)
                                                 std::shared_ptr<T>,                 // host_memory_t
                                                 cl::Buffer,                         // buffer_t
                                                 clEvent_wrapper,                    // even_t
                                                 ::ocal::common::pointer_wrapper<T>, // host_memory_ptr_t
                                                 T
                                               >
{
  // friend class parent
  friend class abstract::abstract_buffer< buffer<T>,                          // (required for static polymorphism)
                                          std::shared_ptr<T>,                 // host_memory_t
                                          cl::Buffer,                         // buffer_t
                                          clEvent_wrapper,                    // even_t
                                          ::ocal::common::pointer_wrapper<T>, // host_memory_ptr_t
                                          T
                                        >;
 
  private:
    // enabling easier access to parent's members
    using abstract::abstract_buffer< buffer<T>, std::shared_ptr<T>, cl::Buffer, clEvent_wrapper, ::ocal::common::pointer_wrapper<T>, T >::_size;
    using abstract::abstract_buffer< buffer<T>, std::shared_ptr<T>, cl::Buffer, clEvent_wrapper, ::ocal::common::pointer_wrapper<T>, T >::_host_memory;
    using abstract::abstract_buffer< buffer<T>, std::shared_ptr<T>, cl::Buffer, clEvent_wrapper, ::ocal::common::pointer_wrapper<T>, T >::_ptr_to_host_memory;
    using abstract::abstract_buffer< buffer<T>, std::shared_ptr<T>, cl::Buffer, clEvent_wrapper, ::ocal::common::pointer_wrapper<T>, T >::_unique_device_id_to_device_buffer;
    using abstract::abstract_buffer< buffer<T>, std::shared_ptr<T>, cl::Buffer, clEvent_wrapper, ::ocal::common::pointer_wrapper<T>, T >::_host_is_dirty;
    using abstract::abstract_buffer< buffer<T>, std::shared_ptr<T>, cl::Buffer, clEvent_wrapper, ::ocal::common::pointer_wrapper<T>, T >::_unique_device_id_to_dirty_flag;
    using abstract::abstract_buffer< buffer<T>, std::shared_ptr<T>, cl::Buffer, clEvent_wrapper, ::ocal::common::pointer_wrapper<T>, T >::_unique_device_id_to_command_queue_or_stream_id;
    using abstract::abstract_buffer< buffer<T>, std::shared_ptr<T>, cl::Buffer, clEvent_wrapper, ::ocal::common::pointer_wrapper<T>, T >::_id_of_last_active_device;
  
  
  public:
    buffer( size_t size = 0 )
      : abstract::abstract_buffer< buffer<T>, std::shared_ptr<T>, cl::Buffer, clEvent_wrapper, ::ocal::common::pointer_wrapper<T>, T >( size, new T[ size ], std::default_delete<T[]>() )
    {}


    buffer( size_t size, T init_value )
      : abstract::abstract_buffer< buffer<T>, std::shared_ptr<T>, cl::Buffer, clEvent_wrapper, ::ocal::common::pointer_wrapper<T>, T >( size, new T[ size ], std::default_delete<T[]>() )
    {
      if( init_value == 0 )
        std::memset( _ptr_to_host_memory.get(), 0, _size * sizeof(T) );
      
      else
        for( int i = 0 ; i < _size ; ++i )
          this->operator[]( i ) = init_value;
    }


    buffer( const std::vector<T>& vec )
      : abstract::abstract_buffer< buffer<T>, std::shared_ptr<T>, cl::Buffer, clEvent_wrapper, ::ocal::common::pointer_wrapper<T>, T >( vec.size(), new T[ vec.size() ], std::default_delete<T[]>() )
    {
      this->set_content( vec.data() );
    }


    buffer( size_t size, T* content )
      : abstract::abstract_buffer< buffer<T>, std::shared_ptr<T>, cl::Buffer, clEvent_wrapper, ::ocal::common::pointer_wrapper<T>, T >( size, content )
    {}


//    // copy ctor - required for storing buffers in an std::vector
//    buffer( buffer<T>& other )
//      : abstract::abstract_buffer< buffer<T>, std::shared_ptr<T>, cl::Buffer, clEvent_wrapper, ::ocal::common::pointer_wrapper<T>, T >( other._size, new T[ other._size ] )
//    {
//      this->set_content( other.get_host_memory_ptr() );
//    }

  
    //buffer()                    = delete;  // Note: buffer are initialized with input size
    buffer( const buffer<T>&  ) = default;
    buffer(       buffer<T>&& ) = default;

    buffer& operator=( const buffer<T>&  ) = default;
    buffer& operator=(       buffer<T>&& ) = default;


    ~buffer() = default;
  
  
    // ----------------------------------------------------------
    //   set/get host memory pointer
    // ----------------------------------------------------------
  
//    void set_host_memory_ptr( std::shared_ptr<T> new_content )
//    {
//      // wait for critical events to finish
//      this->sync_read_events_on_host_memory();
//      this->sync_write_events_on_host_memory();
//
//      // set values in host buffer
//      _host_memory = new_content;
//
//      // set only host buffer as up to date
//      this->set_all_device_buffers_as_dirty();
//      this->set_host_memory_as_up_to_date();
//    }


    T* get_host_memory_ptr()
    {
      this->update_host_memory();
    
      // set only host buffer as up to date
      this->set_all_device_buffers_as_dirty();
      this->set_host_memory_as_up_to_date();
      
      return _ptr_to_host_memory;
    }


    // ----------------------------------------------------------
    //   set/get host memory (static polymorphism)
    // ----------------------------------------------------------
  
    void set_content( T* new_content )
    {
      // wait for critical events to finish
      this->sync_read_events_on_host_memory();
      this->sync_write_events_on_host_memory();
      
      // copy data
      std::memcpy( _ptr_to_host_memory, new_content, _size * sizeof( T ) );
      
      // set only host buffer as up to date
      this->set_all_device_buffers_as_dirty();
      this->set_host_memory_as_up_to_date();
    }


    std::vector<T> get_content()
    {
      this->update_host_memory();
    
      // declare result vector
      std::vector<T> content;
      content.reserve( _size );
      
      // copy data
      content.insert( content.end(), &_ptr_to_host_memory[ 0 ], &_ptr_to_host_memory[ _size ] );
  
      // set only host buffer as up to date
      this->set_all_device_buffers_as_dirty();
      this->set_host_memory_as_up_to_date();
      
      return content;
    }


    // ------------------------------------------------------------------------------
    //   implicit cast to cl::Buffer (enables compatibility to OpenCL-Libraries)
    // ------------------------------------------------------------------------------

    operator cl::Buffer()
    {
      assert( _unique_device_id_to_device_buffer.find( _id_of_last_active_device ) != _unique_device_id_to_device_buffer.end() );
      
      auto stream_id = _unique_device_id_to_command_queue_or_stream_id.at( _id_of_last_active_device );
      
      return this->get_device_buffer_with_read_write_access( _id_of_last_active_device, stream_id );
    }
    

  private:
  
    // ---------------------------------------------------------------------------------------
    //   native Buffer management: creation and H2D/D2H data transfers (static polymorphism)
    // ---------------------------------------------------------------------------------------

    void create_device_buffer_if_not_existent( int unique_device_id, int command_queue_id ) 
    {
      // create device buffer if it does not already exist
      if( _unique_device_id_to_device_buffer.find( unique_device_id ) == _unique_device_id_to_device_buffer.end() )
      {
        // create buffer
        _unique_device_id_to_device_buffer[ unique_device_id ] = cl::Buffer( unique_device_id_to_opencl_context( unique_device_id ),
                                                                             CL_MEM_READ_WRITE,
                                                                             _size * sizeof( T )
                                                                            );
        
        // set dirty flags
        _unique_device_id_to_dirty_flag[ unique_device_id ] = true; 

        // set command queue id
        _unique_device_id_to_command_queue_or_stream_id[ unique_device_id ] = command_queue_id;
      }
    }
  
  
    void copy_data_from_device_buffer_to_device_buffer( int device_id_of_source, int device_id_of_destination ) 
    {
      assert( (device_id_of_source != device_id_of_destination) && "makes no sense" );
      
      assert( _unique_device_id_to_device_buffer.find( device_id_of_source      ) != _unique_device_id_to_device_buffer.end() );  // assert device buffer exists
      assert( _unique_device_id_to_device_buffer.find( device_id_of_destination ) != _unique_device_id_to_device_buffer.end() );  // assert device buffer exists
      
      // copy data from device to device over host
      this->copy_data_from_device_buffer_to_host_memory( device_id_of_source );
      
      this->sync_read_events_on_host_memory();
      this->sync_write_events_on_host_memory();
      
      this->copy_data_from_host_memory_to_device_buffer( device_id_of_destination );
    }
  
  
    void copy_data_from_host_memory_to_device_buffer( int unique_device_id ) 
    {
      assert( _unique_device_id_to_device_buffer.find( unique_device_id ) != _unique_device_id_to_device_buffer.end() );  // assert device buffer exists
      
      // get actual command queue of device "unique_device_id"
      auto  command_queue_id = _unique_device_id_to_command_queue_or_stream_id.at( unique_device_id );
      auto& command_queue    = unique_device_id_to_opencl_command_queues.at( unique_device_id ).at( command_queue_id );
      
      // copy data from host to device
      try
      {
        clEvent_wrapper event;
        command_queue.enqueueWriteBuffer( _unique_device_id_to_device_buffer.at( unique_device_id ),
                                          CL_FALSE,           // blocking write
                                          0,                  // offset
                                          _size * sizeof(T),  // data size
                                          _host_memory.get(), // data content,
                                          nullptr,            // event list
                                          &( event.get() )    // event
                                        );
        
        this->set_event_device_buffer_of_device_is_written( event, unique_device_id );
        this->set_event_host_memory_is_read( event );
      }
      catch( cl::Error& err )
      {
        check_opencl_exception( err );
        throw;
      }
    }
  
  
    void copy_data_from_device_buffer_to_host_memory( int unique_device_id ) 
    {
      assert( _unique_device_id_to_device_buffer.find( unique_device_id ) != _unique_device_id_to_device_buffer.end() );  // assert device buffer exists
      
      // get default command queue of device "unique_device_id"
      auto command_queue_id = _unique_device_id_to_command_queue_or_stream_id.at( unique_device_id );
      auto command_queue    = unique_device_id_to_opencl_command_queues.at( unique_device_id ).at( command_queue_id );
      
      // copy data from device to host
      try
      {
        clEvent_wrapper event;
        command_queue.enqueueReadBuffer( _unique_device_id_to_device_buffer.at( unique_device_id ),
                                         CL_FALSE,           // blocking write
                                         0,                  // offset
                                         _size * sizeof(T),  // data size
                                         _host_memory.get(), // data content
                                         nullptr,            // event list
                                         &( event.get() )    // event
                                        );
        
        this->set_event_device_buffer_of_device_is_read( event, unique_device_id );
        this->set_event_host_memory_is_written( event );
      }
      catch( cl::Error& err )
      {
        check_opencl_exception( err );
        throw;
      }
    }
};


} // namespace "ocl"

} // namespace "core"

} // namespace "ocal"


#endif /* ocl_buffer_hpp */
