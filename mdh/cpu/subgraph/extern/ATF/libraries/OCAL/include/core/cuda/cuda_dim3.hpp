//
//  cuda_dim3.hpp
//  ocal_cuda
//
//  Created by Ari Rasch on 20.09.17.
//  Copyright © 2017 Ari Rasch. All rights reserved.
//

#ifndef cuda_nd_range_hpp
#define cuda_nd_range_hpp


namespace ocal
{

namespace core
{


#ifndef cuda_thread_configuration_set
#define cuda_thread_configuration_set

namespace cuda
{

class dim3 : public abstract::abstract_thread_configuration
{
  // friend class parent
  friend class abstract::abstract_thread_configuration;

  public:
    dim3() = default;

    // generic ctor
    template< typename... Ts >
    dim3( const Ts&... args )
      : abstract_thread_configuration( args... )
    {}
};

} // namespace "cuda"

#endif // namespace "cuda_thread_configuration_set"



#ifndef ocl_thread_configuration_set
#define ocl_thread_configuration_set

namespace ocl
{

class nd_range : public abstract::abstract_thread_configuration
{
  // friend class parent
  friend class abstract::abstract_thread_configuration;

  public:
    nd_range() = default;
  
    // generic ctor
    template< typename... Ts >
    nd_range( const Ts&... args )
      : abstract_thread_configuration( args... )
    {}
};

} // namespace "ocl"

#endif /* ocl_thread_configuration_set */


} // namespace "core"

} // namespace "ocal"


#endif /* cuda_nd_range_hpp */
