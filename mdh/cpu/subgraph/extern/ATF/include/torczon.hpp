//
//  torczon.hpp
//  new_atf_lib
//
//

#ifndef torczon_h
#define torczon_h


#include <random>
#include <chrono>


#include "tuner_with_constraints.hpp"
#include "tuner_without_constraints.hpp"
#include "coordinate_space.hpp"


// TODO: make this customizable
#define INIT_SIMPLEX_NORMALIZED_SIDE_LENGTH 0.1


namespace atf
{
    template< typename T = tuner_with_constraints>
    class torczon_class : public T
    {
    public:
        enum torczon_state { TORC_INITIAL, TORC_REFLECTED, TORC_EXPANDED };

        using simplex = std::vector<point>;

        struct torczon_simplex {
            size_t best_vertex_index = 0;
            simplex simp;
        };


        template< typename... Ts >
        torczon_class( Ts... params )
                : T(  params... ), _search_space(), _coordinate_space(), _search_space_size( 0 ), _num_params( 0 ),
                  _rnd_generator( random_seed() ), _coord_distribution( std::uniform_real_distribution<double>( 0.0, 1.0 ) ),
                  _param_expansion( 2.0 ), _param_contraction( 0.5 ),  // TODO: make those customizable
                  _base_simplex(), _test_simplex(), _current_simplex( nullptr ),
                  _current_vertex_index( 0 ), _current_center_index( 0 ), _current_state( TORC_INITIAL ),
                  _best_cost( std::numeric_limits<unsigned long long>::max() ), _cost_improved( true )
        {}


        void initialize( const search_space& search_space )
        {
          _search_space      = &search_space;
          _coordinate_space  = coordinate_space( search_space );
          _search_space_size = _search_space->num_configs();
          _num_params        = _search_space->num_params();

          _base_simplex.simp              = initial_simplex();
          _base_simplex.best_vertex_index = 0;
          _current_simplex                = &_base_simplex;
        }


        configuration get_next_config()
        {
          if( _current_vertex_index == _num_params + 1 )
            generate_next_simplex();

          return _coordinate_space.get_config( _current_simplex->simp[ _current_vertex_index ] );
        }


        void report_result( const unsigned long long& result )
        {
          if( result < _best_cost )
          {
            _best_cost                          = result;
            _cost_improved                      = true;
            _current_simplex->best_vertex_index = _current_vertex_index;

            if( _current_state == TORC_INITIAL )
              _current_center_index = _current_vertex_index;
          }

          _current_vertex_index++;
        }


        void finalize()
        {}

        std::string display_string() const {
            return "Torczon";
        }


    private:
        search_space const* _search_space;
        coordinate_space    _coordinate_space;
        size_t              _search_space_size;
        size_t              _num_params;

        std::default_random_engine             _rnd_generator;
        std::uniform_real_distribution<double> _coord_distribution;

        double _param_expansion;
        double _param_contraction;

        torczon_simplex  _base_simplex;
        torczon_simplex  _test_simplex;
        torczon_simplex* _current_simplex;
        size_t           _current_vertex_index;
        size_t           _current_center_index;

        torczon_state _current_state;
        unsigned long long        _best_cost;
        bool          _cost_improved;


        size_t random_seed() const
        {
          return std::chrono::system_clock::now().time_since_epoch().count();
        }


        simplex initial_simplex()
        {
          static_assert( INIT_SIMPLEX_NORMALIZED_SIDE_LENGTH > 0.0, "normalized side length has to be greater than 0" );
          static_assert( INIT_SIMPLEX_NORMALIZED_SIDE_LENGTH <= 0.5, "normalized side length has to be less than or equal to 0.5" );

          simplex simp;

          // generate random base vertex
          std::vector<double> base_vertex_coords;
          for( size_t i = 0; i < _num_params; i++ )
            base_vertex_coords.push_back( _coord_distribution( _rnd_generator ) );

          // construct simplex from base vertex
          simp.push_back( ( point( base_vertex_coords ) ) );
          for( size_t i = 0; i < _num_params; i++ )
          {
            auto v = base_vertex_coords;

            if( v[ i ] <= 0.5 )
              v[ i ] += INIT_SIMPLEX_NORMALIZED_SIDE_LENGTH;
            else
              v[ i ] -= INIT_SIMPLEX_NORMALIZED_SIDE_LENGTH;

            simp.push_back( point( v ) );
          }

          return simp;
        }


        simplex expand_base_simplex( double factor ) const
        {
          simplex expanded_simplex;

          const point& center = _base_simplex.simp[ _current_center_index ];
          for( const point& v : _base_simplex.simp )
          {
            point new_vertex = _coordinate_space.convert_to_valid_point_capped( center * ( 1 - factor ) + v * factor );
            expanded_simplex.push_back( new_vertex );
          }

          return expanded_simplex;
        }


        simplex reflect_base_simplex() const
        {
          return expand_base_simplex( -1 );
        }


        simplex expand_base_simplex() const
        {
          return expand_base_simplex( _param_expansion );
        }


        simplex contract_base_simplex() const
        {
          return expand_base_simplex( _param_contraction );
        }


        void switch_state( torczon_state new_state )
        {
          _current_state        = new_state;
          _current_vertex_index = 0;
          _cost_improved        = false;
        }


        void generate_next_simplex()
        {
          switch (_current_state)
          {
            case TORC_INITIAL:
            {
              _test_simplex.simp              = reflect_base_simplex();
              _test_simplex.best_vertex_index = 0;
              _current_simplex                = &_test_simplex;

              switch_state( TORC_REFLECTED );

              break;
            }

            case TORC_REFLECTED:
            {
              if( _cost_improved )
              {
                _base_simplex                   = _test_simplex;
                _test_simplex.simp              = expand_base_simplex();
                _test_simplex.best_vertex_index = 0;
                _current_simplex                = &_test_simplex;

                switch_state( TORC_EXPANDED );
              }
              else
              {
                _base_simplex.simp              = contract_base_simplex();
                _base_simplex.best_vertex_index = 0;
                _current_simplex                = &_base_simplex;

                _best_cost            = std::numeric_limits<unsigned long long>::max();
                _current_center_index = 0;

                switch_state( TORC_INITIAL );
              }

              break;
            }

            case TORC_EXPANDED:
            {
              if( _cost_improved )
                _base_simplex = _test_simplex;

              _current_center_index           = _base_simplex.best_vertex_index;
              _test_simplex.simp              = reflect_base_simplex();
              _test_simplex.best_vertex_index = 0;
              _current_simplex                = &_test_simplex;

              switch_state( TORC_REFLECTED );

              break;
            }

            default:
              throw std::runtime_error( "Invalid algorithm state" );
          }
        }

    };


    template< typename... Ts >
    auto torczon( Ts... args )
    {
      return torczon_class<>{ args... };
    }


    template< typename T, typename... Ts >
    auto torczon( Ts... args )
    {
      return torczon_class<T>{ args... };
    }


} // namespace "atf"



#endif /* torczon_hpp */
