//
// Created by Richard Schulze on 15.06.2019.
//

#ifndef OCAL_MD_HOM_WRAPPER_HPP
#define OCAL_MD_HOM_WRAPPER_HPP

#include "ocal_wrapper.hpp"

namespace atf {
namespace cf {

namespace weighting {

unsigned long long weighted_sum(const std::vector<unsigned long long> &runtimes, const std::vector<std::vector<unsigned long long>> &weights) {
    unsigned long long cost = 0;
    for (int i = 0; i < runtimes.size(); ++i) {
        cost += weights[i][0] * runtimes[i];
    }
    return cost;
}
class baseline_weighting {
public:
    explicit baseline_weighting(double exponent = 1.0, unsigned int precision = 4, bool print_speedups = false)
            : _exponent(exponent), _precision(precision), _print_speedups(print_speedups) {}

    unsigned long long operator()(const std::vector<unsigned long long> &runtimes, const std::vector<std::vector<unsigned long long>> &weights) const {
        if (_print_speedups) {
            std::cout << "speedups: " << std::endl;
            for (int i = 0; i < runtimes.size(); ++i) {
                std::cout << weights[i][0] / static_cast<double>(runtimes[i]) << std::endl;
            }
        }
        double summed_speedup = 0;
        for (int i = 0; i < runtimes.size(); ++i) {
            summed_speedup += std::pow(static_cast<double>(runtimes[i]) / weights[i][0], _exponent);
        }
        return static_cast<unsigned long long>(summed_speedup * std::pow(10, _precision));
    }
private:
    double       _exponent;
    unsigned int _precision;
    bool         _print_speedups;
};

}

template <typename T>
bool equal(const T& v1, const T& v2, double epsilon) {
    if (epsilon == 0) return v1 == v2;
    T diff = v1 - v2;
    if (diff < 0) diff = -diff;
    return diff < epsilon;
}


template<size_t NUM_INPUT_SIZES, typename OCAL_DEVICE_TYPE, typename OCAL_KERNEL_TYPE_1, typename OCAL_KERNEL_TYPE_2,
        typename GS_1_0, typename GS_1_1, typename GS_1_2, typename LS_1_0, typename LS_1_1, typename LS_1_2,
        typename GS_2_0, typename GS_2_1, typename GS_2_2, typename LS_2_0, typename LS_2_1, typename LS_2_2,
        typename G_RES_DEST_T, typename L_RES_DEST_T, typename P_RES_DEST_T, typename REDUCTION_TP_TYPES,
        typename INPUT_TYPES, typename OUTPUT_TYPES, typename WG_R_TYPES, typename WI_R_TYPES>
class ocal_md_hom_wrapper_class {
private:
    typedef std::tuple<std::reference_wrapper<device_info<OCAL_DEVICE_TYPE>>, std::vector<unsigned long long>, std::array<int, NUM_INPUT_SIZES>, INPUT_TYPES, OUTPUT_TYPES> case_type;
    typedef std::function<unsigned long long(const std::vector<unsigned long long>&, const std::vector<std::vector<unsigned long long>>&)> weighting_function_type;
    std::vector<unsigned long long> best_runtimes;
public:
    ocal_md_hom_wrapper_class(const kernel_info<OCAL_KERNEL_TYPE_1>&    kernel_1,
                              const std::tuple<GS_1_0, GS_1_1, GS_1_2>& global_size_1,
                              const std::tuple<LS_1_0, LS_1_1, LS_1_2>& local_size_1,

                              const kernel_info<OCAL_KERNEL_TYPE_2>&    kernel_2,
                              const std::tuple<GS_2_0, GS_2_1, GS_2_2>& global_size_2,
                              const std::tuple<LS_2_0, LS_2_1, LS_2_2>& local_size_2,

                              const WG_R_TYPES&                         tp_wg_r,
                              const WI_R_TYPES&                         tp_wi_r,
                              const std::tuple<G_RES_DEST_T, L_RES_DEST_T, P_RES_DEST_T>& tp_res_dest,
                              const REDUCTION_TP_TYPES&                 tp_reduction,

                              bool                                      pass_input_sizes,

                              const size_t                              warm_ups,
                              const size_t                              num_evaluations,

                              const timeout                             warm_up_timeout,
                              const timeout                             evaluation_timeout,

                              const bool                                silent) :
            _weighting_function(weighting::weighted_sum),
            _kernel_1(kernel_1), _global_size_pattern_1(global_size_1), _local_size_pattern_1(local_size_1),
            _kernel_2(kernel_2), _global_size_pattern_2(global_size_2), _local_size_pattern_2(local_size_2),
            _tp_wg_r(tp_wg_r), _tp_wi_r(tp_wi_r),
            _tp_res_dest(tp_res_dest), _tp_reduction(tp_reduction),
            _pass_input_sizes(pass_input_sizes), _num_evaluations(num_evaluations), _warm_ups(warm_ups), _warm_up_timeout(warm_up_timeout), _evaluation_timeout(evaluation_timeout), _silent(silent),
            _delete_gold_ptr(false), _check_interval(CHECK_NONE), _epsilon(0.0) {

    }

    ~ocal_md_hom_wrapper_class() {
        if (_delete_gold_ptr && !_gold_ptr.empty()) {
            free_gold_pointers(_gold_ptr, std::get<4>(_cases[0]));
        }
    }

    void add_case(device_info<OCAL_DEVICE_TYPE> &device_info, const std::vector<unsigned long long> &weights, const std::array<int, NUM_INPUT_SIZES> &input_sizes, const INPUT_TYPES &inputs, const OUTPUT_TYPES &outputs) {
        _cases.emplace_back(std::ref(device_info), weights, input_sizes, inputs, outputs);
        _weights.push_back(weights);
        best_runtimes.push_back(std::numeric_limits<unsigned long long>::max() / _num_evaluations);
        bool found = false;
        for (const OCAL_DEVICE_TYPE &device : _devices) {
            if (&device == &(std::get<0>(_cases.back()).get().device())) {
                found = true;
                break;
            }
        }
        if (!found)
            _devices.emplace_back(std::get<0>(_cases.back()).get().device());
    }

    void set_weighting_function(const weighting_function_type &f) {
        _weighting_function = f;
    }

    void warm_ups(size_t warm_ups) {
        _warm_ups = warm_ups;
    }

    void evaluations(size_t evaluations) {
        _num_evaluations = evaluations;
    }

    unsigned long long operator()(configuration &configuration,
                                  unsigned long long *compile_time = nullptr, int *error_code = nullptr) {
        // update tp values
        for (auto &tp : configuration) {
            auto tp_value = tp.second;
            tp_value.update_tp();
//            if (!_silent && tp.first.find("NOT_USED") == std::string::npos)
//                std::cout << tp.first << "\t" << tp.second.value().size_t_val()<< std::endl;
        }

        // determine if second kernel is needed
        bool exec_second_kernel = needs_second_kernel(_tp_wg_r);

        // compile kernel
        std::map<OCAL_DEVICE_TYPE*, std::vector<ocal::kernel>> device_to_kernel;
        auto start = std::chrono::system_clock::now();
        std::vector<std::thread>        compile_threads;
        std::vector<int>                error_codes(_devices.size());
        for (int i = 0; i < _devices.size(); ++i) {
            auto *device_ptr = &(_devices[i]);
            auto *error_code_ptr = &(error_codes[i]);
//            compile_threads.emplace_back([&,device_ptr,error_code_ptr] {
                compile_kernel(*device_ptr, exec_second_kernel, configuration, error_code_ptr, device_to_kernel);
//            });
        }
        for (auto &thread : compile_threads) {
            thread.join();
        }
        auto end = std::chrono::system_clock::now();
        auto compile_time_in_ms = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
        std::cout << "compilation time: " << compile_time_in_ms << "ms" << std::endl;
        if (compile_time != nullptr) *compile_time = compile_time_in_ms;
        for (int i = 0; i < _devices.size(); ++i) {
            if (error_codes[i] != 0) {
                if (error_code != nullptr) {
                    *error_code = error_codes[i];
                }
                std::cerr << "compilation error: " << error_codes[i] << std::endl;
                throw std::exception();
            }
        }

        // execute kernel
        start = std::chrono::system_clock::now();
        std::vector<unsigned long long> runtimes;
        for (int i = 0; i < _cases.size(); ++i) {
            // calculate timeouts
            unsigned long warm_up_timeout = 0;
            if (_warm_up_timeout.type == ABSOLUTE) {
                warm_up_timeout = _warm_up_timeout.value.absolute;
            } else if (best_runtimes[i] != std::numeric_limits<unsigned long long>::max() / _num_evaluations
                       && best_runtimes[i] <= std::numeric_limits<unsigned long long>::max() / _warm_up_timeout.value.factor) {
                warm_up_timeout = static_cast<unsigned long>(_warm_up_timeout.value.factor * best_runtimes[i]);
            }
            unsigned long evaluation_timeout = 0;
            if (_evaluation_timeout.type == ABSOLUTE) {
                evaluation_timeout = _evaluation_timeout.value.absolute;
            } else if (best_runtimes[i] != std::numeric_limits<unsigned long long>::max() / _num_evaluations
                       && best_runtimes[i] <= std::numeric_limits<unsigned long long>::max() / _evaluation_timeout.value.factor) {
                evaluation_timeout = static_cast<unsigned long>(_evaluation_timeout.value.factor * best_runtimes[i]);
            }

            try {
                auto runtime = execute_kernel(i, std::get<0>(_cases[i]).get().device(), std::get<2>(_cases[i]), configuration, exec_second_kernel, best_runtimes[i], warm_up_timeout, evaluation_timeout, device_to_kernel);
                best_runtimes[i] = std::min(best_runtimes[i], runtime);
                runtimes.push_back(runtime);
            }
#ifdef OCAL_OCL_WRAPPER
            catch (const cl::Error &err) {
                std::cout << "OpenCL error code: " << err.err() << std::endl;
                if (error_code != nullptr) *error_code = err.err();
                throw;
            }
#endif
#ifdef OCAL_CUDA_WRAPPER
            catch (const std::runtime_error &err) {
                std::cout << "CUDA error code: " << err.what() << std::endl;
                if (error_code != nullptr) *error_code = std::atoi(err.what());
                throw;
            }
#endif
        }
        end = std::chrono::system_clock::now();
        auto execution_time_in_ms = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
        std::cout << "execution time: " << execution_time_in_ms << "ms" << std::endl;
        return _weighting_function(runtimes, _weights);
    }

    void check_results(CHECK_INTERVAL check_interval, double epsilon, const std::vector<std::vector<std::string>> &gold_files) {
        assert(gold_files.size() == _cases.size());
        check_for_gold_file(gold_files, _gold_ptr, std::get<4>(_cases[0]));
        _delete_gold_ptr = true;
        _check_interval = check_interval;
        _epsilon = epsilon;
    }

    void check_results(CHECK_INTERVAL check_interval, double epsilon, const std::vector<std::string> &gold_files) {
        assert(_cases.size() == 1);
        check_for_gold_file(std::vector<std::vector<std::string>>({gold_files}), _gold_ptr, std::get<4>(_cases[0]));
        _delete_gold_ptr = true;
        _check_interval = check_interval;
        _epsilon = epsilon;
    }

private:
    std::vector<case_type>             _cases;
    std::vector<std::reference_wrapper<OCAL_DEVICE_TYPE>> _devices;
    weighting_function_type            _weighting_function;
    std::vector<std::vector<unsigned long long>> _weights;

    // Kernel
    kernel_info<OCAL_KERNEL_TYPE_1>    _kernel_1;
    std::tuple<GS_1_0, GS_1_1, GS_1_2> _global_size_pattern_1;
    std::tuple<LS_1_0, LS_1_1, LS_1_2> _local_size_pattern_1;
    kernel_info<OCAL_KERNEL_TYPE_2>    _kernel_2;
    std::tuple<GS_2_0, GS_2_1, GS_2_2> _global_size_pattern_2;
    std::tuple<LS_2_0, LS_2_1, LS_2_2> _local_size_pattern_2;

    OUTPUT_TYPES                       _res_g;
    OUTPUT_TYPES                       _int_res;

    WG_R_TYPES                         _tp_wg_r;
    WI_R_TYPES                         _tp_wi_r;
    const std::tuple<G_RES_DEST_T, L_RES_DEST_T, P_RES_DEST_T>& _tp_res_dest;
    REDUCTION_TP_TYPES                 _tp_reduction;

    bool                               _pass_input_sizes;

    size_t                             _num_evaluations;
    size_t                             _warm_ups;

    const bool                         _silent;
    const timeout                      _warm_up_timeout;
    const timeout                      _evaluation_timeout;

    std::vector<std::vector<void *>>   _gold_ptr;
    bool                               _delete_gold_ptr;
    CHECK_INTERVAL                     _check_interval;
    double                             _epsilon;

    void compile_kernel(OCAL_DEVICE_TYPE &device, bool exec_second_kernel, atf::configuration &configuration, int *error_code, std::map<OCAL_DEVICE_TYPE*, std::vector<ocal::kernel>> &device_to_kernel) {
        // create kernel objects
        device_to_kernel[&device] = std::vector<ocal::kernel>();
        std::vector<std::string> flags_1 = _kernel_1.flags();
        for (const auto &tp : configuration) {
            flags_1.emplace_back(" -D ");
            flags_1.back().append(tp.second.name());
            flags_1.back().append("=");
            flags_1.back().append(tp.second.value());
        }
        device_to_kernel[&device].emplace_back(_kernel_1.ocal_source(), _kernel_1.kernel_name(), flags_1);
        if (exec_second_kernel) {
            std::vector<std::string> flags_2 = _kernel_2.flags();
            for (const auto &tp : configuration) {
                flags_2.emplace_back(" -D ");
                flags_2.back().append(tp.second.name());
                flags_2.back().append("=");
                flags_2.back().append(tp.second.value());
            }
            device_to_kernel[&device].emplace_back(_kernel_2.ocal_source(), _kernel_2.kernel_name(), flags_2);
        }

        // TODO remove duplicate code
        if (!exec_second_kernel) {
            // compile kernel
            try {
                device(device_to_kernel[&device][0]);
            }
#ifdef OCAL_OCL_WRAPPER
            catch (const cl::Error &err) {
                *error_code = err.err();
                return;
            }
#endif
#ifdef OCAL_CUDA_WRAPPER
            catch (const std::runtime_error &err) {
                *error_code = std::atoi(err.what());
                return;
            }
#endif
        } else {
            int error_code_1 = 0;
            int error_code_2 = 0;
//            std::thread thread_compile_1([&]() {
                // compile kernel
                try {
                    device(device_to_kernel[&device][0]);
                }
#ifdef OCAL_OCL_WRAPPER
                catch (const cl::Error &err) {
                    error_code_1 = err.err();
                    return;
                }
#endif
#ifdef OCAL_CUDA_WRAPPER
                catch (const std::runtime_error &err) {
                    error_code_1 = std::atoi(err.what());
                    return;
                }
#endif
//            });
//            thread_compile_1.join();
//            std::thread thread_compile_2([&]() {
                // compile kernel
                try {
                    device(device_to_kernel[&device][1]);
                }
#ifdef OCAL_OCL_WRAPPER
                catch (const cl::Error &err) {
                    error_code_2 = err.err();
                    return;
                }
#endif
#ifdef OCAL_CUDA_WRAPPER
                catch (const std::runtime_error &err) {
                    error_code_2 = std::atoi(err.what());
                    return;
                }
#endif
//            });
//            thread_compile_2.join();
            if (error_code != nullptr) {
                if (error_code_1 != 0) {
                    *error_code = error_code_1;
                } else {
                    *error_code = error_code_2;
                }
            }
        }
    }

    unsigned long long execute_kernel(int case_nr, OCAL_DEVICE_TYPE &device, const std::array<int, NUM_INPUT_SIZES> &input_sizes,
                                      configuration &configuration, bool exec_second_kernel, unsigned long long best_runtime,
                                      unsigned long long warm_up_timeout, unsigned long long evaluation_timeout,
                                      std::map<OCAL_DEVICE_TYPE*, std::vector<ocal::kernel>> &device_to_kernel) {
        return execute_kernel(case_nr, device, input_sizes, std::make_index_sequence<NUM_INPUT_SIZES>(),
                              configuration, exec_second_kernel, best_runtime, warm_up_timeout, evaluation_timeout, device_to_kernel);
    }
    template <size_t... Is>
    unsigned long long execute_kernel(int case_nr, OCAL_DEVICE_TYPE &device, const std::array<int, NUM_INPUT_SIZES> &input_sizes,
                                      std::index_sequence<Is...>,
                                      configuration &configuration, bool exec_second_kernel, unsigned long long best_runtime,
                                      unsigned long long warm_up_timeout, unsigned long long evaluation_timeout,
                                      std::map<OCAL_DEVICE_TYPE*, std::vector<ocal::kernel>> &device_to_kernel) {
        return execute_kernel(case_nr, device, std::make_tuple(input_sizes[Is]...),
                              configuration, exec_second_kernel, best_runtime, warm_up_timeout, evaluation_timeout, device_to_kernel);
    }
    template<typename... Ts>
    unsigned long long execute_kernel(int case_nr, OCAL_DEVICE_TYPE &device, const std::tuple<Ts...> &input_sizes,
                                      configuration &configuration, bool exec_second_kernel, unsigned long long best_runtime,
                                      unsigned long long warm_up_timeout, unsigned long long evaluation_timeout,
                                      std::map<OCAL_DEVICE_TYPE*, std::vector<ocal::kernel>> &device_to_kernel) {
        size_t output_size = std::get<0>(std::get<4>(_cases[case_nr])).get().size();

        // resize int_res
        size_t int_res_size = output_size;
        if (exec_second_kernel) {
            int_res_size *= mult(_tp_wg_r);
            resize_buffers(int_res_size, _int_res);
        } else {
            resize_buffers(0, _int_res);
        }

        // resize res_g
        size_t res_g_size = output_size;
        if (std::get<0>(_tp_res_dest) == 2) {
            res_g_size *= mult(_tp_wg_r);
        }
        apply_reduction_parameters(res_g_size, _tp_reduction);
        if (res_g_size > int_res_size) {
            resize_buffers(res_g_size, _res_g);
        } else {
            resize_buffers(1, _res_g);
        }

        // get global and local size
        size_t gs_0 = std::get<0>(_global_size_pattern_1).get_value();
        size_t gs_1 = std::get<1>(_global_size_pattern_1).get_value();
        size_t gs_2 = std::get<2>(_global_size_pattern_1).get_value();

        size_t ls_0 = std::get<0>(_local_size_pattern_1).get_value();
        size_t ls_1 = std::get<1>(_local_size_pattern_1).get_value();
        size_t ls_2 = std::get<2>(_local_size_pattern_1).get_value();

//        std::cout << "gs: " << gs_0 << ", " << gs_1 << ", " << gs_2 << std::endl;
//        std::cout << "ls: " << ls_0 << ", " << ls_1 << ", " << ls_2 << std::endl;

        // set kernel, flags, global and local size
        device(device_to_kernel[&device][0])(
                ocl::nd_range(gs_0, gs_1, gs_2),
                ocl::nd_range(ls_0, ls_1, ls_2)
        );

        // warm ups
        if (_warm_ups > 0) {
            // run first warm up with timeout
            if (!exec_second_kernel) {
                if (_pass_input_sizes)
                    std::apply(device, std::tuple_cat(input_sizes, std::get<3>(_cases[case_nr]), _res_g, std::get<4>(_cases[case_nr])));
                else
                    std::apply(device, std::tuple_cat(std::get<3>(_cases[case_nr]), _res_g, std::get<4>(_cases[case_nr])));
            } else {
                if (_pass_input_sizes)
                    std::apply(device, std::tuple_cat(input_sizes, std::get<3>(_cases[case_nr]), _res_g, _int_res));
                else
                    std::apply(device, std::tuple_cat(std::get<3>(_cases[case_nr]), _res_g, _int_res));
            }
            auto timeout_event = device.last_event();
            wait_for_event(timeout_event, warm_up_timeout);
        }
        for (size_t i = 1; i < _warm_ups; ++i) {
            if (!exec_second_kernel) {
                if (_pass_input_sizes)
                    std::apply(device, std::tuple_cat(input_sizes, std::get<3>(_cases[case_nr]), _res_g, std::get<4>(_cases[case_nr])));
                else
                    std::apply(device, std::tuple_cat(std::get<3>(_cases[case_nr]), _res_g, std::get<4>(_cases[case_nr])));
            } else {
                if (_pass_input_sizes)
                    std::apply(device, std::tuple_cat(input_sizes, std::get<3>(_cases[case_nr]), _res_g, _int_res));
                else
                    std::apply(device, std::tuple_cat(std::get<3>(_cases[case_nr]), _res_g, _int_res));
            }
            device.last_event().wait();
        }

        // kernel launch with profiling
        unsigned long long runtime_in_ns = 0;
        unsigned long long kernel_runtime_in_ns = 0;
        bool skip = false;
        if (_num_evaluations > 0) {
            // run first evaluation with timeout
            if (!exec_second_kernel) {
                if (_pass_input_sizes)
                    std::apply(device, std::tuple_cat(input_sizes, std::get<3>(_cases[case_nr]), _res_g, std::get<4>(_cases[case_nr])));
                else
                    std::apply(device, std::tuple_cat(std::get<3>(_cases[case_nr]), _res_g, std::get<4>(_cases[case_nr])));
            } else {
                if (_pass_input_sizes)
                    std::apply(device, std::tuple_cat(input_sizes, std::get<3>(_cases[case_nr]), _res_g, _int_res));
                else
                    std::apply(device, std::tuple_cat(std::get<3>(_cases[case_nr]), _res_g, _int_res));
            }
            auto timeout_event = device.last_event();
            wait_for_event(timeout_event, evaluation_timeout);

            auto tmp_runtime = device.last_runtime();
            kernel_runtime_in_ns += tmp_runtime;

            if (_num_evaluations > 1 && kernel_runtime_in_ns > best_runtime * _num_evaluations) {
                skip = true;
            }
        }
        if (!skip) {
            for (size_t i = 1; i < _num_evaluations; ++i) {
                if (!exec_second_kernel) {
                    if (_pass_input_sizes)
                        std::apply(device, std::tuple_cat(input_sizes, std::get<3>(_cases[case_nr]), _res_g, std::get<4>(_cases[case_nr])));
                    else
                        std::apply(device, std::tuple_cat(std::get<3>(_cases[case_nr]), _res_g, std::get<4>(_cases[case_nr])));
                } else {
                    if (_pass_input_sizes)
                        std::apply(device, std::tuple_cat(input_sizes, std::get<3>(_cases[case_nr]), _res_g, _int_res));
                    else
                        std::apply(device, std::tuple_cat(std::get<3>(_cases[case_nr]), _res_g, _int_res));
                }

                auto tmp_runtime = device.last_runtime();
                kernel_runtime_in_ns += tmp_runtime;

                if (_num_evaluations > 1 && kernel_runtime_in_ns > best_runtime * _num_evaluations) {
                    kernel_runtime_in_ns /= i + 1;
                    skip = true;
                    break;
                }
            }
        }
        if (!skip)
            runtime_in_ns = kernel_runtime_in_ns / _num_evaluations;
        else
            runtime_in_ns = kernel_runtime_in_ns;
        if (!exec_second_kernel && !_gold_ptr.empty() && (_check_interval == CHECK_ALL || _check_interval == CHECK_FINAL))
            check_results_helper(1, case_nr, configuration, std::get<4>(_cases[case_nr]));
        if (!exec_second_kernel)
            return runtime_in_ns;

        // resize res_g
        res_g_size /= mult(_tp_wg_r);
        if (res_g_size > output_size) {
            resize_buffers(res_g_size, _res_g);
        } else {
            resize_buffers(1, _res_g);
        }

        // get global and local size
        gs_0 = std::get<0>(_global_size_pattern_2).get_value();
        gs_1 = std::get<1>(_global_size_pattern_2).get_value();
        gs_2 = std::get<2>(_global_size_pattern_2).get_value();

        ls_0 = std::get<0>(_local_size_pattern_2).get_value();
        ls_1 = std::get<1>(_local_size_pattern_2).get_value();
        ls_2 = std::get<2>(_local_size_pattern_2).get_value();

//        std::cout << "gs: " << gs_0 << ", " << gs_1 << ", " << gs_2 << std::endl;
//        std::cout << "ls: " << ls_0 << ", " << ls_1 << ", " << ls_2 << std::endl;

        // set kernel, global and local size
        device(device_to_kernel[&device][1])(
                ocl::nd_range(gs_0, gs_1, gs_2),
                ocl::nd_range(ls_0, ls_1, ls_2)
        );

        // warm ups
        if (_warm_ups > 0) {
            // run first warm up with timeout
            if (_pass_input_sizes)
                std::apply(device, std::tuple_cat(_int_res, _res_g, std::get<4>(_cases[case_nr]), input_sizes));
            else
                std::apply(device, std::tuple_cat(_int_res, _res_g, std::get<4>(_cases[case_nr])));
            auto timeout_event = device.last_event();
            wait_for_event(timeout_event, warm_up_timeout);
        }
        for (size_t i = 1; i < _warm_ups; ++i) {
            if (_pass_input_sizes)
                std::apply(device, std::tuple_cat(_int_res, _res_g, std::get<4>(_cases[case_nr]), input_sizes));
            else
                std::apply(device, std::tuple_cat(_int_res, _res_g, std::get<4>(_cases[case_nr])));
            device.last_event().wait();
        }

        // kernel launch with profiling
        kernel_runtime_in_ns = 0;
        skip = false;
        if (_num_evaluations > 0) {
            // run first evaluation with timeout
            if (_pass_input_sizes)
                std::apply(device, std::tuple_cat(_int_res, _res_g, std::get<4>(_cases[case_nr]), input_sizes));
            else
                std::apply(device, std::tuple_cat(_int_res, _res_g, std::get<4>(_cases[case_nr])));
            auto timeout_event = device.last_event();
            wait_for_event(timeout_event, evaluation_timeout);

            auto tmp_runtime = device.last_runtime();
            kernel_runtime_in_ns += tmp_runtime;

            if (_num_evaluations > 1 && kernel_runtime_in_ns > (best_runtime - runtime_in_ns) * _num_evaluations) {
                skip = true;
            }
        }
        if (!skip) {
            for (size_t i = 1; i < _num_evaluations; ++i) {
                if (_pass_input_sizes)
                    std::apply(device, std::tuple_cat(_int_res, _res_g, std::get<4>(_cases[case_nr]), input_sizes));
                else
                    std::apply(device, std::tuple_cat(_int_res, _res_g, std::get<4>(_cases[case_nr])));

                auto tmp_runtime = device.last_runtime();
                kernel_runtime_in_ns += tmp_runtime;

                if (_num_evaluations > 1 && kernel_runtime_in_ns > (best_runtime - runtime_in_ns) * _num_evaluations) {
                    kernel_runtime_in_ns /= i + 1;
                    skip = true;
                    break;
                }
            }
        }
        if (!skip)
            runtime_in_ns += kernel_runtime_in_ns / _num_evaluations;
        else
            runtime_in_ns += kernel_runtime_in_ns;
        if (!_gold_ptr.empty() && (_check_interval == CHECK_ALL || _check_interval == CHECK_FINAL))
            check_results_helper(2, case_nr, configuration, std::get<4>(_cases[case_nr]));

        return runtime_in_ns;
    }

    // helper for parsing gold files
    template <typename... Ts>
    void check_for_gold_file(std::vector<std::vector<std::string>> gold_files, std::vector<std::vector<void *>> &gold_ptr, const std::tuple<Ts...> &inputs) {
        gold_ptr.resize(_cases.size());
        check_for_gold_file(gold_files, gold_ptr, inputs, std::make_index_sequence<sizeof...(Ts)>());
    }
    template <typename... Ts, size_t... Is>
    void check_for_gold_file(std::vector<std::vector<std::string>> gold_files, std::vector<std::vector<void *>> &gold_ptr, const std::tuple<Ts...> &inputs, std::index_sequence<Is...>) {
        check_for_gold_file(gold_files, gold_ptr, std::get<Is>(inputs)...);
    }
    template <typename T, typename... Ts>
    void check_for_gold_file(std::vector<std::vector<std::string>> gold_files, std::vector<std::vector<void *>> &gold_ptr, const T &input, const Ts&... inputs) {
        check_for_gold_file(gold_files, gold_ptr, inputs...);
    }
    template <typename T, typename... Ts>
    void check_for_gold_file(std::vector<std::vector<std::string>> gold_files, std::vector<std::vector<void *>> &gold_ptr, const ocal::common::read_class<ocal::buffer<T>> &buffer, const Ts&... inputs) {
        bool recurse = false;
        for (int case_nr = 0; case_nr < _cases.size(); ++case_nr) {
            if (gold_files[case_nr].empty()) continue;
            recurse = recurse || gold_files[case_nr].size() > 1;
            if (!gold_files[case_nr].front().empty()) {
                std::ifstream is(gold_files[case_nr].front());
                std::istream_iterator<T> start(is), end;
                std::vector<T> expected_result(start, end);
                T *gold_data = new T[expected_result.size()];
                for (int i = 0; i < expected_result.size(); ++i) {
                    gold_data[i] = expected_result[i];
                }
                gold_ptr[case_nr].push_back(gold_data);
            } else {
                gold_ptr[case_nr].push_back(nullptr);
            }
            gold_files[case_nr].erase(gold_files[case_nr].begin());
        }
        if (recurse)
            check_for_gold_file(gold_files, gold_ptr, inputs...);
    }
    template <typename T, typename... Ts>
    void check_for_gold_file(std::vector<std::vector<std::string>> gold_files, std::vector<std::vector<void *>> &gold_ptr, const ocal::common::write_class<ocal::buffer<T>> &buffer, const Ts&... inputs) {
        bool recurse = false;
        for (int case_nr = 0; case_nr < _cases.size(); ++case_nr) {
            if (gold_files[case_nr].empty()) continue;
            recurse = recurse || gold_files[case_nr].size() > 1;
            if (!gold_files[case_nr].front().empty()) {
                std::ifstream is(gold_files[case_nr].front());
                std::istream_iterator<T> start(is), end;
                std::vector<T> expected_result(start, end);
                T *gold_data = new T[expected_result.size()];
                for (int i = 0; i < expected_result.size(); ++i) {
                    gold_data[i] = expected_result[i];
                }
                gold_ptr[case_nr].push_back(gold_data);
            } else {
                gold_ptr[case_nr].push_back(nullptr);
            }
            gold_files[case_nr].erase(gold_files[case_nr].begin());
        }
        if (recurse)
            check_for_gold_file(gold_files, gold_ptr, inputs...);
    }
    template <typename T, typename... Ts>
    void check_for_gold_file(std::vector<std::vector<std::string>> gold_files, std::vector<std::vector<void *>> &gold_ptr, const ocal::common::read_write_class<ocal::buffer<T>> &buffer, const Ts&... inputs) {
        bool recurse = false;
        for (int case_nr = 0; case_nr < _cases.size(); ++case_nr) {
            if (gold_files[case_nr].empty()) continue;
            recurse = recurse || gold_files[case_nr].size() > 1;
            if (!gold_files[case_nr].front().empty()) {
                std::ifstream is(gold_files[case_nr].front());
                std::istream_iterator<T> start(is), end;
                std::vector<T> expected_result(start, end);
                T *gold_data = new T[expected_result.size()];
                for (int i = 0; i < expected_result.size(); ++i) {
                    gold_data[i] = expected_result[i];
                }
                gold_ptr[case_nr].push_back(gold_data);
            } else {
                gold_ptr[case_nr].push_back(nullptr);
            }
            gold_files[case_nr].erase(gold_files[case_nr].begin());
        }
        if (recurse)
            check_for_gold_file(gold_files, gold_ptr, inputs...);
    }
    void check_for_gold_file(const std::vector<std::vector<std::string>>& gold_files, std::vector<std::vector<void *>> &gold_ptr) {
    }

    // helper for freeing gold pointers
    template <typename... Ts>
    void free_gold_pointers(std::vector<std::vector<void *>> &gold_ptr, const std::tuple<Ts...> &inputs) {
        free_gold_pointers(gold_ptr, inputs, std::make_index_sequence<sizeof...(Ts)>());
    }
    template <typename... Ts, size_t... Is>
    void free_gold_pointers(std::vector<std::vector<void *>> &gold_ptr, const std::tuple<Ts...> &inputs, std::index_sequence<Is...>) {
        free_gold_pointers(0, gold_ptr, std::get<Is>(inputs)...);
    }
    template <typename T, typename... Ts>
    void free_gold_pointers(int nr, std::vector<std::vector<void *>> &gold_ptr, const T &input, const Ts&... inputs) {
        free_gold_pointers(nr, gold_ptr, inputs...);
    }
    template <typename T, typename... Ts>
    void free_gold_pointers(int nr, std::vector<std::vector<void *>> &gold_ptr, const ocal::common::read_class<ocal::buffer<T>> &buffer, const Ts&... inputs) {
        for (int case_nr = 0; case_nr < _cases.size(); ++case_nr)
            delete[] (T*) gold_ptr[case_nr][nr];
        if (nr < gold_ptr[0].size() - 1)
            free_gold_pointers(nr + 1, gold_ptr, inputs...);
    }
    template <typename T, typename... Ts>
    void free_gold_pointers(int nr, std::vector<std::vector<void *>> &gold_ptr, const ocal::common::write_class<ocal::buffer<T>> &buffer, const Ts&... inputs) {
        for (int case_nr = 0; case_nr < _cases.size(); ++case_nr)
            delete[] (T*) (gold_ptr[case_nr][nr]);
        if (nr < gold_ptr[0].size() - 1)
            free_gold_pointers(nr + 1, gold_ptr, inputs...);
    }
    template <typename T, typename... Ts>
    void free_gold_pointers(int nr, std::vector<std::vector<void *>> &gold_ptr, const ocal::common::read_write_class<ocal::buffer<T>> &buffer, const Ts&... inputs) {
        for (int case_nr = 0; case_nr < _cases.size(); ++case_nr)
            delete[] (T*) gold_ptr[case_nr][nr];
        if (nr < gold_ptr[0].size() - 1)
            free_gold_pointers(nr + 1, gold_ptr, inputs...);
    }
    void free_gold_pointers(int nr, std::vector<std::vector<void *>> &gold_ptr) {
    }

    // helper for checking result buffers against gold data
    template <typename... Ts>
    std::tuple<bool, size_t, size_t, double> check_buffers(int case_nr, std::vector<std::vector<void *>> &gold_ptr, std::tuple<Ts...> &inputs) {
        return check_buffers(case_nr, gold_ptr, inputs, std::make_index_sequence<sizeof...(Ts)>());
    }
    template <typename... Ts, size_t... Is>
    std::tuple<bool, size_t, size_t, double> check_buffers(int case_nr, std::vector<std::vector<void *>> &gold_ptr, std::tuple<Ts...> &inputs, std::index_sequence<Is...>) {
        return check_buffers(case_nr, 0, gold_ptr, std::get<Is>(inputs)...);
    }
    template <typename T, typename... Ts>
    std::tuple<bool, size_t, size_t, double> check_buffers(int case_nr, int nr, std::vector<std::vector<void *>> &gold_ptr, T &input, Ts&... inputs) {
        return check_buffers(case_nr, nr, gold_ptr, inputs...);
    }
    template <typename T, typename... Ts>
    std::tuple<bool, size_t, size_t, double> check_buffers(int case_nr, int nr, std::vector<std::vector<void *>> &gold_ptr, ocal::common::read_class<ocal::buffer<T>> &buffer, Ts&... inputs) {
        T *gold_data = (T*) gold_ptr[case_nr][nr];
        if (gold_data != nullptr) {
            for (int i = 0; i < buffer.get().size(); ++i) {
                if (!equal<T>(buffer.get()[i], gold_data[i], _epsilon)) {
                    return std::make_tuple(true, nr, i, ((T)buffer.get()[i] - (T)gold_data[i]));
                }
            }
        }

        if (nr < gold_ptr[case_nr].size() - 1)
            return check_buffers(case_nr, nr + 1, gold_ptr, inputs...);
        else
            return std::make_tuple(false, 0, 0, 0.0);
    }
    template <typename T, typename... Ts>
    std::tuple<bool, size_t, size_t, double> check_buffers(int case_nr, int nr, std::vector<std::vector<void *>> &gold_ptr, ocal::common::write_class<ocal::buffer<T>> &buffer, Ts&... inputs) {
        T *gold_data = (T*) gold_ptr[case_nr][nr];
        if (gold_data != nullptr) {
            for (int i = 0; i < buffer.get().size(); ++i) {
                if (!equal<T>(buffer.get()[i], gold_data[i], _epsilon)) {
                    return std::make_tuple(true, nr, i, ((T)buffer.get()[i] - (T)gold_data[i]));
                }
            }
        }

        if (nr < gold_ptr[case_nr].size() - 1)
            return check_buffers(case_nr, nr + 1, gold_ptr, inputs...);
        else
            return std::make_tuple(false, 0, 0, 0.0);
    }
    template <typename T, typename... Ts>
    std::tuple<bool, size_t, size_t, double> check_buffers(int case_nr, int nr, std::vector<std::vector<void *>> &gold_ptr, ocal::common::read_write_class<ocal::buffer<T>> &buffer, Ts&... inputs) {
        T *gold_data = (T*) gold_ptr[case_nr][nr];
        if (gold_data != nullptr) {
            for (int i = 0; i < buffer.get().size(); ++i) {
                if (!equal<T>(buffer.get()[i], gold_data[i], _epsilon)) {
                    return std::make_tuple(true, nr, i, ((T)buffer.get()[i] - (T)gold_data[i]));
                }
            }
        }

        if (nr < gold_ptr[case_nr].size() - 1)
            return check_buffers(case_nr, nr + 1, gold_ptr, inputs...);
        else
            return std::make_tuple(false, 0, 0, 0.0);
    }
    std::tuple<bool, size_t, size_t, double> check_buffers(int case_nr, int nr, std::vector<std::vector<void *>> &gold_ptr) {
        return std::make_tuple(!gold_ptr.empty(), 0, 0, 0.0);
    }

    // helper for writing erroneous buffer to file
    template <typename... Ts>
    void write_buffer_to_file(int case_nr, int buffer_nr, std::ofstream &stream, std::vector<std::vector<void *>> &gold_ptr, std::tuple<Ts...> &inputs) {
        write_buffer_to_file(case_nr, buffer_nr, stream, gold_ptr, inputs, std::make_index_sequence<sizeof...(Ts)>());
    }
    template <typename... Ts, size_t... Is>
    void write_buffer_to_file(int case_nr, int buffer_nr, std::ofstream &stream, std::vector<std::vector<void *>> &gold_ptr, std::tuple<Ts...> &inputs, std::index_sequence<Is...>) {
        write_buffer_to_file(case_nr, buffer_nr, stream, 0, gold_ptr, std::get<Is>(inputs)...);
    }
    template <typename T, typename... Ts>
    void write_buffer_to_file(int case_nr, int buffer_nr, std::ofstream &stream, int nr, std::vector<std::vector<void *>> &gold_ptr, T &input, Ts&... inputs) {
        write_buffer_to_file(case_nr, buffer_nr, stream, nr, gold_ptr, inputs...);
    }
    template <typename T, typename... Ts>
    void write_buffer_to_file(int case_nr, int buffer_nr, std::ofstream &stream, int nr, std::vector<std::vector<void *>> &gold_ptr, ocal::common::read_class<ocal::buffer<T>> &buffer, Ts&... inputs) {
        if (buffer_nr == nr) {
            stream << "result buffer: " << std::endl;
            for (int i = 0; i < buffer.get().size(); ++i) {
                stream << buffer.get()[i] << "\t";
            }
            stream << std::endl << "gold buffer: " << std::endl;
            T *gold_data = (T*) gold_ptr[case_nr][nr];
            for (int i = 0; i < buffer.get().size(); ++i) {
                stream << gold_data[i] << "\t";
            }
            stream << std::endl;
        } else {
            write_buffer_to_file(case_nr, buffer_nr, stream, nr + 1, gold_ptr, inputs...);
        }
    }
    template <typename T, typename... Ts>
    void write_buffer_to_file(int case_nr, int buffer_nr, std::ofstream &stream, int nr, std::vector<std::vector<void *>> &gold_ptr, ocal::common::write_class<ocal::buffer<T>> &buffer, Ts&... inputs) {
        if (buffer_nr == nr) {
            stream << "result buffer: " << std::endl;
            for (int i = 0; i < buffer.get().size(); ++i) {
                stream << buffer.get()[i] << "\t";
            }
            stream << std::endl << "gold buffer: " << std::endl;
            T *gold_data = (T*) gold_ptr[case_nr][nr];
            for (int i = 0; i < buffer.get().size(); ++i) {
                stream << gold_data[i] << "\t";
            }
            stream << std::endl;
        } else {
            write_buffer_to_file(case_nr, buffer_nr, stream, nr + 1, gold_ptr, inputs...);
        }
    }
    template <typename T, typename... Ts>
    void write_buffer_to_file(int case_nr, int buffer_nr, std::ofstream &stream, int nr, std::vector<std::vector<void *>> &gold_ptr, ocal::common::read_write_class<ocal::buffer<T>> &buffer, Ts&... inputs) {
        if (buffer_nr == nr) {
            stream << "result buffer: " << std::endl;
            for (int i = 0; i < buffer.get().size(); ++i) {
                stream << buffer.get()[i] << "\t";
            }
            stream << std::endl << "gold buffer: " << std::endl;
            T *gold_data = (T*) gold_ptr[case_nr][nr];
            for (int i = 0; i < buffer.get().size(); ++i) {
                stream << gold_data[i] << "\t";
            }
            stream << std::endl;
        } else {
            write_buffer_to_file(case_nr, buffer_nr, stream, nr + 1, gold_ptr, inputs...);
        }
    }
    void write_buffer_to_file(int case_nr, int buffer_nr, std::ofstream &stream, int nr, std::vector<std::vector<void *>> &gold_ptr) {
    }

    // helper for checking whether second kernel is necessary
    template <typename... Ts>
    bool needs_second_kernel(const std::tuple<Ts...> &tp_num_wg_r) {
        return needs_second_kernel(tp_num_wg_r, std::make_index_sequence<sizeof...(Ts)>());
    }
    template <typename... Ts, size_t... Is>
    bool needs_second_kernel(const std::tuple<Ts...> &tp_num_wg_r, std::index_sequence<Is...>) {
        return needs_second_kernel(std::get<Is>(tp_num_wg_r)...);
    }
    template<typename T, typename... Ts>
    bool needs_second_kernel(const T& num_wg_r, const Ts&... params) const {
        return num_wg_r > 1 || needs_second_kernel(params...);
    }
    bool needs_second_kernel() const {
        return false;
    }

    // helper for multiplying parameter values
    template <typename... Ts>
    int mult(const std::tuple<Ts...> &tp_num_wg_r) {
        return mult(tp_num_wg_r, std::make_index_sequence<sizeof...(Ts)>());
    }
    template <typename... Ts, size_t... Is>
    int mult(const std::tuple<Ts...> &tp_num_wg_r, std::index_sequence<Is...>) {
        return mult(std::get<Is>(tp_num_wg_r)...);
    }
    template<typename T, typename... Ts>
    int mult(const T& num_wg_r, const Ts&... params) const {
        return num_wg_r * mult(params...);
    }
    int mult() const {
        return 1;
    }

    // helper for resizing buffers
    template <typename... Ts>
    void resize_buffers(size_t size, std::tuple<Ts...> &buffers) {
        resize_buffers(size, buffers, std::make_index_sequence<sizeof...(Ts)>());
    }
    template <typename... Ts, size_t... Is>
    void resize_buffers(size_t size, std::tuple<Ts...> &buffers, std::index_sequence<Is...>) {
        resize_buffers(size, std::get<Is>(buffers)...);
    }
    template <typename T, typename... Ts>
    void resize_buffers(size_t size, T &input, Ts&... buffers) {
        resize_buffers(size, buffers...);
    }
    template <typename T, typename... Ts>
    void resize_buffers(size_t size, ocal::common::write_class<ocal::buffer<T>> &buffer, Ts&... buffers) {
        if (size != buffer.get().size())
            buffer.get() = ocal::buffer<T>(size);
        resize_buffers(size, buffers...);
    }
    template <typename T, typename... Ts>
    void resize_buffers(size_t size, ocal::common::read_write_class<ocal::buffer<T>> &buffer, Ts&... buffers) {
        if (size != buffer.get().size())
            buffer.get() = ocal::buffer<T>(size);
        resize_buffers(size, buffers...);
    }
    void resize_buffers(size_t size) {
    }

    // helper for applying reduction parameter to res_g size
    template<typename T>
    void apply_reduction_parameters(size_t &res_g_size, const std::tuple<T> &tps) {
        if ((std::get<2>(_tp_res_dest) == 2 && std::get<0>(tps) >= 0) ||
            (std::get<1>(_tp_res_dest) == 2 && std::get<0>(tps) >= 1) ||
            (std::get<0>(_tp_res_dest) == 2 && std::get<0>(tps) >= 2)) {
            res_g_size *= mult(_tp_wi_r);
        }
    }
    void apply_reduction_parameters(size_t &res_g_size, const std::tuple<> &tps) {
    }

    template<typename... Ts>
    void check_results_helper(int kernel, int case_nr, configuration &config, std::tuple<Ts...> &kernel_arguments) {
        auto check_results = check_buffers(case_nr, _gold_ptr, kernel_arguments);

        if (std::get<0>(check_results)) {
            // std::cout << "result is incorrect" << std::endl;
            std::ofstream log_file;
            log_file.open("errors.txt", std::ofstream::out | std::ofstream::trunc);
            log_file << "error for case " << case_nr << " in buffer " << std::get<1>(check_results) << " at index " << std::get<2>(check_results) << " with epsilon " << std::get<3>(check_results) << std::endl;
            log_file << "configuration: " << std::endl;
            for (const auto &val : config) {
                log_file << "#define " << val.first << " " << val.second.value() << std::endl;
            }
            write_buffer_to_file(case_nr, std::get<1>(check_results), log_file, _gold_ptr, kernel_arguments);
            log_file << std::endl;
            log_file.close();
            throw std::exception();
        } else {
            // std::cout << "result is correct" << std::endl;
        }
    }
};


template<typename OCAL_DEVICE_TYPE, size_t NUM_INPUT_SIZES, typename INPUTs, typename OUTPUTs,
        typename OCAL_KERNEL_TYPE_1, typename OCAL_KERNEL_TYPE_2,
        typename GS_1_0, typename GS_1_1, typename GS_1_2, typename LS_1_0, typename LS_1_1, typename LS_1_2,
        typename GS_2_0, typename GS_2_1, typename GS_2_2, typename LS_2_0, typename LS_2_1, typename LS_2_2,
        typename G_RES_DEST_T, typename L_RES_DEST_T, typename P_RES_DEST_T,
        typename... WG_R_Ts, typename... WI_R_Ts, typename... REDUCTION_TP_Ts>
auto ocal_md_hom(const kernel_info<OCAL_KERNEL_TYPE_1>&    kernel_1,
                 const std::tuple<GS_1_0, GS_1_1, GS_1_2>& global_size_1,
                 const std::tuple<LS_1_0, LS_1_1, LS_1_2>& local_size_1,

                 const kernel_info<OCAL_KERNEL_TYPE_2>&    kernel_2,
                 const std::tuple<GS_2_0, GS_2_1, GS_2_2>& global_size_2,
                 const std::tuple<LS_2_0, LS_2_1, LS_2_2>& local_size_2,

                 const std::tuple<WG_R_Ts...>&             tp_wg_r,
                 const std::tuple<WI_R_Ts...>&             tp_wi_r,
                 const std::tuple<G_RES_DEST_T, L_RES_DEST_T, P_RES_DEST_T>& tp_res_dest,
                 const std::tuple<REDUCTION_TP_Ts...>&     tp_reduction,

                 bool                                      pass_input_sizes,

                 const size_t                              warm_ups = 3,
                 const size_t                              num_evaluations = 5,

                 const timeout                             warm_up_timeout = {ABSOLUTE, {.absolute = 0}},
                 const timeout                             evaluation_timeout = {ABSOLUTE, {.absolute = 0}},

                 const bool                                silent = false) {
    return ocal_md_hom_wrapper_class<NUM_INPUT_SIZES, OCAL_DEVICE_TYPE, OCAL_KERNEL_TYPE_1, OCAL_KERNEL_TYPE_2,
            GS_1_0, GS_1_1, GS_1_2, LS_1_0, LS_1_1, LS_1_2,
            GS_2_0, GS_2_1, GS_2_2, LS_2_0, LS_2_1, LS_2_2,
            G_RES_DEST_T, L_RES_DEST_T, P_RES_DEST_T, std::tuple<REDUCTION_TP_Ts...>,
            INPUTs, OUTPUTs, std::tuple<WG_R_Ts...>, std::tuple<WI_R_Ts...>>(
            kernel_1, global_size_1, local_size_1, kernel_2, global_size_2, local_size_2,
            tp_wg_r, tp_wi_r, tp_res_dest, tp_reduction,
            pass_input_sizes, warm_ups, num_evaluations, warm_up_timeout, evaluation_timeout, silent
    );
}

template<size_t NUM_INPUT_SIZES, typename OCAL_DEVICE_TYPE, typename OCAL_KERNEL_TYPE_1, typename OCAL_KERNEL_TYPE_2,
        typename GS_1_0, typename GS_1_1, typename GS_1_2, typename LS_1_0, typename LS_1_1, typename LS_1_2,
        typename GS_2_0, typename GS_2_1, typename GS_2_2, typename LS_2_0, typename LS_2_1, typename LS_2_2,
        typename G_RES_DEST_T, typename L_RES_DEST_T, typename P_RES_DEST_T,
        typename... INPUT_Ts, typename... OUTPUT_Ts, typename... WG_R_Ts, typename... WI_R_Ts, typename... REDUCTION_TP_Ts>
auto ocal_md_hom(device_info<OCAL_DEVICE_TYPE>&            device,
                 const std::array<int, NUM_INPUT_SIZES>&   input_sizes,
                 const std::tuple<INPUT_Ts...>&            inputs,
                 const std::tuple<OUTPUT_Ts...>&           outputs,

                 const kernel_info<OCAL_KERNEL_TYPE_1>&    kernel_1,
                 const std::tuple<GS_1_0, GS_1_1, GS_1_2>& global_size_1,
                 const std::tuple<LS_1_0, LS_1_1, LS_1_2>& local_size_1,

                 const kernel_info<OCAL_KERNEL_TYPE_2>&    kernel_2,
                 const std::tuple<GS_2_0, GS_2_1, GS_2_2>& global_size_2,
                 const std::tuple<LS_2_0, LS_2_1, LS_2_2>& local_size_2,

                 const std::tuple<WG_R_Ts...>&             tp_wg_r,
                 const std::tuple<WI_R_Ts...>&             tp_wi_r,
                 const std::tuple<G_RES_DEST_T, L_RES_DEST_T, P_RES_DEST_T>& tp_res_dest,
                 const std::tuple<REDUCTION_TP_Ts...>&     tp_reduction,

                 bool                                      pass_input_sizes,

                 const size_t                              warm_ups = 3,
                 const size_t                              num_evaluations = 5,

                 const timeout                             warm_up_timeout = {ABSOLUTE, {.absolute = 0}},
                 const timeout                             evaluation_timeout = {ABSOLUTE, {.absolute = 0}},

                 const bool                                silent = false) {
    ocal_md_hom_wrapper_class<NUM_INPUT_SIZES, OCAL_DEVICE_TYPE, OCAL_KERNEL_TYPE_1, OCAL_KERNEL_TYPE_2,
            GS_1_0, GS_1_1, GS_1_2, LS_1_0, LS_1_1, LS_1_2,
            GS_2_0, GS_2_1, GS_2_2, LS_2_0, LS_2_1, LS_2_2,
            G_RES_DEST_T, L_RES_DEST_T, P_RES_DEST_T, std::tuple<REDUCTION_TP_Ts...>,
            std::tuple<INPUT_Ts...>, std::tuple<OUTPUT_Ts...>,
            std::tuple<WG_R_Ts...>, std::tuple<WI_R_Ts...>> wrapper(
            kernel_1, global_size_1, local_size_1, kernel_2, global_size_2, local_size_2,
            tp_wg_r, tp_wi_r, tp_res_dest, tp_reduction,
            pass_input_sizes, warm_ups, num_evaluations, warm_up_timeout, evaluation_timeout, silent
    );
    wrapper.add_case(device, atf::weights(1), input_sizes, inputs, outputs);
    return wrapper;
}

}
}

#endif //OCAL_MD_HOM_WRAPPER_HP