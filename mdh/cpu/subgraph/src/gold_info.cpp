void calculate_gold(const std::string &data_dir, int input_size_l_1, int input_size_l_2, int input_size_l_3, int input_size_l_4) {
    std::ofstream gold_file(data_dir + "/gold.tsv", std::ios::out | std::ios::trunc);
    gold_file << std::fixed << std::setprecision(0);
    for (size_t i_1 = 0; i_1 < input_size_l_1; ++i_1) {
        for (size_t i_2 = 0; i_2 < input_size_l_2; ++i_2) {
            for (size_t i_3 = 0; i_3 < input_size_l_3; ++i_3) {
                for (size_t i_4 = 0; i_4 < input_size_l_4; ++i_4) {
                    gold_file << ((c_1 - (mul_i_1[i_1 * input_size_l_3 * 1 + i_3 * 1 + 0] * mul_i_2[i_1 * 1 * input_size_l_4 + 0 * input_size_l_4 + i_4])) * c_2) << "\t";
                }
                gold_file << std::endl;
            }
            gold_file << std::endl;
        }
        gold_file << std::endl;
    }
    gold_file.close();
}