#!/usr/bin/env bash

# change this line, if you want to change the tuning time per framework and routine
TUNING_TIME_IN_MINUTES=1440

: ${ARTIFACT_ROOT?"Please execute \"source environment.sh\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    echo "Please execute \"source environment.sh\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

cd $ARTIFACT_ROOT || { echo "Please execute \"source environment.sh\" in the root dir of the artifact (the directory containing the scripts folder) first."; exit 1; }


if [[ -n "$ENABLE_GPU" && "$ENABLE_GPU" -eq 1 ]]; then
    # MDH (BiasAddGrad)
    (
      echo "Tuning MDH BiasAddGrad for GPU"
      rm -rf $ARTIFACT_ROOT/results/gpu/mdh/BiasAddGrad &> /dev/null
      cd $ARTIFACT_ROOT/mdh/gpu/BiasAddGrad/build &&
      ./tune_md_hom_strided_v3_bias_add_grad --path $ARTIFACT_ROOT/results/gpu/mdh/BiasAddGrad -d 0 -r no-pp-l-1 -r no-pp-r-1 -r in-cache-l-1 -r in-cache-p-1 -r l-cb-res-p -r p-cb-res-p -r md_poly --input-size-l-1 768 --input-size-r-1 6144 -t OpenTuner -a minutes -v $TUNING_TIME_IN_MINUTES --wrapper-type local --wrapper-command "./tune_md_hom_strided_v3_bias_add_grad [flags]" -g ../gold/md_hom/strided_v3/bias_add_grad/768x6144/gold.tsv --check-type all --kernel-file-1 ../kernel/md_hom/strided_v3/bias_add_grad/static/bias_add_grad_static_1.cuda &&
      source ../scripts/preprocess.sh $ARTIFACT_ROOT/results/gpu/mdh/BiasAddGrad/tuned_configuration ../kernel/md_hom/strided_v3/bias_add_grad/static/bias_add_grad_static_1.cuda &&
      sed -i '\;^//;d' ../kernel/md_hom/strided_v3/bias_add_grad/static/bias_add_grad_static_1.cuda_preprocessed.cl &&
      mv ../kernel/md_hom/strided_v3/bias_add_grad/static/bias_add_grad_static_1.cuda_preprocessed.cl $ARTIFACT_ROOT/results/gpu/mdh/BiasAddGrad/kernel.cu
    )

    # MDH (BatchMatMul)
    (
      echo "Tuning MDH BatchMatMul for GPU"
      rm -rf $ARTIFACT_ROOT/results/gpu/mdh/BatchMatMul &> /dev/null
      cd $ARTIFACT_ROOT/mdh/gpu/BatchMatMul/build &&
      ./tune_md_hom_strided_v3_batch_matmul --path $ARTIFACT_ROOT/results/gpu/mdh/BatchMatMul -d 0 -r no-pp-l-1 -r no-pp-l-2 -r no-pp-l-3 -r no-pp-l-4 -r no-pp-r-1 -r a-cache-l-1 -r b-cache-l-1 -r a-cache-p-1 -r b-cache-p-1 -r l-cb-res-p -r p-cb-res-p -r md_poly --input-size-l-1 16 --input-size-l-2 12 --input-size-l-3 384 --input-size-l-4 64 --input-size-r-1 384 -t OpenTuner -a minutes -v $TUNING_TIME_IN_MINUTES --wrapper-type local --wrapper-command "./tune_md_hom_strided_v3_batch_matmul [flags]" -g ../gold/md_hom/strided_v3/batch_matmul/16x12x384x64x384/gold.tsv --check-type all &&
      source ../scripts/preprocess.sh $ARTIFACT_ROOT/results/gpu/mdh/BatchMatMul/tuned_configuration ../kernel/md_hom/strided_v3/batch_matmul/static/batch_matmul_skip_pp_l1_l2_l3_l4_r1_static_1.cuda &&
      sed -i '\;^//;d' ../kernel/md_hom/strided_v3/batch_matmul/static/batch_matmul_skip_pp_l1_l2_l3_l4_r1_static_1.cuda_preprocessed.cl &&
      mv ../kernel/md_hom/strided_v3/batch_matmul/static/batch_matmul_skip_pp_l1_l2_l3_l4_r1_static_1.cuda_preprocessed.cl $ARTIFACT_ROOT/results/gpu/mdh/BatchMatMul/kernel.cu
    )

    # MDH (subgraph)
    (
      echo "Tuning MDH subgraph for GPU"
      rm -rf $ARTIFACT_ROOT/results/gpu/mdh/subgraph &> /dev/null
      cd $ARTIFACT_ROOT/mdh/gpu/subgraph/build &&
      ./tune_md_hom_strided_v3_subgraph2 --path $ARTIFACT_ROOT/results/gpu/mdh/subgraph -d 0 -r pow2 -r no-pp-l-1 -r no-pp-l-2 --input-size-l-1 16 --input-size-l-2 1 --input-size-l-3 384 --input-size-l-4 384 -t OpenTuner -a minutes -v $TUNING_TIME_IN_MINUTES --wrapper-type local --wrapper-command "./tune_md_hom_strided_v3_subgraph2 [flags]" -g ../gold/md_hom/strided_v3/subgraph2/16x1x384x384/gold.tsv --check-type all &&
      source ../scripts/preprocess.sh $ARTIFACT_ROOT/results/gpu/mdh/subgraph/tuned_configuration ../kernel/md_hom/strided_v3/subgraph2/static/subgraph2_skip_pp_l1_l2_static_1.cuda &&
      sed -i '\;^//;d' ../kernel/md_hom/strided_v3/subgraph2/static/subgraph2_skip_pp_l1_l2_static_1.cuda_preprocessed.cl &&
      mv ../kernel/md_hom/strided_v3/subgraph2/static/subgraph2_skip_pp_l1_l2_static_1.cuda_preprocessed.cl $ARTIFACT_ROOT/results/gpu/mdh/subgraph/kernel.cu
    )

    # TC (BiasAddGrad)
    (
      echo "Tuning TC BiasAddGrad for GPU"
      mkdir -p $ARTIFACT_ROOT/results/gpu/tc/BiasAddGrad
      rm $ARTIFACT_ROOT/results/gpu/tc/BiasAddGrad/* &> /dev/null
      cd $ARTIFACT_ROOT/tc/BiasAddGrad &&
      START=`date +%s`
      while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
        python tune.py 0 768 6144
      done
      python extract_kernel.py 0 768 6144 &> $ARTIFACT_ROOT/results/gpu/tc/BiasAddGrad/kernel_info.txt
    )

    # TC (BatchMatMul)
    (
      echo "Tuning TC BatchMatMul for GPU"
      mkdir -p $ARTIFACT_ROOT/results/gpu/tc/BatchMatMul
      rm $ARTIFACT_ROOT/results/gpu/tc/BatchMatMul/* &> /dev/null
      cd $ARTIFACT_ROOT/tc/BatchMatMul &&
      START=`date +%s`
      while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
        python tune.py 0 16 12 384 64 384
      done
      python extract_kernel.py 0 16 12 384 64 384 &> $ARTIFACT_ROOT/results/gpu/tc/BatchMatMul/kernel_info.txt
    )

    # TC (subgraph)
    (
      echo "Tuning TC subgraph for GPU"
      mkdir -p $ARTIFACT_ROOT/results/gpu/tc/subgraph
      rm $ARTIFACT_ROOT/results/gpu/tc/subgraph/* &> /dev/null
      cd $ARTIFACT_ROOT/tc/subgraph &&
      START=`date +%s`
      while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
        python tune.py 0 16 1 384 384
      done
      python extract_kernel.py 0 16 1 384 384 &> $ARTIFACT_ROOT/results/gpu/tc/subgraph/kernel_info.txt
    )

    # TVM (BiasAddGrad)
    (
      echo "Tuning TVM BiasAddGrad for GPU"
      mkdir -p $ARTIFACT_ROOT/results/gpu/tvm/BiasAddGrad
      rm $ARTIFACT_ROOT/results/gpu/tvm/BiasAddGrad/* &> /dev/null
      cd $ARTIFACT_ROOT/tvm &&
      START=`date +%s`
      while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
        python BiasAddGrad.py tune 768 6144 gpu
      done
    )

    # TVM (BatchMatMul)
    (
      echo "Tuning TVM BatchMatMul for GPU"
      mkdir -p $ARTIFACT_ROOT/results/gpu/tvm/BatchMatMul
      rm $ARTIFACT_ROOT/results/gpu/tvm/BatchMatMul/* &> /dev/null
      cd $ARTIFACT_ROOT/tvm &&
      START=`date +%s`
      while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
        python BatchMatMul.py tune 16 12 384 64 384 gpu
      done
    )

    # TVM (subgraph)
    (
      echo "Tuning TVM subgraph for GPU"
      mkdir -p $ARTIFACT_ROOT/results/gpu/tvm/subgraph
      rm $ARTIFACT_ROOT/results/gpu/tvm/subgraph/* &> /dev/null
      cd $ARTIFACT_ROOT/tvm &&
      START=`date +%s`
      while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
        python subgraph.py tune 16 1 384 384 gpu
      done
    )
fi

if [[ -n "$ENABLE_CPU" && "$ENABLE_CPU" -eq 1 ]]; then
    # MDH (BiasAddGrad)
    (
      echo "Tuning MDH BiasAddGrad for CPU"
      rm -rf $ARTIFACT_ROOT/results/cpu/mdh/BiasAddGrad &> /dev/null
      cd $ARTIFACT_ROOT/mdh/cpu/BiasAddGrad/build &&
      ./tune_md_hom_strided_v3_bias_add_grad --path $ARTIFACT_ROOT/results/cpu/mdh/BiasAddGrad -d 0 -r no-pp-l-1 -r no-pp-r-1 -r in-cache-l-1 -r in-cache-p-1 -r l-cb-res-p -r p-cb-res-p -r md_poly --input-size-l-1 768 --input-size-r-1 6144 -t OpenTuner -a minutes -v $TUNING_TIME_IN_MINUTES --wrapper-type local --wrapper-command "./tune_md_hom_strided_v3_bias_add_grad [flags]" -g ../gold/md_hom/strided_v3/bias_add_grad/768x6144/gold.tsv --check-type all --kernel-file-1 ../kernel/md_hom/strided_v3/bias_add_grad/static/bias_add_grad_skip_pp_l1_r1_static_1.cl &&
      source ../scripts/preprocess.sh $ARTIFACT_ROOT/results/cpu/mdh/BiasAddGrad/tuned_configuration ../kernel/md_hom/strided_v3/bias_add_grad/static/bias_add_grad_skip_pp_l1_r1_static_1.cl &&
      sed -i '\;^//;d' ../kernel/md_hom/strided_v3/bias_add_grad/static/bias_add_grad_skip_pp_l1_r1_static_1.cl_preprocessed.cl &&
      mv ../kernel/md_hom/strided_v3/bias_add_grad/static/bias_add_grad_skip_pp_l1_r1_static_1.cl_preprocessed.cl $ARTIFACT_ROOT/results/cpu/mdh/BiasAddGrad/kernel.cl
    )

    # MDH (BatchMatMul)
    (
      echo "Tuning MDH BatchMatMul for CPU"
      rm -rf $ARTIFACT_ROOT/results/cpu/mdh/BatchMatMul &> /dev/null
      cd $ARTIFACT_ROOT/mdh/cpu/BatchMatMul/build &&
      ./tune_md_hom_strided_v3_batch_matmul --path $ARTIFACT_ROOT/results/cpu/mdh/BatchMatMul -d 0 -r no-pp-l-1 -r no-pp-l-2 -r no-pp-l-3 -r no-pp-l-4 -r no-pp-r-1 -r a-cache-l-1 -r b-cache-l-1 -r a-cache-p-1 -r b-cache-p-1 -r l-cb-res-p -r p-cb-res-p -r md_poly --input-size-l-1 16 --input-size-l-2 12 --input-size-l-3 384 --input-size-l-4 64 --input-size-r-1 384 -t OpenTuner -a minutes -v $TUNING_TIME_IN_MINUTES --wrapper-type local --wrapper-command "./tune_md_hom_strided_v3_batch_matmul [flags]" -g ../gold/md_hom/strided_v3/batch_matmul/16x12x384x64x384/gold.tsv --check-type all --kernel-file-1 ../kernel/md_hom/strided_v3/batch_matmul/static/batch_matmul_skip_pp_l1_l2_l3_l4_r1_static_1.cl &&
      source ../scripts/preprocess.sh $ARTIFACT_ROOT/results/cpu/mdh/BatchMatMul/tuned_configuration ../kernel/md_hom/strided_v3/batch_matmul/static/batch_matmul_skip_pp_l1_l2_l3_l4_r1_static_1.cl &&
      sed -i '\;^//;d' ../kernel/md_hom/strided_v3/batch_matmul/static/batch_matmul_skip_pp_l1_l2_l3_l4_r1_static_1.cl_preprocessed.cl &&
      mv ../kernel/md_hom/strided_v3/batch_matmul/static/batch_matmul_skip_pp_l1_l2_l3_l4_r1_static_1.cl_preprocessed.cl $ARTIFACT_ROOT/results/cpu/mdh/BatchMatMul/kernel.cl
    )

    # MDH (subgraph)
    (
      echo "Tuning MDH subgraph for CPU"
      rm -rf $ARTIFACT_ROOT/results/cpu/mdh/subgraph &> /dev/null
      cd $ARTIFACT_ROOT/mdh/cpu/subgraph/build &&
      ./tune_md_hom_strided_v3_subgraph2 --path $ARTIFACT_ROOT/results/cpu/mdh/subgraph -d 0 -r pow2 -r no-pp-l-1 -r no-pp-l-2 --input-size-l-1 16 --input-size-l-2 1 --input-size-l-3 384 --input-size-l-4 384 -t OpenTuner -a minutes -v $TUNING_TIME_IN_MINUTES --wrapper-type local --wrapper-command "./tune_md_hom_strided_v3_subgraph2 [flags]" -g ../gold/md_hom/strided_v3/subgraph2/16x1x384x384/gold.tsv --check-type all --kernel-file-1 ../kernel/md_hom/strided_v3/subgraph2/static/subgraph2_skip_pp_l1_l2_l3_l4_static_1.cl &&
      source ../scripts/preprocess.sh $ARTIFACT_ROOT/results/cpu/mdh/subgraph/tuned_configuration ../kernel/md_hom/strided_v3/subgraph2/static/subgraph2_skip_pp_l1_l2_l3_l4_static_1.cl &&
      sed -i '\;^//;d' ../kernel/md_hom/strided_v3/subgraph2/static/subgraph2_skip_pp_l1_l2_l3_l4_static_1.cl_preprocessed.cl &&
      mv ../kernel/md_hom/strided_v3/subgraph2/static/subgraph2_skip_pp_l1_l2_l3_l4_static_1.cl_preprocessed.cl $ARTIFACT_ROOT/results/cpu/mdh/subgraph/kernel.cl
    )

    # TVM (BiasAddGrad)
    (
      echo "Tuning TVM BiasAddGrad for CPU"
      mkdir -p $ARTIFACT_ROOT/results/cpu/tvm/BiasAddGrad
      rm $ARTIFACT_ROOT/results/cpu/tvm/BiasAddGrad/* &> /dev/null
      cd $ARTIFACT_ROOT/tvm &&
      START=`date +%s`
      while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
        python BiasAddGrad.py tune 768 6144 cpu
      done
    )

    # TVM (BatchMatMul)
    (
      echo "Tuning TVM BatchMatMul for CPU"
      mkdir -p $ARTIFACT_ROOT/results/cpu/tvm/BatchMatMul
      rm $ARTIFACT_ROOT/results/cpu/tvm/BatchMatMul/* &> /dev/null
      cd $ARTIFACT_ROOT/tvm &&
      START=`date +%s`
      while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
        python BatchMatMul.py tune 16 12 384 64 384 cpu
      done
    )

    # TVM (subgraph)
    (
      echo "Tuning TVM subgraph for CPU"
      mkdir -p $ARTIFACT_ROOT/results/cpu/tvm/subgraph
      rm $ARTIFACT_ROOT/results/cpu/tvm/subgraph/* &> /dev/null
      cd $ARTIFACT_ROOT/tvm &&
      START=`date +%s`
      while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
        python subgraph.py tune 16 1 384 384 cpu
      done
    )
fi
