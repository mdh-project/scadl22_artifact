import os, sys

import numpy as np
import tvm
from tvm import te, auto_scheduler, topi


mode = sys.argv[1]
N_1 = int(sys.argv[2])
N_2 = int(sys.argv[3])
N_3 = int(sys.argv[4])
N_4 = int(sys.argv[5])
device = sys.argv[6]
if device not in ['cpu', 'gpu']:
    raise ValueError('unknown device "' + device + '"')


@auto_scheduler.register_workload
def subgraph(N_1, N_2, N_3, N_4):
    I_1 = tvm.te.placeholder((N_1, N_3, 1), name="I_1")
    I_2 = tvm.te.placeholder((N_1, 1, N_4), name="I_2")
    C_1 = tvm.te.placeholder((), name="C_1")
    C_2 = tvm.te.placeholder((), name="C_2")

    mul = topi.multiply(I_1, I_2)
    exp_dims = topi.expand_dims(mul, axis=1)
    sub = topi.subtract(C_1, exp_dims)
    O = topi.multiply(sub, C_2)

    return [I_1, I_2, C_1, C_2, O]


if device == 'gpu':
    dev = tvm.cuda(0)
    target = tvm.target.Target("cuda")
    task = auto_scheduler.SearchTask(func=subgraph, args=(N_1, N_2, N_3, N_4), target=target)
else:
    dev = tvm.opencl(0)
    target = tvm.target.Target("opencl")
    max_shared_memory_per_block = dev.max_shared_memory_per_block
    # There is no explicit local memory limition
    # so we can use INT32_MAX to disalbe the check on local_memory.
    max_local_memory_per_block = 2147483647 # INT32_MAX
    max_threads_per_block = dev.max_threads_per_block
    max_vthread_extent = int(dev.warp_size / 4) if int(dev.warp_size / 4) > 1 else dev.warp_size
    warp_size = dev.warp_size
    hardware_params = auto_scheduler.HardwareParams(-1, 16, 64,
                                                    max_shared_memory_per_block, max_local_memory_per_block,
                                                    max_threads_per_block, max_vthread_extent, warp_size)
    task = auto_scheduler.SearchTask(func=subgraph, args=(N_1, N_2, N_3, N_4), target=target, hardware_params=hardware_params)

logfile = f'../results/{device}/tvm/subgraph/tuning_log.json'
runtimefile = f'../results/{device}/tvm/subgraph/runtimes_min'

if mode == 'tune':
    measure_ctx = auto_scheduler.LocalRPCMeasureContext(min_repeat_ms=300, timeout=600)
    tune_option = auto_scheduler.TuningOptions(
        num_measure_trials=1024,
        measure_callbacks=[auto_scheduler.RecordToFile(logfile)],
        verbose=2,
        runner=measure_ctx.runner
    )

    # Run auto-tuning (search)
    if os.path.isfile(logfile):
        print("resume tuning")
        cost_model = auto_scheduler.XGBModel()
        cost_model.update_from_file(logfile)
        search_policy = auto_scheduler.SketchPolicy(
            task, cost_model, init_search_callbacks=[auto_scheduler.PreloadMeasuredStates(logfile)]
        )
        tune_option = auto_scheduler.TuningOptions(
            num_measure_trials=1024, measure_callbacks=[auto_scheduler.RecordToFile(logfile)]
        )
        task.tune(tune_option, search_policy=search_policy)
    else:
        # Run auto-tuning (search)
        task.tune(tune_option)

    # Kill the measurement process
    del measure_ctx

elif mode == 'bench':
    # Apply the best schedule
    sch, args = task.apply_best(logfile)
    func = tvm.build(sch, args, target)

    # Evaluate execution time
    I_1_np = np.random.uniform(size=(N_1, N_3, 1)).astype(np.float32)
    I_2_np = np.random.uniform(size=(N_1, 1, N_4)).astype(np.float32)
    C_1_np = np.random.uniform(size=()).astype(np.float32)
    C_2_np = np.random.uniform(size=()).astype(np.float32)
    O_np = np.random.uniform(size=(N_1, N_2, N_3, N_4)).astype(np.float32)

    I_1_tvm = tvm.nd.array(I_1_np, device=dev)
    I_2_tvm = tvm.nd.array(I_2_np, device=dev)
    C_1_tvm = tvm.nd.array(C_1_np, device=dev)
    C_2_tvm = tvm.nd.array(C_2_np, device=dev)
    O_tvm = tvm.nd.empty(O_np.shape, device=dev)

    evaluator = func.time_evaluator(func.entry_name, dev, min_repeat_ms=500)
    min_runtime_ns = int(np.min(evaluator(I_1_tvm, I_2_tvm, C_1_tvm, C_2_tvm, O_tvm).results) * 1000000000)
    print(
        "Execution time of this operator: %.4f ns"
        % min_runtime_ns
    )
    with open(runtimefile, "w") as f:
        f.write(str(min_runtime_ns))
