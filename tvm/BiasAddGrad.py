import os, sys

import numpy as np
import tvm
from tvm import te, auto_scheduler, topi


mode = sys.argv[1]
I = int(sys.argv[2])
K = int(sys.argv[3])
device = sys.argv[4]
if device not in ['cpu', 'gpu']:
    raise ValueError('unknown device "' + device + '"')


@auto_scheduler.register_workload
def bias_add_grad(I, K):
    IN = tvm.te.placeholder((K, I), name="IN")
    k = tvm.te.reduce_axis((0, K), name="k")
    OUT = tvm.te.compute(
        (I,),
        lambda i: tvm.te.sum(IN[k, i], axis=k),
        name="BiasAddGrad"
    )

    return [IN, OUT]


if device == 'gpu':
    dev = tvm.cuda(0)
    target = tvm.target.Target("cuda")
    task = auto_scheduler.SearchTask(func=bias_add_grad, args=(I, K), target=target)
else:
    dev = tvm.opencl(0)
    target = tvm.target.Target("opencl")
    max_shared_memory_per_block = dev.max_shared_memory_per_block
    # There is no explicit local memory limition
    # so we can use INT32_MAX to disalbe the check on local_memory.
    max_local_memory_per_block = 2147483647 # INT32_MAX
    max_threads_per_block = dev.max_threads_per_block
    max_vthread_extent = int(dev.warp_size / 4) if int(dev.warp_size / 4) > 1 else dev.warp_size
    warp_size = dev.warp_size
    hardware_params = auto_scheduler.HardwareParams(-1, 16, 64,
                                                    max_shared_memory_per_block, max_local_memory_per_block,
                                                    max_threads_per_block, max_vthread_extent, warp_size)
    task = auto_scheduler.SearchTask(func=bias_add_grad, args=(I, K), target=target, hardware_params=hardware_params)

logfile = f'../results/{device}/tvm/BiasAddGrad/tuning_log.json'
runtimefile = f'../results/{device}/tvm/BiasAddGrad/runtimes_min'

if mode == 'tune':
    measure_ctx = auto_scheduler.LocalRPCMeasureContext(min_repeat_ms=300, timeout=600)
    tune_option = auto_scheduler.TuningOptions(
        num_measure_trials=1024,
        measure_callbacks=[auto_scheduler.RecordToFile(logfile)],
        verbose=2,
        runner=measure_ctx.runner
    )

    # Run auto-tuning (search)
    if os.path.isfile(logfile):
        print("resume tuning")
        cost_model = auto_scheduler.XGBModel()
        cost_model.update_from_file(logfile)
        search_policy = auto_scheduler.SketchPolicy(
            task, cost_model, init_search_callbacks=[auto_scheduler.PreloadMeasuredStates(logfile)]
        )
        tune_option = auto_scheduler.TuningOptions(
            num_measure_trials=1024, measure_callbacks=[auto_scheduler.RecordToFile(logfile)]
        )
        task.tune(tune_option, search_policy=search_policy)
    else:
        # Run auto-tuning (search)
        task.tune(tune_option)

    # Kill the measurement process
    del measure_ctx

elif mode == 'bench':
    # Apply the best schedule
    sch, args = task.apply_best(logfile)
    func = tvm.build(sch, args, target)

    # Evaluate execution time
    IN_np = np.random.uniform(size=(K, I)).astype(np.float32)
    OUT_np = np.random.uniform(size=(I)).astype(np.float32)

    IN_tvm = tvm.nd.array(IN_np, device=dev)
    OUT_tvm = tvm.nd.empty(OUT_np.shape, device=dev)

    evaluator = func.time_evaluator(func.entry_name, dev, min_repeat_ms=500)
    min_runtime_ns = int(np.min(evaluator(IN_tvm, OUT_tvm).results) * 1000000000)
    print(
        "Execution time of this operator: %.4f ns"
        % min_runtime_ns
    )
    with open(runtimefile, "w") as f:
        f.write(str(min_runtime_ns))
