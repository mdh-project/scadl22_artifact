import os, sys

import numpy as np
import tvm
from tvm import te, auto_scheduler, topi


mode = sys.argv[1]
B_1 = int(sys.argv[2])
B_2 = int(sys.argv[3])
I = int(sys.argv[4])
J = int(sys.argv[5])
K = int(sys.argv[6])
device = sys.argv[7]
if device not in ['cpu', 'gpu']:
    raise ValueError('unknown device "' + device + '"')


@auto_scheduler.register_workload
def batch_matmul(B_1, B_2, I, J, K):
    A = tvm.te.placeholder((B_1, B_2, I, K), name="A")
    B = tvm.te.placeholder((B_1, B_2, K, J), name="B")
    k = tvm.te.reduce_axis((0, K), name="k")
    C = tvm.te.compute(
        (B_1, B_2, I, J),
        lambda b_1, b_2, i, j: tvm.te.sum(A[b_1, b_2, i, k] * B[b_1, b_2, k, j], axis=k),
        name="BatchMatMul"
    )

    return [A, B, C]


if device == 'gpu':
    dev = tvm.cuda(0)
    target = tvm.target.Target("cuda")
    task = auto_scheduler.SearchTask(func=batch_matmul, args=(B_1, B_2, I, J, K), target=target)
else:
    dev = tvm.opencl(0)
    target = tvm.target.Target("opencl")
    max_shared_memory_per_block = dev.max_shared_memory_per_block
    # There is no explicit local memory limition
    # so we can use INT32_MAX to disalbe the check on local_memory.
    max_local_memory_per_block = 2147483647 # INT32_MAX
    max_threads_per_block = dev.max_threads_per_block
    max_vthread_extent = int(dev.warp_size / 4) if int(dev.warp_size / 4) > 1 else dev.warp_size
    warp_size = dev.warp_size
    hardware_params = auto_scheduler.HardwareParams(-1, 16, 64,
                                                    max_shared_memory_per_block, max_local_memory_per_block,
                                                    max_threads_per_block, max_vthread_extent, warp_size)
    task = auto_scheduler.SearchTask(func=batch_matmul, args=(B_1, B_2, I, J, K), target=target, hardware_params=hardware_params)

logfile = f'../results/{device}/tvm/BatchMatMul/tuning_log.json'
runtimefile = f'../results/{device}/tvm/BatchMatMul/runtimes_min'

if mode == 'tune':
    measure_ctx = auto_scheduler.LocalRPCMeasureContext(min_repeat_ms=300, timeout=600)
    tune_option = auto_scheduler.TuningOptions(
        num_measure_trials=1024,
        measure_callbacks=[auto_scheduler.RecordToFile(logfile)],
        verbose=2,
        runner=measure_ctx.runner
    )

    # Run auto-tuning (search)
    if os.path.isfile(logfile):
        print("resume tuning")
        cost_model = auto_scheduler.XGBModel()
        cost_model.update_from_file(logfile)
        search_policy = auto_scheduler.SketchPolicy(
            task, cost_model, init_search_callbacks=[auto_scheduler.PreloadMeasuredStates(logfile)]
        )
        tune_option = auto_scheduler.TuningOptions(
            num_measure_trials=1024, measure_callbacks=[auto_scheduler.RecordToFile(logfile)]
        )
        task.tune(tune_option, search_policy=search_policy)
    else:
        # Run auto-tuning (search)
        task.tune(tune_option)

    # Kill the measurement process
    del measure_ctx

elif mode == 'bench':
    # Apply the best schedule
    sch, args = task.apply_best(logfile)
    func = tvm.build(sch, args, target)

    # Evaluate execution time
    A_np = np.random.uniform(size=(B_1, B_2, I, K)).astype(np.float32)
    B_np = np.random.uniform(size=(B_1, B_2, K, J)).astype(np.float32)
    C_np = np.random.uniform(size=(B_1, B_2, I, J)).astype(np.float32)

    A_tvm = tvm.nd.array(A_np, device=dev)
    B_tvm = tvm.nd.array(B_np, device=dev)
    C_tvm = tvm.nd.empty(C_np.shape, device=dev)

    evaluator = func.time_evaluator(func.entry_name, dev, min_repeat_ms=500)
    min_runtime_ns = int(np.min(evaluator(A_tvm, B_tvm, C_tvm).results) * 1000000000)
    print(
        "Execution time of this operator: %.4f ns"
        % min_runtime_ns
    )
    with open(runtimefile, "w") as f:
        f.write(str(min_runtime_ns))
